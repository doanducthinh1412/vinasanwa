Ext.define('CMS.store.DanhSachDonDatHang', {
    extend: 'Ext.data.Store',
    alias: 'store.DanhSachDonDatHangStore',
    fields: [
        'building_type',
        { name: 'createdAt', type: 'auto' },
        'cus_address', 'cus_email',
        'cus_incharge', 'cus_name',
        'cus_tel',
        { name: 'factory_out', type: 'auto' },
        'delivery_by',
        'delivery_place',
        'des_change_date',
        'des_date_created_time',
        'des_drawing_no',
        'des_item_no',
        'des_note',
        'des_version_order_sheet',
        'designer',
        'dgm',
        { name: 'drawing_path', type: 'auto' },
        'frame_factory_out',
        'frame_ordered',
        'frame_total',
        'frame_type',
        'id',
        'leaf',
        'leaf_factory_out',
        { name: 'order_type', type: 'int' },
        { name: 'other', type: 'int' },
        'other_factory_out',
        'paint_apply',
        'paint_color',
        'paint_f',
        'paint_p',
        'pj_incharge',
        { name: 'priority', type: 'int' },
        { name: 'project', type: 'auto' },
        { name: 'project_name', mapping: 'project.code' },
        'receive_no',
        'sale_incharge',
        'sale_manager',
        'sale_note',
        'status',
        'sub_no',
        { name: 'updatedAt', type: 'auto' },
        {
            name: 'frame_sd',
            type: 'int',
            mapping: 'frame_type_detail.frame_sd'
        },
        {
            name: 'frame_lsd',
            type: 'int',
            mapping: 'frame_type_detail.frame_lsd'
        },
        {
            name: 'frame_fsd',
            type: 'int',
            mapping: 'frame_type_detail.frame_fsd'
        },
        {
            name: 'frame_flsd',
            type: 'int',
            mapping: 'frame_type_detail.frame_flsd'
        },
        {
            name: 'leaf_sd',
            type: 'int',
            mapping: 'frame_type_detail.leaf_sd'
        },
        {
            name: 'leaf_lsd',
            type: 'int',
            mapping: 'frame_type_detail.leaf_lsd'
        },
        {
            name: 'leaf_fsd',
            type: 'int',
            mapping: 'frame_type_detail.leaf_fsd'
        },
        {
            name: 'leaf_flsd',
            type: 'int',
            mapping: 'frame_type_detail.leaf_flsd'
        },
        {
            name: 'frame_sld',
            type: 'int',
            mapping: 'frame_type_detail.frame_sld'
        },
        {
            name: 'leaf_sld',
            type: 'int',
            mapping: 'frame_type_detail.leaf_sld'
        },
        {
            name: 'frame_sp',
            type: 'int',
            mapping: 'frame_type_detail.frame_sp'
        },
        {
            name: 'leaf_sp',
            type: 'int',
            mapping: 'frame_type_detail.leaf_sp'
        },
        {
            name: 'ssjp',
            type: 'int',
            mapping: 'frame_type_detail.ssjp'
        },
        {
            name: 'ssaichi',
            type: 'int',
            mapping: 'frame_type_detail.ssaichi'
        },
        {
            name: 'ssgrill',
            type: 'int',
            mapping: 'frame_type_detail.ssgrill'
        },
        {
            name: 'srn',
            type: 'int',
            mapping: 'frame_type_detail.srn'
        },
        {
            name: 'grs',
            type: 'int',
            mapping: 'frame_type_detail.grs'
        },
        {
            name: 's13',
            type: 'int',
            mapping: 'frame_type_detail.s13'
        },
        {
            name: 's14',
            type: 'int',
            mapping: 'frame_type_detail.s14'
        },
        {
            name: 'lv',
            type: 'int',
            mapping: 'frame_type_detail.lv'
        },
        {
            name: 'sw',
            type: 'int',
            mapping: 'frame_type_detail.sw'
        }, {
            name: 'frame_sd_draw',
            type: 'int',
            mapping: 'drawings.frame_sd',
            convert: function(v, records) {
                var frame_sd = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.frame_sd) {
                        frame_sd = records.data.frame_type_detail.frame_sd;
                    }
                }
                if (frame_sd === 0) {
                    return "";
                }
                if (v) {
                    return frame_sd + ' / ' + v;
                } else {
                    return frame_sd + ' / 0';
                }
                return frame_sd;
            }
        },
        {
            name: 'frame_lsd_draw',
            type: 'int',
            mapping: 'drawings.frame_lsd',
            convert: function(v, records) {
                var frame_lsd = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.frame_lsd) {
                        frame_lsd = records.data.frame_type_detail.frame_lsd;
                    }
                }
                if (frame_lsd === 0) {
                    return "";
                }
                if (v) {
                    return frame_lsd + ' / ' + v;
                } else {
                    return frame_lsd + ' / 0';
                }
                return frame_lsd;
            }
        },
        {
            name: 'frame_fsd_draw',
            type: 'int',
            mapping: 'drawings.frame_fsd',
            convert: function(v, records) {
                var frame_fsd = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.frame_fsd) {
                        frame_fsd = records.data.frame_type_detail.frame_fsd;
                    }
                }
                if (frame_fsd === 0) {
                    return "";
                }
                if (v) {
                    return frame_fsd + ' / ' + v;
                } else {
                    return frame_fsd + ' / 0';
                }
                return frame_fsd;
            }
        },
        {
            name: 'frame_flsd_draw',
            type: 'int',
            mapping: 'drawings.frame_flsd',
            convert: function(v, records) {
                var frame_flsd = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.frame_flsd) {
                        frame_flsd = records.data.frame_type_detail.frame_flsd;
                    }
                }
                if (frame_flsd === 0) {
                    return "";
                }
                if (v) {
                    return frame_flsd + ' / ' + v;
                } else {
                    return frame_flsd + ' / 0';
                }
                return frame_flsd;
            }
        },
        {
            name: 'leaf_sd_draw',
            type: 'int',
            mapping: 'drawings.leaf_sd',
            convert: function(v, records) {
                var leaf_sd = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.leaf_sd) {
                        leaf_sd = records.data.frame_type_detail.leaf_sd;
                    }
                }
                if (leaf_sd === 0) {
                    return "";
                }
                if (v) {
                    return leaf_sd + ' / ' + v;
                } else {
                    return leaf_sd + ' / 0';
                }
                return leaf_sd;
            }
        },
        {
            name: 'leaf_lsd_draw',
            type: 'int',
            mapping: 'drawings.leaf_lsd',
            convert: function(v, records) {
                var leaf_lsd = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.leaf_lsd) {
                        leaf_lsd = records.data.frame_type_detail.leaf_lsd;
                    }
                }
                if (leaf_lsd === 0) {
                    return "";
                }
                if (v) {
                    return leaf_lsd + ' / ' + v;
                } else {
                    return leaf_lsd + ' / 0';
                }
                return leaf_lsd;
            }
        },
        {
            name: 'leaf_fsd_draw',
            type: 'int',
            mapping: 'drawings.leaf_fsd',
            convert: function(v, records) {
                var leaf_fsd = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.leaf_fsd) {
                        leaf_fsd = records.data.frame_type_detail.leaf_fsd;
                    }
                }
                if (leaf_fsd === 0) {
                    return "";
                }
                if (v) {
                    return leaf_fsd + ' / ' + v;
                } else {
                    return leaf_fsd + ' / 0';
                }
                return leaf_fsd;
            }
        },
        {
            name: 'leaf_flsd_draw',
            type: 'int',
            mapping: 'drawings.leaf_flsd',
            convert: function(v, records) {
                var leaf_flsd = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.leaf_flsd) {
                        leaf_flsd = records.data.frame_type_detail.leaf_flsd;
                    }
                }
                if (leaf_flsd === 0) {
                    return "";
                }
                if (v) {
                    return leaf_flsd + ' / ' + v;
                } else {
                    return leaf_flsd + ' / 0';
                }
                return leaf_flsd;
            }
        },
        {
            name: 'frame_sld_draw',
            type: 'int',
            mapping: 'drawings.frame_sld',
            convert: function(v, records) {
                var frame_sld = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.frame_sld) {
                        frame_sld = records.data.frame_type_detail.frame_sld;
                    }
                }
                if (frame_sld === 0) {
                    return "";
                }
                if (v) {
                    return frame_sld + ' / ' + v;
                } else {
                    return frame_sld + ' / 0';
                }
                return frame_sld;
            }
        },
        {
            name: 'leaf_sld_draw',
            type: 'int',
            mapping: 'drawings.leaf_sld',
            convert: function(v, records) {
                var leaf_sld = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.leaf_sld) {
                        leaf_sld = records.data.frame_type_detail.leaf_sld;
                    }
                }
                if (leaf_sld === 0) {
                    return "";
                }
                if (v) {
                    return leaf_sld + ' / ' + v;
                } else {
                    return leaf_sld + ' / 0';
                }
                return leaf_sld;
            }
        },
        {
            name: 'frame_sp_draw',
            type: 'int',
            mapping: 'drawings.frame_sp',
            convert: function(v, records) {
                var frame_sp = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.frame_sp) {
                        frame_sp = records.data.frame_type_detail.frame_sp;
                    }
                }
                if (frame_sp === 0) {
                    return "";
                }
                if (v) {
                    return frame_sp + ' / ' + v;
                } else {
                    return frame_sp + ' / 0';
                }
                return frame_sp;
            }
        },
        {
            name: 'leaf_sp_draw',
            type: 'int',
            mapping: 'drawings.leaf_sp',
            convert: function(v, records) {
                var leaf_sp = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.leaf_sp) {
                        leaf_sp = records.data.frame_type_detail.leaf_sp;
                    }
                }
                if (leaf_sp === 0) {
                    return "";
                }
                if (v) {
                    return leaf_sp + ' / ' + v;
                } else {
                    return leaf_sp + ' / 0';
                }
                return leaf_sp;
            }
        },
        {
            name: 'ssjp_draw',
            type: 'int',
            mapping: 'drawings.ssjp',
            convert: function(v, records) {
                var ssjp = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.ssjp) {
                        ssjp = records.data.frame_type_detail.ssjp;
                    }
                }
                if (ssjp === 0) {
                    return "";
                }
                if (v) {
                    return ssjp + ' / ' + v;
                } else {
                    return ssjp + ' / 0';
                }
                return ssjp;
            }
        },
        {
            name: 'ssaichi_draw',
            type: 'int',
            mapping: 'drawings.ssaichi',
            convert: function(v, records) {
                var ssaichi = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.ssaichi) {
                        ssaichi = records.data.frame_type_detail.ssaichi;
                    }
                }
                if (ssaichi === 0) {
                    return "";
                }
                if (v) {
                    return ssaichi + ' / ' + v;
                } else {
                    return ssaichi + ' / 0';
                }
                return ssaichi;
            }
        },
        {
            name: 'ssgrill_draw',
            type: 'int',
            mapping: 'drawings.ssgrill',
            convert: function(v, records) {
                var ssgrill = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.ssgrill) {
                        ssgrill = records.data.frame_type_detail.ssgrill;
                    }
                }
                if (ssgrill === 0) {
                    return "";
                }
                if (v) {
                    return ssgrill + ' / ' + v;
                } else {
                    return ssgrill + ' / 0';
                }
                return ssgrill;
            }
        },
        {
            name: 'srn_draw',
            type: 'int',
            mapping: 'drawings.srn',
            convert: function(v, records) {
                var srn = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.srn) {
                        srn = records.data.frame_type_detail.srn;
                    }
                }
                if (srn === 0) {
                    return "";
                }
                if (v) {
                    return srn + ' / ' + v;
                } else {
                    return srn + ' / 0';
                }
                return srn;
            }
        },
        {
            name: 'grs_draw',
            type: 'int',
            mapping: 'drawings.grs',
            convert: function(v, records) {
                var grs = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.grs) {
                        grs = records.data.frame_type_detail.grs;
                    }
                }
                if (grs === 0) {
                    return "";
                }
                if (v) {
                    return grs + ' / ' + v;
                } else {
                    return grs + ' / 0';
                }
                return grs;
            }
        },
        {
            name: 's13_draw',
            type: 'int',
            mapping: 'drawings.s13',
            convert: function(v, records) {
                var s13 = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.s13) {
                        s13 = records.data.frame_type_detail.s13;
                    }
                }
                if (s13 === 0) {
                    return "";
                }
                if (v) {
                    return s13 + ' / ' + v;
                } else {
                    return s13 + ' / 0';
                }
                return s13;
            }
        },
        {
            name: 's14_draw',
            type: 'int',
            mapping: 'drawings.s14',
            convert: function(v, records) {
                var s14 = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.s14) {
                        s14 = records.data.frame_type_detail.s14;
                    }
                }
                if (s14 === 0) {
                    return "";
                }
                if (v) {
                    return s14 + ' / ' + v;
                } else {
                    return s14 + ' / 0';
                }
                return s14;
            }
        },
        {
            name: 'lv_draw',
            type: 'int',
            mapping: 'drawings.lv',
            convert: function(v, records) {
                var lv = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.lv) {
                        lv = records.data.frame_type_detail.lv;
                    }
                }
                if (lv === 0) {
                    return "";
                }
                if (v) {
                    return lv + ' / ' + v;
                } else {
                    return lv + ' / 0';
                }
                return lv;
            }
        },
        {
            name: 'sw_draw',
            type: 'int',
            mapping: 'drawings.sw',
            convert: function(v, records) {
                var sw = 0;
                if (records.data.frame_type_detail) {
                    if (records.data.frame_type_detail.sw) {
                        sw = records.data.frame_type_detail.sw;
                    }
                }
                if (sw === 0) {
                    return "";
                }
                if (v) {
                    return sw + ' / ' + v;
                } else {
                    return sw + ' / 0';
                }
                return sw;
            }
        }, {
            name: 'assignmentOrderSheet',
            type: 'auto'
        }, {
            name: 'drawer',
            type: 'auto',
            mapping: 'assignmentOrderSheet[0].drawer'
        }, {
            name: 'delivery_date',
            mapping: 'assignmentOrderSheet[0].delivery_date'
        }, {
            name: 'working_drawing_receive_date',
            mapping: 'assignmentOrderSheet[0].working_drawing_receive_date'
        }, {
            name: 'end_date',
            mapping: 'assignmentOrderSheet[0].end_date'
        }, {
            name: 'note',
            mapping: 'assignmentOrderSheet[0].note'
        }, {
            name: 'frame_type_detail',
            type: 'auto',
        }, {
            name: 'drawings',
            type: 'auto'
        }, {
            name: 'total',
            type: 'int',
            mapping: 'frame_type_detail',
            convert: function(v, records) {
                var total = 0;
                var total_draw = 0;
                if (records.data.drawings) {
                    for (rec in records.data.drawings) {
                        if (records.data.drawings[rec]) {
                            total_draw = total_draw + parseInt(records.data.drawings[rec]);
                        }
                    }
                }
                if (v) {
                    for (rec in v) {
                        if (v[rec]) {
                            total = total + parseInt(v[rec]);
                        }
                    }
                }
                return total + ' / ' + total_draw;
            }
        }, {
            name: 'status_phancong',
            mapping: 'assignmentOrderSheet[0].status'
        }
    ],
    pageSize: 20,
    autoLoad: true,
    //remoteSort: true,
    groupField: 'project_name',
    proxy: {
        type: 'ajax',
        url: '/api/ordersheet?filter=dp:eq_2',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#logout";
                }
            }
        }
    }
});