Ext.define('CMS.store.ThongKeLoaiCuaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.ThongKeLoaiCuaStore',
    fields: [
		'name',
		'sub_no',
	{	name: 'wood_grain_frame', type: 'int', allowNull: true},
	{	name: 'wood_grain_leaf', type: 'int', allowNull: true},
	{	name: 'standard_door_frame', type: 'int', allowNull: true},
	{	name: 'standard_door_leaf',	type: 'int', allowNull: true},
	{	name: 'smoor_door_frame', type: 'int', allowNull: true},
	{	name: 'smoor_door_leaf', type: 'int', allowNull: true},
	{	name: 'sp_hanger_frame', type: 'int', allowNull: true},
	{	name: 'sp_hanger_leaf',	type: 'int', allowNull: true},
	{	name: 'quicksave_grs', type: 'int', allowNull: true},
	{	name: 'quicksave_srn', type: 'int', allowNull: true},
	{	name: 'quicksave_s13', type: 'int', allowNull: true},
	{	name: 'quicksave_s14', type: 'int', allowNull: true},
	{	name: 'shutter_grill', type: 'int', allowNull: true},
	{	name: 'shutter_typhoon_hook', type: 'int', allowNull: true},
		'month',
		'year'
	],

});