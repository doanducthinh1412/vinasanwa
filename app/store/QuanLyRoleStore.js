Ext.define('CMS.store.QuanLyRoleStore', {
    extend: 'Ext.data.Store',
    alias: 'store.QuanLyRoleStore',
    fields: ['id', 'name', 'resources'],
    idProperty: 'id',
    proxy: {
        type: 'ajax',
        url: '/api/modules',
        reader: {
            type: 'json',
            rootProperty: 'data',
            //totalProperty	: 'total',
        },
        autoLoad: true,
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#logout";
                }
            }
        }
    },

});