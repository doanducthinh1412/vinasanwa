Ext.define('CMS.store.DanhSachNhanVien', {
    extend: 'Ext.data.Store',
    alias: 'store.DanhSachNhanVienStore',
    fields: ['last_name','first_name','date_of_birth','sex','email','phone_number','address','department','position'],
});