Ext.define('CMS.store.RoleStore', {
    extend: 'Ext.data.Store',
    alias: 'store.RoleStore',
    fields: ['id', 'name', 'description', 'module'],
    idProperty: 'id',
    proxy: {
        type: 'ajax',
        url: '/api/roles',
        reader: {
            type: 'json',
            rootProperty: 'data',
            //totalProperty	: 'total',
        },
        autoLoad: true,
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#logout";
                }
            }
        }
    },
});