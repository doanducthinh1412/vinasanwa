/* eslint-disable eol-last */
/* eslint-disable no-unused-vars */
Ext.define('CMS.store.DanhSachDonDatHangChuaVe', {
    extend: 'Ext.data.Store',
    alias: 'store.DanhSachDonDatHangChuaVeStore',
    fields: [
        'id',
        {
            name: 'drawings',
            type: 'auto',
        },
        {
            name: 'project_name',
        },
        {
            name: 'delivery_date',
            type: 'auto',
        },
        {
            name: 'order_date',
            mapping: 'order_date.date',
            type: 'date',
        },
        {
            name: 'sub_no',
            mapping: 'sub_no',
        }, {
            name: 'assignments_done',
            type: 'auto',
        }, {
            name: 'frame_type_detail',
            type: 'auto',
        },
        {
            name: 'assignments_remain',
            type: 'auto',
        },
        {
            name: 'frame_sd',
            type: 'int',
            mapping: 'assignments_remain.frame_sd',
        },
        {
            name: 'frame_lsd',
            type: 'int',
            mapping: 'assignments_remain.frame_lsd',
        },
        {
            name: 'frame_wd',
            type: 'int',
            mapping: 'assignments_remain.frame_wd',
        },
        {
            name: 'frame_susd',
            type: 'int',
            mapping: 'assignments_remain.frame_susd',
        },
        {
            name: 'frame_fsd',
            type: 'int',
            mapping: 'assignments_remain.frame_fsd',
        },
        {
            name: 'leaf_fsd',
            type: 'int',
            mapping: 'assignments_remain.leaf_fsd',
        },
        {
            name: 'leaf_sd',
            type: 'int',
            mapping: 'assignments_remain.leaf_sd',
        },
        {
            name: 'leaf_wd',
            type: 'int',
            mapping: 'assignments_remain.leaf_wd',
        },
        {
            name: 'leaf_susd',
            type: 'int',
            mapping: 'assignments_remain.leaf_susd',
        },
        {
            name: 'leaf_lsd',
            type: 'int',
            mapping: 'assignments_remain.leaf_lsd',
        },
        {
            name: 'frame_flsd',
            type: 'int',
            mapping: 'assignments_remain.frame_flsd',
        },
        {
            name: 'leaf_flsd',
            type: 'int',
            mapping: 'assignments_remain.leaf_flsd',
        },
        {
            name: 'ssjp',
            type: 'int',
            mapping: 'assignments_remain.ssjp',
        },
        {
            name: 'ssaichi',
            type: 'int',
            mapping: 'assignments_remain.ssaichi',
        },
        {
            name: 'ssgrill',
            type: 'int',
            mapping: 'assignments_remain.ssgrill',
        },
        {
            name: 'grs',
            type: 'int',
            mapping: 'assignments_remain.grs',
        },
        {
            name: 'srn',
            type: 'int',
            mapping: 'assignments_remain.srn',
        },
        {
            name: 's13',
            type: 'int',
            mapping: 'assignments_remain.s13',
        },
        {
            name: 's14',
            type: 'int',
            mapping: 'assignments_remain.s14',
        },
        {
            name: 'frame_sp',
            type: 'int',
            mapping: 'assignments_remain.frame_sp',
        },
        {
            name: 'leaf_sp',
            type: 'int',
            mapping: 'assignments_remain.leaf_sp',
        },
        {
            name: 'frame_sld',
            type: 'int',
            mapping: 'assignments_remain.frame_sld',
        },
        {
            name: 'leaf_sld',
            type: 'int',
            mapping: 'assignments_remain.leaf_sld',
        },
        {
            name: 'lv',
            type: 'int',
            mapping: 'assignments_remain.lv',
        },
        {
            name: 'sw',
            type: 'int',
            mapping: 'assignments_remain.frame_sw',
        },
        {
            name: 'other',
            type: 'int',
            mapping: 'assignments_remain.other',
        },
    ],
    pageSize: 50,
    autoLoad: true,
    groupField: 'project_name',
    proxy: {
        type: 'ajax',
        url: '/api/ordersheet/assignment-remain',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total',
        },
        listeners: {
            exception(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = '/index.html#logout';
                }
            },
        },
        autoLoad: true,
    },
});