/* eslint-disable eol-last */
/* eslint-disable no-unused-vars */
Ext.define('CMS.store.QuanLyKeHoachSanXuat', {
    extend: 'Ext.data.Store',
    alias: 'store.QuanLyKeHoachSanXuatStore',
    model: 'CMS.model.KeHoachSanXuat',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: '/api/production-schedule',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total',
        },
        listeners: {
            exception(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = '/index.html#logout';
                }
            },
        },
    },
});