Ext.define('CMS.store.QuanLyDonDatHang', {
    extend: 'Ext.data.Store',
    alias: 'store.QuanLyDonDatHangStore',
    fields: ['building_type', { name: 'createdAt', type: 'auto' }, 'note', 'cus_address', 'cus_email', 'cus_incharge', 'cus_name', 'cus_tel', { name: 'factory_out', type: 'auto' }, 'delivery_by', 'delivery_place', 'des_change_date', 'des_date_created_time', 'des_drawing_no', 'des_item_no', 'des_note', 'des_version_order_sheet', 'designer', 'dgm', { name: 'drawing_path', type: 'auto' }, 'frame_factory_out', 'frame_ordered', 'frame_total', 'frame_type', 'id', 'leaf', 'leaf_factory_out', { name: 'order_type', type: 'int' }, 'other', 'other_factory_out', 'paint_apply', 'paint_color', 'paint_f', 'paint_p', 'pj_incharge', { name: 'priority', type: 'int' }, { name: 'project', type: 'auto' }, { name: 'project_name', mapping: 'project.code' }, 'receive_no', 'sale_incharge', 'sale_manager', 'sale_note', 'status', 'sub_no', { name: 'updatedAt', type: 'auto' }],
    pageSize: 20,
    autoLoad: true,
    //remoteSort: true,
    groupField: 'project_name',
    proxy: {
        type: 'ajax',
        url: '/api/ordersheet',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#login";
                }
            }
        }
    }
});