Ext.define('CMS.store.QuanLyDuAnStore', {
    extend: 'Ext.data.Store',
    alias: 'store.QuanLyDuAnStore',
    fields: [
        'building_type',
        { name: 'createdAt', type: 'auto' },
        'cus_address',
        'cus_email',
        'cus_incharge',
        'cus_name',
        'cus_tel',
        { name: 'factory_out', type: 'auto' },
        'delivery_by', 'delivery_place',
        'des_change_date',
        'des_date_created_time',
        'des_drawing_no',
        'des_item_no',
        'des_note',
        'des_version_order_sheet',
        'designer',
        'dgm',
        { name: 'drawing_path', type: 'auto' },
        'frame_factory_out',
        'frame_ordered',
        'frame_total',
        'frame_type',
        'id',
        'leaf',
        'leaf_factory_out',
        { name: 'order_type', type: 'int' },
        { name: 'other', type: 'int' },
        'other_factory_out',
        'paint_apply',
        'paint_color',
        'paint_f',
        'paint_p',
        'pj_incharge',
        { name: 'priority', type: 'int' },
        { name: 'project', type: 'auto' },
        { name: 'project_name', mapping: 'project.code' },
        'receive_no',
        'sale_incharge',
        'sale_manager',
        'sale_note',
        'status',
        'sub_no',
        'frame_unit',
        'leaf_unit',
        { name: 'updatedAt', type: 'auto' },
        { name: 'frame_sd', type: 'int', mapping: 'frame_type_detail.frame_sd' },
        { name: 'frame_lsd', type: 'int', mapping: 'frame_type_detail.frame_lsd' },
        { name: 'leaf_sd', type: 'int', mapping: 'frame_type_detail.leaf_sd' },
        { name: 'leaf_lsd', type: 'int', mapping: 'frame_type_detail.leaf_lsd' },
        { name: 'frame_flsd', type: 'int', mapping: 'frame_type_detail.frame_flsd' },
        { name: 'leaf_flsd', type: 'int', mapping: 'frame_type_detail.leaf_flsd' },
        { name: 'frame_fsd', type: 'int', mapping: 'frame_type_detail.frame_fsd' },
        { name: 'leaf_fsd', type: 'int', mapping: 'frame_type_detail.leaf_fsd' },
        { name: 'sw', type: 'int', mapping: 'frame_type_detail.sw' },
        { name: 'ssjp', type: 'int', mapping: 'frame_type_detail.ssjp' },
        { name: 'ssaichi', type: 'int', mapping: 'frame_type_detail.ssaichi' },
        { name: 'ssgrill', type: 'int', mapping: 'frame_type_detail.ssgrill' },
        { name: 'ss_m2', type: 'int' },
        { name: 'grs', type: 'int', mapping: 'frame_type_detail.grs' },
        { name: 's13', type: 'int', mapping: 'frame_type_detail.s13' },
        { name: 's14', type: 'int', mapping: 'frame_type_detail.s14' },
        { name: 'srn', type: 'int', mapping: 'frame_type_detail.srn' },
        { name: 'frame_sp', type: 'int', mapping: 'frame_type_detail.frame_sp' },
        { name: 'leaf_sp', type: 'int', mapping: 'frame_type_detail.leaf_sp' },
        { name: 'frame_sld', type: 'int', mapping: 'frame_type_detail.frame_sld' },
        { name: 'leaf_sld', type: 'int', mapping: 'frame_type_detail.leaf_sld' },
        { name: 'lv', type: 'int', mapping: 'frame_type_detail.lv' },
        {
            name: 'order_sheet_change_history',
            type: 'auto'
        },
        {
            name: 'change_1',
            mapping: 'order_sheet_change_history[0].change_after'
        },
        {
            name: 'change_2',
            mapping: 'order_sheet_change_history[1].change_after'
        },
        {
            name: 'change_3',
            mapping: 'order_sheet_change_history[2].change_after'
        },
        {
            name: 'change_4',
            mapping: 'order_sheet_change_history[3].change_after'
        },
        {
            name: 'change_5',
            mapping: 'order_sheet_change_history[4].change_after'
        }
    ],
    pageSize: 50,
    autoLoad: true,
    groupField: 'project_name',
    proxy: {
        type: 'ajax',
        url: '/api/ordersheet',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#logout";
                }
            }
        }
    }
});