Ext.define('CMS.store.DanhMucNguyenVatLieu', {
    extend: 'Ext.data.Store',
    alias: 'store.DanhMucNguyenVatLieuStore',
    fields: ['category','code','name','delivery_time','unit','grs_qty_set','grs_unit','s14_qty_set','s14_unit','s13_qty_set','s13_unit','tensei_qty_set','tensei_unit','jp_qty_set','jp_unit','steel_door_double','steel_door_single','steel_door_unit','sliding_door_double','sliding_door_single','sliding_door_unit','total_need','note']
});