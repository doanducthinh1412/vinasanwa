Ext.define('CMS.store.PositionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.PositionStore',
    fields: ['position_id','position_description'],
	data: [
	{
		'position_id': 1,
		'position_description': 'Nhân viên'
	},
	{
		'position_id': 2,
		'position_description': 'Trưởng phòng'
	},
	{
		'position_id': 3,
		'position_description': 'Kế toán'
	},
	{
		'position_id': 4,
		'position_description': 'Giám đốc'
	},
	]
});