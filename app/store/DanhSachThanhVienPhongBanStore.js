Ext.define('CMS.store.DanhSachThanhVienPhongBan', {
    extend: 'Ext.data.Store',
    alias: 'store.DanhSachThanhVienPhongBanStore',
    fields: ['id', 'last_name', 'first_name', 'date_of_birth', 'sex', 'email', 'phone_number', 'address', 'department', 'position', {
        name: 'name',
        convert: function(v, rec) {
            return rec.get('last_name') + ' ' + rec.get('first_name');
        }
    }],
    proxy: {
        type: 'ajax',
        url: '/api/department/get/2',
        reader: {
            type: 'json',
            rootProperty: 'data.users'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#logout";
                }
            }
        }
    },
    autoLoad: true
});