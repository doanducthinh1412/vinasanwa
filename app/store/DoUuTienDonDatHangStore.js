Ext.define('CMS.store.DoUuTienDonDatHang', {
    extend: 'Ext.data.Store',
    alias: 'store.DoUuTienDonDatHangStore',
    fields: ['name', 'val'],
    data: [{
        name: 'Gấp',
        val: '2'
    }, {
        name: 'Bình thường',
        val: '1'
    }]
});