Ext.define('CMS.store.BPKH_QuanLyPhienBanStore', {
    extend: 'Ext.data.Store',
    alias: 'store.BPKH_QuanLyPhienBanStore',
    fields: ['id', 'receive_no', 'sub_no', {
        name: 'change_history',
        type: 'auto',
    }, {
        name: 'created_by',
    }, {
        name: 'created_at',
        type: 'date',
        mapping: 'created_at.date',
    }, {
        name: 'order_sheets',
        type: 'auto',
    }, {
        name: 'banvethietke',
        type: 'auto',
    }],
    idProperty: 'id',
    pageSize: 20,
    autoLoad: true,
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            rootProperty: 'data',
        },
        listeners: {
            exception(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = '/index.html#logout';
                }
            },
        },

    },
});
