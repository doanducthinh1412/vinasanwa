Ext.define('CMS.store.DanhMucDuAnStore', {
    extend: 'Ext.data.Store',
    alias: 'store.DanhMucDuAnStore',
    fields: ['id', 'name', 'code', 'incharge', 'cus_address', 'cus_name', 'cus_incharge', 'cus_phone_number', 'cus_email', 'deletedAt', 'createdAt', 'updatedAt', 'order_sheets'],
    idProperty: 'id',
    pageSize: 20,
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: '/api/projects',
        reader: {
            type: 'json',
            rootProperty: 'data',
            //totalProperty	: 'total',
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#logout";
                }
            }
        },

    },
});