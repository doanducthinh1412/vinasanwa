Ext.define('CMS.store.LoaiDonDatHang', {
    extend: 'Ext.data.Store',
    alias: 'store.LoaiDonDatHangStore',
    fields: ['name', 'val'],
    data: [{
        name: 'Đơn đặt hàng mới',
        val: '1'
    }, {
        name: 'Đơn đặt hàng chỉnh sửa',
        val: '2'
    }, {
        name: 'Đơn đặt hàng lại',
        val: '3'
    }, {
        name: 'Đơn đặt hàng mua phụ kiện',
        val: '4'
    }, {
        name: 'Đơn đặt hàng bị hủy',
        val: '-1'
    }]
});