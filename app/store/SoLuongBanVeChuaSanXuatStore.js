Ext.define('CMS.store.SoLuongBanVeChuaSanXuat', {
    extend: 'Ext.data.Store',
    alias: 'store.SoLuongBanVeChuaSanXuatStore',
    fields: ['note', 'project_name', 'order_no', 'sub_no', { name: 'frame', type: 'int' }, { name: 'leaf_sd', type: 'int' }, { name: 'leaf_lsd', type: 'int' }, { name: 'fsld', type: 'int' }, { name: 'lsld', type: 'int' }, { name: 'shutter', type: 'int' }, { name: 'qsave', type: 'int' }, { name: 'louver', type: 'int' }, { name: 'other', type: 'int' }, ],
    autoLoad: true,
    
});