Ext.define('CMS.store.QuanLyNguoiDungStore', {
    extend: 'Ext.data.Store',
    alias: 'store.QuanLyNguoiDungStore',
    idProperty: 'id',
    fields: ['id', 'last_name', 'first_name', 'date_of_birth', 'gender', 'email', 'phone_number', 'address', 'department', 'position_id', 'username', 'password', 'avatar_url', 'lang', 'role', { name: 'createdAt', type: 'auto' }, 'employee_id', { name: 'updatedAt', type: 'auto' }, 'status', 'isDeleted'],
    pageSize: 50,
    proxy: {
        type: 'ajax',
        url: '/api/users',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        autoLoad: true,
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#logout";
                }
            }
        }
    }
});