/* eslint-disable eol-last */
/* eslint-disable no-unused-vars */
Ext.define('CMS.store.PhanCongTheoDoiVe', {
    extend: 'Ext.data.Store',
    alias: 'store.PhanCongTheoDoiVeStore',
    fields: [
        'id',
        'note',
        {
            name: 'deadline',
            type: 'date',
        },
        {
            name: 'status',
            type: 'int',
        },
        {
            name: 'deletedAt',
            type: 'auto',
        },
        {
            name: 'createdAt',
            type: 'date',
            mapping: 'createdAt.date',
        },
        {
            name: 'updatedAt',
            type: 'auto',
        },
        {
            name: 'drawer',
            type: 'auto',
        },
        {
            name: 'drawings',
            type: 'auto',
        },
        {
            name: 'order_sheet',
            type: 'auto',
        }, {
            name: 'assignment_date',
            // type: 'date'
        },
        {
            name: 'order_sheet_id',
            mapping: 'order_sheet.id',
        },
        {
            name: 'project_name',
            mapping: 'order_sheet.project_name',
        },
        {
            name: 'delivery_date',
            mapping: 'order_sheet.delivery_date',
            type: 'auto',
        },
        {
            name: 'order_date',
            mapping: 'order_sheet.order_date.date',
            type: 'date',
        },
        {
            name: 'sub_no',
            mapping: 'order_sheet.sub_no',
        },
        {
            name: 'drawer',
            type: 'auto',
        },
        {
            name: 'drawer_id',
            mapping: 'drawer.id',
        },
        {
            name: 'assignment_frame_type_detail',
            type: 'auto',
        },
        {
            name: 'working_drawing_finish',
            type: 'auto',
        },
        {
            name: 'working_drawing_send_khsx',
            type: 'auto',
        },
        {
            name: 'frame_susd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.frame_susd',
        },
        {
            name: 'frame_wd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.frame_wd',
        },
        {
            name: 'frame_sd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.frame_sd',
        },
        {
            name: 'frame_lsd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.frame_lsd',
        },
        {
            name: 'frame_fsd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.frame_fsd',
        },
        {
            name: 'leaf_fsd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.leaf_fsd',
        },
        {
            name: 'leaf_susd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.leaf_susd',
        },
        {
            name: 'leaf_wd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.leaf_wd',
        },
        {
            name: 'leaf_sd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.leaf_sd',
        },
        {
            name: 'leaf_lsd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.leaf_lsd',
        },
        {
            name: 'frame_flsd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.frame_flsd',
        },
        {
            name: 'leaf_flsd',
            type: 'int',
            mapping: 'assignment_frame_type_detail.leaf_flsd',
        },
        {
            name: 'ssjp',
            type: 'int',
            mapping: 'assignment_frame_type_detail.ssjp',
        },
        {
            name: 'ssaichi',
            type: 'int',
            mapping: 'assignment_frame_type_detail.ssaichi',
        },
        {
            name: 'ssgrill',
            type: 'int',
            mapping: 'assignment_frame_type_detail.ssgrill',
        },
        {
            name: 'grs',
            type: 'int',
            mapping: 'assignment_frame_type_detail.grs',
        },
        {
            name: 'srn',
            type: 'int',
            mapping: 'assignment_frame_type_detail.srn',
        },
        {
            name: 's13',
            type: 'int',
            mapping: 'assignment_frame_type_detail.s13',
        },
        {
            name: 's14',
            type: 'int',
            mapping: 'assignment_frame_type_detail.s14',
        },
        {
            name: 'frame_sp',
            type: 'int',
            mapping: 'assignment_frame_type_detail.frame_sp',
        },
        {
            name: 'leaf_sp',
            type: 'int',
            mapping: 'assignment_frame_type_detail.leaf_sp',
        },
        {
            name: 'frame_sld',
            type: 'int',
            mapping: 'assignment_frame_type_detail.frame_sld',
        },
        {
            name: 'leaf_sld',
            type: 'int',
            mapping: 'assignment_frame_type_detail.leaf_sld',
        },
        {
            name: 'lv',
            type: 'int',
            mapping: 'assignment_frame_type_detail.lv',
        },
        {
            name: 'frame_sw',
            type: 'int',
            mapping: 'assignment_frame_type_detail.frame_sw',
        },
        {
            name: 'other',
            type: 'int',
            mapping: 'assignment_frame_type_detail.other',
        },
    ],
    idProperty: 'id',
    pageSize: 500,
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: '/api/assignment-order-sheet',
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total',
        },
        listeners: {
            exception(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = '/index.html#logout';
                }
            },
        },
    },
});