Ext.define('CMS.store.DepartmentStore', {
    extend: 'Ext.data.Store',
    alias: 'store.DepartmentStore',
    fields: ['id', 'name'],
    idProperty: 'id',
    proxy: {
        type: 'ajax',
        url: '/api/department',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#logout";
                }
            }
        },
        autoLoad: true
    }
});