Ext.define('CMS.store.DanhSachKetQuaBanVe', {
    extend: 'Ext.data.Store',
    alias: 'store.DanhSachKetQuaBanVeStore',
    fields: [
        'bottom_bar',
        'checked_date',
        'colour1',
        'colour2',
        'drawing_date',
        'drawing_detail',
        'hardwear_detail',
        'id',
        'lot',
        'note',
        'number',
        'other_num',
        'paint1',
        'paint2',
        'type',
        {
            name: 'createdAt',
            type: 'date',
            mapping: 'createdAt.date'
        }
    ],
    pageSize: 100,
    autoLoad: true,
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            rootProperty: 'data.drawings'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                if (response.status === 302) {
                    window.location = "/index.html#logout";
                }
            }
        }
    }
});