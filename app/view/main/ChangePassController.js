Ext.define('CMS.view.main.ChangePassController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.ChangePass',
    onClickChangePass: function(btn) {
        var windowpass = this.getView();
        var form = windowpass.down('form').getForm();
        if (form.isValid()) {
            windowpass.hide();
            Ext.MessageBox.show({
                msg: 'Đang đổi mật khẩu...',
                progressText: 'Đang đổi mật khẩu...',
                width: 300,
                wait: {
                    interval: 200
                },
                animateTarget: btn
            });
            form.submit({
                url: '/api/users/pass',
                method: 'POST',
                success: function(form, action) {
                    if (action.result.success) {
                        windowpass.close();
                        Ext.MessageBox.alert('Thông báo', 'Đổi mật khẩu thành công. </br> Trình duyệt sẽ tự động khởi động lại!', function() {
                            ClassMain.logOut();
                        });
                    } else {
                        Ext.MessageBox.alert('Thông báo', 'Đổi mật khẩu không thành công!', function(btn, text) {
                            windowpass.close();
                        });
                    }
                },
                failure: function(form, action) {
                    Ext.MessageBox.alert('Thông báo', 'Đổi mật khẩu không thành công!', function(btn, text) {
                        windowpass.close();
                    });
                }
            });
        } else {
            windowpass.hide();
            Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại thông tin!', function(btn, text) {
                windowpass.show();
            });
        }
    }

});