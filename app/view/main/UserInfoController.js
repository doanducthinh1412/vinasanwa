Ext.define('CMS.view.main.UserInfoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.userinfo',
    onLoadData: function(view) {
        var me = this;
        view.el.mask("Đang tải...");
        Ext.Ajax.request({
            url: '/user',
            method: 'GET',
            timeout: 30000,
            useDefaultXhrHeader: false,
            cors: true,
            success: function(result, request) {
                view.el.unmask();
                var response = Ext.decode(result.responseText);
                if (response.success) {
                    var dataIndex = response.data;
                    var formBase = view.down('form').getForm();
                    formBase.findField("fullname").setValue(dataIndex.fullname);
                    formBase.findField("description").setValue(dataIndex.description);
                    formBase.findField("redirectURL").setValue(dataIndex.redirectURL);
                    formBase.findField("facebook_id").setValue(dataIndex.facebook_id);
                    formBase.findField("facebook_secret").setValue(dataIndex.facebook_secret);
                    formBase.findField("google_id").setValue(dataIndex.google_id);
                    formBase.findField("google_secret").setValue(dataIndex.google_secret);
                    formBase.findField("twitter_id").setValue(dataIndex.twitter_id);
                    formBase.findField("twitter_secret").setValue(dataIndex.twitter_secret);
                } else {
                    view.close();
                    Ext.MessageBox.alert('Thông báo', 'Xem thông tin không thành công', function() {});
                }
            },
            failure: function(result, request) {
                view.el.unmask();
                view.close();
            }
        });
    },
    onEditButtonClick: function(btn) {
        var controller = this;
        var employee_id = Config.userInfo.employee_id;
        if (btn.up('form').getForm().isValid()) {
            var frm = btn.up('form').getForm();
			var first_name = frm.findField("first_name").getValue();
			var last_name = frm.findField("last_name").getValue();
			var fullname = last_name+ ' ' + first_name;
            btn.up('form').getForm().submit({
                clientValidation: true,
                submitEmptyText: false,
                method: 'POST',
                url: '/api/users/edit',
                success: function(form, action) {
                    var restext = action.response.responseText;
                    if (restext == "") {
                        btn.up('window').close();
                    }
                    var res = Ext.JSON.decode(restext);
                    if (res.success == false) {
                        Ext.MessageBox.alert('Thông báo', res.result.msg, function() {
                            btn.up('window').close();
                        });
                    } else {
                        Ext.MessageBox.alert('Thông báo', 'Chỉnh sửa thông tin thành công', function() {
                            btn.up('window').close();
                        });
						Config.userInfo = res.data;
                        Ext.getCmp('mainViewPort').getController().getViewModel().set('MainTxtUserName', fullname);
                        //btn.up('window').hide();

                    }
                },
                failure: function(form, action) {
                    var restext = action.response.responseText;
                    if (restext == "") {
                        Ext.MessageBox.alert('Thông báo', 'Không thể  chỉnh sửa thông tin', function() {
                            btn.up('window').close();
                        });
                    } else {
                        var res = Ext.JSON.decode(restext);
                        Ext.MessageBox.alert('Thông báo', res.msg, function() {
                            btn.up('window').show();
                        });
                    }
                },
                waitMsg: "Đang chỉnh sửa..."
            });
        }
    },
    onEditAvatarButtonClick: function() {
        var me = this,
            refs = me.getReferences(),
            avatarUser = refs.avatarUser,
            view = me.getView();
        view.hide();
        var window_avarta = Ext.create('Ext.window.Window', {
            title: 'Upload ảnh đại diện',
            height: 200,
            width: 350,
            layout: 'border',
            border: false,
            glyph: 'xf0ee@FontAwesome',
            modal: true,
            items: [{
                xtype: 'form',
                region: 'center',
                bodyPadding: 20,
                defaults: {
                    anchor: '100%',
                    labelAlign: 'top'
                },
                fieldDefaults: {
                    msgTarget: 'under',
                    autoFitErrors: true
                },
                items: {
                    xtype: 'filefield',
                    name: 'avatar',
                    allowBlank: false,
                    regex: /^.*\.(png|jpg|JPG|PNG|jpeg)$/,
                    regexText: 'Định dạng không cho phép',
                    hideLabel: true,
                    reference: 'imageFile',
                    listeners: {
                        afterrender: function(cmp) {
                            cmp.fileInputEl.set({
                                accept: 'image/*'
                            });
                        }
                    }
                },
                buttons: [{
                    text: 'Thoát',
                    glyph: 'xf00d@FontAwesome',
                    handler: function() {
                        this.up('window').close();
                    }
                }, {
                    text: 'Upload',
                    glyph: 'xf0ee@FontAwesome',
                    ui: 'soft-orange',
                    disabled: true,
                    formBind: true,
                    handler: function(btn) {
                        if (btn.up('form').getForm().isValid()) {
                            btn.up('form').getForm().submit({
                                clientValidation: true,
                                submitEmptyText: false,
                                method: 'POST',
                                url: '/api/users/image/edit',
                                waitMsg: 'Đang cập nhật ảnh đại diện...',
                                success: function(form, action) {
                                    btn.up('window').hide();
                                    var restext = action.response.responseText;
                                    if (restext == "") {
                                        btn.up('window').close();
                                    }
                                    var res = Ext.JSON.decode(restext);
                                    if (res.success == false) {
                                        Ext.MessageBox.alert('Thông báo', 'Không thể  cập nhật ảnh đại diện', function() {
                                            btn.up('window').close();
                                        });
                                    } else {
                                        view.show();
                                        avatarUser.updateLayout();
                                        avatarUser.update('<center><img width="auto" height="140px" src="/api/users/image?_dc=' + new Date().getTime()+'"></center>');
                                        Ext.getCmp('mainViewPort').getController().getReferences().avartaUser.setSrc('/api/users/image?_dc=' + new Date().getTime());
                                        btn.up('window').close();

                                    }
                                },
                                failure: function(form, action) {
                                    btn.up('window').hide();
                                    var restext = action.response.responseText;
                                    if (restext == "") {
                                        Ext.MessageBox.alert('Thông báo', 'Không thể  cập nhật ảnh đại diện', function() {
                                            btn.up('window').close();
                                        });
                                    } else {
                                        var res = Ext.JSON.decode(restext);
                                        Ext.MessageBox.alert('Thông báo', res.data, function() {
                                            btn.up('window').show();
                                        });
                                    }

                                }
                            });
                        }
                    }
                }]
            }]
        }).show();
    }
});