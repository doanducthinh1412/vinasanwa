Ext.define('CMS.view.main.AuthenticationModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.authentication',

    data: {
        user_id: '',
        fullName: '',
        password: '',
        email: '',
        persist: false,
        agrees: false,
        notification: ''
    }
});