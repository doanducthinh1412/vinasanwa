Ext.define('CMS.view.main.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',
    onLoginButton: function() {
        var me = this;
        var mainView = Ext.getCmp('mainViewPort');
        var username = this.getViewModel().get('user_id');
        var password = this.getViewModel().get('password');
        if (username && password) {
            me.getViewModel().set('notification', '<color class="text-green"><i class="fa fa-spinner fa-spin"></i> Đang đăng nhập hệ thống</color>');
            Ext.Ajax.request({
                url: '/api/users/login',
                method: 'POST',
                timeout: 30000,
                useDefaultXhrHeader: false,
                cors: true,
                params: {
                    username: username,
                    password: password
                },
                success: function(result, request) {
                    try {
                        var response = Ext.decode(result.responseText);
                        if (response.success) {
                            Config.userInfo = response.data;
                            Ext.getCmp('mainViewPort').initPermission();
                            Ext.getCmp('mainViewPort').getController().getViewModel().set('MainTxtUserName', Config.userInfo.last_name + " " + Config.userInfo.first_name);
                            Ext.getCmp('mainViewPort').getController().getReferences().avartaUser.setSrc('/api/users/image?_dc=' + new Date().getTime());
                            me.getViewModel().set('notification', '');
                            mainView.getController().redirectTo('dashboard', true);
                            if (Ext.getCmp('dashboard-TabPanel')) {
                                Ext.getCmp('dashboard-TabPanel').getController().loadDataPanel();
                            }
                        } else {
                            me.getViewModel().set('userid', '');
                            me.getViewModel().set('password', '');
                            me.getViewModel().set('notification', '<color class="text-red"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Thông tin đăng nhập không chính xác</color>');
                            mainView.getController().redirectTo('login', true);
                        }
                    } catch (err) {
                        me.getViewModel().set('userid', '');
                        me.getViewModel().set('password', '');
                        me.getViewModel().set('notification', '<color class="text-red"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Thông tin đăng nhập không chính xác</color>');
                        mainView.getController().redirectTo('login', true);
                    }
                },
                failure: function(result, request) {
                    me.getViewModel().set('userid', '');
                    me.getViewModel().set('password', '');
                    me.getViewModel().set('notification', '<color class="text-red"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Thông tin đăng nhập không chính xác</color>');
                    mainView.getController().redirectTo('login', true);
                }
            });
        } else {
            me.getViewModel().set('notification', '<color class="text-red"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Thông tin đăng nhập không chính xác</color>');
        }
        mainView.getController().redirectTo('login', true);
    }
});