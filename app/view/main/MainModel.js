/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('CMS.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        MainTxtUserName: 'Người dùng',
        zoneUser: null,
        language: Config.language[Config.defaultLanguage]
    }
});