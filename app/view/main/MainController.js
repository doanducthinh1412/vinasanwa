/* eslint-disable no-restricted-globals */
/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('CMS.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    listen: {
        controller: {
            '#': {
                unmatchedroute: 'onRouteChange',
            },
        },
    },

    routes: {
        ':node': 'onRouteChange',
    },

    lastView: null,

    onRouteChange(id) {
        this.setCurrentView(id);
    },

    setCurrentView(hashTag) {
        const hashTagn = (hashTag || '').toLowerCase();
        const me = this;
        const refs = me.getReferences();
        const { mainView } = refs;
        if (hashTagn === 'login') {
            if (!Ext.getCmp('mainLogin')) {
                const loginPanel = Ext.create({
                    xtype: 'login',
                    id: 'mainLogin',
                });
            } else {
                Ext.getCmp('mainLogin').show();
            }
        } else if (hashTagn === 'logout') {
            try {
                Ext.util.Cookies.clear('PHPSESSID');
                const messageLogOut = Ext.MessageBox.show({
                    msg: 'Đang đăng xuất hệ thống. Xin chờ...',
                    progressText: 'Đang đăng xuất...',
                    width: 300,
                    wait: {
                        interval: 200,
                    },
                });
                Ext.Ajax.request({
                    url: '/api/users/logout',
                    method: 'POST',
                    timeout: 30000,
                    useDefaultXhrHeader: false,
                    cors: true,
                    scope: this,
                    success(result, request) {
                        messageLogOut.hide();
                        location.reload();
                    },
                    failure(result, request) {
                        messageLogOut.hide();
                        location.reload();
                    },
                });
            } catch (err) {
                location.reload();
                ClassMain.showErrors('Lỗi function ClassMain.logOut', err);
            }
        } else {
            if (Ext.getCmp('mainLogin')) {
                Ext.getCmp('mainLogin').destroy();
            }
            if (Ext.getCmp(`${hashTagn}-TabPanel`)) {
                mainView.setActiveTab(Ext.getCmp(`${hashTagn}-TabPanel`));
            } else if (hashTagn.toLowerCase() === 'dashboard') {
                if (Config.userInfo.department) {
                    if (!Ext.getCmp('dashboard-TabPanel')) {
                        const tab = mainView.add({
                            closable: false,
                            xtype: 'Dashboard',
                            iconCls: 'x-fa fa-desktop',
                            title: '<b>DASHBOARD</b>',
                            id: 'dashboard-TabPanel',
                        });
                        mainView.setActiveTab(tab);
                    } else {
                        mainView.setActiveTab(Ext.getCmp('dashboard-TabPanel'));
                    }
                }
            }
        }
    },
    searchFunction(searchString) {
        const me = this;
        const refs = me.getReferences();
        const { mainView } = refs;
        if (searchString) {
            if (Ext.getCmp('KetQuaTimKiem-TabPanel') == null) {
                const tab = mainView.add({
                    closable: true,
                    xtype: 'KetQuaTimKiem',
                    iconCls: `${Ext.baseCSSPrefix}form-search-trigger`,
                    title: `<b>Kết quả tìm kiếm với từ khóa: ${searchString}</b>`,
                    id: 'KetQuaTimKiem-TabPanel',
                    searchString,

                });
                mainView.setActiveTab(tab);
            } else {
                const tab = Ext.getCmp('KetQuaTimKiem-TabPanel');
                tab.setSearchString(searchString);
                tab.reloadView();
                mainView.setActiveTab(tab);
            }
        }
    },
    onClickFunctionMenu(item, e) {
        const me = this;
        const refs = me.getReferences();
        const { mainView } = refs;
        if (item.functionMenu) {
            this.redirectTo(item.functionMenu.toLowerCase());
            if (item.functionMenu === 'WindowFullPanel') {
                /* var windowFull = Ext.create({
                    xtype: 'WindowFullPanel',
                    titleWindow: node.data.text,
                    iconCls: node.data.iconCls,
                    listeners: {
                        close: function() {
                            model = navigationList.getStore().findRecord('viewType', 'Dashboard');
                            navigationList.setSelection(model);
                        }
                    },
                    items: {
                        layout: 'fit',
                        xtype: node.data.viewType
                    }
                }).show(); */
            } else if (Ext.getCmp(`${item.functionMenu.toLowerCase()}-TabPanel`) == null) {
                const tab = mainView.add({
                    layout: 'fit',
                    iconCls: item.iconCls.replace('text-dark-blue', ''),
                    title: `<b>${item.text.toUpperCase()}</b>`,
                    id: `${item.functionMenu.toLowerCase()}-TabPanel`,
                    closable: item.functionMenu.toLowerCase() !== 'dashboard',
                    items: {
                        xtype: item.functionMenu,
                    },
                });
                mainView.setActiveTab(tab);
            } else {
                mainView.setActiveTab(Ext.getCmp(`${item.functionMenu.toLowerCase()}-TabPanel`));
            }
        }
    },

    onMainViewTabChange(tabPanel, newCard, oldCard, eOpts) {
        const me = this;
        const refs = me.getReferences();
        if (newCard.xtype === 'ViewDonDatHang' || newCard.xtype === 'CreateDonDatHang') {
            newCard.down('tabpanel').getActiveTab().updateLayout();
        }
    },

    onClickBtnLogout() {
        ClassMain.logOut();
    },
    onViewInfoButtonClick() {
        Ext.create({
            xtype: 'UserInfo',
        }).show();
    },
    onChangePassButtonClick() {
        Ext.create({
            xtype: 'ChangePass',
        }).show();
    },
});