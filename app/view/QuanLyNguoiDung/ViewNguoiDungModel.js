Ext.define('CMS.view.ViewNguoiDung.ViewNguoiDungModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.ViewNguoiDung',
	requires: [
		'CMS.store.QuanLyNguoiDungStore',
		'CMS.store.RoleStore',
		'CMS.store.PositionStore',
		'CMS.store.DepartmentStore'
	],
	stores: {
		roleStore : {
			type: 'RoleStore',
			autoLoad: true,
		},
		positionStore: {
			type: 'PositionStore',
			autoLoad: true
		},
		departmentStore: {
			type: 'DepartmentStore',
			autoLoad: true
		}
	}
});