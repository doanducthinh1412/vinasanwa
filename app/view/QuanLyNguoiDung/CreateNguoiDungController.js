Ext.define('CMS.view.CreateNguoiDung.CreateNguoiDungController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.CreateNguoiDung',
    createUser: function() {
        var panel = this.view;
        var win = panel.up('window');
        var frm = panel.getForm();

        var username = frm.findField("username").getValue();
        var store = panel.getGridPanel().getStore();
        if (frm.isValid()) {
            win.hide();
            if (store.findRecord('username', username) != null) {
                Ext.MessageBox.alert('Thông báo', 'Người dùng đã tồn tại. Vui lòng kiểm tra lại!', function() {
                    win.show();
                });
            } else {
                frm.submit({
                    url: '/api/users/create',
                    clientValidation: true,
                    method: 'POST',
                    success: function(form, action) {
                        Ext.MessageBox.alert('Thông báo', 'Tạo mới người dùng thành công', function() {
                            win.close();
                            panel.getGridPanel().getStore().load({
                                callback: function() {}
                            });
                        });
                    },
                    failure: function(form, action) {
                        Ext.Msg.alert('Thông báo', 'Lỗi ' + action.message + '. Vui lòng kiểm tra lại!', function() {
                            win.show();
                        });
                    },
                })
            }
        } else {
            win.hide();
            Ext.Msg.alert('Thông báo', 'Các trường bắt buộc không được để trống. Vui lòng kiểm tra lại', function() {
                win.show();
            });
        }
    }
});