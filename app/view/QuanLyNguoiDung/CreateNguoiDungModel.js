Ext.define('CMS.view.CreateNguoiDung.CreateNguoiDungModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.CreateNguoiDung',
	requires: [
		'CMS.store.QuanLyNguoiDungStore',
		'CMS.store.RoleStore',
		'CMS.store.PositionStore',
		'CMS.store.DepartmentStore'
	],
	stores: {
		roleStore : {
			type: 'RoleStore',
			autoLoad: true,
		},
		positionStore: {
			type: 'PositionStore',
		},
		departmentStore: {
			type: 'DepartmentStore',
			autoLoad: true
		}
	}
});