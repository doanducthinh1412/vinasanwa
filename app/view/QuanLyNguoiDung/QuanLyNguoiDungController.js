Ext.define('CMS.view.QuanLyNguoiDung.QuanLyNguoiDungController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.QuanLyNguoiDung',
    onItemContextMenu: function(view, record, item, index, e) {
        var controller = this;
        var gridPanel = controller.view;
        var position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        gridPanel.menu.showAt(position);
    },
    onCreateButtonClick: function() {
        var panel = this.view;
        var windowss = Ext.create('Ext.window.Window', {
            title: 'Tạo mới người dùng',
            height: 590,
            width: 490,
            layout: 'border',
            border: false,
            maximizable: true,
            glyph: 'xf055@FontAwesome',
            modal: true,
            items: [{
                xtype: 'CreateNguoiDung',
                region: 'center',
                autoScroll: true,
                gridPanel: panel
            }]
        }).show();
    },
    onViewButtonClick: function() {
        main_panel = this.view;
        var selectedRow = main_panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        windowss = Ext.create('Ext.window.Window', {
            title: 'Thông tin người dùng ' + rec.get('last_name') + ' ' + rec.get('first_name'),
            height: 590,
            width: 490,
            layout: 'border',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'ViewNguoiDung',
                region: 'center',
                autoScroll: true,
                rec: rec
            }]
        }).show();
    },
    onViewButtonClickGrid: function() {
        var panel = this.view;
        var rec = gridPanel.getStore().getAt(rowIndex);
        windowss = Ext.create('Ext.window.Window', {
            title: 'Thông tin người dùng ' + rec.get('last_name') + ' ' + rec.get('first_name'),
            height: 590,
            width: 490,
            layout: 'border',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'ViewNguoiDung',
                region: 'center',
                autoScroll: true,
                rec: rec
            }]
        }).show();
    },
    onEditButtonClick: function() {
        var bl_items = [];
        main_panel = this.view;
        var selectedRow = main_panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        windowss = Ext.create('Ext.window.Window', {
            title: 'Chỉnh sửa người dùng ' + rec.get('last_name') + ' ' + rec.get('first_name'),
            height: 590,
            width: 490,
            layout: 'border',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'EditNguoiDung',
                region: 'center',
                autoScroll: true,
                rec: rec,
                gridPanel: main_panel
            }]
        }).show();
    },
    onEditButtonClickGrid: function(gridPanel, rowIndex, colIndex) {
        var panel = this.view;
        var rec = gridPanel.getStore().getAt(rowIndex);
        windowss = Ext.create('Ext.window.Window', {
            title: 'Chỉnh sửa người dùng ' + rec.get('last_name') + ' ' + rec.get('first_name'),
            height: 590,
            width: 490,
            layout: 'border',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'EditNguoiDung',
                region: 'center',
                autoScroll: true,
                rec: rec,
                gridPanel: panel
            }]
        }).show();
    },
    onDeleteButtonClick: function() {
        var panel = this.view;
        var selectedRow = panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        Ext.MessageBox.confirm('Thông báo', 'Xóa người dùng "<b>' + rec.get('last_name') + ' ' + rec.get('first_name') + '</b>" ?', function(btn) {
            if (btn == "yes") {
                panel.el.mask("Đang xóa...");
                Ext.Ajax.request({
                    url: '/api/users/delete/' + rec.get('id'),
                    success: function(response) {
                        panel.el.unmask();
                        var restext = response.responseText;
                        if (restext != "") {
                            var res = Ext.JSON.decode(restext);
                            if (res.success == false) {
                                Ext.MessageBox.alert('Thông báo', res.msg, function() {});
                            } else {
                                panel.getStore().load({
                                    callback: function() {}
                                });
                            }
                        }
                    },
                    failure: function(response) {
                        panel.el.unmask();
                    }
                });
            }
        });
    },
    onDeleteButtonClickGrid: function(gridPanel, rowIndex, colIndex) {
        var panel = this.view;
        var rec = gridPanel.getStore().getAt(rowIndex);
        Ext.MessageBox.confirm('Thông báo', 'Xóa người dùng "<b>' + rec.get('last_name') + ' ' + rec.get('first_name') + '</b>" ?', function(btn) {
            if (btn == "yes") {
                panel.el.mask("Đang xóa...");
                Ext.Ajax.request({
                    url: '/api/users/delete/' + rec.get('id'),
                    success: function(response) {
                        panel.el.unmask();
                        var restext = response.responseText;
                        if (restext != "") {
                            var res = Ext.JSON.decode(restext);
                            if (res.success == false) {
                                Ext.MessageBox.alert('Thông báo', res.msg, function() {});
                            } else {
                                panel.getStore().load({
                                    callback: function() {}
                                });
                            }
                        }
                    },
                    failure: function(response) {
                        panel.el.unmask();
                    }
                });
            }
        });
    },
    onResetPasswordButtonClick: function() {
        var panel = this.view;
        var selectedRow = panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        Ext.MessageBox.confirm('Thông báo', 'Khôi phục mật khẩu người dùng "<b>' + rec.get('last_name') + ' ' + rec.get('first_name') + '</b>" ?', function(btn) {
            if (btn == "yes") {
                panel.el.mask("Đang thực hiện...");
                Ext.Ajax.request({
                    url: '/api/users/reset/' + rec.get('id'),
                    method: 'POST',
                    success: function(response) {
                        panel.el.unmask();
                        var restext = response.responseText;
                        if (restext != "") {
                            var res = Ext.JSON.decode(restext);
                            if (res.success == false) {
                                Ext.MessageBox.alert('Thông báo', res.msg, function() {});
                            } else {
                                panel.getStore().load({
                                    callback: function() {}
                                });
                                var res = Ext.JSON.decode(restext);
                                Ext.MessageBox.alert('Thông báo', 'Đổi mật khẩu thành công<br>Mật khẩu mới là: ' + res.data.new_pass, function() {});
                            }
                        }
                    },
                    failure: function(response) {
                        panel.el.unmask();
                    }
                });
            }
        });
    },
    onResetPasswordButtonClickGrid: function(gridPanel, rowIndex, colIndex) {
        var panel = this.view;
        var rec = gridPanel.getStore().getAt(rowIndex);
        Ext.MessageBox.confirm('Thông báo', 'Khôi phục mật khẩu người dùng "<b>' + rec.get('last_name') + ' ' + rec.get('first_name') + '</b>" ?', function(btn) {
            if (btn == "yes") {
                panel.el.mask("Đang thực hiện...");
                Ext.Ajax.request({
                    url: '/api/users/reset/' + rec.get('id'),
                    method: 'POST',
                    success: function(response) {
                        panel.el.unmask();
                        var restext = response.responseText;
                        if (restext != "") {
                            var res = Ext.JSON.decode(restext);
                            if (res.success == false) {
                                Ext.MessageBox.alert('Thông báo', res.msg, function() {});
                            } else {
                                panel.getStore().load({
                                    callback: function() {}
                                });
                                var res = Ext.JSON.decode(restext);
                                Ext.MessageBox.alert('Thông báo', 'Đổi mật khẩu thành công<br>Mật khẩu mới là: ' + res.data.new_pass, function() {});
                            }
                        }
                    },
                    failure: function(response) {
                        panel.el.unmask();
                    }
                });
            }
        });
    },
    onReloadButtonClick: function() {
        var panel = this.view;
        panel.el.mask('Đang tải...');
        panel.getStore().reload();
        panel.el.unmask();
    }
});