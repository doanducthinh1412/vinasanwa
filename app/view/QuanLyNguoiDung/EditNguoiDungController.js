Ext.define('CMS.view.EditNguoiDung.EditNguoiDungController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.EditNguoiDung',
    onSaveButtonClick: function(btn) {
        var panel = this.view;
        var gridPanel = panel.getGridPanel();
        var record = panel.getRec();
        var userid = record.get('id');
        if (btn.up('form').getForm().isValid()) {
            btn.up('window').hide();
            var frm = btn.up('form').getForm();
            var first_name = frm.findField("first_name").getValue();
            var last_name = frm.findField("last_name").getValue();
            var fullname = last_name + ' ' + first_name;
            btn.up('form').getForm().submit({
                clientValidation: true,
                submitEmptyText: false,
                method: 'POST',
                url: '/api/users/edit/' + userid,
                success: function(form, action) {
                    var restext = action.response.responseText;
                    if (restext == "") {
                        btn.up('window').close();
                    }
                    var res = Ext.JSON.decode(restext);
                    if (res.success == false) {
                        Ext.MessageBox.alert('Thông báo', res.result.msg, function() {
                            btn.up('window').close();
                        });
                    } else {
                        Ext.MessageBox.alert('Thông báo', 'Chỉnh sửa thông tin thành công', function() {
                            btn.up('window').close();
                        });
                        gridPanel.getStore().reload();
                        if (userid == Config.userInfo.id) {
                            Config.userInfo = res.data;
                            Ext.getCmp('mainViewPort').getController().getViewModel().set('MainTxtUserName', fullname);
                        }
                    }
                },
                failure: function(form, action) {
                    var restext = action.response.responseText;
                    if (restext == "") {
                        Ext.MessageBox.alert('Thông báo', 'Hệ thống không khả dụng. Vui lòng kiểm tra lại!', function() {
                            btn.up('window').close();
                        });
                    } else {
                        var res = Ext.JSON.decode(restext);
                        Ext.MessageBox.alert('Thông báo', 'Lỗi ' + res.msg + '. Vui lòng kiểm tra lại!', function() {
                            btn.up('window').show();
                        });
                    }
                },
                waitMsg: "Đang chỉnh sửa..."
            });
        } else {
            btn.up('window').hide();
            Ext.Msg.alert('Thông báo', 'Các trường bắt buộc không được để trống. Vui lòng kiểm tra lại!', function() {
                btn.up('window').show();
            });
        }
    }
});