Ext.define('CMS.view.EditNguoiDung.EditNguoiDungModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.EditNguoiDung',
	requires: [
		'CMS.store.QuanLyNguoiDungStore',
		'CMS.store.RoleStore',
		'CMS.store.PositionStore',
		'CMS.store.DepartmentStore'
	],
	stores: {
		roleStore : {
			type: 'RoleStore',
			autoLoad: true,
		},
		positionStore: {
			type: 'PositionStore',
			autoLoad: true
		},
		departmentStore: {
			type: 'DepartmentStore',
			autoLoad: true
		}
	}
});