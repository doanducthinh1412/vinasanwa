Ext.define('CMS.view.DanhSachNhanVien.DanhSachNhanVienController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.DanhSachNhanVien',
    onItemContextMenu: function(view, record, item, index, e) {
        var controller = this;
        var gridPanel = controller.view;
        var position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        gridPanel.menu.showAt(position);
    },
    onCreateButtonClick: function() {
        var panel = this.view;
        var windowss = Ext.create('Ext.window.Window', {
            title: 'Tạo mới nhân viên',
            height: 590,
            width: 490,
            layout: 'border',
            border: false,
            maximizable: true,
            glyph: 'xf055@FontAwesome',
            modal: true,
            items: [{
                xtype: 'CreateNhanVien',
                region: 'center',
                autoScroll: true
            }]
        }).show();
    },
    onEditButtonClick: function() {
        var bl_items = [];
        main_panel = this.view;
        var selectedRow = main_panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        windowss = Ext.create('Ext.window.Window', {
            title: 'Chỉnh sửa thông tin nhân viên ' + rec.get('last_name') + ' ' + rec.get('first_name'),
            height: 590,
            width: 490,
            layout: 'border',
            glyph: 'xf044@FontAwesome',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'EditNhanVien',
                region: 'center',
                autoScroll: true,
                rec: rec
            }]
        }).show();
    },
	onAddUserButtonClick: function() {
		var bl_items = [];
        main_panel = this.view;
        var selectedRow = main_panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        windowss = Ext.create('Ext.window.Window', {
            title: 'Tạo tài khoản hệ thống cho nhân viên ' + rec.get('last_name') + ' ' + rec.get('first_name'),
            height: 290,
            width: 490,
            layout: 'border',
            glyph: 'xf044@FontAwesome',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'AddNguoiDung',
                region: 'center',
                autoScroll: true,
                rec: rec
            }]
        }).show();
	},
    onDeleteButtonClick: function() {
        var panel = this.view;
        var selectedRow = panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        Ext.MessageBox.confirm('Thông báo', 'Xóa nhân viên "<b>' + rec.get('last_name') + ' ' + rec.get('first_name')+'</b>" ?', function(btn) {
            if (btn == "yes") {
                panel.el.mask("Đang xóa...");
                Ext.Ajax.request({
                    url: '/user/delete/' + rec.data.ID,
                    success: function(response) {
                        panel.el.unmask();
                        var restext = response.responseText;
                        if (restext != "") {
                            var res = Ext.JSON.decode(restext);
                            if (res.success == false) {
                                Ext.MessageBox.alert('Thông báo', res.msg, function() {});
                            } else {
                                panel.getStore().load({
                                    callback: function() {}
                                });
                            }
                        }
                    },
                    failure: function(response) {
                        panel.el.unmask();
                    }
                });
            }
        });
    },
    onReloadButtonClick: function() {
        var panel = this.view;
        panel.el.mask('Đang tải...');
        panel.getStore().reload();
        panel.el.unmask();
    }
});