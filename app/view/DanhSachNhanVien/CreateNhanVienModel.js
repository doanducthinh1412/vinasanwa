Ext.define('CMS.view.CreateNhanVien.CreateNhanVienModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.CreateNhanVien',
	requires: [
		'CMS.store.QuanLyNguoiDungStore',
		'CMS.store.RoleStore',
		'CMS.store.PositionStore',
		'CMS.store.DepartmentStore'
	],
	stores: {
		quanLyNguoiDungStore: {
			type: 'QuanLyNguoiDungStore',
			autoLoad: true,
			data: [{
				'last_name':'Nguyễn',
				'first_name': 'Văn A',
				'date_of_birth': '26/05/1974',
				'sex':'1',
				'email':'nva@email.com',
				'phone_number':'0123456',
				'address':'12 đường A phường B quận C thành phố D',
				'department_id': 1,
				'position_id': 1,
				'username': 'nva',
				'password': '123456',
				'avatar_url': '/images/nva_avatar.png',
				'lang':'vi',
				'role_id': [1,2,3],
				'employee_id': 1,
				'createAt': '12/08/2017',
				'updateAt': '15/08/2017',
				'status': '1'
			},{
				'last_name':'Nguyễn',
				'first_name': 'Văn B',
				'date_of_birth': '26/05/1974',
				'sex':'1',
				'email':'nvb@email.com',
				'phone_number':'0123456',
				'address':'12 đường A phường B quận C thành phố D',
				'department_id': 2,
				'position_id': 2,
				'username': 'nvb',
				'password': '123456',
				'avatar_url': '/images/nva_avatar.png',
				'lang':'vi',
				'role_id': 2,
				'employee_id': 2,
				'createAt': '12/08/2017',
				'updateAt': '15/08/2017',
				'status': '1'
			},{
				'last_name':'Nguyễn',
				'first_name': 'Văn C',
				'date_of_birth': '26/05/1974',
				'sex':'1',
				'email':'nvc@email.com',
				'phone_number':'0123456',
				'address':'12 đường A phường B quận C thành phố D',
				'department_id': 3,
				'position_id': 3,
				'username': 'nvc',
				'password': '123456',
				'avatar_url': '/images/nva_avatar.png',
				'lang':'vi',
				'role_id': 3,
				'employee_id': 3,
				'createAt': '12/08/2017',
				'updateAt': '15/08/2017',
				'status': '1'
			},{
				'last_name':'Nguyễn',
				'first_name': 'Thị D',
				'date_of_birth': '26/05/1974',
				'sex':'0',
				'email':'nva@email.com',
				'phone_number':'0123456',
				'address':'12 đường A phường B quận C thành phố D',
				'department_id': 4,
				'position_id': 4,
				'username': 'nvd',
				'password': '123456',
				'avatar_url': '/images/nva_avatar.png',
				'lang':'vi',
				'role_id': 4,
				'employee_id': 4,
				'createAt': '12/08/2017',
				'updateAt': '15/08/2017',
				'status': '0'
			}]
		},
		roleStore : {
			type: 'RoleStore',
			autoLoad: true
		},
		positionStore: {
			type: 'PositionStore',
			autoLoad: true
		},
		departmentStore: {
			type: 'DepartmentStore',
			autoLoad: true
		}
	}
});