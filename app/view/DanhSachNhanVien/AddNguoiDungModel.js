Ext.define('CMS.view.AddNguoiDung.AddNguoiDungModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.AddNguoiDung',
	requires: [
		'CMS.store.PositionStore',
	],
	stores: {
		roleStore : {
			type: 'RoleStore',
			autoLoad: true
		}
	}
});