Ext.define('CMS.view.BPKeHoach.DinhMucThoiGianSanXuat.DinhMucThoiGianSanXuatModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.DinhMucThoiGianSanXuat',
    data: {
        dinhMucChungData: [{
            'product_name': 'Khung etai / Frame',
            'shearing_time_per_product': '15',
            'shearing_people_per_machine': '2',
            'punching_time_per_product': '10',
            'punching_people_per_machine': '2',
            'bending_time_per_product': '45',
            'bending_people_per_machine': '2'
        }, {
            'product_name': 'Cánh LSD / LSD leaf',
            'shearing_time_per_product': '15',
            'shearing_people_per_machine': '2',
            'punching_time_per_product': '7',
            'punching_people_per_machine': '2',
            'bending_time_per_product': '20',
            'bending_people_per_machine': '2'
        }, {
            'product_name': 'Cánh SD / SD Leaf',
            'shearing_time_per_product': '30',
            'shearing_people_per_machine': '2',
            'punching_time_per_product': '8',
            'punching_people_per_machine': '2',
            'bending_time_per_product': '15',
            'bending_people_per_machine': '2'
        }],
        dinhMucHanKhung: [{
            'product_name': 'Khung',
            'producing_process': 'Lắp ráp chi tiết tiêu chuẩn, ke chống cháy',
            'worker_per_process': '12 người (5040 phút/ngày)',
            'manufacture_time': '700 phút/ngày',
            'average_time_per_product': '5 phút/cửa ',
            'frame_per_day': '140',
            'total_frame': '120 khung/ngày'
        }, {
            'product_name': 'Khung',
            'producing_process': 'Bắn vít',
            'worker_per_process': '12 người (5040 phút/ngày)',
            'manufacture_time': '1120 phút/ngày',
            'average_time_per_product': '12 phút/cửa',
            'frame_per_day': '140',
            'total_frame': '120 khung/ngày'
        }, {
            'product_name': 'Khung',
            'producing_process': 'Lắp ráp',
            'worker_per_process': '12 người (5040 phút/ngày)',
            'manufacture_time': '3220 phút/ngày',
            'average_time_per_product': '26 phút/cửa',
            'frame_per_day': '140',
            'total_frame': '120 khung/ngày'
        }],
        dinhMucHanCanh: [{
            'product_type': 'Cửa SD',
            'product_name': 'Cửa đơn, không có ô kính hay louver',
            'total_product': 'Hoàn thiện 115 cánh/ngày',
        }, {
            'product_type': 'Cửa SD',
            'product_name': 'Cửa đôi, không có ô kính hay louver',
            'total_product': 'Hoàn thiện 75 cánh/ngày + 40 cánh ép chờ hoàn thiện',
        }, {
            'product_type': 'Cửa SD',
            'product_name': 'Cửa đơn, có ô kính hay louver',
            'total_product': 'Hoàn thiện 90 cánh/ngày',
        }, {
            'product_type': 'Cửa SD',
            'product_name': 'Cửa đôi, có ô kính hay louver',
            'total_product': 'Hoàn thiện 60 cánh/ngày + 60 cánh ép chờ hoàn thiện',
        }, {
            'product_type': 'Cửa LSD',
            'product_name': 'Cửa đơn, không có ô kính hay louver',
            'total_product': 'Hoàn thiện 100 cánh/ngày',
        }, {
            'product_type': 'Cửa LSD',
            'product_name': 'Cửa đôi, không có ô kính hay louver',
            'total_product': 'Hoàn thiện 80 cánh/ngày + 20 cánh ép chờ hoàn thiện<br> hoặc <br>Hoàn thiện 90 cánh/ngày + 10 cánh ép chờ hoàn thiện',
        }, {
            'product_type': 'Cửa LSD',
            'product_name': 'Cửa đơn, có ô kính hay louver',
            'total_product': 'Hoàn thiện 75 cánh/ngày',
        }, {
            'product_type': 'Cửa LSD',
            'product_name': 'Cửa đôi, có ô kính hay louver',
            'total_product': 'Hoàn thiện 60 cánh/ngày + 15cánh ép chờ hoàn thiện',
        }]
    }
});