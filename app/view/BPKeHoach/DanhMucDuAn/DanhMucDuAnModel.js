Ext.define('CMS.view.BPKeHoach.DanhMucDuAn.DanhMucDuAnModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.DanhMucDuAn',
    requires: [
        'CMS.store.DanhMucDuAnStore',
    ],
    stores: {
        danhMucDuAnStore: {
            type: 'DanhMucDuAnStore',
            autoLoad: true,
            /*data: [{
            	'project_name':'VAP Hung Yen Factory G3',
            	'project_code':'VAP HUNG',
            	'project_id':'1',
            	'project_manager':'Nguyễn Văn Nam',
            	'customer_address':'Hưng Yên',
            	'customer_name':'Nguyễn Văn Tuấn',
            	'customer_contact':'Nguyễn Thị Liên',
            	'customer_phone':'00321654',
            	'customer_email':'nvtuan@gmail.com'
            },{
            	'project_name':'Trung tâm thương mại VP',
            	'project_code':'TRUNG TAM TM VP',
            	'project_id':'2',
            	'project_manager':'Nguyễn Văn Nam',
            	'customer_address':'Hưng Yên',
            	'customer_name':'Nguyễn Văn Tuấn',
            	'customer_contact':'Nguyễn Thị Liên',
            	'customer_phone':'00321654',
            	'customer_email':'nvtuan@gmail.com'
            },{
            	'project_name':'VAP Hung Yen Factory G3',
            	'project_code':'VAP HUNG',
            	'project_id':'3',
            	'project_manager':'Nguyễn Văn Nam',
            	'customer_address':'Hưng Yên',
            	'customer_name':'Nguyễn Văn Tuấn',
            	'customer_contact':'Nguyễn Thị Liên',
            	'customer_phone':'00321654',
            	'customer_email':'nvtuan@gmail.com'
            },{
            	'project_name':'VAP Hung Yen Factory G3',
            	'project_code':'VAP HUNG',
            	'project_id':'4',
            	'project_manager':'Nguyễn Văn Nam',
            	'customer_address':'Hưng Yên',
            	'customer_name':'Nguyễn Văn Tuấn',
            	'customer_contact':'Nguyễn Thị Liên',
            	'customer_phone':'00321654',
            	'customer_email':'nvtuan@gmail.com'
            }]*/
        }
    }
});