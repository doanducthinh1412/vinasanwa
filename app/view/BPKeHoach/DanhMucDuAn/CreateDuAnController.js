Ext.define('CMS.view.BPKeHoach.CreateDuAn.CreateDuAnController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.CreateDuAn',
    createProject: function() {
        var panel = this.view;
        var win = panel.up('window');
        var frm = panel.getForm();
        if (frm.isValid()) {
            win.hide();
            frm.submit({
                url: '/api/projects/create',
                clientValidation: true,
                method: 'POST',
                success: function(form, action) {
                    if (action.result.success) {
                        Ext.MessageBox.alert('Thông báo', 'Tạo mới dự án thành công', function() {
                            win.close();
                            if (panel.getGridPanel()) {
                                panel.getGridPanel().getStore().load({
                                    callback: function() {}
                                });
                            }
                            if (panel.getStoreParent()) {
                                panel.getStoreParent().load();
                            }
                            if (panel.getRelationComponent()) {
                                if (panel.getRelationComponent().xtype === "combobox") {
                                    if (action.result.data) {
                                        panel.getRelationComponent().setValue(action.result.data.id);
                                    }
                                }
                            }
                        });
                    } else {
                        Ext.Msg.alert('Thông báo', 'Tạo mới dự án không thành công', function() {
                            win.show();
                        });
                    }
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Thông báo', 'Tạo mới dự án không thành công', function() {
                        win.show();
                    });
                },
            })
        }
    }
});