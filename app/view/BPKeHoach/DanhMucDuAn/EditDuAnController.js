Ext.define('CMS.view.BPKeHoach.EditDuAn.EditDuAnController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.EditDuAn',
    editProject: function() {
        var panel = this.view;
        var win = panel.up('window');
        var frm = panel.getForm();
        if (frm.isValid()) {
            win.hide();
            frm.submit({
                url: '/api/projects/edit/' + panel.getRec().get('id'),
                clientValidation: true,
                method: 'POST',
                success: function(form, action) {
                    Ext.MessageBox.alert('Thông báo', 'Chỉnh sửa thành công', function() {
                        win.close();
                        panel.getGridPanel().getStore().load({
                            callback: function() {}
                        });
                    });
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Chỉnh sửa không thành công', action.result.message, function() {
                        win.show();
                    });
                },
            })
        } else {
            //panel.show
        }
    }
});