Ext.define('CMS.view.BPKeHoach.DanhMucDuAn.DanhMucDuAnController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.DanhMucDuAn',
    onItemContextMenu: function(view, record, item, index, e) {
        var controller = this;
        var gridPanel = controller.view;
        var position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        gridPanel.menu.showAt(position);
    },
    onCreateButtonClick: function() {
        var panel = this.view;
        var windowss = Ext.create('Ext.window.Window', {
            title: 'Tạo mới dự án',
            height: 520,
            width: 550,
            layout: 'border',
            border: false,
            maximizable: true,
            glyph: 'xf055@FontAwesome',
            modal: true,
            items: [{
                xtype: 'CreateDuAn',
                region: 'center',
                autoScroll: true,
                gridPanel: panel
            }]
        }).show();
    },
    onEditButtonClick: function() {
        var bl_items = [];
        panel = this.view;
        var selectedRow = panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        windowss = Ext.create('Ext.window.Window', {
            title: 'Chỉnh sửa thông tin dự án ' + rec.get('name'),
            height: 520,
            width: 550,
            layout: 'border',
            glyph: 'xf044@FontAwesome',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'EditDuAn',
                region: 'center',
                autoScroll: true,
                rec: rec,
                gridPanel: panel
            }]
        }).show();
    },
    onEditButtonClickGrid: function(gridPanel, rowIndex, colIndex) {
        var panel = this.view;
        var rec = gridPanel.getStore().getAt(rowIndex);
        windowss = Ext.create('Ext.window.Window', {
            title: 'Chỉnh sửa thông tin dự án ' + rec.get('name'),
            height: 520,
            width: 550,
            layout: 'border',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'EditDuAn',
                region: 'center',
                autoScroll: true,
                rec: rec,
                gridPanel: panel
            }]
        }).show();
    },
    onDeleteButtonClick: function() {
        var panel = this.view;
        var selectedRow = panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        Ext.MessageBox.confirm('Thông báo', 'Xóa dự án "<b>' + rec.get('name') + '</b>" ?', function(btn) {
            if (btn == "yes") {
                panel.el.mask("Đang xóa...");
                Ext.Ajax.request({
                    url: '/user/projects/delete/' + rec.get('id'),
                    success: function(response) {
                        panel.el.unmask();
                        var restext = response.responseText;
                        if (restext != "") {
                            var res = Ext.JSON.decode(restext);
                            if (res.success == false) {
                                Ext.MessageBox.alert('Thông báo', res.message, function() {});
                            } else {
                                panel.getStore().load({
                                    callback: function() {

                                    }
                                });
                            }
                        }
                    },
                    failure: function(response) {
                        panel.el.unmask();
                    }
                });
            }
        });
    },
    onDeleteButtonClickGrid: function(gridPanel, rowIndex, colIndex) {
        var panel = this.view;
        var rec = gridPanel.getStore().getAt(rowIndex);
        Ext.MessageBox.confirm('Thông báo', 'Xóa dự án "<b>' + rec.get('name') + '</b>" ?', function(btn) {
            if (btn == "yes") {
                panel.el.mask("Đang xóa...");
                Ext.Ajax.request({
                    url: '/api/projects/delete/' + rec.get('id'),
                    success: function(response) {
                        panel.el.unmask();
                        var restext = response.responseText;
                        if (restext != "") {
                            var res = Ext.JSON.decode(restext);
                            if (res.success == false) {
                                Ext.MessageBox.alert('Thông báo', res.message, function() {});
                            } else {
                                panel.getStore().load({
                                    callback: function() {}
                                });
                            }
                        }
                    },
                    failure: function(response) {
                        panel.el.unmask();
                    }
                });
            }
        });
    },
    onReloadButtonClick: function() {
        var panel = this.view;
        panel.el.mask('Đang tải...');
        panel.getStore().reload();
        panel.el.unmask();
    }
});