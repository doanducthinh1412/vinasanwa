/* eslint-disable no-console */
/* eslint-disable no-empty */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable eol-last */
/* eslint-disable max-len */
/* eslint-disable no-template-curly-in-string */
/* eslint-disable no-param-reassign */
/* eslint-disable radix */
/* eslint-disable no-plusplus */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
Ext.define('CMS.view.BPKeHoach.QuanLyKeHoachSanXuat.QuanLyKeHoachSanXuatController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.QuanLyKeHoachSanXuat',

    getSaveClass(v, meta, rec) {
        const controller = this;
        const data_change = rec.getChanges();
        if (Object.keys(data_change).length > 0) {
            if (rec.get('note') !== '') {
                return 'fas fa-save text-blue';
            }
        }
        if (rec.get('status') === 0) {
            if (rec.get('note') !== '') {
                return 'fas fa-trash-alt text-blue';
            }
        }
        if (rec.get('status') === 1) {
            return 'fas fa-pause-circle text-blue';
        }
        if (rec.get('status') === 2) {
            return 'far fa-stop-circle text-red';
        }
        if (rec.get('status') === 3) {
            return 'far fa-check-circle text-green';
        }
        if (rec.get('status') === 4) {
            return 'fas fa-play text-green';
        }
        if (rec.get('status') === 5) {
            return 'fas fa-ban text-red';
        }
        return false;
    },

    getSaveTip(v, meta, rec) {
        const data_change = rec.getChanges();
        if (Object.keys(data_change).length > 0) {
            if (rec.get('note') !== '') {
                return 'Lưu kế hoạch';
            }
        }
        if (rec.get('status') === 0) {
            if (rec.get('note') !== '') {
                return 'Xóa sản xuất';
            }
            return '';
        }
        if (rec.get('status') === 1) {
            return 'Đang sản xuất';
        }
        if (rec.get('status') === 2) {
            return 'Đã dừng sản xuất';
        }
        if (rec.get('status') === 3) {
            return 'Hoàn thành sản xuất';
        }
        if (rec.get('status') === 4) {
            return 'Đã gửi kế hoạch';
        }
        if (rec.get('status') === 5) {
            return 'Đã hủy sản xuất';
        }
        return false;
    },

    onLoadGridKeHoachSanXuat(store, records, successful, operation, eOpts) {
        const controller = this;
        const refs = controller.getReferences();
        const { QuanLyKeHoachSanXuatGrid } = refs;
        const QuanLyKeHoachSanXuatPanel = controller.getView();
        const showBtnSanXuat = controller.getViewModel().get('showBtnSanXuat');
        const data = [];
        const data_cur_am = [];
        const data_cur_pm = [];
        const data_other = [];
        const data_kehoach = {};
        const cur_date = new Date();
        let maxDateKeHoach = new Date();
        if (records) {
            for (let j = 0; j < records.length; j += 1) {
                const date_record = Ext.Date.parse(records[j].data.time, 'Y-m-d');
                if (date_record - maxDateKeHoach > 0) {
                    maxDateKeHoach = date_record;
                }
                if (Ext.Date.diff(cur_date, date_record, Ext.Date.DAY) >= -1) {
                    if (!data_kehoach[records[j].data.time]) {
                        data_kehoach[records[j].data.time] = {
                            am: [],
                            pm: [],
                        };
                    }
                    if (records[j].data.time_type === 'am') {
                        data_kehoach[records[j].data.time].am.push(records[j].data);
                    }
                    if (records[j].data.time_type === 'pm') {
                        data_kehoach[records[j].data.time].pm.push(records[j].data);
                    }
                }
            }
            maxDateKeHoach.setDate(maxDateKeHoach.getDate() + 1);
            const arr_tmp = [];
            for (let day = cur_date; day <= maxDateKeHoach; day.setDate(day.getDate() + 1)) {
                const day_s = Ext.Date.format(day, 'Y-m-d');
                if (!data_kehoach[day_s]) {
                    data_kehoach[day_s] = {
                        am: [],
                        pm: [],
                    };
                }
                if (showBtnSanXuat) {
                    let len_am = 5 - data_kehoach[day_s].am.length;
                    if (len_am <= 0) {
                        len_am = 1;
                    }
                    for (let k = 0; k < len_am; k += 1) {
                        arr_tmp.push({
                            note: '',
                            project_name: '',
                            order_no: '',
                            sub_no: '',
                            frame_wd: null,
                            frame_susd: null,
                            leaf_wd: null,
                            leaf_susd: null,
                            frame_sd: null,
                            frame_lsd: null,
                            leaf_sd: null,
                            leaf_lsd: null,
                            ssjp: null,
                            ssaichi: null,
                            ssgrill: null,
                            srn: null,
                            grs: null,
                            s13: null,
                            s14: null,
                            frame_sp: null,
                            leaf_sp: null,
                            frame_sld: null,
                            leaf_sld: null,
                            frame_flsd: null,
                            leaf_flsd: null,
                            frame_fsd: null,
                            leaf_fsd: null,
                            sw: null,
                            lv: null,
                            other: null,
                            other_note: '',
                            date_drawing: '',
                            date_delivery: '',
                            drawing_note: null,
                            delivery_note: null,
                            shearing_note: null,
                            tpp_note: null,
                            bending_note: null,
                            grew_press_asembly_note: null,
                            prime_paint_note: null,
                            finish_paint_note: null,
                            product_check_note: null,
                            shearing: '',
                            shearing_result: '',
                            tpp: '',
                            tpp_result: '',
                            bending: '',
                            bending_result: '',
                            prepare_frame: '',
                            inner_frame: '',
                            grew_press_asembly: '',
                            grew_press_asembly_result: '',
                            prime_paint: '',
                            prime_paint_result: '',
                            finish_paint: '',
                            finish_paint_result: '',
                            product_check: '',
                            check_result: '',
                            color: '',
                            ghichu: '',
                            incharge: '',
                            time_type: 'am',
                            time: day_s,
                            status: 0,
                            type: '',
                            num_done: null,
                            num_other_done: null,
                            number: null,
                            other_num: null,
                            other_remain: null,
                            remain: null,
                            id_note: null,
                        });
                    }
                    let len_pm = 5 - data_kehoach[day_s].pm.length;
                    if (len_pm <= 0) {
                        len_pm = 1;
                    }
                    for (let k = 0; k < len_pm; k++) {
                        arr_tmp.push({
                            note: '',
                            project_name: '',
                            order_no: '',
                            sub_no: '',
                            frame_wd: null,
                            frame_susd: null,
                            leaf_wd: null,
                            leaf_susd: null,
                            frame_sd: null,
                            frame_lsd: null,
                            leaf_sd: null,
                            leaf_lsd: null,
                            ssjp: null,
                            ssaichi: null,
                            ssgrill: null,
                            srn: null,
                            grs: null,
                            s13: null,
                            s14: null,
                            frame_sp: null,
                            leaf_sp: null,
                            frame_sld: null,
                            leaf_sld: null,
                            frame_flsd: null,
                            leaf_flsd: null,
                            frame_fsd: null,
                            leaf_fsd: null,
                            sw: null,
                            lv: null,
                            other: null,
                            other_note: '',
                            date_drawing: '',
                            drawing_note: null,
                            delivery_note: null,
                            shearing_note: null,
                            tpp_note: null,
                            bending_note: null,
                            grew_press_asembly_note: null,
                            prime_paint_note: null,
                            finish_paint_note: null,
                            product_check_note: null,
                            date_delivery: '',
                            shearing: '',
                            shearing_result: '',
                            tpp: '',
                            tpp_result: '',
                            bending: '',
                            bending_result: '',
                            prepare_frame: '',
                            inner_frame: '',
                            grew_press_asembly: '',
                            grew_press_asembly_result: '',
                            prime_paint: '',
                            prime_paint_result: '',
                            finish_paint: '',
                            finish_paint_result: '',
                            product_check: '',
                            check_result: '',
                            color: '',
                            ghichu: '',
                            incharge: '',
                            time_type: 'pm',
                            time: day_s,
                            status: 0,
                            type: '',
                            num_done: null,
                            num_other_done: null,
                            number: null,
                            other_num: null,
                            other_remain: null,
                            remain: null,
                            id_note: null,
                        });
                    }
                }
            }
            store.add(arr_tmp);
            // store.loadData(arr_tmp, false);
        }
        QuanLyKeHoachSanXuatPanel.setMaxDateKeHoach(maxDateKeHoach);
    },

    onSaveClick(grid, rowIndex, colIndex, item, e, rec) {
        e.stopEvent();
        const controller = this;
        const data_change = rec.getChanges();
        const refs = controller.getReferences();
        const { gridDanhSachBanVeChuaLap } = refs;
        const { QuanLyKeHoachSanXuatGrid } = refs;
        if (rec.get('status') === 0) {
            if (Object.keys(data_change).length > 0) {
                // Goi API save ke hoach
                rec.commit();
            } else if (rec.get('note') !== '') {
                Ext.MessageBox.confirm('Thông báo', `Bạn có chắc chắn muốn xóa kế hoạch Note "<b>${rec.get('note')}"?`, (btn) => {
                    if (btn === 'yes') {
                        // Goi API xoa ke hoach
                        Ext.Ajax.request({
                            url: `/api/production-schedule/delete/${rec.data.id}`,
                            method: 'POST',
                            timeout: 30000,
                            success(result, request) {
                                try {
                                    const response = Ext.decode(result.responseText);
                                    if (response.success) {
                                        controller.clearRowRecord(rec);
                                        rec.commit();
                                        gridDanhSachBanVeChuaLap.getStore().load();
                                    } else {
                                        Ext.MessageBox.alert('Thông báo', `Xóa bản vẽ <b>Note: ${rec.data.note}</b> không thành công`, () => {
                                            rec.reject();
                                        });
                                    }
                                } catch (err) {
                                    console.log(err);
                                    Ext.MessageBox.alert('Thông báo', `Xóa bản vẽ <b>Note: ${rec.data.note}</b> không thành công`, () => {
                                        rec.reject();
                                    });
                                }
                            },
                            failure(result, request) {
                                Ext.MessageBox.alert('Thông báo', `Xóa bản vẽ <b>Note: ${rec.data.note}</b> không thành công`, () => {
                                    rec.reject();
                                });
                            },
                        });
                    }
                });
            }
        } else if (rec.get('status') === 1 || rec.get('status') === 4) {
            const win = Ext.create('Ext.window.Window', {
                title: 'Bạn có muốn dừng hoặc hủy kế hoạch sản xuất?',
                iconCls: 'far fa-calendar-alt',
                width: 500,
                layout: 'fit',
                modal: true,
                viewModel: true,
                items: [{
                    xtype: 'form',
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                    },
                    border: false,
                    bodyPadding: 10,
                    fieldDefaults: {
                        msgTarget: 'side',
                        labelStyle: 'font-weight:bold',
                    },
                    buttons: ['->', {
                        text: 'Dừng sản xuất',
                        iconCls: 'fas fa-pause',
                        ui: 'soft-orange',
                        hidden: true,
                        bind: {
                            hidden: '{!actionRadioDung.checked}',
                        },
                        handler() {
                            win.hide();
                            const formPanel = this.up('window').down('form');
                            const form = formPanel.getForm();
                            const f_dung_num = formPanel.getComponent('dung_num');
                            const f_dung_reason = formPanel.getComponent('dung_reason');
                            const obj_post = {};
                            if (f_dung_num.getValue() && f_dung_reason.getValue()) {
                                obj_post[f_dung_num.getName()] = f_dung_num.getValue();
                                obj_post[f_dung_reason.getName()] = f_dung_reason.getValue();
                                Ext.Ajax.request({
                                    url: `/api/production-schedule/cancel/${rec.data.id}`,
                                    method: 'POST',
                                    timeout: 30000,
                                    params: obj_post,
                                    success(result, request) {
                                        try {
                                            const response = Ext.decode(result.responseText);
                                            if (response.success) {
                                                controller.loadDataGrid();
                                            } else {
                                                Ext.MessageBox.alert('Thông báo', 'Dừng kế hoạch sản xuất không thành công', () => {});
                                            }
                                            win.close();
                                        } catch (err) {
                                            Ext.MessageBox.alert('Thông báo', 'Dừng kế hoạch sản xuất không thành công', () => {
                                                win.close();
                                            });
                                        }
                                    },
                                    failure(result, request) {
                                        Ext.MessageBox.alert('Thông báo', 'Dừng kế hoạch sản xuất không thành công', () => {
                                            win.close();
                                        });
                                    },
                                });
                            }
                        },
                    }, {
                        text: 'Hủy sản xuất',
                        iconCls: 'fas fa-pause',
                        ui: 'soft-orange',
                        hidden: true,
                        bind: {
                            hidden: '{!actionRadioHuy.checked}',
                        },
                        handler() {
                            win.hide();
                            const formPanel = this.up('window').down('form');
                            const form = formPanel.getForm();
                            const f_huy_num = formPanel.getComponent('huy_num');
                            const f_huy_reason = formPanel.getComponent('huy_reason');
                            const obj_post = {};
                            if (f_huy_num.getValue() && f_huy_reason.getValue()) {
                                obj_post[f_huy_num.getName()] = f_huy_num.getValue();
                                obj_post[f_huy_reason.getName()] = f_huy_reason.getValue();
                                Ext.Ajax.request({
                                    url: `/api/production-schedule/stop/${rec.data.id}`,
                                    method: 'POST',
                                    timeout: 30000,
                                    params: obj_post,
                                    success(result, request) {
                                        try {
                                            const response = Ext.decode(result.responseText);
                                            if (response.success) {
                                                controller.loadDataGrid();
                                            } else {
                                                Ext.MessageBox.alert('Thông báo', 'Dừng kế hoạch sản xuất không thành công', () => {});
                                            }
                                            win.close();
                                        } catch (err) {
                                            Ext.MessageBox.alert('Thông báo', 'Dừng kế hoạch sản xuất không thành công', () => {
                                                win.close();
                                            });
                                        }
                                    },
                                    failure(result, request) {
                                        Ext.MessageBox.alert('Thông báo', 'Dừng kế hoạch sản xuất không thành công', () => {
                                            win.close();
                                        });
                                    },
                                });
                            }
                        },
                    }, {
                        text: 'Thoát',
                        glyph: 'xf00d@FontAwesome',
                        handler() {
                            this.up('window').close();
                        },
                    }, '->'],
                    defaultType: 'textfield',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Số lượng sản xuất',
                        defaultType: 'textfield',
                        defaults: {
                            anchor: '100%',
                        },
                        items: [{
                                readOnly: true,
                                fieldLabel: 'Loại cửa',
                                submitValue: false,
                                value: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? 'Other' : rec.get('type'),
                            },
                            {
                                readOnly: true,
                                fieldLabel: 'Số lượng lập KHSX',
                                submitValue: false,
                                value: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? rec.get('other_num') : rec.get('type_num'),
                            }, {
                                readOnly: true,
                                fieldLabel: 'Số lượng trên bản vẽ',
                                submitValue: false,
                                value: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? rec.get('other') : rec.get('total'),
                            },
                        ],
                    }, {
                        xtype: 'radiogroup',
                        fieldLabel: 'Thực hiện',
                        name: 'action',
                        columns: 2,
                        vertical: true,
                        reference: 'actionRadio',
                        items: [{
                                boxLabel: 'Dừng sản xuất',
                                inputValue: 1,
                                checked: true,
                                reference: 'actionRadioDung',
                            },
                            { boxLabel: 'Hủy sản xuất', inputValue: 2, reference: 'actionRadioHuy' },
                        ],
                    }, {
                        labelAlign: 'top',
                        fieldLabel: 'Số lượng dừng sản xuất',
                        minValue: 1,
                        bind: {
                            hidden: '{!actionRadioDung.checked}',
                        },
                        maxValue: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? rec.get('other_num') : rec.get('type_num'),
                        xtype: 'numberfield',
                        itemId: 'dung_num',
                        name: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? 'cancel_other_num' : 'cancel_num',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>',
                        ],
                        allowBlank: false,
                    }, {
                        labelAlign: 'top',
                        fieldLabel: 'Lý do dừng sản xuất',
                        xtype: 'textareafield',
                        itemId: 'dung_reason',
                        bind: {
                            hidden: '{!actionRadioDung.checked}',
                        },
                        name: 'reason',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>',
                        ],
                        allowBlank: false,
                    }, {
                        labelAlign: 'top',
                        fieldLabel: 'Số lượng hủy sản xuất',
                        minValue: 1,
                        hidden: true,
                        itemId: 'huy_num',
                        bind: {
                            hidden: '{!actionRadioHuy.checked}',
                        },
                        maxValue: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? rec.get('other_num') : rec.get('type_num'),
                        xtype: 'numberfield',
                        name: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? 'stop_other_num' : 'stop_num',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>',
                        ],
                        allowBlank: false,
                    }, {
                        labelAlign: 'top',
                        fieldLabel: 'Lý do hủy sản xuất',
                        xtype: 'textareafield',
                        hidden: true,
                        itemId: 'huy_reason',
                        bind: {
                            hidden: '{!actionRadioHuy.checked}',
                        },
                        name: 'reason',
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>',
                        ],
                        allowBlank: false,
                    }],
                }],
            }).show();
        } else if (rec.get('status') === 2) {
            const win = Ext.create('Ext.window.Window', {
                title: 'Thông báo',
                iconCls: 'far fa-calendar-alt',
                width: 450,
                layout: 'fit',
                modal: true,
                viewModel: true,
                buttons: ['->', {
                    text: 'Đóng',
                    glyph: 'xf00d@FontAwesome',
                    handler() {
                        this.up('window').close();
                    },
                }, '->'],
                items: [{
                    xtype: 'form',
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                    },
                    border: false,
                    bodyPadding: 10,
                    fieldDefaults: {
                        msgTarget: 'side',
                        labelStyle: 'font-weight:bold',
                    },
                    defaultType: 'textfield',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Số lượng sản xuất',
                        defaultType: 'textfield',
                        defaults: {
                            anchor: '100%',
                        },
                        items: [{
                            readOnly: true,
                            fieldLabel: 'Loại cửa',
                            submitValue: false,
                            value: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? 'Other' : rec.get('type'),
                        }, {
                            readOnly: true,
                            fieldLabel: 'Số lượng lập KHSX',
                            submitValue: false,
                            value: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? rec.get('other_num') : rec.get('type_num'),
                        }, {
                            readOnly: true,
                            fieldLabel: 'Số lượng trên bản vẽ',
                            submitValue: false,
                            value: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? rec.get('other') : rec.get('total'),
                        }],
                    }, {
                        labelAlign: 'top',
                        fieldLabel: 'Số lượng dừng sản xuất',
                        readOnly: true,
                        value: (rec.get('cancel_num') === null || rec.get('cancel_num') === '' || parseInt(rec.get('cancel_num')) === 0) ? rec.get('cancel_other_num') : rec.get('cancel_num'),
                    }, {
                        labelAlign: 'top',
                        fieldLabel: 'Lý do dừng sản xuất',
                        xtype: 'textareafield',
                        value: rec.get('cancel_reason'),
                        readOnly: true,
                    }],
                }],
            }).show();
        } else if (parseInt(rec.get('status')) === 5) {
            const win = Ext.create('Ext.window.Window', {
                title: 'Thông báo',
                iconCls: 'far fa-calendar-alt',
                width: 450,
                layout: 'fit',
                modal: true,
                viewModel: true,
                buttons: ['->', {
                    text: 'Đóng',
                    glyph: 'xf00d@FontAwesome',
                    handler() {
                        this.up('window').close();
                    },
                }, '->'],
                items: [{
                    xtype: 'form',
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                    },
                    border: false,
                    bodyPadding: 10,
                    fieldDefaults: {
                        msgTarget: 'side',
                        labelStyle: 'font-weight:bold',
                    },
                    defaultType: 'textfield',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Số lượng sản xuất',
                        defaultType: 'textfield',
                        defaults: {
                            anchor: '100%',
                        },
                        items: [{
                            readOnly: true,
                            fieldLabel: 'Loại cửa',
                            submitValue: false,
                            value: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? 'Other' : rec.get('type'),
                        }, {
                            readOnly: true,
                            fieldLabel: 'Số lượng lập KHSX',
                            submitValue: false,
                            value: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? rec.get('other_num') : rec.get('type_num'),
                        }, {
                            readOnly: true,
                            fieldLabel: 'Số lượng trên bản vẽ',
                            submitValue: false,
                            value: (parseInt(rec.get('type_num')) === 0 || rec.get('type_num') === '') ? rec.get('other') : rec.get('total'),
                        }],
                    }, {
                        labelAlign: 'top',
                        fieldLabel: 'Số lượng hủy sản xuất',
                        readOnly: true,
                        value: (rec.get('cancel_num') === null || rec.get('stop_num') === '' || parseInt(rec.get('stop_num')) === 0) ? rec.get('stop_other_num') : rec.get('stop_num'),
                    }, {
                        labelAlign: 'top',
                        fieldLabel: 'Lý do hủy sản xuất',
                        xtype: 'textareafield',
                        value: rec.get('cancel_reason'),
                        readOnly: true,
                    }],
                }],
            }).show();
        }
    },

    onClickAddDayButton(btn) {
        const controller = this;
        const refs = controller.getReferences();
        const QuanLyKeHoachSanXuatPanel = controller.getView();
        const { QuanLyKeHoachSanXuatGrid } = refs;
        let date_kehoach = new Date();
        if (QuanLyKeHoachSanXuatPanel.getMaxDateKeHoach()) {
            date_kehoach = QuanLyKeHoachSanXuatPanel.getMaxDateKeHoach();
        }
        date_kehoach.setDate(date_kehoach.getDate() + 1);
        const data_add = [];
        for (let k = 0; k < 5; k += 1) {
            data_add.push({
                note: '',
                project_name: '',
                order_no: '',
                sub_no: '',
                frame_wd: null,
                frame_susd: null,
                leaf_wd: null,
                leaf_susd: null,
                frame_sd: null,
                frame_lsd: null,
                leaf_sd: null,
                leaf_lsd: null,
                ssjp: null,
                ssaichi: null,
                ssgrill: null,
                srn: null,
                grs: null,
                s13: null,
                s14: null,
                frame_sp: null,
                leaf_sp: null,
                frame_sld: null,
                leaf_sld: null,
                frame_flsd: null,
                leaf_flsd: null,
                frame_fsd: null,
                leaf_fsd: null,
                sw: null,
                lv: null,
                other: null,
                other_note: '',
                date_drawing: '',
                drawing_note: null,
                delivery_note: null,
                shearing_note: null,
                tpp_note: null,
                bending_note: null,
                grew_press_asembly_note: null,
                prime_paint_note: null,
                finish_paint_note: null,
                product_check_note: null,
                date_delivery: '',
                shearing: '',
                shearing_result: '',
                tpp: '',
                tpp_result: '',
                bending: '',
                bending_result: '',
                prepare_frame: '',
                inner_frame: '',
                grew_press_asembly: '',
                grew_press_asembly_result: '',
                prime_paint: '',
                prime_paint_result: '',
                finish_paint: '',
                finish_paint_result: '',
                product_check: '',
                check_result: '',
                color: '',
                ghichu: '',
                incharge: '',
                time_type: 'am',
                time: Ext.Date.format(date_kehoach, 'Y-m-d'),
                status: 0,
                type: '',
                num_done: null,
                num_other_done: null,
                number: null,
                other_num: null,
                other_remain: null,
                remain: null,
                id_note: null,
            });
            data_add.push({
                note: '',
                project_name: '',
                order_no: '',
                sub_no: '',
                frame_wd: null,
                frame_susd: null,
                leaf_wd: null,
                leaf_susd: null,
                frame_sd: null,
                frame_lsd: null,
                leaf_sd: null,
                leaf_lsd: null,
                ssjp: null,
                ssaichi: null,
                ssgrill: null,
                srn: null,
                grs: null,
                s13: null,
                s14: null,
                frame_sp: null,
                leaf_sp: null,
                frame_sld: null,
                leaf_sld: null,
                frame_flsd: null,
                leaf_flsd: null,
                frame_fsd: null,
                leaf_fsd: null,
                sw: null,
                lv: null,
                other: null,
                other_note: '',
                date_drawing: '',
                drawing_note: null,
                delivery_note: null,
                shearing_note: null,
                tpp_note: null,
                bending_note: null,
                grew_press_asembly_note: null,
                prime_paint_note: null,
                finish_paint_note: null,
                product_check_note: null,
                date_delivery: '',
                shearing: '',
                shearing_result: '',
                tpp: '',
                tpp_result: '',
                bending: '',
                bending_result: '',
                prepare_frame: '',
                inner_frame: '',
                grew_press_asembly: '',
                grew_press_asembly_result: '',
                prime_paint: '',
                prime_paint_result: '',
                finish_paint: '',
                finish_paint_result: '',
                product_check: '',
                check_result: '',
                color: '',
                ghichu: '',
                incharge: '',
                time_type: 'pm',
                time: Ext.Date.format(date_kehoach, 'Y-m-d'),
                status: 0,
                type: '',
                num_done: null,
                num_other_done: null,
                number: null,
                other_num: null,
                other_remain: null,
                remain: null,
                id_note: null,
            });
        }
        QuanLyKeHoachSanXuatGrid.getStore().add(data_add);
        QuanLyKeHoachSanXuatPanel.setMaxDateKeHoach(date_kehoach);
    },

    renderDrawingColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return rec.get('drawing_note');
        }
        return Ext.Date.format(value, 'j-M');
    },

    renderDeliveryColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return rec.get('delivery_note');
        }
        return Ext.Date.format(value, 'j-M');
    },

    renderShearingColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return rec.get('shearing_note');
        }
        if (rec.get('shearing_note') !== '' && rec.get('shearing_note') !== null && rec.get('shearing_note') !== undefined) {
            return `${Ext.Date.format(value, 'j-M')}<i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i>`;
        }
        return Ext.Date.format(value, 'j-M');
    },

    renderTPPColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return rec.get('tpp_note');
        }
        if (rec.get('tpp_note') !== '' && rec.get('tpp_note') !== null && rec.get('tpp_note') !== undefined) {
            return `${Ext.Date.format(value, 'j-M')}<i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i>`;
        }
        return Ext.Date.format(value, 'j-M');
    },

    renderBendingColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return rec.get('bending_note');
        }
        if (rec.get('bending_note') !== '' && rec.get('bending_note') !== null && rec.get('bending_note') !== undefined) {
            return `${Ext.Date.format(value, 'j-M')}<i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i>`;
        }
        return Ext.Date.format(value, 'j-M');
    },

    renderGrewPressColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return rec.get('grew_press_asembly_note');
        }
        if (rec.get('grew_press_asembly_note') !== '' && rec.get('grew_press_asembly_note') !== null && rec.get('grew_press_asembly_note') !== undefined) {
            return `${Ext.Date.format(value, 'j-M')}<i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i>`;
        }
        return Ext.Date.format(value, 'j-M');
    },

    renderPrimePaintColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return rec.get('prime_paint_note');
        }
        if (rec.get('prime_paint_note') !== '' && rec.get('prime_paint_note') !== null && rec.get('prime_paint_note') !== undefined) {
            return `${Ext.Date.format(value, 'j-M')}<i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i>`;
        }
        return Ext.Date.format(value, 'j-M');
    },

    renderFinishPaintColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return rec.get('finish_paint_note');
        }
        if (rec.get('finish_paint_note') !== '' && rec.get('finish_paint_note') !== null && rec.get('finish_paint_note') !== undefined) {
            return `${Ext.Date.format(value, 'j-M')}<i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i>`;
        }
        return Ext.Date.format(value, 'j-M');
    },

    renderCheckColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return rec.get('product_check_note');
        }
        if (rec.get('product_check_note') !== '' && rec.get('product_check_note') !== null && rec.get('product_check_note') !== undefined) {
            return `${Ext.Date.format(value, 'j-M')}<i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i>`;
        }
        return Ext.Date.format(value, 'j-M');
    },

    renderResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        // meta.style = "background-color:#9cc96b;";
        if (value === null || value === '') {
            return '';
        }
        return value;
    },

    renderCheckResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        // meta.style = "background-color:#9cc96b;";
        if (value === null || value === '') {
            return rec.get('product_check_note');
        }
        return value;
    },

    renderFinishPaintResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        // meta.style = "background-color:#9cc96b;";
        if (value === null || value === '') {
            return rec.get('finish_paint_note');
        }
        return value;
    },

    renderPrimePaintResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        // meta.style = "background-color:#9cc96b;";
        if (value === null || value === '') {
            return rec.get('prime_paint_note');
        }
        return value;
    },

    renderGrewPressResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        // meta.style = "background-color:#9cc96b;";
        if (value === null || value === '') {
            return rec.get('grew_press_asembly_note');
        }
        return value;
    },

    renderInnerFrameResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        // meta.style = "background-color:#9cc96b;";
        if (value === null || value === '') {
            return rec.get('bending_note');
        }
        return value;
    },

    renderPrepareFrameResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        // meta.style = "background-color:#9cc96b;";
        if (value === null || value === '') {
            return rec.get('bending_note');
        }
        return value;
    },

    renderBendingResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        // meta.style = "background-color:#9cc96b;";
        if (value === null || value === '') {
            return rec.get('bending_note');
        }
        return value;
    },

    renderTPPResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        // meta.style = "background-color:#9cc96b;";
        if (value === null || value === '') {
            return rec.get('tpp_note');
        }
        return value;
    },

    renderShearingResultColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === null || value === '') {
            return rec.get('shearing_note');
        }
        return value;
    },

    renderNumberColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (parseInt(value) === 0 || value === null) {
            if (parseInt(rec.get('status')) === 2 || parseInt(rec.get('status')) === 5) {
                const field = view.getHeaderCt().getHeaderAtIndex(meta.cellIndex).dataIndex;
                const fieldRecord = rec.get('type');
                if (field === fieldRecord) {
                    if (parseInt(rec.get('status')) === 2) {
                        meta.style = 'background-color:#ec4040;';
                        if (rec.get('type_note') !== '' && rec.get('type_note') !== null && rec.get('type_note') !== undefined) {
                            return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('cancel_num')}</b></font>  <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
                        }
                        // return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('cancel_num')}</b></font></font>`;
                        return `<font color="#FFFFFF">${value}</font>`;
                    }
                    if (parseInt(rec.get('status')) === 5) {
                        meta.style = 'background-color:#ec4040;';
                        if (rec.get('type_note') !== '' && rec.get('type_note') !== null && rec.get('type_note') !== undefined) {
                            return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('stop_num')}</b></font>  <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
                        }
                        // return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('stop_num')}</b></font></font>`;
                        return `<font color="#FFFFFF">${value}</font>`;
                    }
                }
                return '';
            }
            return '';
        }
        if (parseInt(rec.get('status')) === 2) {
            meta.style = 'background-color:#ec4040;';
            if (rec.get('type_note') !== '' && rec.get('type_note') !== null && rec.get('type_note') !== undefined) {
                return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('cancel_num')}</b></font>  <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
            }
            // return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('cancel_num')}</b></font></font>`;
            return `<font color="#FFFFFF">${value}</font>`;
        }
        if (parseInt(rec.get('status')) === 5) {
            meta.style = 'background-color:#ec4040;';
            if (rec.get('type_note') !== '' && rec.get('type_note') !== null && rec.get('type_note') !== undefined) {
                return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('stop_num')}</b></font>  <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
            }
            // return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('stop_num')}</b></font></font>`;
            return `<font color="#FFFFFF">${value}</font>`;
        }
        meta.style = 'background-color:#167abc;';
        if (rec.get('type_note') !== '' && rec.get('type_note') !== null && rec.get('type_note') !== undefined) {
            return `<font color="#FFFFFF">${value} <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
        }
        return `<font color="#FFFFFF">${value}</font>`;
    },

    renderNumberColumnOther(value, meta, rec, rowIndex, colIndex, store, view) {
        if (parseInt(value) === 0 || value === null) {
            if (parseInt(rec.get('cancel_other_num') !== null)) {
                if (parseInt(rec.get('status')) === 2) {
                    meta.style = 'background-color:#ec4040;';
                    if (rec.get('other_note') !== '' && rec.get('other_note') !== null && rec.get('other_note') !== undefined) {
                        return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('cancel_other_num')}</b></font> <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
                    }
                    // return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('cancel_other_num')}</b></font></font>`;
                    return `<font color="#FFFFFF">${value}</font>`;
                }
            }
            if (parseInt(rec.get('stop_other_num') !== null)) {
                if (parseInt(rec.get('status')) === 5) {
                    meta.style = 'background-color:#ec4040;';
                    if (rec.get('other_note') !== '' && rec.get('other_note') !== null && rec.get('other_note') !== undefined) {
                        return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('stop_other_num')}</b></font> <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
                    }
                    // return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('stop_other_num')}</b></font></font>`;
                    return `<font color="#FFFFFF">${value}</font>`;
                }
            }
            return '';
        }
        if (parseInt(rec.get('status')) === 2) {
            meta.style = 'background-color:#ec4040;';
            if (rec.get('other_note') !== '' && rec.get('other_note') !== null && rec.get('other_note') !== undefined) {
                return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('cancel_other_num')}</b></font> <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
            }
            // return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('cancel_other_num')}</b></font></font>`;
            return `<font color="#FFFFFF">${value}</font>`;
        }
        if (parseInt(rec.get('status')) === 5) {
            meta.style = 'background-color:#ec4040;';
            if (rec.get('other_note') !== '' && rec.get('other_note') !== null && rec.get('other_note') !== undefined) {
                return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('stop_other_num')}</b></font> <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
            }
            // return `<font color="#FFFFFF">${value} / <font color="yellow"><b>${rec.get('stop_other_num')}</b></font></font>`;
            return `<font color="#FFFFFF">${value}</font>`;
        }
        meta.style = 'background-color:#167abc;';
        if (rec.get('other_note') !== '' && rec.get('other_note') !== null && rec.get('other_note') !== undefined) {
            return `<font color="#FFFFFF">${value} <i style="position: absolute;top: 5px;margin-left: 1px;" class="fas fa-comment-alt text-red"></i></font>`;
        }
        return `<font color="#FFFFFF">${value}</font>`;
    },

    viewThongTinBanVe(record) {
        Ext.create('Ext.window.Window', {
            title: 'Thông tin kế hoạch sản xuất',
            iconCls: 'fas fa-info-circle',
            width: 400,
            layout: 'fit',
            modal: true,
            viewModel: true,
            buttons: ['->', {
                text: 'Thoát',
                glyph: 'xf00d@FontAwesome',
                handler() {
                    this.up('window').close();
                },
            }, '->'],
            items: [{
                xtype: 'form',
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                },
                border: false,
                bodyPadding: 10,
                fieldDefaults: {
                    msgTarget: 'side',
                    labelStyle: 'font-weight:bold',
                    labelAlign: 'top',
                },
                defaultType: 'textfield',
                items: [{
                    xtype: 'fieldset',
                    title: 'Số lượng sản xuất',
                    defaultType: 'textfield',
                    defaults: {
                        anchor: '100%',
                    },
                    items: [{
                            readOnly: true,
                            fieldLabel: 'Loại cửa',
                            value: record.data.type,
                        },
                        {
                            readOnly: true,
                            fieldLabel: 'Số lượng trên bản vẽ',
                            value: record.data.total,
                        },
                        {
                            readOnly: true,
                            fieldLabel: 'Số lượng lập kế hoạch',
                            value: record.data.type_num,
                        }, {
                            readOnly: true,
                            fieldLabel: 'Số lượng dừng sản xuất',
                            value: record.data.cancel_num,
                        }, {
                            readOnly: true,
                            fieldLabel: 'Số lượng chưa lập kế hoạch',
                            value: record.data.total - (record.data.type_num - record.data.cancel_num),
                        },
                    ],
                }],
            }],
        }).show();
    },

    onViewGridReady(grid) {
        const { view } = grid;
        grid.tip = Ext.create('Ext.tip.ToolTip', {
            target: view.el,
            delegate: '.x-grid-cell',
            trackMouse: false,
            anchor: 'right',
            constrainPosition: false,
            showDelay: 0,
            hideDelay: 0,
            autoHide: false,
            listeners: {
                beforeshow: function updateTipBody(tip) {
                    const tipGridView = tip.target.component;
                    const rec = tipGridView.getRecord(tip.triggerElement);
                    const colname = tipGridView.getHeaderCt().getHeaderAtIndex(tip.triggerElement.cellIndex).dataIndex;
                    const arr_info = [];
                    if (rec) {
                        if (rec.get('type').toLowerCase() === colname) {
                            if (rec.get('note') !== '') {
                                if (rec.get('type_num') !== '0' && rec.get('type_num') !== 0 && rec.get('type_num') !== null && rec.get('type_num') !== undefined) {
                                    if (rec.get('type_note') !== null && rec.get('type_note') !== '') {
                                        arr_info.push(`+ Ghi chú: ${rec.get('type_note')}`);
                                    }
                                    arr_info.push(`+ Loại cửa: ${rec.get('type').toUpperCase()}`);
                                    arr_info.push(`+ Số lượng trên bản vẽ: ${rec.get('total')}`);
                                    arr_info.push(`+ Số lượng lập kế hoạch: ${rec.get('type_num')}`);
                                    if (rec.get('status') === 0) {
                                        arr_info.push('+ Trạng thái: <font color="aqua">Chưa gửi kế hoạch sản xuất</font>');
                                    } else if (rec.get('status') === 1) {
                                        arr_info.push('+ Trạng thái: <font color="chartreuse">Đang sản xuất</font>');
                                    } else if (rec.get('status') === 2) {
                                        const num_dungsx = (rec.get('cancel_num') === null || rec.get('cancel_num') === '' || parseInt(rec.get('cancel_num')) === 0) ? rec.get('cancel_other_num') : rec.get('cancel_num');
                                        arr_info.push('+ Trạng thái: <font color="yellow">Dừng sản xuất</font>');
                                        arr_info.push(`+ Số lượng dừng sản xuất: <font color="yellow">${num_dungsx} / ${rec.get('type_num')}</font>`);
                                        arr_info.push(`+ Lý do dừng: <font color="yellow">${rec.get('cancel_reason')}</font>`);
                                    } else if (rec.get('status') === 3) {
                                        arr_info.push('+ Trạng thái: <font color="chartreuse">Hoàn thành sản xuất</font>');
                                    } else if (rec.get('status') === 4) {
                                        arr_info.push('+ Trạng thái: <font color="chartreuse">Kế hoạch đã được gửi</font>');
                                    } else if (rec.get('status') === 5) {
                                        const num_huysx = (rec.get('stop_num') === null || rec.get('stop_num') === '' || parseInt(rec.get('stop_num')) === 0) ? rec.get('stop_other_num') : rec.get('stop_num');
                                        arr_info.push('+ Trạng thái: <font color="yellow">Hủy sản xuất</font>');
                                        arr_info.push(`+ Số lượng hủy sản xuất: <font color="yellow">${num_huysx} / ${rec.get('type_num')}</font>`);
                                        arr_info.push(`+ Lý do hủy: <font color="yellow">${rec.get('cancel_reason')}</font>`);
                                    }
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        } else if (colname === 'other_num') {
                            if (rec.get('note') !== '') {
                                if (rec.get('other_num') !== '0' && rec.get('other_num') !== 0 && rec.get('other_num') !== null && rec.get('other_num') !== undefined) {
                                    if (rec.get('other_note') !== null && rec.get('other_note') !== '') {
                                        arr_info.push(`+ Ghi chú: ${rec.get('other_note')}`);
                                    }
                                    arr_info.push('+ Loại cửa: OTHER');
                                    arr_info.push(`+ Số lượng trên bản vẽ: ${rec.get('other')}`);
                                    arr_info.push(`+ Số lượng lập kế hoạch: ${rec.get('other_num')}`);
                                    if (rec.get('status') === 0) {
                                        arr_info.push('+ Trạng thái: <font color="aqua">Chưa gửi kế hoạch sản xuất</font>');
                                    } else if (rec.get('status') === 1) {
                                        arr_info.push('+ Trạng thái: <font color="chartreuse">Đang sản xuất</font>');
                                    } else if (rec.get('status') === 2) {
                                        const num_dungsx = (rec.get('cancel_num') === null || rec.get('cancel_num') === '' || parseInt(rec.get('cancel_num')) === 0) ? rec.get('cancel_other_num') : rec.get('cancel_num');
                                        arr_info.push('+ Trạng thái: <font color="yellow">Dừng sản xuất</font>');
                                        arr_info.push(`+ Số lượng dừng sản xuất: <font color="yellow">${num_dungsx} / ${rec.get('other_num')}</font>`);
                                        arr_info.push(`+ Lý do dừng: <font color="yellow">${rec.get('cancel_reason')}</font>`);
                                    } else if (rec.get('status') === 3) {
                                        arr_info.push('+ Trạng thái: <font color="chartreuse">Hoàn thành sản xuất</font>');
                                    } else if (rec.get('status') === 4) {
                                        arr_info.push('+ Trạng thái: <font color="chartreuse">Kế hoạch đã được gửi</font>');
                                    } else if (rec.get('status') === 5) {
                                        const num_huysx = (rec.get('stop_num') === null || rec.get('stop_num') === '' || parseInt(rec.get('stop_num')) === 0) ? rec.get('stop_other_num') : rec.get('stop_num');
                                        arr_info.push('+ Trạng thái: <font color="yellow">Hủy sản xuất</font>');
                                        arr_info.push(`+ Số lượng hủy sản xuất: <font color="yellow">${num_huysx} / ${rec.get('other_num')}</font>`);
                                        arr_info.push(`+ Lý do hủy: <font color="yellow">${rec.get('cancel_reason')}</font>`);
                                    }
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        } else if (colname === 'shearing') {
                            if (rec.get('note') !== '') {
                                if (rec.get('shearing_note') !== null && rec.get('shearing_note') !== '') {
                                    arr_info.push(`+ Ghi chú: ${rec.get('shearing_note')}`);
                                }
                            }
                        } else if (colname === 'tpp') {
                            if (rec.get('note') !== '') {
                                if (rec.get('tpp_note') !== null && rec.get('tpp_note') !== '') {
                                    arr_info.push(`+ Ghi chú: ${rec.get('tpp_note')}`);
                                }
                            }
                        } else if (colname === 'bending') {
                            if (rec.get('note') !== '') {
                                if (rec.get('bending_note') !== null && rec.get('bending_note') !== '') {
                                    arr_info.push(`+ Ghi chú: ${rec.get('bending_note')}`);
                                }
                            }
                        } else if (colname === 'grew_press_asembly') {
                            if (rec.get('note') !== '') {
                                if (rec.get('grew_press_asembly_note') !== null && rec.get('grew_press_asembly_note') !== '') {
                                    arr_info.push(`+ Ghi chú: ${rec.get('grew_press_asembly_note')}`);
                                }
                            }
                        } else if (colname === 'prime_paint') {
                            if (rec.get('note') !== '') {
                                if (rec.get('prime_paint_note') !== null && rec.get('prime_paint_note') !== '') {
                                    arr_info.push(`+ Ghi chú: ${rec.get('prime_paint_note')}`);
                                }
                            }
                        } else if (colname === 'finish_paint') {
                            if (rec.get('note') !== '') {
                                if (rec.get('finish_paint_note') !== null && rec.get('finish_paint_note') !== '') {
                                    arr_info.push(`+ Ghi chú: ${rec.get('finish_paint_note')}`);
                                }
                            }
                        } else if (colname === 'product_check') {
                            if (rec.get('note') !== '') {
                                if (rec.get('check_note') !== null && rec.get('check_note') !== '') {
                                    arr_info.push(`+ Ghi chú: ${rec.get('check_note')}`);
                                }
                            }
                        } else {
                            return false;
                        }
                        if (arr_info.length === 0) {
                            return false;
                        }
                        tip.update(arr_info.join('<br>'));
                    }
                    if (arr_info.length === 0) {
                        return false;
                    }
                    return true;
                },
            },
        });
    },

    onCellContextMenuGrid(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        if (tr.childElementCount > 6) {
            cellIndex += 6;
        }
        e.stopEvent();
        const selModel = grid.getSelectionModel();
        const controller = this;
        const refs = controller.getReferences();
        const { QuanLyKeHoachSanXuatGrid } = refs;
        let cellValue = null;
        let cellField = null;
        const position = [e.getX() - 10, e.getY() - 10];
        selModel.selectCells([cellIndex, rowIndex], [cellIndex, rowIndex]);
        try {
            cellField = selModel.getSelected().startCell.column.dataIndex;
            cellValue = record.data[cellField];
        } catch (err) {
            cellValue = null;
        }
        const menu_data = [];
        if (['project_name', 'order_no', 'sub_no'].indexOf(cellField) === -1) {
            if (record.data.note !== '' && (record.data.status === 0 || record.data.status === 4)) {
                if (cellValue && (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'frame_sp', 'leaf_sp', 'frame_sld', 'leaf_sld', 'frame_flsd', 'leaf_flsd', 'frame_fsd', 'leaf_fsd', 'sw', 'lv', 'other_num'].indexOf(cellField) !== -1)) {
                    Ext.create('Ext.menu.Menu', {
                        plain: true,
                        mouseLeaveDelay: 10,
                        items: [{
                            text: 'Ghi chú',
                            iconCls: 'fas fa-comments text-dark-blue',
                            handler() {
                                controller.onCommentCellButtonClick(record, cellField);
                            },
                        }, '-', {
                            text: 'Chỉnh sửa',
                            iconCls: 'fas fa-edit text-dark-blue',
                            handler() {
                                controller.onEditCellButtonClick(grid, cellIndex, record, rowIndex);
                            },
                        }, {
                            text: 'Xóa',
                            iconCls: 'fas fa-trash-alt text-dark-blue',
                            handler() {
                                controller.onDeleteCellButtonClick(selModel, record);
                            },
                        }],
                    }).showAt(position);
                } else if (['shearing', 'tpp', 'bending', 'grew_press_asembly', 'prime_paint', 'finish_paint', 'product_check'].indexOf(cellField) !== -1) {
                    Ext.create('Ext.menu.Menu', {
                        plain: true,
                        mouseLeaveDelay: 10,
                        items: [{
                            text: 'Ghi chú',
                            iconCls: 'fas fa-comments text-dark-blue',
                            handler() {
                                controller.onCommentCellDateButtonClick(record, cellField);
                            },
                        }, '-', {
                            text: 'Chỉnh sửa',
                            iconCls: 'fas fa-edit text-dark-blue',
                            handler() {
                                controller.onEditCellButtonClick(grid, cellIndex, record, rowIndex);
                            },
                        }, {
                            text: 'Xóa',
                            iconCls: 'fas fa-trash-alt text-dark-blue',
                            handler() {
                                controller.onDeleteCellButtonClick(selModel, record);
                            },
                        }],
                    }).showAt(position);
                }
                /* else if (["shearing_result", "tpp_result", "bending_result", "prepare_frame", "inner_frame", "grew_press_asembly_result", "prime_paint_result", "finish_paint_result", "check_result"].indexOf(cellField) !== -1) {
                                   Ext.create('Ext.menu.Menu', {
                                       plain: true,
                                       mouseLeaveDelay: 10,
                                       items: [{
                                           text: 'Ghi chú',
                                           iconCls: 'fas fa-comments text-dark-blue',
                                           handler: function() {
                                               controller.onCommentCellButtonClick(record, cellField);
                                           }
                                       }]
                                   }).showAt(position);
                               } */
            }
        }
    },

    onUpdateGridKeHoach(store, record, operation, modifiedFieldNames, details, eOpts) {
        const controller = this;
        const refs = controller.getReferences();
        const { gridDanhSachBanVeChuaLap } = refs;
        if (modifiedFieldNames) {
            if (modifiedFieldNames.indexOf('note') > -1) {
                const record_period = record.data.time_type;
                const record_time = record.data.time;
                const record_find = store.queryRecordsBy((record_f) => {
                    if (record_f.data.time_type === record_period && record_f.data.time === record_time && record_f.data.note !== '') {
                        return true;
                    }
                    return false;
                });
                if (record_find.length >= 5) {
                    store.add({
                        note: '',
                        project_name: '',
                        order_no: '',
                        sub_no: '',
                        frame_wd: null,
                        frame_susd: null,
                        leaf_wd: null,
                        leaf_susd: null,
                        frame_sd: null,
                        frame_lsd: null,
                        leaf_sd: null,
                        leaf_lsd: null,
                        ssjp: null,
                        ssaichi: null,
                        ssgrill: null,
                        srn: null,
                        grs: null,
                        s13: null,
                        s14: null,
                        frame_sp: null,
                        leaf_sp: null,
                        frame_sld: null,
                        leaf_sld: null,
                        frame_flsd: null,
                        leaf_flsd: null,
                        frame_fsd: null,
                        leaf_fsd: null,
                        sw: null,
                        lv: null,
                        other: null,
                        other_note: '',
                        date_drawing: '',
                        drawing_note: null,
                        delivery_note: null,
                        shearing_note: null,
                        tpp_note: null,
                        bending_note: null,
                        grew_press_asembly_note: null,
                        prime_paint_note: null,
                        finish_paint_note: null,
                        product_check_note: null,
                        date_delivery: '',
                        shearing: '',
                        shearing_result: '',
                        tpp: '',
                        tpp_result: '',
                        bending: '',
                        bending_result: '',
                        prepare_frame: '',
                        inner_frame: '',
                        grew_press_asembly: '',
                        grew_press_asembly_result: '',
                        prime_paint: '',
                        prime_paint_result: '',
                        finish_paint: '',
                        finish_paint_result: '',
                        product_check: '',
                        check_result: '',
                        color: '',
                        ghichu: '',
                        incharge: '',
                        time_type: record_period,
                        time: record_time,
                        status: 0,
                        type: '',
                        num_done: null,
                        num_other_done: null,
                        number: null,
                        other_num: null,
                        other_remain: null,
                        remain: null,
                        id_note: null,
                    });
                }
            }
            if (parseInt(record.data.id)) {
                if (record.get('note') !== '' && record.get('note') !== null && record.get('sub_no') !== '' && record.get('order_no') !== '') {
                    const data_record = {};
                    for (const i in modifiedFieldNames) {
                        const key = modifiedFieldNames[i];
                        if (typeof key === 'string') {
                            if (key !== 'id') {
                                if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'leaf_susd', 'leaf_wd', 'leaf_lsd', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'frame_sp', 'leaf_sp', 'frame_sld', 'leaf_sld', 'frame_flsd', 'leaf_flsd', 'frame_fsd', 'leaf_fsd', 'sw', 'lv'].indexOf(key) > -1) {
                                    data_record.type_num = record.data[key];
                                } else if (key === 'other_num') {
                                    data_record.other_num = record.data[key];
                                } else {
                                    data_record[key] = record.data[key];
                                }
                            }
                        }
                    }
                    if (Object.getOwnPropertyNames(data_record).length !== 0) {
                        Ext.Ajax.request({
                            url: `/api/production-schedule/edit/${record.data.id}`,
                            method: 'POST',
                            timeout: 30000,
                            params: data_record,
                            success(result, request) {
                                try {
                                    const response = Ext.decode(result.responseText);
                                    if (response.success) {
                                        for (const i in modifiedFieldNames) {
                                            const key = modifiedFieldNames[i];
                                            if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'frame_sp', 'leaf_sp', 'frame_sld', 'leaf_sld', 'frame_flsd', 'leaf_flsd', 'frame_fsd', 'leaf_fsd', 'sw', 'lv', 'other_num'].indexOf(key) > -1) {
                                                gridDanhSachBanVeChuaLap.getStore().load();
                                                // controller.loadDataGrid();
                                                break;
                                            }
                                        }
                                        record.commit();
                                    } else if (response.statusCode === 1) {
                                        console.log(record);
                                        Ext.MessageBox.alert('Thông báo', `Số lượng không hợp lệ ( <= ${record.get('total')})`, () => {
                                            record.reject();
                                        });
                                    } else if (response.statusCode === 2) {
                                        Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note: ${record.get('note')}</b> đã hoàn thành kế hoạch sản xuất`, () => {
                                            record.reject();
                                        });
                                    } else {
                                        Ext.MessageBox.alert('Thông báo', `Chỉnh sửa bản vẽ <b>Note: ${record.get('note')}</b> không thành công`, () => {
                                            record.reject();
                                        });
                                    }
                                } catch (err) {
                                    console.log(err);
                                    Ext.MessageBox.alert('Thông báo', `Chỉnh sửa bản vẽ <b>Note: ${record.get('note')}</b> không thành công`, () => {
                                        record.reject();
                                    });
                                }
                            },
                            failure(result, request) {
                                Ext.MessageBox.alert('Thông báo', `Chỉnh sửa bản vẽ <b>Note: ${record.get('note')}</b> không thành công`, () => {
                                    record.reject();
                                });
                            },
                        });
                    }
                }
            } else if (record.get('note') !== '' && record.get('note') !== null && record.get('sub_no') !== '' && record.get('order_no') !== '') {
                // Goi API tao san xuat
                const data_record = record.getData();
                if (data_record) {
                    delete data_record.id;
                    delete data_record.cancel_num;
                    delete data_record.cancel_other_num;
                    delete data_record.check_result;
                    delete data_record.finish_paint_result;
                    delete data_record.grew_press_asembly_result;
                    delete data_record.num_done;
                    delete data_record.num_other_done;
                    delete data_record.other_remain;
                    delete data_record.remain;
                    delete data_record.note;
                    delete data_record.project_name;
                    delete data_record.oder_no;
                    delete data_record.sub_no;
                    delete data_record.type;
                }
                data_record.drawing = record.data.id_note;
                data_record.type_num = record.data[record.data.type.toLowerCase()];
                for (const index in data_record) {
                    const attr = data_record[index];
                    if (attr === null || attr === '') {
                        delete data_record[index];
                    }
                }
                Ext.Ajax.request({
                    url: '/api/production-schedule/create',
                    method: 'POST',
                    timeout: 30000,
                    params: data_record,
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                gridDanhSachBanVeChuaLap.getStore().load();
                                if (response.data.id) {
                                    record.set({
                                        id: response.data.id,
                                    });
                                    record.commit();
                                }
                            } else if (response.statusCode === 1) {
                                Ext.MessageBox.alert('Thông báo', 'Số lượng không hợp lệ', () => {
                                    record.reject();
                                });
                            } else if (response.statusCode === 2) {
                                Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note: ${record.get('note')}</b> đã hoàn thành kế hoạch sản xuất`, () => {
                                    record.reject();
                                });
                            } else {
                                Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note: ${record.get('note')}</b> không tồn tại`, () => {
                                    record.reject();
                                });
                            }
                        } catch (err) {
                            console.log(err);
                            Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note: ${record.get('note')}</b> không tồn tại`, () => {
                                record.reject();
                            });
                        }
                    },
                    failure(result, request) {
                        Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note: ${record.get('note')}</b> không tồn tại`, () => {
                            record.reject();
                        });
                    },
                });
            }
        }
    },

    onCommentCellDateButtonClick(rec, cellField) {
        const controller = this;
        const refs = controller.getReferences();
        const { gridDanhSachBanVeChuaLap } = refs;
        const { QuanLyKeHoachSanXuatGrid } = refs;
        let text_note = '';
        if (rec) {
            if (cellField === 'date_drawing') {
                text_note = rec.get('drawing_note');
            } else if (cellField === 'date_delivery') {
                text_note = rec.get('delivery_note');
            } else if (cellField === 'shearing') {
                text_note = rec.get('shearing_note');
            } else if (cellField === 'tpp') {
                text_note = rec.get('tpp_note');
            } else if (cellField === 'bending') {
                text_note = rec.get('bending_note');
            } else if (cellField === 'grew_press_asembly') {
                text_note = rec.get('grew_press_asembly_note');
            } else if (cellField === 'prime_paint') {
                text_note = rec.get('prime_paint_note');
            } else if (cellField === 'finish_paint') {
                text_note = rec.get('finish_paint_note');
            } else if (cellField === 'product_check') {
                text_note = rec.get('product_check_note');
            }
            Ext.MessageBox.prompt('Thông báo', 'Nhập nội dung ghi chú:', (btn, text) => {
                if (btn === 'ok') {
                    if (text_note !== text) {
                        if (cellField === 'date_drawing') {
                            rec.set('drawing_note', text);
                        } else if (cellField === 'date_delivery') {
                            rec.set('delivery_note', text);
                        } else if (cellField === 'shearing') {
                            rec.set('shearing_note', text);
                        } else if (cellField === 'tpp') {
                            rec.set('tpp_note', text);
                        } else if (cellField === 'bending') {
                            rec.set('bending_note', text);
                        } else if (cellField === 'grew_press_asembly') {
                            rec.set('grew_press_asembly_note', text);
                        } else if (cellField === 'prime_paint') {
                            rec.set('prime_paint_note', text);
                        } else if (cellField === 'finish_paint') {
                            rec.set('finish_paint_note', text);
                        } else if (cellField === 'product_check') {
                            rec.set('product_check_note', text);
                        }
                        rec.commit();
                    }
                }
            }, this, true, text_note);
        }
    },

    onCommentCellButtonClick(rec, cellField) {
        const controller = this;
        const refs = controller.getReferences();
        const { gridDanhSachBanVeChuaLap } = refs;
        const { QuanLyKeHoachSanXuatGrid } = refs;
        let text_note = '';
        if (rec) {
            if (cellField === 'other_num') {
                text_note = rec.get('other_note');
            } else {
                text_note = rec.get('type_note');
            }
            Ext.MessageBox.prompt('Thông báo', 'Nhập nội dung ghi chú:', (btn, text) => {
                if (btn === 'ok') {
                    if (text_note !== text) {
                        if (cellField === 'other_num') {
                            rec.set('other_note', text);
                        } else {
                            rec.set('type_note', text);
                        }
                        rec.commit();
                    }
                }
            }, this, true, text_note);
        }
    },

    onDeleteCellButtonClick(selModel, record) {
        if (record) {
            record.set(selModel.getSelected().startCell.column.dataIndex, '');
            record.commit();
        }
    },

    onEditCellButtonClick(grid, cellIndex, record, rowIndex) {
        const controller = this;
        const refs = controller.getReferences();
        const { QuanLyKeHoachSanXuatGrid } = refs;
        const editPlugin = QuanLyKeHoachSanXuatGrid.findPlugin('cellediting');
        if (editPlugin) {
            editPlugin.startEdit(rowIndex, cellIndex);
        }
    },

    onItemKeydown(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        const controller = this;
        const refs = controller.getReferences();
        const { QuanLyKeHoachSanXuatGrid } = refs;
        const editPlugin = QuanLyKeHoachSanXuatGrid.findPlugin('cellediting');
        let index_type = null;
        if (!editPlugin.editing) {
            if (!e.event.ctrlKey) {
                if (['Control', 'Alt', 'Shift', 'Tab', 'Delete', 'Enter', 'CapsLock', 'ArrowLeft', 'ArrowRight', 'ArrowDown', 'ArrowUp', 'CapsLock', 'Escape', 'Insert', 'Backspace', 'Meta'].indexOf(e.event.key) === -1) {
                    const selection = grid.getSelectionModel().getSelected();
                    editPlugin.startEdit(selection.startCell.rowIdx, selection.startCell.colIdx);
                    editPlugin.context.column.field.setValue(null);
                } else if (e.event.key === 'Tab') {
                    // e.stopEvent();
                    const selection = grid.getSelectionModel().getSelected();
                    if (['note', 'project_name', 'order_no', 'sub_no'].indexOf(selection.startCell.column.dataIndex) !== -1) {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex(record.get('type').toLowerCase()).getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'frame_sp', 'leaf_sp', 'frame_sld', 'leaf_sld', 'frame_flsd', 'leaf_flsd', 'frame_fsd', 'leaf_fsd', 'sw', 'lv'].indexOf(selection.startCell.column.dataIndex) !== -1) {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('shearing').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'shearing') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('tpp').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'tpp') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('bending').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'bending') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('grew_press_asembly').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'grew_press_asembly') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('prime_paint').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'prime_paint') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('finish_paint').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'finish_paint') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('product_check').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'product_check') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('color').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'color') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('ghichu').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'ghichu') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('incharge').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type + 6, rowIndex], [index_type + 6, rowIndex]);
                        editPlugin.startEdit(rowIndex, index_type + 6);
                    } else if (selection.startCell.column.dataIndex === 'incharge') {
                        index_type = QuanLyKeHoachSanXuatGrid.columnManager.getHeaderByDataIndex('note').getIndex();
                        QuanLyKeHoachSanXuatGrid.getSelectionModel().selectCells([index_type, rowIndex + 1], [index_type, rowIndex + 1]);
                        editPlugin.startEdit(rowIndex + 1, index_type);
                    }
                }
            }
        }
        /* if (parseInt(record.get('status')) !== 0) {
            return false;
        } */
        if (e.getKey() === e.DELETE) {
            const selModelSelected = grid.getSelectionModel().getSelected();
            if (selModelSelected.startCell) {
                const field = selModelSelected.startCell.column.dataIndex;
                if (['note', 'project_name', 'order_no', 'sub_no'].indexOf(field) === -1) {
                    record.set(field, '');
                    record.commit();
                }
                if (field === 'note') {
                    // Goi API xoa san xuat
                    Ext.Ajax.request({
                        url: `/api/production-schedule/delete/${record.data.id}`,
                        method: 'POST',
                        timeout: 30000,
                        success(result, request) {
                            try {
                                const response = Ext.decode(result.responseText);
                                if (response.success) {
                                    controller.clearRowRecord(record);
                                    record.commit();
                                } else {
                                    Ext.MessageBox.alert('Thông báo', `Xóa bản vẽ <b>Note: ${record.data.note}</b> không thành công`, () => {
                                        record.reject();
                                    });
                                }
                            } catch (err) {
                                console.log(err);
                                Ext.MessageBox.alert('Thông báo', `Xóa bản vẽ <b>Note: ${record.data.note}</b> không thành công`, () => {
                                    record.reject();
                                });
                            }
                        },
                        failure(result, request) {
                            Ext.MessageBox.alert('Thông báo', `Xóa bản vẽ <b>Note: ${record.data.note}</b> không thành công`, () => {
                                record.reject();
                            });
                        },
                    });
                }
            }
        }
        // return false;
    },

    onEditCell(editor, context, eOpts) {
        const controller = this;
        const view = this.getView();
        const selModel = editor.grid.getSelectionModel();
        if (context.field === 'note' && context.value !== context.originalValue) {
            if (context.value !== '' && context.value !== null) {
                // Goi API lay thong tin cua note
                Ext.Ajax.request({
                    url: `/api/drawing/note/${context.value}`,
                    method: 'GET',
                    timeout: 30000,
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                if (response.data) {
                                    if (response.data.remain !== 0 && response.data.remain !== null) {
                                        let date_delivery = '';
                                        if (response.data.type) {
                                            if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'frame_fsd', 'frame_flsd', 'frame_sld', 'frame_sp'].indexOf(response.data.type) !== -1) {
                                                date_delivery = response.data.date_delivery.frame;
                                            } else if (['leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'leaf_fsd', 'leaf_flsd', 'leaf_sld', 'leaf_sp'].indexOf(response.data.type) !== -1) {
                                                date_delivery = response.data.date_delivery.leaf;
                                            } else {
                                                for (const date_d in response.data.date_delivery) {
                                                    if (response.data.date_delivery[date_d] !== null && response.data.date_delivery[date_d] !== '') {
                                                        date_delivery = response.data.date_delivery[date_d];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        context.record.set({
                                            project_name: response.data.project_name,
                                            order_no: response.data.order_no,
                                            sub_no: response.data.sub_no,
                                            frame_wd: response.data.frame_wd ? response.data.remain : '',
                                            frame_susd: response.data.frame_susd ? response.data.remain : '',
                                            leaf_wd: response.data.leaf_wd ? response.data.remain : '',
                                            leaf_susd: response.data.leaf_susd ? response.data.remain : '',
                                            frame_sd: response.data.frame_sd ? response.data.remain : '',
                                            frame_lsd: response.data.frame_lsd ? response.data.remain : '',
                                            leaf_sd: response.data.leaf_sd ? response.data.remain : '',
                                            leaf_lsd: response.data.leaf_lsd ? response.data.remain : '',
                                            ssjp: response.data.ssjp ? response.data.remain : '',
                                            ssaichi: response.data.ssaichi ? response.data.remain : '',
                                            ssgrill: response.data.ssgrill ? response.data.remain : '',
                                            srn: response.data.srn ? response.data.remain : '',
                                            grs: response.data.grs ? response.data.remain : '',
                                            s13: response.data.s13 ? response.data.remain : '',
                                            s14: response.data.s14 ? response.data.remain : '',
                                            frame_sp: response.data.frame_sp ? response.data.remain : '',
                                            leaf_sp: response.data.leaf_sp ? response.data.remain : '',
                                            frame_sld: response.data.frame_sld ? response.data.remain : '',
                                            leaf_sld: response.data.leaf_sld ? response.data.remain : '',
                                            frame_flsd: response.data.frame_flsd ? response.data.remain : '',
                                            leaf_flsd: response.data.leaf_flsd ? response.data.remain : '',
                                            frame_fsd: response.data.frame_fsd ? response.data.remain : '',
                                            leaf_fsd: response.data.leaf_fsd ? response.data.remain : '',
                                            sw: response.data.sw ? response.data.remain : '',
                                            lv: response.data.lv ? response.data.remain : '',
                                            // other_num: response.data.other_remain ? response.data.other_remain : "",
                                            type: response.data.type ? response.data.type.toUpperCase() : '',
                                            num_done: response.data.num_done,
                                            num_other_done: response.data.num_other_done,
                                            number: response.data.number,
                                            other_remain: response.data.other_remain,
                                            remain: response.data.remain,
                                            date_drawing: response.data.send_date,
                                            date_delivery,
                                            shearing: Ext.Date.format(Ext.Date.parse(context.record.data.time, 'Y-m-d'), 'm/d/Y'),
                                            id_note: response.data.id,
                                        });
                                    } else {
                                        context.record.set({
                                            project_name: response.data.project_name,
                                            order_no: response.data.order_no,
                                            sub_no: response.data.sub_no,
                                            other_num: response.data.other_remain ? response.data.other_remain : '',
                                            type: response.data.type ? response.data.type.toUpperCase() : '',
                                            num_done: response.data.num_done,
                                            num_other_done: response.data.num_other_done,
                                            number: response.data.number,
                                            other_remain: response.data.other_remain,
                                            remain: response.data.remain,
                                            shearing: Ext.Date.format(Ext.Date.parse(context.record.data.time, 'Y-m-d'), 'm/d/Y'),
                                            id_note: response.data.id,
                                        });
                                    }
                                } else {
                                    Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note: ${context.value}</b> không tồn tại`, () => {
                                        context.record.reject();
                                    });
                                }
                            } else {
                                Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note: ${context.value}</b> không tồn tại`, () => {
                                    context.record.reject();
                                });
                            }
                        } catch (err) {
                            console.log(err);
                            Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note: ${context.value}</b> không tồn tại`, () => {
                                context.record.reject();
                            });
                        }
                    },
                    failure(result, request) {
                        Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note: ${context.value}</b> không tồn tại`, () => {
                            context.record.reject();
                        });
                    },
                });
            } else {
                context.record.reject();
            }
        } else if (['date_drawing', 'date_delivery', 'shearing', 'tpp', 'bending', 'grew_press_asembly', 'prime_paint', 'finish_paint', 'product_check'].indexOf(context.field) > -1 && context.value !== context.originalValue) {
            // Kiem tra dieu kien ngay ke hoach
            let date_dif = -1;

            if (context.field === 'date_delivery') {
                date_dif = Ext.Date.diff(context.record.get('date_drawing'), context.record.get('date_delivery'), Ext.Date.DAY);
                if (date_dif < -1) {
                    Ext.MessageBox.alert('Thông báo', 'Thời gian giao hàng nhỏ hơn ngày vẽ', () => {});
                }
            } else if (context.field === 'tpp') {
                date_dif = Ext.Date.diff(context.record.get('shearing'), context.record.get('tpp'), Ext.Date.DAY);
                if (date_dif < -1) {
                    Ext.MessageBox.alert('Thông báo', 'Thời gian công đoạn Đột nhỏ hơn công đoạn Cắt', () => {});
                }
            } else if (context.field === 'bending') {
                date_dif = Ext.Date.diff(context.record.get('tpp'), context.record.get('bending'), Ext.Date.DAY);
                if (date_dif < -1) {
                    Ext.MessageBox.alert('Thông báo', 'Thời gian công đoạn Uốn nhỏ hơn công đoạn Đột', () => {});
                }
            } else if (context.field === 'grew_press_asembly') {
                date_dif = Ext.Date.diff(context.record.get('bending'), context.record.get('grew_press_asembly'), Ext.Date.DAY);
                if (date_dif < -1) {
                    Ext.MessageBox.alert('Thông báo', 'Thời gian công đoạn Hàn nhỏ hơn công đoạn Uốn', () => {});
                }
            } else if (context.field === 'prime_paint') {
                date_dif = Ext.Date.diff(context.record.get('grew_press_asembly'), context.record.get('prime_paint'), Ext.Date.DAY);
                if (date_dif < -1) {
                    Ext.MessageBox.alert('Thông báo', 'Thời gian công đoạn Prime paint nhỏ hơn công đoạn Hàn', () => {});
                }
            } else if (context.field === 'finish_paint') {
                date_dif = Ext.Date.diff(context.record.get('prime_paint'), context.record.get('finish_paint'), Ext.Date.DAY);
                if (date_dif < -1) {
                    Ext.MessageBox.alert('Thông báo', 'Thời gian công đoạn Finish paint nhỏ hơn công đoạn Prime paint', () => {});
                }
            } else if (context.field === 'product_check') {
                date_dif = Ext.Date.diff(context.record.get('finish_paint'), context.record.get('product_check'), Ext.Date.DAY);
                if (date_dif < -1) {
                    Ext.MessageBox.alert('Thông báo', 'Thời gian công đoạn KCS nhỏ hơn công đoạn Finish paint', () => {});
                }
            }
        }
    },

    onCellDBClick(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        const controller = this;
        const selModel = grid.getSelectionModel();
        const selection = selModel.getSelected();
        const refs = controller.getReferences();
        const { QuanLyKeHoachSanXuatGrid } = refs;
        const editPlugin = QuanLyKeHoachSanXuatGrid.findPlugin('cellediting');
        if (selection.startCell && (ClassMain.checkPermission('quan_ly_ke_hoach_san_xuat', 'ProductSchedule', 'create') === true)) {
            const field = selection.startCell.column.dataIndex;
            if (field === 'note' && (record.get('note') === null || record.get('note') === '')) {
                const win = Ext.create('Ext.window.Window', {
                    title: 'Chọn bản vẽ chưa lập kế hoạch sản xuất',
                    iconCls: 'far fa-list-alt',
                    width: '95%',
                    height: '85%',
                    layout: 'border',
                    modal: true,
                    viewModel: true,
                    items: {
                        region: 'center',
                        xtype: 'QuanLyBanVeSanXuat',
                        modeFilter: 2,
                        store: {
                            type: 'SoLuongBanVeStore',
                            storeId: 'SoLuongBanVeStore',
                            proxy: {
                                url: '/api/drawing/remain?number=gt_0',
                            },
                        },
                        listeners: {
                            itemdblclick(gridPanel, record_bd, item, index, event) {
                                if (record_bd) {
                                    if (editPlugin) {
                                        editPlugin.startEdit(rowIndex, cellIndex);
                                        editPlugin.context.column.field.setValue(record_bd.get('note'));
                                        editPlugin.completeEdit();
                                        win.close();
                                    }
                                }
                            },
                        },
                    },
                    buttons: ['->', {
                        text: 'Lập kế hoạch',
                        iconCls: 'fas fa-drafting-compass',
                        ui: 'soft-orange',
                        handler() {
                            const grid_chuave = this.up('window').down('grid');
                            const record_bd = grid_chuave.getSelectionModel().getSelection()[0];
                            if (record_bd) {
                                if (editPlugin) {
                                    editPlugin.startEdit(rowIndex, cellIndex);
                                    editPlugin.context.column.field.setValue(record_bd.get('note'));
                                    editPlugin.completeEdit();
                                    win.close();
                                }
                            }
                        },
                    }, {
                        text: 'Đóng',
                        glyph: 'xf00d@FontAwesome',
                        handler() {
                            this.up('window').close();
                        },
                    }, '->'],
                }).show();
            } else if (field === 'sub_no' && (record.get('sub_no') !== null && record.get('sub_no') !== '')) {
                controller.onViewDonDatHang(record);
            } else if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'frame_fsd', 'frame_flsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'leaf_fsd', 'leaf_flsd', 'frame_sld', 'leaf_sld', 'frame_sp', 'leaf_sp', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'lv', 'frame_sw', 'other'].indexOf(field) !== -1) {

            }
        }
    },

    onViewDonDatHang(record) {
        const controller = this;
        const { mainView } = Ext.getCmp('mainViewPort').getReferences();
        if (record !== null && record !== undefined) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp(`ViewDonDatHang${record.id}-TabPanel`) == null) {
                Ext.Ajax.request({
                    url: `/api/ordersheet/get/${record.id}`,
                    method: 'GET',
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                const tab = mainView.add({
                                    xtype: 'ViewDonDatHang',
                                    // gridView: grid,
                                    dataObject: response.data,
                                    closable: true,
                                    iconCls: 'fas fa-file-invoice-dollar',
                                    title: `<b>ĐƠN HÀNG: ${record.get('sub_no').toUpperCase()}</b>`,
                                    id: `ViewDonDatHang${record.id}-TabPanel`,
                                    listeners: {
                                        render() {
                                            ClassMain.hideLoadingMask();
                                        },
                                    },
                                });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', response.message, () => {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                        }
                    },
                    failure(result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                    },
                });
            } else {
                mainView.setActiveTab(Ext.getCmp(`ViewDonDatHang${record.id}-TabPanel`));
                ClassMain.hideLoadingMask();
            }
        }
    },

    onBeforeEditCell(editor, context, eOpts) {
        if (ClassMain.checkPermission('quan_ly_ke_hoach_san_xuat', 'ProductSchedule', 'create') === false) {
            return false;
        }
        const controller = this;
        // var showBtnSanXuat = controller.getViewModel().get("showBtnSanXuat");
        const disBtnSanXuat = controller.getViewModel().get('disBtnSanXuat');
        /* if (!showBtnSanXuat) {
            return false;
        } */
        if (context.record.get('status') === 2 || context.record.get('status') === 3 || context.record.get('status') === 5) {
            return false;
        }
        if (context.field !== 'note') {
            if (context.record.data.note === '') {
                return false;
            }
            if (['project_name', 'order_no', 'sub_no'].indexOf(context.field) !== -1) {
                return false;
            }
            if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'frame_sp', 'leaf_sp', 'frame_sld', 'leaf_sld', 'frame_flsd', 'leaf_flsd', 'frame_fsd', 'leaf_fsd', 'sw', 'lv'].indexOf(context.field) !== -1) {
                if (context.record.get('status') === 1) {
                    return false;
                }
                if (context.field !== context.record.data.type.toLowerCase()) {
                    return false;
                }
                if (context.record.get('other_num') !== 0) {
                    return false;
                }
            }
            if (context.field === 'other_num') {
                if (context.record.get('type')) {
                    if (context.record.get(context.record.get('type').toLowerCase()) === 0) {
                        return true;
                    }
                    return false;
                }
            }
        }
        if (context.field === 'note') {
            if (context.record.data.note !== '' && context.record.data.note !== null) {
                return false;
            }
            // Khong edit khi da dua vao san xuat
            /* if (Ext.Date.diff(Ext.Date.parse(context.record.get("time"), "Y-m-d"), new Date(), Ext.Date.DAY) === 0) {
                if (disBtnSanXuat) {
                    return false;
                }
            } */
        }
        return true;
    },

    onClickLapKeHoachButton(time) {
        const controller = this;
        const refs = controller.getReferences();
        const { gridDanhSachBanVeChuaLap } = refs;
        const { QuanLyKeHoachSanXuatGrid } = refs;
        Ext.MessageBox.confirm('Thông báo', 'Bạn có chắc chắn đưa kế hoạch ngày hiện tại vào sản xuất?', (btn) => {
            if (btn === 'yes') {
                const list_id = [];
                const datenow_str = time;
                const list_record = QuanLyKeHoachSanXuatGrid.getStore().queryRecordsBy((record) => {
                    if (record.get('time') === datenow_str && (record.get('id') % 1) === 0 && record.get('status') === 0) {
                        list_id.push(record.get('id'));
                        return true;
                    }
                    return false;
                });
                // Goi API dua vao san xuat
                Ext.Ajax.request({
                    url: `/api/production-schedule/production?ids=${list_id.join(',')}`,
                    method: 'POST',
                    timeout: 30000,
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                Ext.MessageBox.alert('Thông báo', 'Đưa vào sản xuất thành công', () => {
                                    // QuanLyKeHoachSanXuatGrid.getStore().load();
                                    controller.loadDataGrid();
                                });
                            } else {
                                Ext.MessageBox.alert('Thông báo', 'Đưa vào sản xuất không thành công', () => {});
                            }
                        } catch (err) {
                            console.log(err);
                            Ext.MessageBox.alert('Thông báo', 'Đưa vào sản xuất không thành công', () => {});
                        }
                    },
                    failure(result, request) {
                        Ext.MessageBox.alert('Thông báo', 'Đưa vào sản xuất không thành công', () => {});
                    },
                });
            }
        });
    },


    goToSelectCell(selModel, cellIndex, rowIndex) {
        selModel.selectCells([cellIndex, rowIndex], [cellIndex, rowIndex]);
    },

    goToSelectCellDup(selModel, record, cellIndex, rowIndex) {
        selModel.selectCells([cellIndex, rowIndex], [cellIndex, rowIndex]);
        const fieldIndex = selModel.getSelected().startCell.column.dataIndex;
        Ext.MessageBox.alert('Thông báo', `Bản vẽ <b>Note - ${record.data.note}</b> đã được lập kế hoạch <br><br><center><b>${selModel.getSelected().startCell.column.text} : ${record.data[fieldIndex]}</b></center>`, () => {
            record.set(fieldIndex, '');
        });
    },

    clearRowRecord(record) {
        record.set({
            note: '',
            project_name: '',
            order_no: '',
            sub_no: '',
            frame_wd: null,
            frame_susd: null,
            leaf_wd: null,
            leaf_susd: null,
            frame_sd: null,
            frame_lsd: null,
            leaf_sd: null,
            leaf_lsd: null,
            ssjp: null,
            ssaichi: null,
            ssgrill: null,
            srn: null,
            grs: null,
            s13: null,
            s14: null,
            frame_sp: null,
            leaf_sp: null,
            frame_sld: null,
            leaf_sld: null,
            frame_flsd: null,
            leaf_flsd: null,
            frame_fsd: null,
            leaf_fsd: null,
            sw: null,
            lv: null,
            other: null,
            other_note: '',
            date_drawing: '',
            drawing_note: null,
            delivery_note: null,
            shearing_note: null,
            tpp_note: null,
            bending_note: null,
            grew_press_asembly_note: null,
            prime_paint_note: null,
            finish_paint_note: null,
            product_check_note: null,
            date_delivery: '',
            shearing: '',
            shearing_result: '',
            tpp: '',
            tpp_result: '',
            bending: '',
            bending_result: '',
            prepare_frame: '',
            inner_frame: '',
            grew_press_asembly: '',
            grew_press_asembly_result: '',
            prime_paint: '',
            prime_paint_result: '',
            finish_paint: '',
            finish_paint_result: '',
            product_check: '',
            check_result: '',
            color: '',
            ghichu: '',
            incharge: '',
            status: 0,
            type: '',
            num_done: null,
            num_other_done: null,
            number: null,
            other_num: null,
            other_remain: null,
            remain: null,
            id_note: null,
            id: Ext.id(),
        });
    },

    onBtnReloadDanhSachBanVe(btn, e) {
        e.stopEvent();
        const controller = this;
        const refs = controller.getReferences();
        const { gridDanhSachBanVeChuaLap } = refs;
        const { QuanLyKeHoachSanXuatGrid } = refs;
        gridDanhSachBanVeChuaLap.getStore().load();
        controller.loadDataGrid();
    },

    onReloadButtonClick(btn, e) {
        e.stopEvent();
        const controller = this;
        const refs = controller.getReferences();
        const { gridDanhSachBanVeChuaLap } = refs;
        const { QuanLyKeHoachSanXuatGrid } = refs;
        gridDanhSachBanVeChuaLap.getStore().load();
        controller.loadDataGrid();
    },

    onBtnHideDanhSachBanVe(btn, e) {
        e.stopEvent();
        const controller = this;
        const refs = controller.getReferences();
        const { gridDanhSachBanVeChuaLap } = refs;
        const { btnViewDanhSachBanVe } = refs;
        btnViewDanhSachBanVe.toggle();
    },

    onBtnSearchRangeDate(btn, e) {
        e.stopEvent();
        const controller = this;
        controller.loadDataGrid();
    },
    onBtnViewDangSanXuat(btn, e) {
        e.stopEvent();
        const controller = this;
        const refs = controller.getReferences();
        const { SearchField } = refs;
        SearchField.setValue('');
        SearchField.getTrigger('clear').hide();
        SearchField.updateLayout();
        const { QuanLyKeHoachSanXuatGrid } = refs;
        delete QuanLyKeHoachSanXuatGrid.getStore().getProxy().extraParams.filter;
        delete QuanLyKeHoachSanXuatGrid.getStore().getProxy().extraParams.query;
        QuanLyKeHoachSanXuatGrid.getStore().load();
    },

    loadDataGrid() {
        const controller = this;
        const showBtnSanXuat = controller.getViewModel().get('showBtnSanXuat');
        const refs = controller.getReferences();
        const { SearchField } = refs;
        const { FilterMenu } = refs;
        const refs_FilterMenu = FilterMenu.getMenu().getReferences();
        const { QuanLyKeHoachSanXuatGrid } = refs;
        const { StartDateField } = refs;
        const { EndDateField } = refs;
        const filterValue = '';
        let time_filter = '';
        const arr_filter = [];
        let txt_filter = '';
        let valueSearch = SearchField.getValue();
        if (FilterMenu) {
            for (const ref in refs_FilterMenu) {
                if (refs_FilterMenu[ref].getValue() !== '' && refs_FilterMenu[ref].getValue() !== null && refs_FilterMenu[ref].fieldIndex) {
                    if (refs_FilterMenu[ref].fieldIndex === 'date_delivery') {
                        arr_filter.push(`${refs_FilterMenu[ref].fieldIndex}:eq_${Ext.Date.format(refs_FilterMenu[ref].getValue(), 'Y-m-d')}`);
                    } else {
                        arr_filter.push(`${refs_FilterMenu[ref].fieldIndex}:eq_${refs_FilterMenu[ref].getValue()}`);
                    }
                }
            }
        }

        if (!valueSearch) {
            valueSearch = '';
        }
        if (QuanLyKeHoachSanXuatGrid) {
            if (!showBtnSanXuat) {
                time_filter = `time:gteq_${Ext.Date.format(StartDateField.getValue(), 'Y-m-d')}_lteq_${Ext.Date.format(EndDateField.getValue(), 'Y-m-d')}`;
                arr_filter.push(time_filter);
            }
            txt_filter = arr_filter.join(',');
            if (txt_filter !== '') {
                QuanLyKeHoachSanXuatGrid.getStore().getProxy().setExtraParam('filter', txt_filter);
            } else {
                delete QuanLyKeHoachSanXuatGrid.getStore().getProxy().extraParams.filter;
            }
            if (valueSearch !== '') {
                QuanLyKeHoachSanXuatGrid.getStore().getProxy().setExtraParam('query', valueSearch);
            } else {
                delete QuanLyKeHoachSanXuatGrid.getStore().getProxy().extraParams.query;
            }
            QuanLyKeHoachSanXuatGrid.getStore().load();
        }
    },
});