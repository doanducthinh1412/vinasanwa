/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPKeHoach.QuanLyKeHoachSanXuat.QuanLyBanVeSanXuatController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.QuanLyBanVeSanXuat',

    renderNumberColumn(value, meta, rec, rowIndex, colIndex, view) {
        if (value === 0 || value === null) {
            return '';
        }
        if (rec.get('remain') > 0) {
            meta.style = 'background-color:#cf6311;';
            return `<font color="#FFFFFF"><font color="yellow">${rec.get('remain')}</font> / ${rec.get('num_done')} / ${value}</font>`;
        }
        meta.style = 'background-color:#9cc96b;';
        return `<font color="#000000">${rec.get('remain')} / ${rec.get('num_done')} / ${value}</font>`;
    },

    renderNumberColumnOther(value, meta, rec, rowIndex, colIndex, view) {
        if (value === 0 || value === null) {
            return '';
        }
        if (rec.get('other_remain') > 0) {
            meta.style = 'background-color:#cf6311;';
            return `<font color="#FFFFFF"><font color="yellow">${rec.get('other_remain')}</font> / ${rec.get('num_other_done')} / ${value}</font>`;
        }
        meta.style = 'background-color:#9cc96b;';
        return `<font color="#000000">${rec.get('other_remain')} / ${rec.get('num_other_done')} / ${value}</font>`;
    },

    onSelectFilterBanVe(combo, record, eOpts) {
        const controller = this;
        const refs = controller.getReferences();
        controller.loadDataBanVe();
    },

    loadDataBanVe() {
        const controller = this;
        const refs = controller.getReferences();
        const gridview = controller.getView();
        const { ComboboxFilterBanVe } = refs;
        const { SearchBanVe } = refs;
        const valueSearch = SearchBanVe.getValue();
        let txtSearch = '';
        if (valueSearch.length > 0) {
            txtSearch = `&query=${valueSearch}`;
        }
        if (ComboboxFilterBanVe.getValue() === 1) {
            if (valueSearch.length > 0) {
                txtSearch = `?query=${valueSearch}`;
            }
            gridview.getStore().getProxy().url = `/api/drawing?filter=status:eq_2${txtSearch}`;
            gridview.getStore().load();
        } else if (ComboboxFilterBanVe.getValue() === 2) {
            gridview.getStore().getProxy().url = `/api/drawing/remain?number=gt_0${txtSearch}`;
            gridview.getStore().load();
        } else if (ComboboxFilterBanVe.getValue() === 3) {
            gridview.getStore().getProxy().url = `/api/drawing/remain?number=eq_0${txtSearch}`;
            gridview.getStore().load();
        }
    },

    onReloadButtonClick() {
        const controller = this;
        const refs = controller.getReferences();
        controller.loadDataBanVe();
    },

});