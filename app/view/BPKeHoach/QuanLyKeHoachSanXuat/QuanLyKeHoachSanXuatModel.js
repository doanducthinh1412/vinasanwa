Ext.define('CMS.view.BPKeHoach.QuanLyKeHoachSanXuat.QuanLyKeHoachSanXuatModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.QuanLyKeHoachSanXuat',
    data: {
        daySanXuat: new Date(),
        disBtnSanXuat: false,
        modeViewDayPressed: false,
        startdate: new Date(),
        enddate: new Date()
    },
    formulas: {
        showBtnSanXuat: {
            get: function(get) {
                if (get('modeViewDayPressed')) {
                    return false;
                } else {
                    return true;
                }
                return false;
            },
            set: function() {
                return true;
            }
        }
    }
});