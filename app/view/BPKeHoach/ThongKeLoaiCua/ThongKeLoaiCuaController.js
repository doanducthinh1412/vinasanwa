Ext.define('CMS.view.BPKeHoach.ThongKeLoaiCua.ThongKeLoaiCuaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.ThongKeLoaiCua',
    onReloadButtonClick: function() {
        var panel = this.view;
        panel.getStore().clearFilter();
        panel.getStore().reload();
    },
    onItemDblClick: function(grid, record, item, index) {
        var me = this.view;
        var refs = me.getReferences();
        var year = me.getViewModel().get('year');
        var month = record.data.month;
        me.getViewModel().set('month', month);
        me.getViewModel().getStore('thongKeLoaiCuaMonthStore').getProxy().url = '/api/ordersheet/statistic-door?year=' + year + '&&month=' + month;
        me.getViewModel().getStore('thongKeLoaiCuaMonthStore').reload();
    }
});