Ext.define('CMS.view.BPKeHoach.ThongKeLoaiCua.ThongKeLoaiCuaModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.ThongKeLoaiCua',
    requires: [
        'CMS.store.ThongKeLoaiCuaStore',
    ],
    stores: {
        thongKeLoaiCuaStore: {
            type: 'ThongKeLoaiCuaStore',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: '/api/ordersheet/statistic-door?year={year}',
                reader: {
                    type: 'json',
                    transform: {
                        fn: function(res) {
                            if (res.success) {
                                var data = res.data;
                                if (data.length) {
                                    var year = data[0].year;
                                    var current_year = (new Date()).getFullYear();
                                    if (year == current_year) {
                                        var current_month = (new Date()).getMonth() + 1;
                                    } else {
                                        var current_month = 12
                                    }
                                } else {
                                    var year = null;
                                    var current_month = 12
                                }
                                var gridData = [];
                                for (i = 1; i < current_month + 1; i++) {
                                    //var name = Array(2 - Math.floor(Math.log10(i))).join('0') + i;
                                    var new_row = {};
                                    new_row.year = year;
                                    new_row.month = i;
                                    new_row.name = i;
                                    new_row.wood_grain_frame = 0;
                                    new_row.wood_grain_leaf = 0;
                                    new_row.standard_door_frame = 0;
                                    new_row.smoor_door_frame = 0;
                                    new_row.smoor_door_leaf = 0;
                                    new_row.sp_hanger_frame = 0;
                                    new_row.sp_hanger_leaf = 0;
                                    new_row.quicksave_grs = 0;
                                    new_row.quicksave_srn = 0;
                                    new_row.quicksave_s13 = 0;
                                    new_row.quicksave_s14 = 0;
                                    new_row.shutter_grill = 0;
                                    new_row.shutter_typhoon_hook = 0;
                                    gridData.push(new_row);
                                }
                                data.forEach(function(row) {
                                    if (row.frame_type == 'sld') {
                                        gridData[row.month - 1].smoor_door_frame = row.frame_total;
                                        gridData[row.month - 1].smoor_door_leaf = row.leaf_total;
                                    } else if (row.frame_type == 'sp') {
                                        gridData[row.month - 1].sp_hanger_frame = row.frame_total;
                                        gridData[row.month - 1].sp_hanger_leaf = row.leaf_total;
                                    } else if (row.frame_type == 'grs') {
                                        gridData[row.month - 1].quicksave_grs = row.type_total;
                                    } else if (row.frame_type == 'srn') {
                                        gridData[row.month - 1].quicksave_srn = row.type_total;
                                    } else if (row.frame_type == 's13') {
                                        gridData[row.month - 1].quicksave_s13 = row.type_total;
                                    } else if (row.frame_type == 's14') {
                                        gridData[row.month - 1].quicksave_s14 = row.type_total;
                                    } else if (row.frame_type == 'ssgrill') {
                                        gridData[row.month - 1].shutter_grill = row.type_total;
                                    }
                                });
                                //console.log(gridData);
                                return gridData
                            } else {
                                return res
                            }
                        },
                        scope: this
                    }
                }

            }
        },
        thongKeLoaiCuaMonthStore: {
            type: 'ThongKeLoaiCuaStore',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: '/api/ordersheet/statistic-door?year={year}&&month={month}',
                reader: {
                    type: 'json',
                    transform: {
                        fn: function(res) {
                            if (res.success) {
                                var data = res.data;
                                var gridData = [];
                                if (data.length) {
                                    var year = data[0].year;
                                    var month = data[0].month;
                                    data.forEach(function(row) {
                                        var new_row = {};
                                        new_row.name = row.name;
                                        new_row.sub_no = row.sub_no;
                                        new_row.year = year;
                                        new_row.month = month;
                                        new_row.wood_grain_frame = 0;
                                        new_row.wood_grain_leaf = 0;
                                        new_row.standard_door_frame = 0;
                                        new_row.smoor_door_frame = 0;
                                        new_row.smoor_door_leaf = 0;
                                        new_row.sp_hanger_frame = 0;
                                        new_row.sp_hanger_leaf = 0;
                                        new_row.quicksave_grs = 0;
                                        new_row.quicksave_srn = 0;
                                        new_row.quicksave_s13 = 0;
                                        new_row.quicksave_s14 = 0;
                                        new_row.shutter_grill = 0;
                                        new_row.shutter_typhoon_hook = 0;
                                        if (row.frame_type == 'sld') {
                                            new_row.smoor_door_frame = row.frame;
                                            new_row.smoor_door_leaf = row.leaf;
                                        } else if (row.frame_type == 'sp') {
                                            new_row.sp_hanger_frame = row.frame;
                                            new_row.sp_hanger_leaf = row.leaf;
                                        } else if (row.frame_type == 'grs') {
                                            new_row.quicksave_grs = row.frame_total;
                                        } else if (row.frame_type == 'srn') {
                                            new_row.quicksave_srn = row.frame_total;
                                        } else if (row.frame_type == 's13') {
                                            new_row.quicksave_s13 = row.frame_total;
                                        } else if (row.frame_type == 's14') {
                                            new_row.quicksave_s14 = row.frame_total;
                                        } else if (row.frame_type == 'ssgrill') {
                                            new_row.shutter_grill = row.frame_total;
                                        }
                                        gridData.push(new_row);
                                    });
                                    return gridData
                                } else {
                                    return gridData
                                }
                            } else {
                                return gridData
                            }
                        },
                        scope: this
                    }
                }

            }
        }

    },
    data: {
        month: null,
        year: null
    }
});