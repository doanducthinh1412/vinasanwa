Ext.define('CMS.view.BPKeHoach.KeHoachSanXuatThang.KeHoachSanXuatThangModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.KeHoachSanXuatThang',
    stores: {
        cummulativeTotalStore: Ext.create('Ext.data.Store', {
            fields: ['id', 'date', 'frame_sd', 'frame_lsd', 'leaf_sd', 'leaf_lsd', 'ss', 'ss_m2', 'qs', 'frame_sp', 'leaf_sp', 'frame_sld', 'leaf_sld', 'lv', 'other'],
            idProperty: 'id',
            data: []
        }),
        usQuantityStore: Ext.create('Ext.data.Store', {
            fields: ['id', 'date', 'frame_sd', 'frame_lsd', 'leaf_sd', 'leaf_lsd', 'ss', 'qs', 'frame_sp', 'leaf_sp', 'frame_sld', 'leaf_sld', 'lv', 'other', 'total', 'ratio', 'plan'],
            idProperty: 'id',
            data: []
        }),
        eachDayStore: Ext.create('Ext.data.Store', {
            fields: ['id', 'date', 'frame_sd', 'frame_lsd', 'leaf_sd', 'leaf_lsd', 'ss', 'ss_m2', 'qs', 'frame_sp', 'leaf_sp', 'frame_sld', 'leaf_sld', 'lv', 'other'],
            idProperty: 'id',
            data: []
        }),
    },
    data: {
        priceData: {

        },
        month: null,
        year: null,
        startdate: null,
        enddate: null,
        cummulativeTotalStoreData: null,
        usQuantityStoreData: null,
        eachDayStoreData: null,
    }
});