Ext.define('CMS.view.BPKeHoach.KeHoachSanXuatThang.KeHoachSanXuatThangController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.KeHoachSanXuatThang',
    onItemClick: function(grid, record, item, index) {
        var me = this.view;
        var refs = me.getReferences();
        refs.cummulativeTotalGrid.getSelectionModel().select(index);
        refs.usQuantityGrid.getSelectionModel().select(index);
        refs.eachDayGrid.getSelectionModel().select(index);
    },
    onEditPlanCell: function(editor, context, eOpts) {
        var controller = this;
        var view = this.getView();
        var selModel = editor.grid.getSelectionModel();
        var field = '';
        if (context.field == 'leaf_lsd') {
            field = 'leaf'
        } else if (context.field == 'ss_m2') {
            field = 'ss'
        } else if (context.field == 'smd_leaf') {
            field = 'smd'
        } else if (context.field == 'sph_leaf') {
            field = 'sph'
        } else {
            field = context.field
        }
        Ext.Ajax.request({
            url: 'api/production-schedule/report/plan',
            params: {
                product_code: field,
                year: context.record.data.year,
                month: context.record.data.month,
                plan_num: context.value
            },
            method: 'POST',
            success: function(response) {
                var res = Ext.decode(response.responseText);
                if (res.success) {
                    view.reloadGrid();
                }
            },
            failure: function(response) {

            }
        })
    },
    onEditM2Cell: function(editor, context, eOpts) {
        var controller = this;
        var view = this.getView();
        var selModel = editor.grid.getSelectionModel();
        Ext.Ajax.request({
            url: '/api/production-schedule/report/edit-m2/' + context.record.data.id,
            params: {
                m2: context.value
            },
            method: 'POST',
            success: function(response) {
                var res = Ext.decode(response.responseText);
                if (res.success) {
                    view.reloadGrid();
                }
            },
            failure: function(response) {

            }
        })
    },
    renderMergeColumn: function(value, metadata, record) {
        if (record.get('date') == 'Plan') {
            metadata.tdStyle = 'border:none';
            return null
        }
        if (value) {
            return value
        } else {
            return '-'
        }
    },
    renderPlanColumn: function(value, metadata, record) {
        if (record.get('date') == 'Plan') {
            if (value) {
                return '<b>' + value + '</b>'
            } else {
                return '-'
            }
        }

        if (value) {
            return value
        } else {
            return '-'
        }
    },
    renderDateColumn: function(value, metadata, record) {
        if (record.get('date') == 'Plan') {
            return '<b>' + value + '</b>'
        } else {
            var [year, month, day] = value.split('-');
            var list_month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            return '<b>' + day + '-' + list_month[parseInt(month) - 1] + '</b>'
        }
    },
    onExportButtonClick: function(btn) {
        var controller = this;
        var view = this.getView();
        var data = view.getViewModel().data;
        var link = document.createElement("a");
        var jsonData = {};
        Ext.MessageBox.show({
            title: 'Tải xuống',
            msg: 'Đang tải xuống...',
            progressText: 'Đang tải xuống...',
            width: 300,
            progress: true,
            closable: false,
            animateTarget: btn
        });
        /*
        var querystring = self.encodeQueryData(data);
        link.download = 'report.xls';
        link.href = querystring;
        link.click();
        */
        var formData = new FormData();
        for (i = 0; i < data.eachDayStoreData.length; i++) {
            formData.append('eachDayStoreData[]', JSON.stringify(data.eachDayStoreData[i]));
        }
        for (i = 0; i < data.usQuantityStoreData.length; i++) {
            formData.append('usQuantityStoreData[]', JSON.stringify(data.usQuantityStoreData[i]));
        }
        for (i = 0; i < data.cummulativeTotalStoreData.length; i++) {
            formData.append('cummulativeTotalStoreData[]', JSON.stringify(data.cummulativeTotalStoreData[i]));
        }
        formData.append('priceData', JSON.stringify(data.priceData));
        var url = 'api/production-schedule/report/download';
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';
        var filename = 'reportKeHoachSanXuatThang.xlsx';
        xhr.onload = function() {
            var a = document.createElement('a');
            a.href = window.URL.createObjectURL(xhr.response); // xhr.response is a blob
            if (filename) {
                a.download = filename; // Set the file name.
            }
            a.style.display = 'none';
            document.body.appendChild(a);
            a.click();
            delete a;
            Ext.MessageBox.hide();
        };
        xhr.open('POST', url);
        xhr.onerror = function() {
            Ext.MessageBox.alert('Thông báo', 'Xuất File Excel không thành công!', function() {});
            Ext.MessageBox.hide();
        };
        xhr.send(formData);
    },
    encodeQueryData: function encodeQueryData(data) {
        var ret = [];
        for (let d in data)
            ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
        return ret.join('&');
    }
});