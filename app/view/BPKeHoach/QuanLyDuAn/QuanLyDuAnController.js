Ext.define('CMS.view.BPKeHoach.QuanLyDuAn.QuanLyDuAnController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.QuanLyDuAn',
    renderNumberColumn: function(value, meta, rec, rowIndex, colIndex, view) {
        if (value === 0 || value === null) {
            return "";
        }
        return value;
    },
    renderDateColumn: function(value, meta, rec, rowIndex, colIndex, view) {
        if (value === "0000-00-00" || value === null) {
            return "";
        }
        return value;
    },
    onReloadButtonClick: function() {
        var panel = this.view;
        panel.el.mask('Đang tải...');
        panel.getStore().reload();
        panel.el.unmask();
    },

    loadDataGrid: function() {
        var controller = this;
        var refs = controller.getReferences();
        var gird = controller.getView();
        var SearchField = refs.SearchField;
        var txt_search = "";
        if (SearchField) {
            if (SearchField.getValue() !== "" && SearchField.getValue() !== null) {
                txt_search = SearchField.getValue();
            }
        }
        if (gird) {
            if (txt_search !== "") {
                gird.getStore().getProxy().setExtraParam("query", txt_search);
            } else {
                delete gird.getStore().getProxy().extraParams["query"];
            }
            gird.getStore().load();
        }
    },

    onViewButtonClick: function() {
        var gridPanel = this.view;
        var record = gridPanel.getSelectionModel().getSelection()[0];
        var mainView = Ext.getCmp('mainViewPort').getReferences().mainView;
        if (record) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel') == null) {
                Ext.Ajax.request({
                    url: '/api/ordersheet/get/' + record.data.id,
                    method: 'GET',
                    success: function(result, request) {
                        try {
                            var response = Ext.decode(result.responseText);
                            if (response.success) {
                                var tab = mainView.add({
                                    xtype: 'ViewDonDatHang',
                                    recordView: record,
                                    gridView: gridPanel,
                                    dataObject: response.data,
                                    closable: true,
                                    doorType: record.data.frame_type,
                                    iconCls: 'fas fa-file-invoice-dollar',
                                    title: "<b>ĐƠN HÀNG: " + record.data.sub_no.toUpperCase() + "</b>",
                                    id: 'ViewDonDatHang' + record.data.id + '-TabPanel',
                                    listeners: {
                                        render: function() {
                                            ClassMain.hideLoadingMask();
                                        }
                                    }
                                });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function() {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function() {});
                        }
                    },
                    failure: function(result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function() {});
                    }
                });
            } else {
                mainView.setActiveTab(Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel'));
                ClassMain.hideLoadingMask();
            }
        }
    }

});