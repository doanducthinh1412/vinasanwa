Ext.define('CMS.view.BPKeHoach.QuanLyDuAn.QuanLyDuAnModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.QuanLyDuAn',
    requires: [
        'CMS.store.QuanLyDuAnStore',
    ],
    stores: {
        DuAnStore: {
            type: 'QuanLyDuAnStore',
            autoLoad: true
        }
    }
});