/* eslint-disable max-len */
/* eslint-disable no-console */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.UploadDonDatHangController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.UploadDonDatHang',
    onChangeFile(field) {
        const controller = this;
        const refs = controller.getReferences();
        const { GridListFile } = refs;
        const listFile = field.fileInputEl.dom.files;
        const arrFile = [];
        if (field.typeFile === 'phu') {
            const recordPhu = GridListFile.getStore().findRecord('type', 'phu');
            if (recordPhu === null) {
                if (GridListFile.getStore().findRecord('name', listFile[0].name) === null) {
                    arrFile.push({
                        name: listFile[0].name,
                        type: field.typeFile,
                        typefile: listFile[0].type,
                        size: listFile[0].size,
                        file: listFile[0],
                    });
                }
            } else {
                GridListFile.getStore().remove(recordPhu);
                arrFile.push({
                    name: listFile[0].name,
                    type: field.typeFile,
                    typefile: listFile[0].type,
                    size: listFile[0].size,
                    file: listFile[0],
                });
            }
        } else {
            for (let i = 0; i < listFile.length; i += 1) {
                if (GridListFile.getStore().findRecord('name', listFile[i].name) === null) {
                    arrFile.push({
                        name: listFile[i].name,
                        type: field.typeFile,
                        typefile: listFile[i].type,
                        size: listFile[i].size,
                        file: listFile[i],
                    });
                }
            }
        }
        GridListFile.getStore().add(arrFile);
    },
    onClickButtonRemoveFile(grid, rowIndex) {
        const controller = this;
        const refs = controller.getReferences();
        const { btnUploadChiTiet } = refs;
        const { btnUploadPhuChiTiet } = refs;
        const { btnUploadPhu } = refs;
        const record = grid.getStore().getAt(rowIndex);
        if (record) {
            grid.getStore().remove(record);
            ClassMain.clearFileUpload(btnUploadPhuChiTiet.getId());
            ClassMain.clearFileUpload(btnUploadChiTiet.getId());
            ClassMain.clearFileUpload(btnUploadPhu.getId());
        }
    },

    onClickButtonImportFile() {
        const controller = this;
        const refs = controller.getReferences();
        const view_panel = controller.view;
        const recordView = view_panel.getRecordView();
        const { GridListFile } = refs;
        const { ComboboxLoaiDon } = refs;
        const { ComboboxDoUuTien } = refs;
        const { ComboboxGhiChu } = refs;
        const gridView = view_panel.getGridView();
        const data = new FormData();
        const { mainView } = Ext.getCmp('mainViewPort').getReferences();
        Ext.MessageBox.show({
            title: 'Xin chờ',
            msg: 'Đang nhập liệu đơn đặt hàng từ Excel...',
            progressText: 'Đang xử lý...',
            width: 300,
            wait: {
                interval: 200,
            },
        });
        let mode = 1;
        const doorType = view_panel.getDoorType();
        if (doorType === 'sd' || doorType === 'fsd' || doorType === 'lsd' || doorType === 'flsd' || doorType === 'sw' || doorType === 'sld' || doorType === 'sp' || doorType === 'susd' || doorType === 'wd') {
            mode = 1;
        } else {
            mode = 2;
        }
        let filebanve = false;
        GridListFile.getStore().each((record) => {
            if (mode === 1) {
                if (record.data.type === 'phu+chitiet') {
                    data.append('order_sheet_import', record.data.file);
                }
            } else if (mode === 2) {
                if (record.data.type === 'phu') {
                    data.append('order_sheet_import', record.data.file);
                } else if (record.data.type === 'chitiet') {
                    data.append('order_sheet_details[]', record.data.file);
                }
            }
            if (record.data.type === 'banve') {
                filebanve = true;
            }
        });
        if (filebanve) {
            data.append('type', doorType);

            Ext.Ajax.request({
                url: recordView === null ? '/api/ordersheet/import' : `/api/ordersheet/import/${recordView.get('id')}`,
                rawData: data,
                headers: { 'Content-Type': null },
                success(result) {
                    Ext.MessageBox.hide();
                    try {
                        const response = Ext.decode(result.responseText);
                        if (response.success) {
                            if (Ext.getCmp('CreateDonDatHang-TabPanel') == null) {
                                const tab = mainView.add({
                                    xtype: 'CreateDonDatHang',
                                    gridView,
                                    recordView,
                                    dataOrderSheet: {
                                        loaidon: ComboboxLoaiDon.getValue(),
                                        douutien: ComboboxDoUuTien.getValue(),
                                        note: ComboboxGhiChu.getValue(),
                                    },
                                    dataFile: GridListFile.getStore(),
                                    dataPanel: response.data,
                                    doorType,
                                    mode,
                                    closable: true,
                                    iconCls: 'fas fa-plus-circle',
                                    title: recordView === null ? '<b>TẠO MỚI ĐƠN ĐẶT HÀNG</b>' : `CẬP NHẬT ĐƠN ĐẶT HÀNG - ${recordView.get('sub_no')}`,
                                    id: 'CreateDonDatHang-TabPanel',
                                    listeners: {
                                        render() {
                                            ClassMain.hideLoadingMask();
                                        },
                                    },
                                });
                                mainView.setActiveTab(tab);
                            } else {
                                mainView.setActiveTab(Ext.getCmp('CreateDonDatHang-TabPanel'));
                            }
                            view_panel.hide();
                        } else if (response.statusCode === 409) {
                            Ext.MessageBox.alert('Thông báo', 'Đơn đặt hàng đã tồn tại. Vui lòng kiểm tra lại!', () => {
                                view_panel.show();
                            });
                        } else if (response.statusCode === 111) {
                            Ext.MessageBox.alert('Thông báo', 'File Excel nhập liệu không đúng loại cửa đã chọn. Vui lòng kiểm tra lại!', () => {
                                view_panel.show();
                            });
                        } else if (response.statusCode === -1) {
                            const err = controller.convertErr(response.data);
                            if (err !== '') {
                                Ext.MessageBox.alert('Thông báo', `<b>Vui lòng kiểm tra dữ liệu File Excel nhập liệu:</b><br>${err}`, () => {
                                    view_panel.show();
                                });
                            } else {
                                Ext.MessageBox.alert('Thông báo', 'Vui lòng kiểm tra lại File Excel nhập liệu đơn đặt hàng', () => {
                                    view_panel.show();
                                });
                            }
                        } else {
                            Ext.MessageBox.alert('Thông báo', 'Vui lòng kiểm tra lại File Excel nhập liệu đơn đặt hàng', () => {
                                view_panel.show();
                            });
                        }
                    } catch (err) {
                        console.log(err);
                        Ext.MessageBox.alert('Thông báo', 'Vui lòng kiểm tra lại File Excel nhập liệu đơn đặt hàng', () => {
                            view_panel.show();
                        });
                    }
                },
                failure() {
                    Ext.MessageBox.hide();
                    Ext.MessageBox.alert('Thông báo', 'Vui lòng kiểm tra lại File Excel nhập liệu đơn đặt hàng', () => {
                        view_panel.show();
                    });
                },
            });
        } else {
            Ext.MessageBox.hide();
            Ext.MessageBox.alert('Thông báo', 'Chưa tải bản vẽ thiết kế (PDF). Vui lòng kiểm tra lại!', () => {
                view_panel.show();
            });
        }
    },

    convertErr(obj_err) {
        const mapping_err = {
            frame: 'Frame',
            frame_total: 'Frame Total',
        };
        const arr_err = [];
        if (obj_err) {
            if (obj_err.orderSheet1) {
                obj_err.orderSheet1.forEach((item) => {
                    item.forEach((item_, key) => {
                        arr_err.push(`+ Tờ phủ, trường ${mapping_err[key]}, ${item_}`);
                    });
                });
                /* for (field in obj_err.orderSheet1) {
                    for (err in obj_err.orderSheet1[field]) {
                        arr_err.push(`+ Tờ phủ, trường ${mapping_err[field] }, ${obj_err.orderSheet1[field][err]}`);
                    }
                } */
            }
            return arr_err.join('<br>');
        }
        return '';
    },
});