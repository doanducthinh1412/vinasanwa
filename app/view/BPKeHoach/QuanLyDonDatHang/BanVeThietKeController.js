Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.BanVeThietKeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.BanVeThietKe',
    selectionBanVe: function(combo, record) {
        var controller = this;
        var view_panel = controller.view;
        var refs = view_panel.getReferences();
        var ViewPDF = refs.ViewPDF;
        ViewPDF.changeUrlPdf(record.data.filepath);
    }
});