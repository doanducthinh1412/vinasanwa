Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.CoverPageController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.CoverPage',
    projectExist: function() {

    },
    checkProjectExist: function() {
        var controller = this;
        var view_panel = controller.view;
        var refs = view_panel.getReferences();
        var comboboxDuAn = refs.comboboxDuAn;
        var view_model = controller.getViewModel();
        var dataPanel = view_model.getData().dataPanel;
        var record_prj = comboboxDuAn.getStore().findRecord('name', dataPanel.orderSheet1.generalInfo.pj_name, 0, false, true, true);
        if (record_prj) {
            controller.getViewModel().set('dataPanel.orderSheet1.generalInfo.project_id', record_prj.data.id);
        } else {
            Ext.MessageBox.confirm('Thông báo', 'Dự án <b>' + dataPanel.orderSheet1.generalInfo.pj_name + '</b> chưa tồn tại. Bạn có muốn tạo mới?', function(btn) {
                if (btn === "yes") {
                    var windowss = Ext.create('Ext.window.Window', {
                        title: 'Tạo mới dự án',
                        height: 520,
                        width: 550,
                        layout: 'border',
                        border: false,
                        maximizable: true,
                        glyph: 'xf055@FontAwesome',
                        modal: true,
                        items: [{
                            xtype: 'CreateDuAn',
                            region: 'center',
                            autoScroll: true,
                            storeParent: comboboxDuAn.getStore(),
                            relationComponent: comboboxDuAn,
                            relationData: {
                                name: dataPanel.orderSheet1.generalInfo.pj_name,
                                code: dataPanel.orderSheet1.generalInfo.pj_name
                            }
                        }]
                    }).show();
                } else {
                    comboboxDuAn.focus();
                }
            });
        }
    },

    onSearchProjectClick: function(combo) {
        var windowss = Ext.create('Ext.window.Window', {
            title: 'Tạo mới dự án',
            height: 520,
            width: 550,
            layout: 'border',
            border: false,
            maximizable: true,
            glyph: 'xf055@FontAwesome',
            modal: true,
            items: [{
                xtype: 'CreateDuAn',
                region: 'center',
                autoScroll: true,
                storeParent: combo.getStore(),
                relationComponent: combo
            }]
        }).show();
    }
});