/* eslint-disable eol-last */
/* eslint-disable max-len */
/* eslint-disable no-redeclare */
/* eslint-disable no-plusplus */
/* eslint-disable no-undef */
/* eslint-disable radix */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable no-use-before-define */
/* eslint-disable vars-on-top */
Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.ViewDonDatHangController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.ViewDonDatHang',

    renderPanelWorkFlow(c) {
        const controller = this;
        const viewPanel = this.view;
        const { recordView } = viewPanel;

        if (c.xtype !== 'draw') {
            c.el.on('mouseover', () => {
                c.setBodyStyle('background-color', '#919191');
            });
            c.el.on('mouseout', () => {
                c.setBodyStyle('background-color', c.bind.bodyStyle.lastValue['background-color']);
            });
            c.el.on('click', () => {
                let itemKetQua = [];
                const workflow = controller.getViewModel().get('workflow');
                if (c.itemId === 'workflowVe') {
                    if (parseInt(workflow.ve.code) >= 2) {
                        itemKetQua = [{
                            xtype: 'KetQuaVe',
                            region: 'center',
                            autoScroll: true,
                            recordPanel: recordView,
                            viewMode: true,
                            layout: 'fit',
                        }];
                    }
                    if (itemKetQua.length > 0) {
                        Ext.create('Ext.window.Window', {
                            title: `${c.title} - ĐƠN ĐẶT HÀNG: ${recordView.data.sub_no}`,
                            height: '90%',
                            width: '85%',
                            layout: 'border',
                            border: false,
                            maximizable: true,
                            iconCls: c.iconCls,
                            modal: true,
                            autoShow: true,
                            items: itemKetQua,
                            buttons: ['->', {
                                text: 'Đóng',
                                glyph: 'xf00d@FontAwesome',
                                handler() {
                                    this.up('window').close();
                                },
                            }],
                        });
                    } else {
                        Ext.MessageBox.alert('Thông báo', 'Bộ phận chưa có kết quả. Vui lòng kiểm tra lại sau!', () => {});
                    }
                } else if (c.itemId === 'workflowTinhGia') {
                    if (parseInt(workflow.tinhgia.code) === 2) {
                        itemKetQua = [{
                            xtype: 'form',
                            region: 'center',
                            autoScroll: true,
                            viewMode: true,
                            defaultType: 'textfield',
                            bodyPadding: 10,
                            layout: 'anchor',
                            fieldDefaults: {
                                msgTarget: 'under',
                                labelAlign: 'left',
                                autoFitErrors: true,
                                width: '100%',
                                labelWidth: 105,
                            },
                            items: [{
                                xtype: 'fieldset',
                                title: 'Thông tin tính giá',
                                defaultType: 'textfield',
                                items: [{
                                    fieldLabel: 'Giá Frame',
                                    name: 'frame_price',
                                    allowDecimals: true,
                                    xtype: 'numberfield',
                                    value: recordView.data.order_sheet_price[0] ? recordView.data.order_sheet_price[0].frame_price : null,
                                }, {
                                    fieldLabel: 'Giá Leaf',
                                    name: 'leaf_price',
                                    allowDecimals: true,
                                    xtype: 'numberfield',
                                    value: recordView.data.order_sheet_price[0] ? recordView.data.order_sheet_price[0].leaf_price : null,
                                }, {
                                    fieldLabel: 'Giá Other',
                                    name: 'other_price',
                                    allowDecimals: true,
                                    xtype: 'numberfield',
                                    value: recordView.data.order_sheet_price[0] ? recordView.data.order_sheet_price[0].other_price : null,
                                }, {
                                    fieldLabel: 'Ghi chú',
                                    xtype: 'textareafield',
                                    name: 'note',
                                    value: recordView.data.order_sheet_price[0] ? recordView.data.order_sheet_price[0].note : null,
                                }],
                            }],
                        }];
                    }
                    if (itemKetQua.length > 0) {
                        Ext.create('Ext.window.Window', {
                            title: `${c.title} - ĐƠN ĐẶT HÀNG: ${recordView.data.sub_no}`,
                            height: 400,
                            width: 500,
                            layout: 'border',
                            border: false,
                            maximizable: true,
                            iconCls: c.iconCls,
                            modal: true,
                            autoShow: true,
                            items: itemKetQua,
                            buttons: ['->', {
                                text: 'Đóng',
                                glyph: 'xf00d@FontAwesome',
                                handler() {
                                    this.up('window').close();
                                },
                            }],
                        });
                    } else {
                        Ext.MessageBox.alert('Thông báo', 'Bộ phận chưa có kết quả. Vui lòng kiểm tra lại!', () => {});
                    }
                } else {
                    Ext.MessageBox.alert('Thông báo', 'Bộ phận chưa có kết quả. Vui lòng kiểm tra lại!', () => {});
                }
            });
        }
    },

    onReloadButtonClick() {
        const controller = this;
        const view_panel = controller.view;
        const { recordView } = view_panel;
        if (recordView) {
            ClassMain.showLoadingMask();
            Ext.Ajax.request({
                url: `/api/ordersheet/get/${recordView.data.id}`,
                method: 'GET',
                success(result) {
                    ClassMain.hideLoadingMask();
                    try {
                        const response = Ext.decode(result.responseText);
                        if (response.success) {
                            controller.getViewModel().set('dataPanel', response.data);
                        } else {
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng. Vui lòng kiểm tra lại!', () => {});
                        }
                    } catch (err) {
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng. Vui lòng kiểm tra lại!', () => {});
                    }
                },
                failure(result, request) {
                    ClassMain.hideLoadingMask();
                    Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng. Vui lòng kiểm tra lại!', () => {});
                },
            });
        }
    },

    onEditButtonClick() {
        const controller = this;
        const view_panel = controller.view;
        const { recordView } = view_panel;
        const dataObject = view_panel.getDataObject();
        const refs = controller.getReferences();
        const { mainView } = Ext.getCmp('mainViewPort').getReferences();
        const frame_type = recordView.get('frame_type');
        if (frame_type !== null && frame_type !== '') {
            if (dataObject.orderSheet1) {
                Ext.create({
                    xtype: 'UploadDonDatHang',
                    title: `<b>Cập nhật đơn đặt hàng ${dataObject.orderSheet1.generalInfo.sub_no} - Loại cửa ${frame_type.toUpperCase()}</b>`,
                    doorType: frame_type,
                    gridView: view_panel.getGridView(),
                    recordView,
                });
                mainView.remove(Ext.getCmp(`ViewDonDatHang${recordView.data.id}-TabPanel`));
            }
        }
    },

    onSelectLoaiDon() {

    },

    onEnterGhiChu(combobox, e) {
        if (e.getKey() === e.ENTER) {
            const controller = this;
            const view_panel = controller.view;
            const { recordView } = view_panel;
            Ext.MessageBox.show({
                title: 'Xin chờ',
                msg: 'Đang chỉnh sửa đơn đặt hàng...',
                progressText: 'Đang xử lý...',
                width: 300,
                wait: {
                    interval: 200,
                },
            });
            Ext.Ajax.request({
                url: `/api/ordersheet/edit/${recordView.data.id}`,
                method: 'POST',
                params: {
                    note: combobox.getValue(),
                },
                headers: { 'Content-Type': null },
                success(result, request) {
                    Ext.MessageBox.hide();
                    try {
                        const response = Ext.decode(result.responseText);
                        if (response.success) {
                            Ext.MessageBox.alert('Thông báo', 'Chỉnh sửa đơn đặt hàng thành công', () => {
                                if (view_panel.gridView) {
                                    view_panel.gridView.getStore().load();
                                }
                            });
                        } else if (response.statusCode === 409) {
                            Ext.MessageBox.alert('Thông báo', 'Đơn đặt hàng đã tồn tại. Vui lòng kiểm tra lại!', () => {});
                        } else {
                            Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                        }
                    } catch (err) {
                        Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                    }
                },
                failure(result, request) {
                    Ext.MessageBox.hide();
                    Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                },
            });
        }
    },

    onSelectGhiChu(combobox, record) {
        const controller = this;
        const view_panel = controller.view;
        const { recordView } = view_panel;
        Ext.MessageBox.show({
            title: 'Xin chờ',
            msg: 'Đang chỉnh sửa đơn đặt hàng...',
            progressText: 'Đang xử lý...',
            width: 300,
            wait: {
                interval: 200,
            },
        });
        Ext.Ajax.request({
            url: `/api/ordersheet/edit/${recordView.data.id}`,
            method: 'POST',
            params: {
                note: record.get('val'),
            },
            headers: { 'Content-Type': null },
            success(result, request) {
                Ext.MessageBox.hide();
                try {
                    const response = Ext.decode(result.responseText);
                    if (response.success) {
                        Ext.MessageBox.alert('Thông báo', 'Chỉnh sửa đơn đặt hàng thành công', () => {
                            if (view_panel.gridView) {
                                view_panel.gridView.getStore().load();
                            }
                        });
                    } else if (response.statusCode === 409) {
                        Ext.MessageBox.alert('Thông báo', 'Đơn đặt hàng đã tồn tại. Vui lòng kiểm tra lại!', () => {});
                    } else {
                        Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                    }
                } catch (err) {
                    Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                }
            },
            failure(result, request) {
                Ext.MessageBox.hide();
                Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
            },
        });
    },

    onSelectDoUuTien(combobox, record) {
        const controller = this;
        const view_panel = controller.view;
        const { recordView } = view_panel;
        Ext.MessageBox.show({
            title: 'Xin chờ',
            msg: 'Đang chỉnh sửa đơn đặt hàng...',
            progressText: 'Đang xử lý...',
            width: 300,
            wait: {
                interval: 200,
            },
        });
        Ext.Ajax.request({
            url: `/api/ordersheet/edit/${recordView.data.id}`,
            method: 'POST',
            params: {
                priority: record.get('val'),
            },
            headers: { 'Content-Type': null },
            success(result, request) {
                Ext.MessageBox.hide();
                try {
                    const response = Ext.decode(result.responseText);
                    if (response.success) {
                        Ext.MessageBox.alert('Thông báo', 'Chỉnh sửa đơn đặt hàng thành công', () => {
                            if (view_panel.gridView) {
                                view_panel.gridView.getStore().load();
                            }
                        });
                    } else if (response.statusCode === 409) {
                        Ext.MessageBox.alert('Thông báo', 'Đơn đặt hàng đã tồn tại. Vui lòng kiểm tra lại!', () => {});
                    } else {
                        Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                    }
                } catch (err) {
                    Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                }
            },
            failure(result, request) {
                Ext.MessageBox.hide();
                Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
            },
        });
    },

    onItemKeydown(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        const controller = this;
        if (e.getKey() === e.DELETE) {
            const selModelSelected = grid.getSelectionModel().getSelected();
            if (selModelSelected.startCell) {
                const field = selModelSelected.startCell.column.dataIndex;
                record.set(field, '');
            }
        }
    },

    onExportdButtonClick() {
        const controller = this;
        const view_panel = controller.view;
        const { recordView } = view_panel;

        Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', (btn, text) => {
            if (btn === 'yes') {
                const url = `api/ordersheet/download/${recordView.data.id}`;
                Ext.MessageBox.show({
                    title: 'Tải xuống',
                    msg: 'Đang tải xuống...',
                    progressText: 'Đang tải xuống...',
                    width: 300,
                    progress: true,
                    closable: false,
                    animateTarget: btn,
                });
                ClassMain.saveFile(url, `${recordView.data.sub_no}.zip`);
            }
        });
    },

    onViewFileButtonClick(btn) {
        const controller = this;
        const view_panel = controller.view;
        const { recordView } = view_panel;
        const jsonOderSheet = controller.getViewModel().getData().dataPanel;
        const listFile = [];

        if (jsonOderSheet.order_sheets) {
            if (jsonOderSheet.order_sheets.order_sheet_import) {
                var p_ext = jsonOderSheet.order_sheets.order_sheet_import.file_name.lastIndexOf('.');
                listFile.push({
                    name: `${jsonOderSheet.order_sheets.order_sheet_import.file_name.substring(0, p_ext)}.pdf`,
                    type: 'Tờ phủ',
                });
            }
            if (jsonOderSheet.order_sheets.order_sheet_details) {
                for (i = 0; i < jsonOderSheet.order_sheets.order_sheet_details.length; i++) {
                    var p_ext = jsonOderSheet.order_sheets.order_sheet_details[i].file_name.lastIndexOf('.');
                    listFile.push({
                        name: `${jsonOderSheet.order_sheets.order_sheet_details[i].file_name.substring(0, p_ext)}.pdf`,
                        type: 'Tờ chi tiết',
                    });
                }
            }
        }
        if (jsonOderSheet.banvethietke) {
            for (i = 0; i < jsonOderSheet.banvethietke.length; i++) {
                listFile.push({
                    name: jsonOderSheet.banvethietke[i].file_name,
                    type: 'Bản vẽ',
                });
            }
        }

        var windows_panel = Ext.create('Ext.window.Window', {
            title: `Xem File - Đơn đặt hàng - ${recordView.data.sub_no}`,
            height: '90%',
            width: '95%',
            minWidth: 800,
            minHeight: 600,
            layout: 'border',
            border: false,
            maximizable: true,
            iconCls: 'far fa-file-pdf',
            modal: true,
            items: [{
                region: 'west',
                width: 300,
                xtype: 'grid',
                store: Ext.create('Ext.data.Store', {
                    fields: ['name', 'type'],
                    data: listFile,
                }),
                columns: [{
                    text: '<br>Tên File<br><br>',
                    dataIndex: 'name',
                    flex: 1,
                }, {
                    text: '<br>Loại<br><br>',
                    dataIndex: 'type',
                    flex: 0.5,
                }],
                listeners: {
                    select(grid, record) {
                        let url = `/api/ordersheet/pdf/${recordView.data.id}?name=${record.data.name}`;
                        if (record.data.type === 'Bản vẽ') {
                            url = `/api/ordersheet/banvethietke/${recordView.data.id}?name=${record.data.name}`;
                        }
                        windows_panel.down('ViewPDF').changeUrlPdf(url);
                    },
                },
            }, {
                region: 'center',
                width: '100%',
                xtype: 'ViewPDF',
                reference: 'ViewPDF',
                urlPdf: `/api/ordersheet/pdf/${recordView.data.id}?name=${listFile[0].name}`,
            }],
        }).show();
    },
});