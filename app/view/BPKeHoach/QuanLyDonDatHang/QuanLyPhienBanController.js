/* eslint-disable no-plusplus */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable block-scoped-var */
/* eslint-disable camelcase */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.QuanLyPhienBanController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.BPKH_QuanLyPhienBan',
    renderVersionColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return 'Phiên bản';
        }
        return `Phiên bản ngày ${Ext.Date.format(value, 'H:i d/m/Y')}`;
    },
    renderDateColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return '';
        }
        return Ext.Date.format(value, 'H:i d/m/Y');
    },

    onButtonHistoryClick(btn) {
        const controller = this;
        const grid = controller.view;
        const rec = btn.getWidgetRecord();
        if (rec) {
            controller.onViewDonDatHang(rec);
        }
    },

    onViewDonDatHang(record) {
        const controller = this;
        const { mainView } = Ext.getCmp('mainViewPort').getReferences();
        if (record !== null && record !== undefined) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp(`ViewDonDatHang${record.id}-TabPanel`) == null) {
                Ext.Ajax.request({
                    url: `/api/ordersheet/get/${record.id}`,
                    method: 'GET',
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                const tab = mainView.add({
                                    xtype: 'ViewDonDatHang',
                                    // gridView: grid,
                                    dataObject: response.data,
                                    closable: true,
                                    iconCls: 'fas fa-file-invoice-dollar',
                                    title: `<b>PHIÊN BẢN ${Ext.Date.format(record.get('created_at'), 'H:i d/m/Y')} - ĐƠN HÀNG: ${record.get('sub_no').toUpperCase()}</b>`,
                                    modeVersion: true,
                                    id: `ViewDonDatHang${record.id}-TabPanel`,
                                    listeners: {
                                        render() {
                                            ClassMain.hideLoadingMask();
                                        },
                                    },
                                });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', response.message, () => {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                        }
                    },
                    failure(result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                    },
                });
            } else {
                mainView.setActiveTab(Ext.getCmp(`ViewDonDatHang${record.id}-TabPanel`));
                ClassMain.hideLoadingMask();
            }
        }
    },
    onViewClick(gridPanel, rowIndex, colIndex) {
        const controller = this;
        if (ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'get') === true) {
            const record = gridPanel.getStore().getAt(rowIndex);
            const listFile = [];
            const jsonOderSheet = record.data;
            if (jsonOderSheet.order_sheets) {
                if (jsonOderSheet.order_sheets.order_sheet_import) {
                    var p_ext = jsonOderSheet.order_sheets.order_sheet_import.file_name.lastIndexOf('.');
                    listFile.push({
                        name: `${jsonOderSheet.order_sheets.order_sheet_import.file_name.substring(0, p_ext)}.pdf`,
                        type: 'Tờ phủ',
                    });
                }
                if (jsonOderSheet.order_sheets.order_sheet_details) {
                    for (var i = 0; i < jsonOderSheet.order_sheets.order_sheet_details.length; i++) {
                        var p_ext = jsonOderSheet.order_sheets.order_sheet_details[i].file_name.lastIndexOf('.');
                        listFile.push({
                            name: `${jsonOderSheet.order_sheets.order_sheet_details[i].file_name.substring(0, p_ext)}.pdf`,
                            type: 'Tờ chi tiết',
                        });
                    }
                }
            }
            if (jsonOderSheet.banvethietke) {
                for (i = 0; i < jsonOderSheet.banvethietke.length; i++) {
                    listFile.push({
                        name: jsonOderSheet.banvethietke[i].file_name,
                        type: 'Bản vẽ',
                    });
                }
            }

            const windows_panel = Ext.create('Ext.window.Window', {
                title: `Xem File - Phiên bản ${Ext.Date.format(record.get('created_at'), 'H:i d/m/Y')} - Đơn đặt hàng - ${record.data.sub_no}`,
                height: '90%',
                width: '95%',
                minWidth: 800,
                minHeight: 600,
                layout: 'border',
                border: false,
                maximizable: true,
                iconCls: 'far fa-file-pdf',
                modal: true,
                items: [{
                    region: 'west',
                    width: 300,
                    xtype: 'grid',
                    store: Ext.create('Ext.data.Store', {
                        fields: ['name', 'type'],
                        data: listFile,
                    }),
                    columns: [{
                        text: '<br>Tên File<br><br>',
                        dataIndex: 'name',
                        flex: 1,
                    }, {
                        text: '<br>Loại<br><br>',
                        dataIndex: 'type',
                        flex: 0.5,
                    }],
                    listeners: {
                        select(grid, record_) {
                            let url = `/api/ordersheet/pdf/${record.data.id}?name=${record_.data.name}`;
                            if (record_.data.type === 'Bản vẽ') {
                                url = `/api/ordersheet/banvethietke/${record.data.id}?name=${record_.data.name}`;
                            }
                            windows_panel.down('ViewPDF').changeUrlPdf(url);
                        },
                    },
                }, {
                    region: 'center',
                    width: '100%',
                    xtype: 'ViewPDF',
                    reference: 'ViewPDF',
                    urlPdf: `/api/ordersheet/pdf/${record.data.id}?name=${listFile[0].name}`,
                }],
            }).show();
        }
    },

    onDownloadClick(gridPanel, rowIndex, colIndex) {
        const controller = this;
        if (ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'get') === true) {
            const record = gridPanel.getStore().getAt(rowIndex);
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', (btn, text) => {
                if (btn === 'yes') {
                    const url = `api/ordersheet/download/${record.data.id}`;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btn,
                    });
                    ClassMain.saveFile(url, `${record.data.sub_no}-Phiên bản ${Ext.Date.format(record.get('created_at'), 'H:i d/m/Y')}.zip`);
                }
            });
        }
    },
});
