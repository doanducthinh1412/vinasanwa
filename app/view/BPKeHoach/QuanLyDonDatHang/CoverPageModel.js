Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.CoverPageModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.CoverPage',
    stores: {
        SoLuongCuaDatHang: {
            fields: ['type', 'sd', 'fsd', 'lsd', 'flsd', 'sstw', 'ssjp', 'ssgrill', 'grs', 'srn', 's13', 's14', 'lv', 'sld', 'sp'],
            data: '{dataPanel.orderSheet1.detailInfo.detailInfoType}',
            autoLoad: true
        },
        hwtype: {
            fields: ['hw_type_name', 'supply_name', 'sample'],
            autoLoad: true,
            data: '{dataPanel.orderSheet1.detailInfo.detailInfoHwType}'
        },
        changeHistory: {
            fields: ['item_no', 'date', 'change_before', 'change_after', 'reasons'],
            autoLoad: true,
            data: '{dataPanel.orderSheet1.changeHistory}'
        }
    }
});