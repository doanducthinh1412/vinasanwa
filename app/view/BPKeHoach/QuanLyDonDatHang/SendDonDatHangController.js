Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.SendDonDatHangController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.SendDonDatHang',
    onClickSendDonDatHangButton: function() {
        var controller = this;
        var view = controller.view;
        var refs = view.getReferences();
        var ChuyenXuLyCheckBox = refs.ChuyenXuLyCheckBox;
        var data_chuyenxuly = ChuyenXuLyCheckBox.getValue();
        var arr_chuyenxuly = [];
        for (xuly in data_chuyenxuly) {
            if (data_chuyenxuly[xuly] === "on") {
                arr_chuyenxuly.push(xuly);
            }
        }
        var str_chuyenxuly = arr_chuyenxuly.join(',');
        var recordData = view.getRecordData();
        var gridPanel = view.getGridPanel();
        if (recordData) {
            view.hide();
            Ext.MessageBox.show({
                title: 'Xin chờ',
                msg: 'Đang chuyển xử lý đơn đặt hàng...',
                progressText: 'Đang chuyển xử lý...',
                width: 300,
                wait: {
                    interval: 200
                }
            });
            Ext.Ajax.request({
                url: '/api/ordersheet/send/' + recordData.data.id + '?dp=' + str_chuyenxuly,
                method: 'GET',
                success: function(result, request) {
                    try {
                        Ext.MessageBox.hide();
                        var response = Ext.decode(result.responseText);
                        if (response.success) {
                            Ext.MessageBox.alert('Thông báo', 'Đơn đặt hàng đã được gửi tới bộ phận', function() {});
                            if (gridPanel) {
                                gridPanel.reloadView();
                            }
                        } else {
                            Ext.MessageBox.alert('Thông báo', 'Đơn đặt hàng đã được gửi tới bộ phận', function() {});
                        }
                    } catch (err) {
                        console.log(err);
                        Ext.MessageBox.hide();
                        Ext.MessageBox.alert('Thông báo', 'Đơn đặt hàng đã được gửi tới bộ phận', function() {});
                    }
                },
                failure: function(result, request) {
                    Ext.MessageBox.hide();
                    Ext.MessageBox.alert('Thông báo', 'Đơn đặt hàng đã được gửi tới bộ phận', function() {});
                }
            });
        }
    }
});