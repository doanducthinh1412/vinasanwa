/* eslint-disable no-shadow */
/* eslint-disable no-restricted-syntax */
/* eslint-disable camelcase */
/* eslint-disable radix */
/* eslint-disable no-unused-vars */
Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.QuanLyDonDatHangController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.QuanLyDonDatHang',
    onItemActionMenu(view, record, e) {
        const controller = this;
        const gridPanel = controller.view;
        const menu = controller.createMenu(record);
        controller.getViewModel().set('actionMenu', menu);
    },
    onItemContextMenu(view, record, item, index, e) {
        const controller = this;
        const gridPanel = controller.view;
        const position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        const menu = controller.createMenu(record);
        menu.showAt(position);
    },

    createMenu(record) {
        const controller = this;
        if (record) {
            return Ext.create('Ext.menu.Menu', {
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                        text: 'Xem thông tin',
                        iconCls: 'fas fa-info-circle text-dark-blue',
                        hidden: ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'get') !== true,
                        handler() {
                            controller.onViewButtonClick();
                        },
                    }, {
                        text: 'Tải File - Đơn đặt hàng',
                        iconCls: 'fas fa-file-export text-dark-blue',
                        hidden: ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'get') !== true,
                        handler() {
                            controller.onExportdButtonClick();
                        },
                    }, {
                        xtype: 'menuseparator',
                        hidden: !((record.data.status.order_sheet === 0 && ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'delete') === true)),
                    }, {
                        text: 'Cập nhật đơn đặt hàng',
                        iconCls: 'fas fa-edit text-dark-blue',
                        hidden: !((record.data.status.order_sheet === 0 && ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'create') === true)),
                        handler() {
                            controller.onUpdateButtonClick();
                        },
                    }, {
                        text: 'Xóa đơn đặt hàng',
                        iconCls: 'far fa-trash-alt text-dark-blue',
                        hidden: !((record.data.status.order_sheet === 0 && ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'delete') === true)),
                        handler() {
                            controller.onDeleteButtonClick();
                        },
                    },
                    {
                        text: 'Chuyển xử lý',
                        iconCls: 'fas fa-paper-plane text-dark-blue',
                        hidden: !((record.data.status.order_sheet === 0 && ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'send') === true)),
                        handler() {
                            controller.onSendButtonClick();
                        },
                    }, {
                        xtype: 'menuseparator',
                        hidden: !((record.data.status.order_sheet === 1 && ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'cancel') === true)),
                    }, {
                        text: 'Hủy đơn đặt hàng',
                        iconCls: 'fas fa-ban text-dark-blue',
                        hidden: !((record.data.status.order_sheet === 1 && ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'cancel') === true)),
                        handler() {
                            controller.onCancelButtonClick();
                        },
                    },
                ],
            });
        }
            return Ext.create('Ext.menu.Menu', {
                plain: true,
                mouseLeaveDelay: 10,
                items: [],
            });
    },

    getSendClass(v, meta, rec) {
        if (parseInt(rec.get('status').order_sheet) === 0 || rec.get('status').order_sheet === null) {
            return 'fas fa-paper-plane';
        } if (parseInt(rec.get('status').order_sheet) === 1) {
            return 'fas fa-paper-plane text-blue';
        }
        return '';
    },

    onDeleteButtonClick(btn) {
        const gridPanel = this.view;
        const record = gridPanel.getSelectionModel().getSelection()[0];
        Ext.create({
            xtype: 'DeleteDonDatHang',
            recordView: record,
            gridView: gridPanel,
        });
    },

    onUpdateButtonClick(btn) {
        const controller = this;
            const view_panel = controller.view;
            const recordView = view_panel.getSelectionModel().getSelection()[0];
            const { mainView } = Ext.getCmp('mainViewPort').getReferences();
            const frame_type = recordView.get('frame_type');
        if (frame_type !== null && frame_type !== '') {
            if (recordView) {
                Ext.create({
                    xtype: 'UploadDonDatHang',
                    title: `<b>Cập nhật đơn đặt hàng ${recordView.get('sub_no')} - Loại cửa ${frame_type.toUpperCase()}</b>`,
                    doorType: frame_type,
                    gridView: view_panel,
                    recordView,
                });
                mainView.remove(Ext.getCmp(`ViewDonDatHang${recordView.data.id}-TabPanel`));
            }
        }
    },

    onCancelButtonClick() {
        Ext.create({
            xtype: 'CancelDonDatHang',
        });
    },

    onSendButtonClick() {
        const gridPanel = this.view;
        const record = gridPanel.getSelectionModel().getSelection()[0];
        Ext.create({
            xtype: 'SendDonDatHang',
            recordData: record,
            gridPanel,
        });
    },

    onSendButtonClickGrid(grid, rowIndex, colIndex) {
        const gridPanel = this.view;
        const record = gridPanel.getStore().getAt(rowIndex);
        Ext.create({
            xtype: 'SendDonDatHang',
            recordData: record,
            gridPanel,
        });
    },
    onCreateDoorButtonClick(btn) {
        const gridPanel = this.view;
        Ext.create({
            xtype: 'UploadDonDatHang',
            title: `<b>TẢI LÊN ORDER SHEET DÀNH CHO LOẠI - ${btn.text.toUpperCase()}</b>`,
            doorType: btn.doortype,
            gridView: gridPanel,
        });
    },

    onViewButtonClick() {
        if (ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'get') === true) {
            const gridPanel = this.view;
            const record = gridPanel.getSelectionModel().getSelection()[0];
            const { mainView } = Ext.getCmp('mainViewPort').getReferences();
            if (record) {
                ClassMain.showLoadingMask();
                if (Ext.getCmp(`ViewDonDatHang${record.data.id}-TabPanel`) == null) {
                    Ext.Ajax.request({
                        url: `/api/ordersheet/get/${record.data.id}`,
                        method: 'GET',
                        success(result, request) {
                            try {
                                const response = Ext.decode(result.responseText);
                                if (response.success) {
                                    const tab = mainView.add({
                                        xtype: 'ViewDonDatHang',
                                        recordView: record,
                                        gridView: gridPanel,
                                        dataObject: response.data,
                                        closable: true,
                                        doorType: record.data.frame_type,
                                        iconCls: 'fas fa-file-invoice-dollar',
                                        title: `<b>ĐƠN HÀNG: ${record.data.sub_no.toUpperCase()}</b>`,
                                        id: `ViewDonDatHang${record.data.id}-TabPanel`,
                                        itemId: `ViewDonDatHang${record.data.id}-TabPanel`,
                                        listeners: {
                                            render() {
                                                ClassMain.hideLoadingMask();
                                            },
                                        },
                                    });
                                    mainView.setActiveTab(tab);
                                } else {
                                    ClassMain.hideLoadingMask();
                                    Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                                }
                            } catch (err) {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                            }
                        },
                        failure(result, request) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                        },
                    });
                } else {
                    mainView.setActiveTab(Ext.getCmp(`ViewDonDatHang${record.data.id}-TabPanel`));
                    ClassMain.hideLoadingMask();
                }
            }
        } else {
            Ext.MessageBox.alert('Thông báo', 'Bạn không có quyền thực hiện chức năng!', () => {});
        }
    },

    onReloadButtonClick() {
        const controller = this;
        controller.loadDataGrid();
    },

    onExportdButtonClick() {
        const controller = this;
            const view_panel = controller.view;
            const recordView = view_panel.getSelectionModel().getSelection()[0];

        if (recordView) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', (btn, text) => {
                if (btn === 'yes') {
                    const url = `api/ordersheet/download/${recordView.data.id}`;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btn,
                    });
                    ClassMain.saveFile(url, `${recordView.data.sub_no}.zip`);
                }
            });
        }
    },

    onExportGriddButtonClick() {

    },

    onViewButtonClickGrid(gridPanel, rowIndex, colIndex) {
        if (ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'get') === true) {
            const record = gridPanel.getStore().getAt(rowIndex);
            const { mainView } = Ext.getCmp('mainViewPort').getReferences();
            if (record) {
                ClassMain.showLoadingMask();
                Ext.Ajax.request({
                    url: `/api/ordersheet/get/${record.data.id}`,
                    method: 'GET',
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                if (Ext.getCmp(`ViewDonDatHang${response.data.id}-TabPanel`) == null) {
                                    const tab = mainView.add({
                                        xtype: 'ViewDonDatHang',
                                        recordView: record,
                                        gridView: gridPanel,
                                        dataObject: response.data,
                                        doorType: record.data.frame_type,
                                        closable: true,
                                        iconCls: 'fas fa-file-invoice-dollar',
                                        title: `<b>ĐƠN HÀNG: ${record.data.sub_no.toUpperCase()}</b>`,
                                        id: `ViewDonDatHang${record.data.id}-TabPanel`,
                                        listeners: {
                                            render(panel) {
                                                ClassMain.hideLoadingMask();
                                            },
                                        },
                                    });
                                    mainView.setActiveTab(tab);
                                } else {
                                    mainView.setActiveTab(Ext.getCmp(`ViewDonDatHang${record.data.id}-TabPanel`));
                                    ClassMain.hideLoadingMask();
                                }
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                        }
                    },
                    failure(result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                    },
                });
            }
        } else {
            Ext.MessageBox.alert('Thông báo', 'Bạn không có quyền thực hiện chức năng!', () => {});
        }
    },

    onChangeModeViewByTime() {
        const controller = this;
        const refs = controller.getReferences();
        const grid = controller.getView();
        grid.getStore().setGroupField(undefined);
        controller.loadDataGrid();
    },

    onChangeModeViewByGroup() {
        const controller = this;
        const refs = controller.getReferences();
        const grid = controller.getView();
        grid.getStore().setGroupField('project_name');
        controller.loadDataGrid();
    },

    loadDataGrid() {
        const controller = this;
        const refs = controller.getReferences();
        const gird = controller.getView();
        const { SearchField } = refs;
        const { FilterMenu } = refs;
        const refs_FilterMenu = FilterMenu.getMenu().getReferences();
        let txt_search = '';
        const arr_filter = [];
        let txt_filter = '';
        if (FilterMenu) {
            for (const ref in refs_FilterMenu) {
                if (refs_FilterMenu[ref].getValue() !== '' && refs_FilterMenu[ref].getValue() !== null && refs_FilterMenu[ref].fieldIndex) {
                    arr_filter.push(`${refs_FilterMenu[ref].fieldIndex}:eq_${refs_FilterMenu[ref].getValue()}`);
                }
            }
        }
        txt_filter = arr_filter.join(',');
        if (SearchField) {
            if (SearchField.getValue() !== '' && SearchField.getValue() !== null) {
                txt_search = SearchField.getValue();
            }
        }
        if (gird.filterString) {
            txt_search = gird.filterString;
        }
        if (gird) {
            if (txt_filter !== '') {
                gird.getStore().getProxy().setExtraParam('filter', txt_filter);
            } else {
                delete gird.getStore().getProxy().extraParams.filter;
            }
            if (txt_search !== '') {
                gird.getStore().getProxy().setExtraParam('query', txt_search);
            } else {
                delete gird.getStore().getProxy().extraParams.query;
            }
            gird.getStore().load();
            gird.getSelectionModel().deselectAll();
        }
    },
});
