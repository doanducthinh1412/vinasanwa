Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.QuanLyDonDatHangModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.QuanLyDonDatHang',
    stores: {
        quanLyDonDatHangStore: {
            type: 'QuanLyDonDatHangStore'
        }
    },
    data: {
        actionMenu: Ext.create('Ext.menu.Menu', {
            plain: true,
            mouseLeaveDelay: 10,
            items: []
        })
    }
});