Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.ViewDonDatHangModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.ViewDonDatHang',
    data: {
        dataPanel: null,
        dataOldPanel: null,
        selectedFile: null,
        workflow: {
            ve: {
                code: '',
                status: 'Chưa thực hiện',
                color: '#e13345',
                time: ''
            },
            tinhgia: {
                code: '',
                status: 'Chưa thực hiện',
                color: '#e13345',
                time: ''
            },
            muahang: {
                code: '',
                status: 'Chưa thực hiện',
                color: '#e13345',
                time: ''
            },
            kehoach: {
                code: '',
                status: 'Chưa thực hiện',
                color: '#e13345',
                time: ''
            },
            sanxuat: {
                code: '',
                status: 'Chưa thực hiện',
                color: '#e13345',
                time: ''
            },
            kcs: {
                code: '',
                status: 'Chưa thực hiện',
                color: '#e13345',
                time: ''
            },
            giaohang: {
                code: '',
                status: 'Chưa thực hiện',
                color: '#e13345',
                time: ''
            },
            kho: {
                code: '',
                status: 'Chưa thực hiện',
                color: '#e13345',
                time: ''
            }
        }
    }
});