/* eslint-disable max-len */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPKeHoach.CreateDonDatHang.CreateDonDatHangController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.CreateDonDatHang',
    onExcelButtonClick(field, value) {
        const controller = this;
        const view_panel = controller.view;
        const file = field.fileInputEl.dom.files[0];
        const data = new FormData();
        const refs = view_panel.getReferences();
        const { comboboxDuAn } = refs;
        const { ComboboxLoaiDon } = refs;
        const { ComboboxDoUuTien } = refs;
        const order_type = ComboboxLoaiDon.getValue();
        const priority = ComboboxDoUuTien.getValue();
        if (file) {
            if (file.type === 'application/vnd.ms-excel') {
                // controller.getViewModel().set('dataPanel', null);
                data.append('order_sheet_import', file);
                Ext.MessageBox.show({
                    title: 'Xin chờ',
                    msg: 'Đang nhập liệu đơn đặt hàng từ Excel...',
                    progressText: 'Đang xử lý...',
                    width: 300,
                    wait: {
                        interval: 200,
                    },
                });
                Ext.Ajax.request({
                    url: '/api/ordersheet/import',
                    rawData: data,
                    headers: { 'Content-Type': null },
                    success(result, request) {
                        const fielz = field;
                        fielz.fileInputEl.dom.value = '';
                        Ext.MessageBox.hide();
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                response.data.orderSheet1.generalInfo.order_type = order_type;
                                response.data.orderSheet1.generalInfo.priority = priority;
                                controller.getViewModel().set('dataPanel', response.data);
                                const record_prj = controller.getViewModel().getStore('DanhMucDuAnStore').findRecord('name', response.data.orderSheet1.generalInfo.pj_name);
                                if (record_prj) {
                                    controller.getViewModel().set('dataPanel.orderSheet1.generalInfo.project_id', record_prj.data.id);
                                } else {
                                    Ext.MessageBox.confirm('Thông báo', `Dự án <b>${response.data.orderSheet1.generalInfo.pj_name}</b> chưa tồn tại. Bạn có muốn tạo mới?`, (btn) => {
                                        if (btn === 'yes') {
                                            Ext.create('Ext.window.Window', {
                                                title: 'Tạo mới dự án',
                                                height: 520,
                                                width: 550,
                                                layout: 'border',
                                                border: false,
                                                maximizable: true,
                                                glyph: 'xf055@FontAwesome',
                                                modal: true,
                                                items: [{
                                                    xtype: 'CreateDuAn',
                                                    region: 'center',
                                                    autoScroll: true,
                                                    storeParent: comboboxDuAn.getStore(),
                                                    relationComponent: comboboxDuAn,
                                                    relationData: {
                                                        name: response.data.orderSheet1.generalInfo.pj_name,
                                                        code: response.data.orderSheet1.generalInfo.pj_name,
                                                    },
                                                }],
                                            }).show();
                                        } else {
                                            comboboxDuAn.focus();
                                        }
                                    });
                                }
                            } else if (response.statusCode === 409) {
                                Ext.MessageBox.alert('Thông báo', 'Đơn đặt hàng đã tồn tại. Vui lòng kiểm tra lại!', () => {});
                            } else {
                                Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại File Excel nhập liệu đơn đặt hàng', () => {});
                            }
                        } catch (err) {
                            console.log(err);
                            Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại File Excel nhập liệu đơn đặt hàng', () => {});
                        }
                    },
                    failure(result, request) {
                        const fielz = field;
                        console.log(result);
                        fielz.fileInputEl.dom.value = '';
                        Ext.MessageBox.hide();
                        Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại File Excel nhập liệu đơn đặt hàng', () => {});
                    },
                });
            } else {
                Ext.MessageBox.alert('Thông báo', 'Định dạng File không hỗ trợ. Vui lòng kiểm tra lại!', () => {});
            }
        }
    },
    onItemKeydown(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        const controller = this;
        if (e.getKey() === e.DELETE) {
            const selModelSelected = grid.getSelectionModel().getSelected();
            if (selModelSelected.startCell) {
                const field = selModelSelected.startCell.column.dataIndex;
                record.set(field, '');
            }
        }
    },
    onChangeProject(combo, newValue, oldValue, eOpts) {
        const controller = this;
        const receive_no = controller.getViewModel().get('dataPanel.orderSheet1.generalInfo.receive_no');
        if (newValue !== null && newValue !== '') {
            Ext.Ajax.request({
                url: `/api/projects/check-receive-no?id=${newValue}&receive_no=${receive_no}`,
                method: 'GET',
                success(result, request) {
                    try {
                        const response = Ext.decode(result.responseText);
                        if (response.success) {
                            if (response.statusCode === -1) {
                                Ext.MessageBox.alert('Thông báo', `Receive No: <b>${receive_no}</b> chưa tồn tại`, () => {});
                            } else if (response.statusCode === 1) {
                                /* Ext.MessageBox.alert('Thông báo', 'Receive No: <b>' + receive_no + '</b> đã tồn tại', function() {
                                    controller.getViewModel().set('dataPanel.orderSheet1.generalInfo.receive_no', '');
                                }); */
                            }
                        } else {
                            Ext.MessageBox.alert('Thông báo', `Mã Receive No: <b>${receive_no}</b> không đúng. Vui lòng kiểm tra lại!`, () => {
                                controller.getViewModel().set('dataPanel.orderSheet1.generalInfo.receive_no', '');
                            });
                        }
                    } catch (err) {
                        Ext.MessageBox.alert('Thông báo', `Mã Receive No: <b>${receive_no}</b> không đúng. Vui lòng kiểm tra lại!`, () => {
                            controller.getViewModel().set('dataPanel.orderSheet1.generalInfo.receive_no', '');
                        });
                    }
                },
                failure(result, request) {
                    Ext.MessageBox.alert('Thông báo', `Mã Receive No: <b>${receive_no}</b> không đúng. Vui lòng kiểm tra lại!`, () => {
                        controller.getViewModel().set('dataPanel.orderSheet1.generalInfo.receive_no', '');
                    });
                },
            });
        }
    },
    onReloadButtonClick(btn) {
        const controller = this;
        console.log(controller.getViewModel().getData());
        /* controller.getViewModel().set('dataPanel', null);
        controller.getViewModel().set('dataPanel.orderSheet1.generalInfo.order_type', '1');
        controller.getViewModel().set('dataPanel.orderSheet1.generalInfo.priority', '1');
        controller.getViewModel().getData().SoLuongChiTiet.loadData([]);
        controller.getViewModel().getData().SoLuongCuaDatHang.loadData([]);
        controller.getViewModel().getData().hwtype.loadData([]);
        controller.getViewModel().getData().changeHistory.loadData([]); */
    },
    onCreateButtonClick(btn) {
        try {
            const controller = this;
            const view_panel = controller.view;
            const recordView = view_panel.getRecordView();
            const refs = controller.getReferences();
            const dataForm = new FormData();
            const jsonOderSheet = controller.getViewModel().getData().dataPanel;
            const dataFile = view_panel.getDataFile();
            const gridView = view_panel.getGridView();
            const { mainView } = Ext.getCmp('mainViewPort').getReferences();
            if (dataFile) {
                dataFile.each((record) => {
                    if (record.data.type === 'banve') {
                        dataForm.append('banvethietke[]', record.data.file);
                    } else if (record.data.type === 'phu+chitiet') {
                        dataForm.append('order_sheet_import', record.data.file);
                    } else if (record.data.type === 'phu') {
                        dataForm.append('order_sheet_import', record.data.file);
                    } else if (record.data.type === 'chitiet') {
                        dataForm.append('order_sheet_details[]', record.data.file);
                    }
                });
            }
            for (let i = 0; i < jsonOderSheet.orderSheet1.detailInfo.detailInfoType.length; i += 1) {
                delete jsonOderSheet.orderSheet1.detailInfo.detailInfoType[i].id;
            }
            for (let i = 0; i < jsonOderSheet.orderSheet1.detailInfo.detailInfoHwType.length; i += 1) {
                delete jsonOderSheet.orderSheet1.detailInfo.detailInfoHwType[i].id;
            }

            dataForm.append('ordersheet', Ext.encode(jsonOderSheet));
            Ext.MessageBox.show({
                title: 'Xin chờ',
                msg: recordView === null ? 'Đang tạo mới đơn đặt hàng...' : 'Đang cập nhật đơn đặt hàng...',
                progressText: 'Đang xử lý...',
                width: 300,
                wait: {
                    interval: 200,
                },
            });
            Ext.Ajax.request({
                url: recordView === null ? '/api/ordersheet/create' : `/api/ordersheet/create/${recordView.get('id')}`,
                method: 'POST',
                rawData: dataForm,
                headers: { 'Content-Type': null },
                success(result, request) {
                    Ext.MessageBox.close();
                    try {
                        const response = Ext.decode(result.responseText);
                        if (response.success) {
                            /* if (gridView) {
                                gridView.getStore().load();
                            } */
                            Ext.MessageBox.alert('Thông báo', recordView === null ? 'Tạo mới đơn đặt hàng thành công!' : 'Cập nhật đơn đặt hàng thành công!', () => {
                                if (Ext.getCmp('quanlydondathang-TabPanel')) {
                                    mainView.setActiveTab(Ext.getCmp('quanlydondathang-TabPanel'));
                                    Ext.getCmp('quanlydondathang-TabPanel').down('grid').getStore().load();
                                }
                                view_panel.close();
                            });
                        } else if (response.statusCode === 409) {
                            Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b>${jsonOderSheet.orderSheet1.generalInfo.sub_no}</b> đã tồn tại. Vui lòng kiểm tra lại!`, () => {});
                        } else {
                            Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                        }
                    } catch (err) {
                        Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                    }
                },
                failure(result, request) {
                    Ext.MessageBox.hide();
                    Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
                },
            });
        } catch (err) {
            console.log(err);
            Ext.MessageBox.alert('Thông báo', 'Kiểm tra lại dữ liệu đơn đặt hàng', () => {});
        }
    },
    onSelectLoaiDon(combo, record) {
        const controller = this;
        const view_panel = controller.view;
        const refs = controller.getReferences();
        const { ComboboxLoaiDon } = refs;
        if (record.data.val === '02') {
            Ext.create('Ext.window.Window', {
                title: 'Đơn đặt hàng chỉnh sửa',
                height: 500,
                width: 500,
                minWidth: 300,
                minHeight: 250,
                layout: 'fit',
                border: false,
                maximizable: true,
                iconCls: 'far fa-edit',
                modal: true,
                buttons: ['->', {
                    text: 'Đồng ý',
                    ui: 'soft-orange',
                }, {
                    text: 'Thoát',
                    glyph: 'xf00d@FontAwesome',
                    handler() {
                        this.up('window').close();
                        ComboboxLoaiDon.setValue('1');
                    },
                }, '->'],
                items: [{
                    xtype: 'form',
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelWidth: 120,
                    },
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                    },
                    bodyPadding: 25,
                    border: false,
                    items: [{
                        fieldLabel: 'Nhập mã Đơn đặt hàng chỉnh sửa',
                        xtype: 'fieldcontainer',
                        labelAlign: 'top',
                        layout: 'hbox',
                        labelStyle: 'font-weight:bold;',
                        defaultType: 'textfield',
                        items: [{
                            fieldLabel: '',
                            hideLabel: true,
                            name: 'code',
                            flex: 1,
                        }, {
                            xtype: 'button',
                            text: 'Kiểm tra',
                            width: 120,
                            iconCls: 'fas fa-search',
                        }],
                    }, {
                        xtype: 'fieldset',
                        title: 'THÔNG TIN ĐƠN ĐẶT HÀNG',
                        layout: {
                            type: 'vbox',
                            align: 'stretch',
                        },
                        defaultType: 'textfield',
                        items: [{
                            fieldLabel: '<b>Receive No.</b>',
                        }, {
                            fieldLabel: '<b>Building type</b>',
                        }, {
                            fieldLabel: '<b>Sub No.</b>',
                        }, {
                            fieldLabel: '<b>Project Name</b>',
                        }],
                    }],
                }],
            }).show();
        } else if (record.data.val === '03') {
            Ext.create('Ext.window.Window', {
                title: 'Đơn đặt hàng sản xuất lại',
                height: 500,
                width: 500,
                minWidth: 300,
                minHeight: 250,
                layout: 'fit',
                border: false,
                maximizable: true,
                iconCls: 'far fa-edit',
                modal: true,
                buttons: ['->', {
                    text: 'Đồng ý',
                    ui: 'soft-orange',
                }, {
                    text: 'Thoát',
                    glyph: 'xf00d@FontAwesome',
                    handler() {
                        this.up('window').close();
                        ComboboxLoaiDon.setValue('1');
                    },
                }, '->'],
                items: [{
                    xtype: 'form',
                    defaultType: 'textfield',
                    fieldDefaults: {
                        labelWidth: 120,
                    },
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                    },
                    bodyPadding: 25,
                    border: false,
                    items: [{
                        fieldLabel: 'Nhập mã Đơn đặt hàng sản xuất lại',
                        xtype: 'fieldcontainer',
                        labelAlign: 'top',
                        layout: 'hbox',
                        labelStyle: 'font-weight:bold;',
                        defaultType: 'textfield',
                        items: [{
                            fieldLabel: '',
                            hideLabel: true,
                            name: 'code',
                            flex: 1,
                        }, {
                            xtype: 'button',
                            text: 'Kiểm tra',
                            width: 120,
                            iconCls: 'fas fa-search',
                        }],
                    }, {
                        xtype: 'fieldset',
                        title: 'THÔNG TIN ĐƠN ĐẶT HÀNG',
                        layout: {
                            type: 'vbox',
                            align: 'stretch',
                        },
                        defaultType: 'textfield',
                        items: [{
                            fieldLabel: '<b>Receive No.</b>',
                        }, {
                            fieldLabel: '<b>Building type</b>',
                        }, {
                            fieldLabel: '<b>Sub No.</b>',
                        }, {
                            fieldLabel: '<b>Project Name</b>',
                        }],
                    }],
                }],
            }).show();
        }
    },
});