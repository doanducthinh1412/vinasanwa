Ext.define('CMS.view.Dashboard.DashboardModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.dashboard',
    data: {
        name: 'CMS',
        dataDashboard: [],
        orderSheet: 0,
        users: 0
    }
});