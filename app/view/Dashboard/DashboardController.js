/* eslint-disable no-unused-vars */
Ext.define('CMS.view.Dashboard.DashboardController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.dashboard',
    loadDataPanel() {
        const controller = this;
        const view = controller.getView();
        if (ClassMain.checkDepartment('kehoach') === true) {
            Ext.Ajax.request({
                url: '/api/dashboard/quan-ly-khsx',
                method: 'GET',
                timeout: 30000,
                success(result, request) {
                    const response = Ext.decode(result.responseText);
                    controller.getViewModel().data.dataDashboard = response.data;
                    const refs = controller.getReferences();
                    const { newTodayPanel } = refs;
                    newTodayPanel.getViewModel().set('orderSheet', {
                        amount: response.data.orderSheet,
                        icon: 'chart-line',
                        iconColor: '#167abc',
                    });
                    const { returningTodayPanel } = refs;
                    returningTodayPanel.getViewModel().set('today_production_schedule', {
                        amount: response.data.today_production_schedule.processing,
                        icon: 'calendar-alt',
                        iconColor: '#167abc',
                    });
                    const { totalClientPanel } = refs;
                    totalClientPanel.getViewModel().set('total_user', {
                        amount: response.data.total_user,
                        icon: 'users',
                        iconColor: '#167abc',
                    });
                },
                failure(result, request) {
                    console.log(result);
                },
            });
        } else if (ClassMain.checkDepartment('ve') === true) {
            if (ClassMain.checkPermission('dashboard', 'WorkingDrawingDashboard', 'quanlyVe') === true) {
                Ext.Ajax.request({
                    url: '/api/dashboard/quan-ly-ve',
                    method: 'GET',
                    timeout: 30000,
                    success(result, request) {
                        const response = Ext.decode(result.responseText);
                        controller.getViewModel().data.dataDashboard = response.data;
                        const refs = controller.getReferences();
                        const { newTodayPanel } = refs;
                        newTodayPanel.getViewModel().set('new_receive_order_sheet', {
                            amount: response.data.new_receive_order_sheet,
                            icon: 'chart-line',
                            iconColor: '#167abc',
                        });
                        const { assignmentProcessPanel } = refs;
                        assignmentProcessPanel.getViewModel().set('assignment_process', {
                            amount: response.data.assignment_process,
                            icon: 'calendar-alt',
                            iconColor: '#167abc',
                        });
                        const { assignmentDonePanel } = refs;
                        assignmentDonePanel.getViewModel().set('assignment_done', {
                            amount: response.data.assignment_done,
                            icon: 'calendar-alt',
                            iconColor: '#167abc',
                        });
                        const { totalClientPanel } = refs;
                        totalClientPanel.getViewModel().set('total_user', {
                            amount: response.data.total_user,
                            icon: 'users',
                            iconColor: '#167abc',
                        });
                    },
                    failure(result, request) {
                        console.log(result);
                    },
                });
            } else if (ClassMain.checkPermission('dashboard', 'WorkingDrawingDashboard', 'nhanvienVe') === true) {
                Ext.Ajax.request({
                    url: '/api/dashboard/nhan-vien-ve',
                    method: 'GET',
                    timeout: 30000,
                    success(result, request) {
                        const response = Ext.decode(result.responseText);
                        controller.getViewModel().data.dataDashboard = response.data;
                        const refs = controller.getReferences();
                        const { newAssignemtPanel } = refs;
                        newAssignemtPanel.getViewModel().set('new_assignment', {
                            amount: response.data.new_assignment,
                            icon: 'chart-line',
                            iconColor: '#167abc',
                        });
                        const { assignmentProcessPanel } = refs;
                        assignmentProcessPanel.getViewModel().set('assignment_process', {
                            amount: response.data.assignment_process,
                            icon: 'calendar-alt',
                            iconColor: '#167abc',
                        });
                        const { assignmentDonePanel } = refs;
                        assignmentDonePanel.getViewModel().set('assignment_done', {
                            amount: response.data.assignment_done,
                            icon: 'calendar-alt',
                            iconColor: '#167abc',
                        });
                        const { totalClientPanel } = refs;
                        totalClientPanel.getViewModel().set('total_user', {
                            amount: response.data.total_user,
                            icon: 'users',
                            iconColor: '#167abc',
                        });
                    },
                    failure(result, request) {
                        console.log(result);
                    },
                });
            }
        }
    },
});