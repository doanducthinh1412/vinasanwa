Ext.define('CMS.view.BPVe.WorkingDrawing.WorkingDrawingModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.WorkingDrawing',
    stores: {
        WorkingDrawingStore: {
            type: 'WorkingDrawingStore'
        }
    },
    data: {
        actionMenu: Ext.create('Ext.menu.Menu', {
            plain: true,
            mouseLeaveDelay: 10,
            items: []
        })
    }
});