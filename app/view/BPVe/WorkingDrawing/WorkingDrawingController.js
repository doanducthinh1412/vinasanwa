/* eslint-disable no-console */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable radix */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPVe.WorkingDrawing.WorkingDrawingController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.WorkingDrawing',
    onItemActionMenu(view, record, e) {
        const controller = this;
        const gridPanel = controller.view;
        const menu = controller.createMenu(record);
        controller.getViewModel().set('actionMenu', menu);
    },
    onItemContextMenu(view, record, item, index, e) {
        const controller = this;
        const gridPanel = controller.view;
        const position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        const menu = controller.createMenu(record);
        menu.showAt(position);
    },

    createMenu(record) {
        const controller = this;
        if (record) {
            return Ext.create('Ext.menu.Menu', {
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'Xem bản vẽ',
                    iconCls: 'far fa-eye text-dark-blue',
                    handler() {
                        controller.onViewButtonClick();
                    },
                }, {
                    text: 'Tải bản vẽ',
                    iconCls: 'fas fa-download text-dark-blue',
                    handler() {
                        controller.onExportdButtonClick();
                    },
                }, '-', {
                    text: 'Gửi kết quả vẽ',
                    iconCls: 'fas fa-paper-plane text-dark-blue',
                    handler() {
                        controller.onSenddButtonClick();
                    },
                }],
            });
        }
        return Ext.create('Ext.menu.Menu', {
            plain: true,
            mouseLeaveDelay: 10,
            items: [],
        });
    },

    onViewButtonClick() {
        const controller = this;
        const view_panel = controller.view;
        const recordView = view_panel.getSelectionModel().getSelection()[0];
        if (recordView) {
            controller.showBanVePdf(recordView);
        }
    },

    onViewButtonClickGrid(gridPanel, rowIndex, colIndex) {
        const controller = this;
        const view_panel = controller.view;
        const recordView = gridPanel.getStore().getAt(rowIndex);
        if (recordView) {
            controller.showBanVePdf(recordView);
        }
    },

    showBanVePdf(record) {
        if (record) {
            const windows_panel = Ext.create('Ext.window.Window', {
                title: `Xem bản vẽ - NOTE: ${record.data.note} - ${record.data.lot}`,
                height: '90%',
                width: '95%',
                minWidth: 800,
                minHeight: 600,
                layout: 'border',
                border: false,
                maximizable: true,
                iconCls: 'far fa-file-pdf',
                modal: true,
                items: [{
                    region: 'center',
                    width: '100%',
                    xtype: 'ViewPDF',
                    layout: 'fit',
                    reference: 'ViewPDF',
                    urlPdf: `/api/drawing/final-result/${record.data.id}`,
                }],
            }).show();
        }
    },

    onReloadButtonClick() {
        const controller = this;
        controller.loadDataGrid();
    },

    onExportdButtonClick(btn) {
        const controller = this;
        const view_panel = controller.view;
        const recordView = view_panel.getSelectionModel().getSelection()[0];
        let p_ext = 0;
        if (recordView.data.drawing_path) {
            p_ext = recordView.data.drawing_path.lastIndexOf('.');
        }
        if (recordView) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', (btnc, text) => {
                if (btnc === 'yes') {
                    const url = `/api/drawing/final-result/${recordView.data.id}`;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btnc,
                    });
                    ClassMain.saveFile(url, `${recordView.data.drawing_path.substring(0, p_ext)}.pdf`);
                }
            });
        }
    },

    onExportdButtonClickGrid(gridPanel, rowIndex, colIndex) {
        const controller = this;
        const view_panel = controller.view;
        const recordView = gridPanel.getStore().getAt(rowIndex);
        let p_ext = 0;
        if (recordView.data.drawing_path) {
            p_ext = recordView.data.drawing_path.lastIndexOf('.');
        }
        if (recordView) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', (btn, text) => {
                if (btn === 'yes') {
                    const url = `/api/drawing/final-result/${recordView.data.id}`;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btn,
                    });
                    ClassMain.saveFile(url, `${recordView.data.drawing_path.substring(0, p_ext)}.pdf`);
                }
            });
        }
    },

    onSenddButtonClick(btn) {
        const controller = this;
        const view_panel = controller.view;
        const record = view_panel.getSelectionModel().getSelection()[0];
        if (record) {
            if (ClassMain.checkPermission('quan_ly_ban_ve', 'Drawing', 'sendKHSX') === false) {
                Ext.MessageBox.alert('Thông báo', 'Bạn không có quyền gửi bản vẽ. Vui lòng liên hệ quản trị!', () => {});
            } else if (parseInt(record.get('status')) === 1 && ClassMain.checkPermission('quan_ly_ban_ve', 'Drawing', 'sendKHSX') === true) {
                Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn gửi kết quả bản vẽ sang bộ phận Kế hoạch?', (btnc, text) => {
                    if (btnc === 'yes') {
                        Ext.Ajax.request({
                            url: `/api/drawing/send/${record.data.id}`,
                            method: 'GET',
                            success(result, request) {
                                try {
                                    const response = Ext.decode(result.responseText);
                                    if (response.success === true) {
                                        controller.loadDataGrid();
                                        Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ thành công', () => {});
                                    } else if (response.message) {
                                        Ext.MessageBox.alert('Thông báo', response.message, () => {});
                                    } else {
                                        Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ không thành công', () => {});
                                    }
                                } catch (err) {
                                    console.log(err);
                                    Ext.MessageBox.alert('Thông báo', `Gửi kết quả vẽ không thành công<br>${err.toString()}`, () => {});
                                }
                            },
                            failure(result, request) {
                                Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ không thành công', () => {});
                            },
                        });
                    }
                });
            } else if (parseInt(record.get('status')) === 2) {
                Ext.MessageBox.alert('Thông báo', 'Bản vẽ đã được gửi sang bộ phận Kế hoạch', () => {});
            } else {
                Ext.MessageBox.alert('Thông báo', 'Bản vẽ chưa được phê duyệt. Vui lòng kiểm tra lại!', () => {});
            }
        }
    },

    onSenddButtonClickGrid(gridPanel, rowIndex, colIndex) {
        const controller = this;
        const view_panel = controller.view;
        const record = gridPanel.getStore().getAt(rowIndex);
        if (record) {
            if (ClassMain.checkPermission('quan_ly_ban_ve', 'Drawing', 'sendKHSX') === false) {
                Ext.MessageBox.alert('Thông báo', 'Bạn không có quyền gửi bản vẽ. Vui lòng liên hệ quản trị!', () => {});
            } else if (parseInt(record.get('status')) === 1) {
                Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn gửi kết quả bản vẽ sang bộ phận Kế hoạch?', (btn, text) => {
                    if (btn === 'yes') {
                        Ext.Ajax.request({
                            url: `/api/drawing/send/${record.data.id}`,
                            method: 'GET',
                            success(result, request) {
                                try {
                                    const response = Ext.decode(result.responseText);
                                    if (response.success === true) {
                                        controller.loadDataGrid();
                                        Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ thành công', () => {});
                                    } else if (response.message) {
                                        Ext.MessageBox.alert('Thông báo', response.message, () => {});
                                    } else {
                                        Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ không thành công', () => {});
                                    }
                                } catch (err) {
                                    console.log(err);
                                    Ext.MessageBox.alert('Thông báo', `Gửi kết quả vẽ không thành công<br>${err.toString()}`, () => {});
                                }
                            },
                            failure(result, request) {
                                Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ không thành công', () => {});
                            },
                        });
                    }
                });
            } else if (parseInt(record.get('status')) === 2) {
                Ext.MessageBox.alert('Thông báo', 'Bản vẽ đã được gửi sang bộ phận Kế hoạch', () => {});
            } else {
                Ext.MessageBox.alert('Thông báo', 'Bản vẽ chưa được phê duyệt. Vui lòng kiểm tra lại!', () => {});
            }
        }
    },

    getSendActionClass(v, meta, rec) {
        if (parseInt(rec.get('status')) === 1) {
            return 'far fa-paper-plane';
        }
        if (parseInt(rec.get('status')) === 2) {
            return 'fas fa-paper-plane text-blue';
        }
        return '';
    },

    renderNumberColumn(value, meta, rec, rowIndex, colIndex, view) {
        if (value === 0 || value === null) {
            return '';
        }
        return value;
    },

    onExportGriddButtonClick() {

    },

    onSelectFilterBanVe(combo, record, eOpts) {
        const controller = this;
        const refs = controller.getReferences();
        controller.loadDataGrid();
    },

    loadDataGrid() {
        const controller = this;
        const refs = controller.getReferences();
        const gird = controller.getView();
        const { SearchField } = refs;
        const { ComboboxFilterBanVe } = refs;
        let txt_search = '';
        let txt_filter = '';
        if (SearchField) {
            if (SearchField.getValue() !== '' && SearchField.getValue() !== null) {
                txt_search = SearchField.getValue();
            }
        }
        if (ComboboxFilterBanVe) {
            if (ComboboxFilterBanVe.getValue() !== '' && ComboboxFilterBanVe.getValue() !== null) {
                txt_filter = ComboboxFilterBanVe.getValue();
            }
        }
        if (gird) {
            if (txt_filter !== '') {
                gird.getStore().getProxy().setExtraParam('filter', txt_filter);
            } else {
                delete gird.getStore().getProxy().extraParams.filter;
            }
            if (txt_search !== '') {
                gird.getStore().getProxy().setExtraParam('query', txt_search);
            } else {
                delete gird.getStore().getProxy().extraParams.query;
            }
            gird.getStore().load();
        }
    },

    getSendTip(v, meta, rec) {
        if (parseInt(rec.get('status')) === 0 || rec.get('status') === null) {
            return 'Chưa phê duyệt bản vẽ';
        }
        if (parseInt(rec.get('status')) === 1) {
            return 'Chưa gửi sang bộ phận kế hoạch';
        }
        if (parseInt(rec.get('status')) === 2) {
            return 'Đã gửi sang bộ phận kế hoạch';
        }
        return false;
    },
});