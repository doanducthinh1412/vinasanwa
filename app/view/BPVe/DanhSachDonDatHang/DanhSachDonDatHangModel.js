Ext.define('CMS.view.BPVe.DanhSachDonDatHang.DanhSachDonDatHangModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.DanhSachDonDatHang',
    stores: {
        quanLyDonDatHangStore: {
            type: 'DanhSachDonDatHangStore'
        }
    },
    data: {
        actionMenu: Ext.create('Ext.menu.Menu', {
            plain: true,
            mouseLeaveDelay: 10,
            items: []
        })
    }
});