Ext.define('CMS.view.BPVe.PhanCongTheoDoi.KetQuaVeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.KetQuaVe',

    onClickCreateBanVeButton: function() {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var frm = FormCreateBanVe.getForm();
        if (frm.isValid()) {
            Ext.MessageBox.show({
                title: 'Xin chờ',
                msg: 'Đang cập nhật kết quả...',
                progressText: 'Đang xử lý...',
                width: 300,
                wait: {
                    interval: 200
                }
            });
            frm.submit({
                url: '/api/drawing/create',
                clientValidation: true,
                method: 'POST',
                success: function(form, action) {
                    Ext.MessageBox.hide();
                    FormCreateBanVe.reset();
                    GridListKetQuaBanVe.getStore().load({
                        callback: function() {}
                    });
                    if (Ext.getCmp('phancongtheodoi-TabPanel')) {
                        Ext.getCmp('phancongtheodoi-TabPanel').down("grid").getStore().load();
                    }
                },
                failure: function(form, action) {
                    Ext.MessageBox.hide();
                    FormCreateBanVe.reset();
                    if (action.result.statusCode) {
                        if (action.result.statusCode === 111) {
                            Ext.Msg.alert('Thông báo', action.result.message, function() {});
                        } else if (action.result.statusCode === 110) {
                            Ext.Msg.alert('Thông báo', action.result.message, function() {});
                        } else {
                            Ext.Msg.alert('Thông báo', 'Kiểm tra định dạng File Excel', function() {});
                        }
                    } else {
                        Ext.Msg.alert('Thông báo', 'Nhập kết quả vẽ không thành công!', function() {});
                    }
                },
            })
        } else {
            Ext.Msg.alert('Thông báo', 'Nhập kết quả vẽ không thành công!', function() {});
        }
    },

    onClickButtonRemoveFile: function(gridPanel, rowIndex, colIndex) {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var record = GridListKetQuaBanVe.getStore().getAt(rowIndex);
        Ext.Ajax.request({
            url: '/api/drawing/delete/' + record.data.id,
            method: 'GET',
            success: function(result, request) {
                try {
                    var response = Ext.decode(result.responseText);
                    if (response.success) {
                        GridListKetQuaBanVe.getStore().load({
                            callback: function() {}
                        });
                        if (Ext.getCmp('phancongtheodoi-TabPanel')) {
                            Ext.getCmp('phancongtheodoi-TabPanel').down("grid").getStore().load();
                        }
                    } else {
                        Ext.MessageBox.alert('Thông báo', 'Xóa kết quả vẽ không thành công', function() {});
                    }
                } catch (err) {
                    console.log(err);
                    Ext.MessageBox.alert('Thông báo', 'Xóa kết quả vẽ không thành công', function() {});
                }
            },
            failure: function(result, request) {
                Ext.MessageBox.alert('Thông báo', 'Xóa kết quả vẽ không thành công', function() {});
            }
        });
    },

    onClickButtonViewFile: function(gridPanel, rowIndex, colIndex) {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var record = GridListKetQuaBanVe.getStore().getAt(rowIndex);
        var p_ext = 0;
        if (record.data.drawing_path) {
            p_ext = record.data.drawing_path.lastIndexOf('.');
        }
        var windows_panel = Ext.create('Ext.window.Window', {
            title: 'Xem bản vẽ - NOTE: ' + record.data.note + ' - ' + record.data.lot,
            height: '90%',
            width: '95%',
            minWidth: 800,
            minHeight: 600,
            layout: 'border',
            border: false,
            maximizable: true,
            iconCls: 'far fa-file-pdf',
            modal: true,
            items: [{
                region: 'center',
                width: '100%',
                xtype: 'ViewPDF',
                layout: 'fit',
                reference: 'ViewPDF',
                urlPdf: '/api/drawing/pdf/' + record.data.id + '?name=' + record.data.drawing_path.substring(0, p_ext) + '.pdf'
            }],
        }).show();
    },

    onClickButtonExportFile: function(gridPanel, rowIndex, colIndex) {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var record = GridListKetQuaBanVe.getStore().getAt(rowIndex);
        if (record) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', function(btn, text) {
                if (btn == "yes") {
                    var url = '/api/drawing/download/' + record.data.id + '?name=' + record.data.drawing_path;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btn
                    });
                    ClassMain.saveFile(url, record.data.drawing_path);
                }
            });

        }
    },

    onClickButtonViewFileDBClick: function(gridPanel, record) {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var p_ext = 0;
        if (record.data.drawing_path) {
            p_ext = record.data.drawing_path.lastIndexOf('.');
        }
        var windows_panel = Ext.create('Ext.window.Window', {
            title: 'Xem bản vẽ - NOTE: ' + record.data.note + ' - ' + record.data.lot,
            height: '90%',
            width: '95%',
            minWidth: 800,
            minHeight: 600,
            layout: 'border',
            border: false,
            maximizable: true,
            iconCls: 'far fa-file-pdf',
            modal: true,
            items: [{
                region: 'center',
                width: '100%',
                xtype: 'ViewPDF',
                layout: 'fit',
                reference: 'ViewPDF',
                urlPdf: '/api/drawing/pdf/' + record.data.id + '?name=' + record.data.drawing_path.substring(0, p_ext) + '.pdf'
            }],
        }).show();
    }
});