Ext.define('CMS.view.BPVe.PhanCongTheoDoi.CreatePhanCongVeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.CreatePhanCongVe',
    onClickPhanCongButton: function() {
        var controller = this;
        var view = controller.view;
        var modeedit = view.getModeedit();
        var win = view.up('window');
        var frm = view.getForm();
        if (frm.isValid()) {
            win.hide();
            frm.submit({
                url: modeedit === true ? '/api/assignment-order-sheet/edit/' + view.getRecordPanel().data.assignmentOrderSheet[0].id : '/api/assignment-order-sheet/create',
                clientValidation: true,
                method: 'POST',
                success: function(form, action) {
                    Ext.MessageBox.alert('Thông báo', 'Phân công vẽ thành công', function() {
                        win.close();
                        view.getGridPanel().getStore().load({
                            callback: function() {}
                        });
                    });
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Thông báo', 'Phân công vẽ không thành công: ', function() {
                        win.show();
                    });
                },
            })
        } else {
            win.hide();
            Ext.Msg.alert('Thông báo', 'Phân công vẽ không thành công: Các trường bắt buộc không được để trống!', function() {
                win.show();
            });
        }
    }
});