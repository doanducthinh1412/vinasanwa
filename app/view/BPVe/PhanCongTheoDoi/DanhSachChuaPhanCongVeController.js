/* eslint-disable eol-last */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
Ext.define('CMS.view.BPVe.PhanCongTheoDoi.DanhSachChuaPhanCongVeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.DanhSachChuaPhanCongVe',

    onItemContextMenu(view, record, item, index, e) {
        const controller = this;
        const gridPanel = controller.view;
        const position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        const menu = controller.createMenu(record);
        menu.showAt(position);
    },

    createMenu(record) {
        const controller = this;
        if (record) {
            return Ext.create('Ext.menu.Menu', {
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'Xem đơn đặt hàng',
                    iconCls: 'fas fa-edit text-dark-blue',
                    handler() {
                        controller.onViewButtonClick();
                    },
                }, {
                    text: 'Tải File - Đơn đặt hàng',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    handler() {
                        controller.onExportdButtonClick();
                    },
                }],
            });
        }
        return Ext.create('Ext.menu.Menu', {
            plain: true,
            mouseLeaveDelay: 10,
            items: [],
        });
    },

    renderNumberColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === 0 || value === null) {
            return '';
        }
        return value;
    },

    renderDeliveryColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value) {
            if (value.frame !== '0000-00-00') {
                return Ext.Date.format(Ext.Date.parse(value.frame, 'Y-m-d'), 'j-M');
            }
            if (value.leaf !== '0000-00-00') {
                return Ext.Date.format(Ext.Date.parse(value.leaf, 'Y-m-d'), 'j-M');
            }
            if (value.other !== '0000-00-00') {
                return Ext.Date.format(Ext.Date.parse(value.other, 'Y-m-d'), 'j-M');
            }
            return '';
        }
        return '';
    },

    renderDateColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return '';
        }
        return Ext.Date.format(value, 'j-M');
    },

    onReloadButtonClick() {
        const controller = this;
        controller.loadDataGrid();
    },

    onExportdButtonClick() {
        const controller = this;
        const refs = controller.getReferences();
        const gridPanel = controller.view;
        const record = gridPanel.getSelectionModel().getSelection()[0];
        if (record) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', (btn, text) => {
                if (btn === 'yes') {
                    const url = `api/ordersheet/download/${record.get('id')}`;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                    });
                    ClassMain.saveFile(url, `${record.get('sub_no')}.zip`);
                }
            });
        }
    },

    onViewButtonClick() {
        const controller = this;
        const view_panel = controller.view;
        const { mainView } = Ext.getCmp('mainViewPort').getReferences();
        const record = view_panel.getSelectionModel().getSelection()[0];
        if (record) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp(`ViewDonDatHang${record.get('id')}-TabPanel`) == null) {
                Ext.Ajax.request({
                    url: `/api/ordersheet/get/${record.get('id')}`,
                    method: 'GET',
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                const tab = mainView.add({
                                    xtype: 'ViewDonDatHang',
                                    dataObject: response.data,
                                    closable: true,
                                    iconCls: 'fas fa-file-invoice-dollar',
                                    title: `<b>ĐƠN HÀNG: ${record.get('sub_no').toUpperCase()}</b>`,
                                    id: `ViewDonDatHang${record.get('id')}-TabPanel`,
                                    listeners: {
                                        render() {
                                            ClassMain.hideLoadingMask();
                                        },
                                    },
                                });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng. Vui lòng kiểm tra lại!', () => {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', `Không tải được đơn đặt hàng. Vui lòng kiểm tra lại!<br>${err.toString()}`, () => {});
                        }
                    },
                    failure(result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng. Vui lòng kiểm tra lại!', () => {});
                    },
                });
            } else {
                mainView.setActiveTab(Ext.getCmp(`ViewDonDatHang${record.get('id')}-TabPanel`));
                ClassMain.hideLoadingMask();
            }
        }
    },

    loadDataGrid() {
        const controller = this;
        const refs = controller.getReferences();
        const gird = controller.getView();
        const { SearchField } = refs;
        const { FilterMenu } = refs;
        const refs_FilterMenu = FilterMenu.getMenu().getReferences();
        let txt_search = '';
        const filterValue = '';
        const arr_filter = [];
        let txt_filter = '';
        if (FilterMenu) {
            refs_FilterMenu.forEach((item, key) => {
                if (item.getValue() !== '' && item.getValue() !== null && item.fieldIndex) {
                    if (['order_date', 'delivery_date'].indexOf(item.fieldIndex) !== -1) {
                        arr_filter.push(`${item.fieldIndex}:eq_${Ext.Date.format(item.getValue(), 'Y-m-d')}`);
                    } else {
                        arr_filter.push(`${item.fieldIndex}:eq_${item.getValue()}`);
                    }
                }
            });
        }
        if (SearchField) {
            if (SearchField.getValue() !== '' && SearchField.getValue() !== null) {
                txt_search = SearchField.getValue();
            }
        }
        if (gird) {
            txt_filter = arr_filter.join(',');
            if (txt_filter !== '') {
                gird.getStore().getProxy().setExtraParam('filter', txt_filter);
            } else {
                delete gird.getStore().getProxy().extraParams.filter;
            }
            if (txt_search !== '') {
                gird.getStore().getProxy().setExtraParam('query', txt_search);
            } else {
                delete gird.getStore().getProxy().extraParams.query;
            }
            gird.getStore().load();
        }
    },
});