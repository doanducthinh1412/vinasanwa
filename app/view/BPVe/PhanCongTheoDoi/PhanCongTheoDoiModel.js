Ext.define('CMS.view.BPVe.PhanCongTheoDoi.PhanCongTheoDoiModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.PhanCongTheoDoi',
    stores: {
        danhsachnhanvienStore: {
            type: 'DanhSachThanhVienPhongBanStore',
        },
    },
    data: {
        modePhanCong: true,
    },
});
