/* eslint-disable no-console */
/* eslint-disable no-lonely-if */
/* eslint-disable guard-for-in */
/* eslint-disable no-continue */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable no-empty */
/* eslint-disable no-restricted-syntax */
/* eslint-disable eol-last */
/* eslint-disable radix */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
Ext.define('CMS.view.BPVe.PhanCongTheoDoi.PhanCongTheoDoiController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.PhanCongTheoDoi',

    getSaveClass(v, meta, rec) {
        if (rec.get('status') === 0) {
            if (rec.get('sub_no') !== '' && rec.get('sub_no') !== null) {
                return 'fas fa-trash-alt text-blue';
            }
            return '';
        }
        if (rec.get('status') === 1) {
            return 'fas fa-drafting-compass text-blue';
        }
        if (rec.get('status') === 2) {
            return 'far fa-check-circle text-green';
        }
        if (rec.get('status') === 3) {
            return 'fas fa-drafting-compass';
        }
        if (rec.get('status') === 4) {
            return 'fas fa-paper-plane text-green';
        }
        if (rec.get('status') === -1) {
            return 'fas fa-ban text-red';
        }
        return '';
    },

    getSaveTip(v, meta, rec) {
        if (rec.get('status') === 0) {
            if (rec.get('sub_no') !== '' && rec.get('sub_no') !== null) {
                return 'Xóa phân công';
            }
            return '';
        }
        if (rec.get('status') === 1) {
            return 'Đang thực hiện vẽ';
        }
        if (rec.get('status') === 2) {
            return 'Đã hoàn thành vẽ';
        }
        if (rec.get('status') === 3) {
            return 'Đã phân công - Chưa có kết quả vẽ';
        }
        if (rec.get('status') === 4) {
            return 'Đã gửi sang BP kế hoạch';
        }
        if (rec.get('status') === -1) {
            return 'Đã dừng vẽ';
        }
        return false;
    },

    onSaveClick(grid, rowIndex, colIndex, item, e, rec) {
        e.stopEvent();
        const controller = this;
        const data_change = rec.getChanges();
        const refs = controller.getReferences();

        if (parseInt(rec.get('status')) === 0 && (ClassMain.checkPermission('quan_ly_ban_ve', 'AssignmentOrderSheet', 'delete') === true)) {
            if (rec.get('sub_no') !== '' && rec.get('sub_no') !== null) {
                Ext.MessageBox.confirm('Thông báo', `Bạn có chắc chắn muốn xóa phân công vẽ đơn đặt hàng "<b>${rec.get('sub_no')}"?`, (btn) => {
                    if (btn === 'yes') {
                        // Goi API xoa
                        Ext.Ajax.request({
                            url: `/api/assignment-order-sheet/delete/${rec.data.id}`,
                            method: 'POST',
                            timeout: 30000,
                            success(result, request) {
                                try {
                                    const response = Ext.decode(result.responseText);
                                    if (response.success) {
                                        controller.clearRowRecord(rec);
                                        rec.commit();
                                        if (Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore')) {
                                            Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore').load();
                                        }
                                    } else {
                                        Ext.MessageBox.alert('Thông báo', `Xóa phân công đơn đặt hàng ${rec.data.sub_no}</b> không thành công`, () => {
                                            rec.reject();
                                        });
                                    }
                                } catch (err) {
                                    console.log(err);
                                    Ext.MessageBox.alert('Thông báo', `Xóa phân công đơn đặt hàng ${rec.data.sub_no}</b> không thành công`, () => {
                                        rec.reject();
                                    });
                                }
                            },
                            failure(result, request) {
                                Ext.MessageBox.alert('Thông báo', `Xóa phân công đơn đặt hàng ${rec.data.sub_no}</b> không thành công`, () => {
                                    rec.reject();
                                });
                            },
                        });
                    }
                });
            }
        } else if (parseInt(rec.get('status')) === 1) {}
    },

    renderUnFinishColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value !== null) {
            if (typeof value === 'object' && value.constructor === Object) {
                let total_finish = 0;
                let total = 0;
                for (const type_finish in value) {
                    if (value[type_finish] !== null) {
                        total_finish += parseInt(value[type_finish]);
                    }
                }
                if (typeof rec.get('assignment_frame_type_detail') === 'object' && rec.get('assignment_frame_type_detail').constructor === Object) {
                    for (const type_unfinish in rec.get('assignment_frame_type_detail')) {
                        if (rec.get('assignment_frame_type_detail')[type_unfinish] !== null) {
                            total += parseInt(rec.get('assignment_frame_type_detail')[type_unfinish]);
                        }
                    }
                }
                const total_unfinish = total - total_finish;
                if (total_unfinish !== 0) {
                    return total_unfinish;
                }
            }
        }
        return '';
    },

    renderFinishColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value !== null) {
            if (typeof value === 'object' && value.constructor === Object) {
                let total_finish = 0;
                for (const type_finish in value) {
                    if (value[type_finish] !== null) {
                        total_finish += parseInt(value[type_finish]);
                    }
                }
                if (total_finish !== 0) {
                    return total_finish;
                }
            }
        }
        return '';
    },

    renderNumberColumnType(value, rec, meta, type) {
        if (value === 0 || value === null) {
            return '';
        }
        if (rec.get('working_drawing_finish') !== null && rec.get('working_drawing_finish') !== undefined) {
            if (typeof rec.get('working_drawing_finish') === 'object' && rec.get('working_drawing_finish').constructor === Object) {
                if (rec.get('working_drawing_finish')[type] !== null && rec.get('working_drawing_finish')[type] !== undefined) {
                    if (rec.get('working_drawing_finish')[type] === rec.get('assignment_frame_type_detail')[type]) {
                        meta.style = 'background-color:#4CAF50;';
                        return `<font color="#FFFFFF">${value}</font>`;
                    }
                    meta.style = 'background-color:#167abc;';
                    return `<font color="#FFFFFF">${value}</font>`;
                }
            }
        }
        meta.style = 'background-color:#919191;';
        return `<font color="#FFFFFF">${value}</font>`;
    },

    renderNumberColumn_Frame_SUSD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'frame_susd');
    },

    renderNumberColumn_LEAF_SUSD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'leaf_susd');
    },

    renderNumberColumn_Frame_WD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'frame_wd');
    },

    renderNumberColumn_LEAF_WD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'leaf_wd');
    },

    renderNumberColumn_Frame_FLSD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'frame_flsd');
    },

    renderNumberColumn_LEAF_FLSD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'leaf_flsd');
    },

    renderNumberColumn_FRAME_SD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'frame_sd');
    },

    renderNumberColumn_FRAME_LSD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'frame_lsd');
    },

    renderNumberColumn_FRAME_FSD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'frame_fsd');
    },

    renderNumberColumn_LEAF_SD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'leaf_sd');
    },

    renderNumberColumn_LEAF_LSD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'leaf_lsd');
    },

    renderNumberColumn_LEAF_FSD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'leaf_fsd');
    },

    renderNumberColumn_FRAME_SLD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'frame_sld');
    },

    renderNumberColumn_LEAF_SLD(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'leaf_sld');
    },

    renderNumberColumn_FRAME_SP(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'frame_sp');
    },

    renderNumberColumn_LEAF_SP(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'leaf_sp');
    },

    renderNumberColumn_SSJP(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'ssjp');
    },

    renderNumberColumn_SSAICHI(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'ssaichi');
    },

    renderNumberColumn_SSGRILL(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'ssgrill');
    },

    renderNumberColumn_SRN(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'srn');
    },

    renderNumberColumn_GRS(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'grs');
    },

    renderNumberColumn_S13(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 's13');
    },

    renderNumberColumn_S14(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 's14');
    },

    renderNumberColumn_LV(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'lv');
    },

    renderNumberColumn_FRAME_SW(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'frame_sw');
    },

    renderNumberColumn_OTHER(value, meta, rec, rowIndex, colIndex, store, view) {
        return this.renderNumberColumnType(value, rec, meta, 'other');
    },

    renderNumberColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === 0 || value === null) {
            return '';
        }
        return value;
    },

    renderDeliveryColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value) {
            if (value.frame !== '0000-00-00') {
                return Ext.Date.format(Ext.Date.parse(value.frame, 'Y-m-d'), 'j-M');
            }
            if (value.leaf !== '0000-00-00') {
                return Ext.Date.format(Ext.Date.parse(value.leaf, 'Y-m-d'), 'j-M');
            }
            if (value.other !== '0000-00-00') {
                return Ext.Date.format(Ext.Date.parse(value.other, 'Y-m-d'), 'j-M');
            }
            return '';
        }
        return '';
    },

    renderDrawerColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        const controller = this;
        const danhsachnhanvienStore = controller.getViewModel().get('danhsachnhanvienStore');
        if (value) {
            if (rec.get('drawer').id) {
                if (parseInt(rec.get('drawer').id) === parseInt(value)) {
                    return rec.get('drawer').last_name;
                }
            } else {
                const record_nv = danhsachnhanvienStore.findRecord('id', value);
                if (record_nv) {
                    return record_nv.get('last_name');
                }
            }
            return '';
        }
        return '';
    },

    renderDateColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return '';
        }
        return Ext.Date.format(value, 'j-M');
    },

    onViewGridReady(grid) {
        const { view } = grid;
        grid.tip = Ext.create('Ext.tip.ToolTip', {
            target: view.el,
            delegate: '.x-grid-cell',
            trackMouse: false,
            anchor: 'right',
            constrainPosition: false,
            showDelay: 0,
            hideDelay: 0,
            autoHide: false,
            listeners: {
                beforeshow: function updateTipBody(tip) {
                    const tipGridView = tip.target.component;
                    const rec = tipGridView.getRecord(tip.triggerElement);
                    const colname = tipGridView.getHeaderCt().getHeaderAtIndex(tip.triggerElement.cellIndex).dataIndex;
                    const arr_info = [];
                    let total = 0;
                    let total_finish = 0;
                    if (rec) {
                        if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'frame_fsd', 'frame_flsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'leaf_fsd', 'leaf_flsd', 'frame_sld', 'leaf_sld', 'frame_sp', 'leaf_sp', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'lv', 'frame_sw', 'other'].indexOf(colname) > -1) {
                            if (rec.get('working_drawing_finish') !== null && rec.get('working_drawing_finish') !== undefined) {
                                if (typeof rec.get('working_drawing_finish') === 'object' && rec.get('working_drawing_finish').constructor === Object) {
                                    if (rec.get('working_drawing_finish')[colname] !== null && rec.get('working_drawing_finish')[colname] !== undefined) {
                                        arr_info.push(`+ Hoàn thành: ${rec.get('working_drawing_finish')[colname]}`);
                                        total_finish += parseInt(rec.get('working_drawing_finish')[colname]);
                                    }
                                    if (typeof rec.get('assignment_frame_type_detail') === 'object' && rec.get('assignment_frame_type_detail').constructor === Object) {
                                        if (rec.get('assignment_frame_type_detail')[colname] !== null && rec.get('assignment_frame_type_detail')[colname] !== undefined) {
                                            total += parseInt(rec.get('assignment_frame_type_detail')[colname]);
                                        }
                                    }
                                    const total_unfinish = total - total_finish;
                                    if (total_unfinish !== 0) {
                                        arr_info.push(`+ Chưa hoàn thành: ${total_unfinish}`);
                                    }
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                            if (rec.get('working_drawing_send_khsx') !== null && rec.get('working_drawing_send_khsx') !== undefined) {
                                if (typeof rec.get('working_drawing_send_khsx') === 'object' && rec.get('working_drawing_send_khsx').constructor === Object) {
                                    if (rec.get('working_drawing_send_khsx')[colname] !== null && rec.get('working_drawing_send_khsx')[colname] !== undefined) {
                                        arr_info.push(`+ Đã gửi sang KH: ${rec.get('working_drawing_send_khsx')[colname]}`);
                                    }
                                }
                            }
                            if (arr_info.length === 0) {
                                return false;
                            }
                            tip.update(arr_info.join('<br>'));
                        } else {
                            return false;
                        }
                    }
                    if (arr_info.length === 0) {
                        return false;
                    }
                    return true;
                },
            },
        });
    },

    onBeforeEditCell(editor, context, eOpts) {
        try {
            const controller = this;
            if (ClassMain.checkPermission('quan_ly_ban_ve', 'AssignmentOrderSheet', 'create') === false) {
                return false;
            }
            if (context.field !== 'sub_no') {
                if (context.record.data.sub_no === '' || context.record.data.sub_no === null) {
                    return false;
                }
                if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'frame_fsd', 'frame_flsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'leaf_fsd', 'leaf_flsd', 'frame_sld', 'leaf_sld', 'frame_sp', 'leaf_sp', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'lv', 'frame_sw', 'other'].indexOf(context.field) !== -1) {
                    if (context.record.data.order_sheet.frame_type_detail[context.field]) {
                        if (parseInt(context.record.data.order_sheet.frame_type_detail[context.field]) === 0) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
                if (parseInt(context.record.data.status) !== 0) {
                    return false;
                }
            } else if (context.record.data.sub_no !== '' && context.record.data.sub_no !== null) {
                return false;
            }
        } catch (err) {
            console.log(err);
            return false;
        }
        return true;
    },

    onCellDBClick(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        const controller = this;
        const selModel = grid.getSelectionModel();
        const selection = selModel.getSelected();
        if (selection.startCell && (ClassMain.checkPermission('quan_ly_ban_ve', 'AssignmentOrderSheet', 'create') === true)) {
            const field = selection.startCell.column.dataIndex;
            if (field === 'sub_no' && (record.get('sub_no') === null || record.get('sub_no') === '')) {
                const win = Ext.create('Ext.window.Window', {
                    title: 'Chọn đơn đặt hàng chưa phân công vẽ',
                    iconCls: 'far fa-list-alt',
                    width: '95%',
                    height: '85%',
                    layout: 'border',
                    modal: true,
                    viewModel: true,
                    items: {
                        region: 'center',
                        xtype: 'DanhSachChuaPhanCongVe',
                        listeners: {
                            itemdblclick(gridPanel, record_bd, item, index, event) {
                                if (record_bd) {
                                    const select_chuave = record_bd;
                                    if (select_chuave.get('sub_no') !== '' && select_chuave.get('sub_no') !== null) {
                                        // Goi API lay thong tin cua phan cong don dat hang
                                        Ext.Ajax.request({
                                            url: `/api/ordersheet/assignment-remain/${select_chuave.get('sub_no')}`,
                                            method: 'GET',
                                            timeout: 30000,
                                            success(result, request) {
                                                try {
                                                    const response = Ext.decode(result.responseText);
                                                    if (response.success) {
                                                        if (response.total !== 0) {
                                                            if (response.data[0].assignments_remain !== {} && response.data[0].assignments_remain !== null) {
                                                                record.set({
                                                                    sub_no: select_chuave.get('sub_no'),
                                                                    project_name: response.data[0].project_name,
                                                                    delivery_date: response.data[0].delivery_date,
                                                                    order_date: response.data[0].order_date.date,
                                                                    order_sheet_id: response.data[0].id ? response.data[0].id : '',
                                                                });
                                                                let check_empty = true;
                                                                const obj_remain = {};
                                                                for (const remain in response.data[0].assignments_remain) {
                                                                    if (response.data[0].assignments_remain[remain] === 0) {
                                                                        continue;
                                                                    }
                                                                    obj_remain[remain] = response.data[0].assignments_remain[remain];
                                                                    check_empty = false;
                                                                }
                                                                if (obj_remain !== {}) {
                                                                    record.set(obj_remain);
                                                                }
                                                                if (check_empty) {
                                                                    record.reject();
                                                                }
                                                                if (Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore')) {
                                                                    Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore').load();
                                                                }
                                                                win.close();
                                                            } else {
                                                                Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                                            }
                                                        } else {
                                                            Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                                        }
                                                    } else {
                                                        Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                                    }
                                                } catch (err) {
                                                    console.log(err);
                                                    Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                                }
                                            },
                                            failure(result, request) {
                                                Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                            },
                                        });
                                    }
                                }
                            },
                        },
                    },
                    buttons: ['->', {
                        text: 'Chọn phân công',
                        iconCls: 'fas fa-drafting-compass',
                        ui: 'soft-orange',
                        handler() {
                            const grid_chuave = this.up('window').down('grid');
                            const select_chuave = grid_chuave.getSelectionModel().getSelection()[0];
                            if (select_chuave) {
                                if (select_chuave.get('sub_no') !== '' && select_chuave.get('sub_no') !== null) {
                                    // Goi API lay thong tin cua phan cong don dat hang
                                    Ext.Ajax.request({
                                        url: `/api/ordersheet/assignment-remain/${select_chuave.get('sub_no')}`,
                                        method: 'GET',
                                        timeout: 30000,
                                        success(result, request) {
                                            try {
                                                const response = Ext.decode(result.responseText);
                                                if (response.success) {
                                                    if (response.total !== 0) {
                                                        if (response.data[0].assignments_remain !== {} && response.data[0].assignments_remain !== null) {
                                                            record.set({
                                                                sub_no: select_chuave.get('sub_no'),
                                                                project_name: response.data[0].project_name,
                                                                delivery_date: response.data[0].delivery_date,
                                                                order_date: response.data[0].order_date.date,
                                                                order_sheet_id: response.data[0].id ? response.data[0].id : '',
                                                            });
                                                            let check_empty = true;
                                                            const obj_remain = {};
                                                            for (const remain in response.data[0].assignments_remain) {
                                                                if (response.data[0].assignments_remain[remain] === 0) {
                                                                    continue;
                                                                }
                                                                obj_remain[remain] = response.data[0].assignments_remain[remain];
                                                                check_empty = false;
                                                            }
                                                            if (obj_remain !== {}) {
                                                                record.set(obj_remain);
                                                            }
                                                            if (check_empty) {
                                                                record.reject();
                                                            }
                                                            if (Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore')) {
                                                                Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore').load();
                                                            }
                                                            win.close();
                                                        } else {
                                                            Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                                        }
                                                    } else {
                                                        Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                                    }
                                                } else {
                                                    Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                                }
                                            } catch (err) {
                                                console.log(err);
                                                Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                            }
                                        },
                                        failure(result, request) {
                                            Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${select_chuave.get('sub_no')}</b> đã được phân công hoặc không tồn tại`, () => {});
                                        },
                                    });
                                }
                            }
                        },
                    }, {
                        text: 'Đóng',
                        glyph: 'xf00d@FontAwesome',
                        handler() {
                            this.up('window').close();
                        },
                    }, '->'],
                }).show();
            } else if (field === 'sub_no' && (record.get('sub_no') !== null && record.get('sub_no') !== '')) {
                controller.onViewDonDatHang(record.get('order_sheet'));
            } else if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'frame_fsd', 'frame_flsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'leaf_fsd', 'leaf_flsd', 'frame_sld', 'leaf_sld', 'frame_sp', 'leaf_sp', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'lv', 'frame_sw', 'other'].indexOf(field) !== -1) {
                if (record.get('working_drawing_finish') !== null && record.get('working_drawing_finish') !== undefined) {
                    if (typeof record.get('working_drawing_finish') === 'object' && record.get('working_drawing_finish').constructor === Object) {
                        if (record.get('working_drawing_finish')[field] !== null && record.get('working_drawing_finish')[field] !== undefined) {
                            // Xem ket qua ve
                            const win = Ext.create('Ext.window.Window', {
                                title: `Kết quả vẽ - Order No ${record.get('sub_no')}`,
                                iconCls: 'fas fa-drafting-compass',
                                width: '95%',
                                height: '85%',
                                layout: 'border',
                                modal: true,
                                viewModel: true,
                                items: {
                                    xtype: 'ViewKetQuaVe',
                                    region: 'center',
                                    recordPanel: record,
                                },
                            }).show();
                        }
                    }
                }
            }
        }
    },

    onCellContextMenuGrid(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        const controller = this;
        e.stopEvent();
        if (record.get('sub_no') !== null && record.get('sub_no') !== undefined && record.get('sub_no') !== '') {
            if (tr.childElementCount > 4) {
                cellIndex += 4;
            }
            const selModel = grid.getSelectionModel();
            const refs = controller.getReferences();
            const position = [e.getX() - 10, e.getY() - 10];
            selModel.selectCells([cellIndex, rowIndex], [cellIndex, rowIndex]);
            const selection = selModel.getSelected();
            if (selection.startCell) {
                const field = selection.startCell.column.dataIndex;
                if (field === 'sub_no') {
                    Ext.create('Ext.menu.Menu', {
                        plain: true,
                        mouseLeaveDelay: 10,
                        items: [{
                            text: 'Xem đơn đặt hàng',
                            iconCls: 'fas fa-edit text-dark-blue',
                            handler() {
                                controller.onViewDonDatHang(record.get('order_sheet'));
                            },
                        }, {
                            text: 'Tải đơn đặt hàng',
                            iconCls: 'fas fa-file-export text-dark-blue',
                            handler() {
                                controller.onExportdButton(record.get('order_sheet'));
                            },
                        }],
                    }).showAt(position);
                } else if (['frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'frame_fsd', 'frame_flsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'leaf_fsd', 'leaf_flsd', 'frame_sld', 'leaf_sld', 'frame_sp', 'leaf_sp', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'lv', 'frame_sw', 'other'].indexOf(field) !== -1) {
                    if (record.get('working_drawing_finish') !== null && record.get('working_drawing_finish') !== undefined) {
                        if (typeof record.get('working_drawing_finish') === 'object' && record.get('working_drawing_finish').constructor === Object) {
                            if (record.get('working_drawing_finish')[field] !== null && record.get('working_drawing_finish')[field] !== undefined) {
                                Ext.create('Ext.menu.Menu', {
                                    plain: true,
                                    mouseLeaveDelay: 10,
                                    items: [{
                                        text: 'Xem kết quả vẽ',
                                        iconCls: 'fas fa-drafting-compass text-dark-blue',
                                        handler() {
                                            const win = Ext.create('Ext.window.Window', {
                                                title: `Kết quả vẽ - Order No ${record.get('sub_no')}`,
                                                iconCls: 'fas fa-drafting-compass',
                                                width: '95%',
                                                height: '85%',
                                                layout: 'border',
                                                modal: true,
                                                viewModel: true,
                                                items: {
                                                    xtype: 'ViewKetQuaVe',
                                                    region: 'center',
                                                    recordPanel: record,
                                                },
                                            }).show();
                                        },
                                    }],
                                }).showAt(position);
                            }
                        }
                    }
                }
            }
        }
    },

    onExportdButton(record) {
        const controller = this;
        if (record !== null && record !== undefined) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', (btn, text) => {
                if (btn === 'yes') {
                    const url = `api/ordersheet/download/${record.id}`;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btn,
                    });
                    ClassMain.saveFile(url, `${record.sub_no}.zip`);
                }
            });
        }
    },

    onViewDonDatHang(record) {
        const controller = this;
        const { mainView } = Ext.getCmp('mainViewPort').getReferences();
        if (record !== null && record !== undefined) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp(`ViewDonDatHang${record.id}-TabPanel`) == null) {
                Ext.Ajax.request({
                    url: `/api/ordersheet/get/${record.id}`,
                    method: 'GET',
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                const tab = mainView.add({
                                    xtype: 'ViewDonDatHang',
                                    // gridView: grid,
                                    dataObject: response.data,
                                    closable: true,
                                    iconCls: 'fas fa-file-invoice-dollar',
                                    title: `<b>ĐƠN HÀNG: ${record.sub_no.toUpperCase()}</b>`,
                                    id: `ViewDonDatHang${record.id}-TabPanel`,
                                    listeners: {
                                        render() {
                                            ClassMain.hideLoadingMask();
                                        },
                                    },
                                });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                        }
                    },
                    failure(result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                    },
                });
            } else {
                mainView.setActiveTab(Ext.getCmp(`ViewDonDatHang${record.data.id}-TabPanel`));
                ClassMain.hideLoadingMask();
            }
        }
    },

    onEditCell(editor, context, eOpts) {
        const controller = this;
        const selModel = editor.grid.getSelectionModel();
        if (context.field === 'sub_no' && context.value !== context.originalValue) {
            if (context.value !== '') {
                // Goi API lay thong tin cua phan cong don dat hang
                Ext.Ajax.request({
                    url: `/api/ordersheet/assignment-remain/${context.value}`,
                    method: 'GET',
                    timeout: 30000,
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                if (response.total !== 0) {
                                    if (response.data[0].assignments_remain !== {} && response.data[0].assignments_remain !== null) {
                                        context.record.set({
                                            project_name: response.data[0].project_name,
                                            delivery_date: response.data[0].delivery_date,
                                            order_date: response.data[0].order_date.date,
                                            order_sheet_id: response.data[0].id ? response.data[0].id : '',
                                        });
                                        let check_empty = true;
                                        const obj_remain = {};
                                        for (const remain in response.data[0].assignments_remain) {
                                            if (response.data[0].assignments_remain[remain] === 0) {
                                                continue;
                                            }
                                            obj_remain[remain] = response.data[0].assignments_remain[remain];
                                            // context.record.set(remain, response.data[0].assignments_remain[remain]);
                                            check_empty = false;
                                            // break;
                                        }
                                        if (obj_remain !== {}) {
                                            context.record.set(obj_remain);
                                        }
                                        if (check_empty) {
                                            context.record.reject();
                                        }
                                        if (Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore')) {
                                            Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore').load();
                                        }
                                    } else {
                                        Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${context.value}</b> đã được phân công hoặc không tồn tại`, () => {
                                            context.record.reject();
                                        });
                                    }
                                } else {
                                    Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${context.value}</b> đã được phân công hoặc không tồn tại`, () => {
                                        context.record.reject();
                                    });
                                }
                            } else {
                                Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${context.value}</b> đã được phân công hoặc không tồn tại`, () => {
                                    context.record.reject();
                                });
                            }
                        } catch (err) {
                            console.log(err);
                            Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${context.value}</b> đã được phân công hoặc không tồn tại`, () => {
                                context.record.reject();
                            });
                        }
                    },
                    failure(result, request) {
                        Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${context.value}</b> đã được phân công hoặc không tồn tại`, () => {
                            context.record.reject();
                        });
                    },
                });
            }
        }
    },

    onItemKeydown(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        const controller = this;
        const refs = controller.getReferences();
        const { PhanCongTheoDoiGrid } = refs;
        const editPlugin = PhanCongTheoDoiGrid.findPlugin('cellediting');

        if (!editPlugin.editing) {
            if (!e.event.ctrlKey) {
                if (['Control', 'Alt', 'Shift', 'Tab', 'Delete', 'Enter', 'CapsLock', 'ArrowLeft', 'ArrowRight', 'ArrowDown', 'ArrowUp', 'CapsLock', 'Escape', 'Insert', 'Backspace', 'Meta'].indexOf(e.event.key) === -1) {
                    const selection = grid.getSelectionModel().getSelected();
                    editPlugin.startEdit(selection.startCell.rowIdx, selection.startCell.colIdx);
                    editPlugin.context.column.field.setValue(null);
                }
            }
        }

        if (e.getKey() === e.DELETE) {
            const selModelSelected = grid.getSelectionModel().getSelected();
            if (selModelSelected.startCell) {
                const field = selModelSelected.startCell.column.dataIndex;
                if (['project_name', 'sub_no', 'order_date', 'delivery_date'].indexOf(field) === -1 && record.get('status') === 0) {
                    record.set(field, '');
                    record.commit();
                }
                if (field === 'sub_no' && record.get('status') === 0) {
                    Ext.MessageBox.confirm('Thông báo', `Bạn có chắc chắn muốn xóa phân công vẽ đơn đặt hàng "<b>${record.get('sub_no')}"?`, (btn) => {
                        if (btn === 'yes') {
                            // Goi API xoa phan cong
                            Ext.Ajax.request({
                                url: `/api/assignment-order-sheet/delete/${record.data.id}`,
                                method: 'POST',
                                timeout: 30000,
                                success(result, request) {
                                    try {
                                        const response = Ext.decode(result.responseText);
                                        if (response.success) {
                                            controller.clearRowRecord(record);
                                            record.commit();
                                            if (Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore')) {
                                                Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore').load();
                                            }
                                        } else {
                                            Ext.MessageBox.alert('Thông báo', `Xóa phân công đơn đặt hàng ${record.data.sub_no}</b> không thành công`, () => {
                                                record.reject();
                                            });
                                        }
                                    } catch (err) {
                                        console.log(err);
                                        Ext.MessageBox.alert('Thông báo', `Xóa phân công đơn đặt hàng ${record.data.sub_no}</b> không thành công.<br>${err.toString()}`, () => {
                                            record.reject();
                                        });
                                    }
                                },
                                failure(result, request) {
                                    Ext.MessageBox.alert('Thông báo', `Xóa phân công đơn đặt hàng ${record.data.sub_no}</b> không thành công. Vui lòng kiểm tra lại!`, () => {
                                        record.reject();
                                    });
                                },
                            });
                        }
                    });
                }
            }
        }
    },

    clearRowRecord(record) {
        record.set({
            note: null,
            deadline: null,
            status: null,
            deletedAt: null,
            updatedAt: null,
            drawer: null,
            drawer_id: null,
            drawings: null,
            order_sheet: null,
            project_name: null,
            delivery_date: null,
            order_date: null,
            sub_no: null,
            frame_type_detail: null,
            working_drawing_send_khsx: null,
            frame_sd: null,
            frame_lsd: null,
            frame_fsd: null,
            frame_wd: null,
            frame_susd: null,
            leaf_fsd: null,
            leaf_sd: null,
            leaf_lsd: null,
            leaf_wd: null,
            leaf_susd: null,
            frame_flsd: null,
            leaf_flsd: null,
            ssjp: null,
            ssaichi: null,
            ssgrill: null,
            grs: null,
            srn: null,
            s13: null,
            s14: null,
            frame_sp: null,
            leaf_sp: null,
            frame_sld: null,
            leaf_sld: null,
            lv: null,
            sw: null,
            other: null,
            id: Ext.id(),
        });
    },

    onLoadGridKeHoachSanXuat(store, records, successful, operation, eOpts) {
        const controller = this;
        const PhanCongTheoDoiGrid = controller.getView();
        const data = [];
        const data_cur_am = [];
        const data_cur_pm = [];
        const data_other = [];
        const data_kehoach = {};
        const cur_date = new Date();
        let maxDateKeHoach = new Date();
        const modePhanCong = controller.getViewModel().get('modePhanCong');
        if (records) {
            for (let j = 0; j < records.length; j += 1) {
                const str_date_record = records[j].data.assignment_date;
                const date_record = Ext.Date.parse(str_date_record, 'Y-m-d');
                if (date_record - maxDateKeHoach > 0) {
                    maxDateKeHoach = date_record;
                }
                if (Ext.Date.diff(cur_date, date_record, Ext.Date.DAY) >= -1) {
                    if (!data_kehoach[str_date_record]) {
                        data_kehoach[str_date_record] = [];
                    }
                    data_kehoach[str_date_record].push(records[j].data);
                }
            }
            maxDateKeHoach.setDate(maxDateKeHoach.getDate() + 1);
            const arr_tmp = [];
            for (let day = cur_date; day <= maxDateKeHoach; day.setDate(day.getDate() + 1)) {
                const day_s = Ext.Date.format(day, 'Y-m-d');
                if (!data_kehoach[day_s]) {
                    data_kehoach[day_s] = [];
                }
                if (modePhanCong === true) {
                    let len_date = 5 - data_kehoach[day_s].length;
                    if (len_date <= 0) {
                        len_date = 1;
                    }
                    for (let k = 0; k < len_date; k += 1) {
                        arr_tmp.push({
                            note: null,
                            deadline: null,
                            status: null,
                            deletedAt: null,
                            assignment_date: day_s,
                            updatedAt: null,
                            drawer: null,
                            drawer_id: null,
                            drawings: null,
                            order_sheet: null,
                            project_name: null,
                            delivery_date: null,
                            order_date: null,
                            sub_no: null,
                            frame_type_detail: null,
                            working_drawing_send_khsx: null,
                            frame_sd: null,
                            frame_lsd: null,
                            frame_fsd: null,
                            frame_wd: null,
                            frame_susd: null,
                            leaf_fsd: null,
                            leaf_sd: null,
                            leaf_lsd: null,
                            leaf_wd: null,
                            leaf_susd: null,
                            frame_flsd: null,
                            leaf_flsd: null,
                            ssjp: null,
                            ssaichi: null,
                            ssgrill: null,
                            grs: null,
                            srn: null,
                            s13: null,
                            s14: null,
                            frame_sp: null,
                            leaf_sp: null,
                            frame_sld: null,
                            leaf_sld: null,
                            lv: null,
                            sw: null,
                            other: null,
                        });
                    }
                }
            }
            store.add(arr_tmp);
        }
        PhanCongTheoDoiGrid.setMaxDateKeHoach(maxDateKeHoach);
    },

    onUpdateGridKeHoach(store, record, operation, modifiedFieldNames, details, eOpts) {
        const controller = this;
        const refs = controller.getReferences();
        const { PhanCongTheoDoiGrid } = refs;
        const view = controller.getView();
        if (modifiedFieldNames) {
            if (modifiedFieldNames.indexOf('sub_no') > -1 || modifiedFieldNames.indexOf('project_name') > -1 || modifiedFieldNames.indexOf('delivery_date') > -1 || modifiedFieldNames.indexOf('order_date') > -1) {
                return true;
            }
            if (parseInt(record.data.id)) {
                // Edit phan cong
                if (modifiedFieldNames && record.get('sub_no') !== '' && record.get('assignment_date') !== '' && record.get('assignment_date') !== null && record.get('sub_no') !== null && record.get('project_name') !== '' && record.get('order_date') !== '') {
                    const data_record = {};
                    for (const i in modifiedFieldNames) {
                        const key = modifiedFieldNames[i];
                        if (typeof key === 'string') {
                            if (key !== 'id') {
                                if (['drawer_id'].indexOf(key) > -1) {
                                    data_record.drawer = record.data[key];
                                }
                                if (['deadline', 'frame_susd', 'frame_wd', 'frame_sd', 'frame_lsd', 'leaf_susd', 'leaf_wd', 'leaf_sd', 'leaf_lsd', 'ssjp', 'ssaichi', 'ssgrill', 'srn', 'grs', 's13', 's14', 'frame_sp', 'leaf_sp', 'frame_sld', 'leaf_sld', 'frame_flsd', 'leaf_flsd', 'frame_fsd', 'leaf_fsd', 'sw', 'lv'].indexOf(key) > -1) {
                                    data_record[key] = record.data[key];
                                }
                            }
                        }
                    }
                    if (Object.getOwnPropertyNames(data_record).length !== 0) {
                        Ext.Ajax.request({
                            url: `/api/assignment-order-sheet/edit/${record.data.id}`,
                            method: 'POST',
                            timeout: 30000,
                            params: data_record,
                            success(result, request) {
                                try {
                                    const response = Ext.decode(result.responseText);
                                    if (response.success) {
                                        record.commit();
                                        if (Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore')) {
                                            Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore').load();
                                        }
                                    } else if (response.statusCode === 1) {
                                        Ext.MessageBox.alert('Thông báo', 'Số lượng không hợp lệ. Vui lòng kiểm tra lại!', () => {
                                            record.reject();
                                        });
                                    } else if (response.statusCode === 2) {
                                        Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${record.get('note')}</b> đã hoàn thành kế hoạch sản xuất`, () => {
                                            record.reject();
                                        });
                                    } else {
                                        Ext.MessageBox.alert('Thông báo', 'Chỉnh sửa phân công không thành công. Vui lòng kiểm tra lại!', () => {
                                            record.reject();
                                        });
                                    }
                                } catch (err) {
                                    console.log(err);
                                    Ext.MessageBox.alert('Thông báo', `Chỉnh sửa phân công không thành công. Vui lòng kiểm tra lại!<br>${err.toString()}`, () => {
                                        record.reject();
                                    });
                                }
                            },
                            failure(result, request) {
                                Ext.MessageBox.alert('Thông báo', 'Chỉnh sửa phân công không thành công. Vui lòng kiểm tra lại!', () => {
                                    record.reject();
                                });
                            },
                        });
                    }
                }
            } else {
                // Tao moi phan cong
                if (modifiedFieldNames && record.get('sub_no') !== '' && record.get('assignment_date') !== '' && record.get('assignment_date') !== null && record.get('sub_no') !== null && record.get('project_name') !== '' && record.get('order_date') !== '') {
                    // Goi API tao san xuat
                    const data_record = record.getData();
                    data_record.order_sheet = data_record.order_sheet_id;
                    data_record.assignment_date = data_record.assignment_date;

                    if (data_record) {
                        delete data_record.id;
                        delete data_record.order_sheet_id;
                        delete data_record.createdAt;
                        delete data_record.delivery_date;
                        delete data_record.order_date;
                        delete data_record.project_name;
                        delete data_record.sub_no;
                    }
                    for (const index in data_record) {
                        const attr = data_record[index];
                        if (attr === null || attr === '' || attr === 0) {
                            delete data_record[index];
                        }
                    }
                    Ext.Ajax.request({
                        url: '/api/assignment-order-sheet/create',
                        method: 'POST',
                        timeout: 30000,
                        params: data_record,
                        success(result, request) {
                            try {
                                const response = Ext.decode(result.responseText);
                                if (response.success) {
                                    PhanCongTheoDoiGrid.getStore().load();
                                    if (Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore')) {
                                        Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore').load();
                                    }
                                } else if (response.statusCode === 1) {
                                    Ext.MessageBox.alert('Thông báo', 'Số lượng không hợp lệ. Vui lòng kiểm tra lại!', () => {
                                        record.reject();
                                    });
                                } else if (response.statusCode === 2) {
                                    Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${record.get('sub_no')}</b> đã hoàn thành phân công vẽ`, () => {
                                        record.reject();
                                    });
                                } else {
                                    Ext.MessageBox.alert('Thông báo', `Đơn đặt hàng <b> ${record.get('sub_no')}</b> không tồn tại. Vui lòng kiểm tra lại!`, () => {
                                        record.reject();
                                    });
                                }
                            } catch (err) {
                                console.log(err);
                                Ext.MessageBox.alert('Thông báo', `Phân công vẽ không thành công. Vui lòng kiểm tra lại!<br>${err.toString()}`, () => {
                                    record.reject();
                                });
                            }
                        },
                        failure(result, request) {
                            Ext.MessageBox.alert('Thông báo', 'Phân công vẽ không thành công. Vui lòng kiểm tra lại!', () => {
                                record.reject();
                            });
                        },
                    });
                }
            }
        }
        return true;
    },

    onClickPhanCongVeButton(time) {
        const controller = this;
        const refs = controller.getReferences();
        const { PhanCongTheoDoiGrid } = refs;
        Ext.MessageBox.confirm('Thông báo', 'Bạn có chắc chắn phân công vẽ?', (btn) => {
            if (btn === 'yes') {
                const list_id = [];
                const datenow_str = time;
                const list_record = PhanCongTheoDoiGrid.getStore().queryRecordsBy((record) => {
                    if (record.get('assignment_date') === datenow_str && (record.get('id') % 1) === 0 && record.get('status') === 0) {
                        list_id.push(record.get('id'));
                        return true;
                    }
                    return false;
                });
                if (list_id.length > 0) {
                    // Goi API phan cong ve
                    Ext.Ajax.request({
                        url: `/api/assignment-order-sheet/send-employee?ids=${list_id.join(',')}`,
                        method: 'POST',
                        timeout: 30000,
                        success(result, request) {
                            try {
                                const response = Ext.decode(result.responseText);
                                if (response.success) {
                                    Ext.MessageBox.alert('Thông báo', 'Phân công vẽ thành công', () => {
                                        PhanCongTheoDoiGrid.getStore().load();
                                    });
                                } else {
                                    Ext.MessageBox.alert('Thông báo', response.message, () => {});
                                }
                            } catch (err) {
                                console.log(err);
                                Ext.MessageBox.alert('Thông báo', `Phân công vẽ không thành công<br>${err.toString()}`, () => {});
                            }
                        },
                        failure(result, request) {
                            Ext.MessageBox.alert('Thông báo', 'Phân công vẽ không thành công', () => {});
                        },
                    });
                }
            }
        });
    },

    onClickAddDayButton(btn) {
        const controller = this;
        const refs = controller.getReferences();
        const { PhanCongTheoDoiGrid } = refs;
        const PhanCongBanVe = controller.getView();
        let date_kehoach = new Date();
        if (PhanCongBanVe.getMaxDateKeHoach()) {
            date_kehoach = PhanCongBanVe.getMaxDateKeHoach();
        }
        date_kehoach.setDate(date_kehoach.getDate() + 1);
        const data_add = [];
        for (let k = 0; k < 5; k += 1) {
            data_add.push({
                assignment_date: Ext.Date.format(date_kehoach, 'Y-m-d'),
                note: null,
                deadline: null,
                status: null,
                deletedAt: null,
                updatedAt: null,
                drawer: null,
                drawer_id: null,
                drawings: null,
                order_sheet: null,
                project_name: null,
                delivery_date: null,
                order_date: null,
                sub_no: null,
                frame_type_detail: null,
                working_drawing_send_khsx: null,
                frame_sd: null,
                frame_lsd: null,
                frame_fsd: null,
                frame_wd: null,
                frame_susd: null,
                leaf_fsd: null,
                leaf_sd: null,
                leaf_lsd: null,
                leaf_wd: null,
                leaf_susd: null,
                frame_flsd: null,
                leaf_flsd: null,
                ssjp: null,
                ssaichi: null,
                ssgrill: null,
                grs: null,
                srn: null,
                s13: null,
                s14: null,
                frame_sp: null,
                leaf_sp: null,
                frame_sld: null,
                leaf_sld: null,
                lv: null,
                sw: null,
                other: null,
            });
        }
        PhanCongTheoDoiGrid.getStore().add(data_add);
        PhanCongBanVe.setMaxDateKeHoach(date_kehoach);
    },

    onReloadButtonClick(btn) {
        const controller = this;
        const refs = controller.getReferences();
        controller.loadDataGrid();
    },

    onBtnSearchRangeDate(btn, e) {
        e.stopEvent();
        const controller = this;
        const refs = controller.getReferences();
        const { StartDateField } = refs;
        const { EndDateField } = refs;
        if (StartDateField.getValue() !== null && StartDateField.getValue() !== '') {
            const newLocal = controller.getViewModel().set('modePhanCong', false);
            controller.loadDataGrid();
        }
    },

    onBtnViewDangPhanCongVe(btn, e) {
        e.stopEvent();
        const controller = this;
        const refs = controller.getReferences();
        const { FilterMenu } = refs;
        const refs_FilterMenu = FilterMenu.getMenu().getReferences();
        const { StartDateField } = refs;
        const { EndDateField } = refs;
        const newLocal = controller.getViewModel().set('modePhanCong', true);
        if (FilterMenu) {
            refs_FilterMenu.forEach((item, key) => {
                item.reset();
                item.getTrigger('clear').hide();
            });
        }
        StartDateField.reset();
        EndDateField.reset();
        controller.loadDataGrid();
    },

    loadDataGrid() {
        const controller = this;
        const refs = controller.getReferences();
        const { SearchField } = refs;
        const { FilterMenu } = refs;
        const refs_FilterMenu = FilterMenu.getMenu().getReferences();
        const { PhanCongTheoDoiGrid } = refs;
        const { StartDateField } = refs;
        const { EndDateField } = refs;
        const filterValue = '';
        let time_filter = '';
        const arr_filter = [];
        let txt_filter = '';
        let valueSearch = SearchField.getValue();
        if (FilterMenu) {
            refs_FilterMenu.forEach((item, key) => {
                if (item.getValue() !== '' && item.getValue() !== null && item.fieldIndex) {
                    if (['order_date', 'delivery_date', 'deadline'].indexOf(item.fieldIndex) !== -1) {
                        arr_filter.push(`${item.fieldIndex}:eq_${Ext.Date.format(item.getValue(), 'Y-m-d')}`);
                    } else {
                        arr_filter.push(`${item.fieldIndex}:eq_${item.getValue()}`);
                    }
                }
            });
        }
        if (!valueSearch) {
            valueSearch = '';
        }
        if (PhanCongTheoDoiGrid) {
            if (StartDateField.getValue() !== null && StartDateField.getValue() !== '') {
                time_filter = `assignment_date:gteq_${Ext.Date.format(StartDateField.getValue(), 'Y-m-d')}_lteq_${Ext.Date.format(EndDateField.getValue(), 'Y-m-d')}`;
                arr_filter.push(time_filter);
            }
            txt_filter = arr_filter.join(',');
            if (txt_filter !== '') {
                PhanCongTheoDoiGrid.getStore().getProxy().setExtraParam('filter', txt_filter);
            } else {
                delete PhanCongTheoDoiGrid.getStore().getProxy().extraParams.filter;
            }
            if (valueSearch !== '') {
                PhanCongTheoDoiGrid.getStore().getProxy().setExtraParam('query', valueSearch);
            } else {
                delete PhanCongTheoDoiGrid.getStore().getProxy().extraParams.query;
            }
            PhanCongTheoDoiGrid.getStore().load();
        }
    },
});