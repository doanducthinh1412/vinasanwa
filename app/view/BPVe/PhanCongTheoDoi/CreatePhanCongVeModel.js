Ext.define('CMS.view.BPVe.PhanCongTheoDoi.CreatePhanCongVeModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.CreatePhanCongVe',
    stores: {
        nguoithuchienStore: {
            type: 'DanhSachThanhVienPhongBanStore'
        }
    },
    data: {}
});