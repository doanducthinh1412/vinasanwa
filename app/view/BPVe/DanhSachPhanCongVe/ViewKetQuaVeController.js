Ext.define('CMS.view.BPVe.DanhSachPhanCongVe.ViewKetQuaVeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.ViewKetQuaVe',

    onClickCreateBanVeButton: function() {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var frm = FormCreateBanVe.getForm();
        if (frm.isValid()) {
            Ext.MessageBox.show({
                title: 'Xin chờ',
                msg: 'Đang cập nhật kết quả...',
                progressText: 'Đang xử lý...',
                width: 300,
                wait: {
                    interval: 200
                }
            });
            frm.submit({
                url: '/api/drawing/create',
                clientValidation: true,
                method: 'POST',
                success: function(form, action) {
                    Ext.MessageBox.hide();
                    FormCreateBanVe.reset();
                    GridListKetQuaBanVe.getStore().load({
                        callback: function() {}
                    });
                    if (Ext.getCmp('danhsachphancongve-TabPanel')) {
                        Ext.getCmp('danhsachphancongve-TabPanel').down("grid").getStore().load();
                    }
                },
                failure: function(form, action) {
                    Ext.MessageBox.hide();
                    FormCreateBanVe.reset();
                    if (action.result.statusCode) {
                        if (action.result.statusCode === 111) {
                            Ext.Msg.alert('Thông báo', action.result.message, function() {});
                        } else if (action.result.statusCode === 110) {
                            Ext.Msg.alert('Thông báo', action.result.message, function() {});
                        } else {
                            Ext.Msg.alert('Thông báo', 'Kiểm tra định dạng File Excel', function() {});
                        }
                    } else {
                        Ext.Msg.alert('Thông báo', 'Nhập kết quả vẽ không thành công!', function() {});
                    }
                },
            })
        } else {
            Ext.Msg.alert('Thông báo', 'Nhập kết quả vẽ không thành công!', function() {});
        }
    },

    onClickButtonRemoveFile: function(gridPanel, rowIndex, colIndex) {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var record = GridListKetQuaBanVe.getStore().getAt(rowIndex);
        if (parseInt(record.get('status')) === 0 || record.get('status') === null) {
            Ext.Ajax.request({
                url: '/api/drawing/delete/' + record.data.id,
                method: 'GET',
                success: function(result, request) {
                    try {
                        var response = Ext.decode(result.responseText);
                        if (response.success) {
                            GridListKetQuaBanVe.getStore().load({
                                callback: function() {}
                            });
                            if (Ext.getCmp('danhsachphancongve-TabPanel')) {
                                Ext.getCmp('danhsachphancongve-TabPanel').down("grid").getStore().load();
                            }
                        } else {
                            Ext.MessageBox.alert('Thông báo', 'Xóa kết quả vẽ không thành công', function() {});
                        }
                    } catch (err) {
                        console.log(err);
                        Ext.MessageBox.alert('Thông báo', 'Xóa kết quả vẽ không thành công', function() {});
                    }
                },
                failure: function(result, request) {
                    Ext.MessageBox.alert('Thông báo', 'Xóa kết quả vẽ không thành công', function() {});
                }
            });
        }
        if (parseInt(record.get('status')) === 1 || parseInt(record.get('status')) === 2) {
            Ext.MessageBox.alert('Thông báo', 'Bản vẽ đã được phê duyệt. Không thể xóa bản vẽ.', function() {});
        }
    },

    onClickButtonViewFile: function(gridPanel, rowIndex, colIndex) {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var record = GridListKetQuaBanVe.getStore().getAt(rowIndex);
        var p_ext = 0;
        if (record.data.drawing_path) {
            p_ext = record.data.drawing_path.lastIndexOf('.');
        }
        var urlBanVe = null;
        if (parseInt(record.get('status')) === 0) {
            urlBanVe = '/api/drawing/pdf/' + record.data.id + '?name=' + record.data.drawing_path.substring(0, p_ext) + '.pdf';
        } else if (parseInt(record.get('status')) === 2 || parseInt(record.get('status')) === 1) {
            urlBanVe = '/api/drawing/final-result/' + record.data.id;
        }
        if (urlBanVe !== null) {
            var windows_panel = Ext.create('Ext.window.Window', {
                title: 'Xem bản vẽ - NOTE: ' + record.data.note + ' - ' + record.data.lot,
                height: '90%',
                width: '95%',
                minWidth: 800,
                minHeight: 600,
                layout: 'border',
                border: false,
                maximizable: true,
                iconCls: 'far fa-file-pdf',
                modal: true,
                items: [{
                    region: 'center',
                    width: '100%',
                    xtype: 'ViewPDF',
                    layout: 'fit',
                    reference: 'ViewPDF',
                    urlPdf: urlBanVe
                }],
            }).show();
        } else {
            Ext.MessageBox.alert('Thông báo', 'Lỗi xem bản vẽ. Vui lòng liên hệ quản trị viên!', function() {});
        }
    },

    onClickButtonExportFile: function(gridPanel, rowIndex, colIndex) {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var record = GridListKetQuaBanVe.getStore().getAt(rowIndex);
        if (record) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', function(btn, text) {
                if (btn == "yes") {
                    var url = '/api/drawing/download/' + record.data.id + '?name=' + record.data.drawing_path;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btn
                    });
                    ClassMain.saveFile(url, record.data.drawing_path);
                }
            });

        }
    },

    renderDateColumn: function(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === "0000-00-00" || value === null || value === "") {
            return "";
        }
        return Ext.Date.format(value, 'H:i d/m/Y');
    },

    onClickButtonViewFileDBClick: function(gridPanel, record) {
        var controller = this;
        var view = controller.view;
        var refs = controller.getReferences();
        var FormCreateBanVe = refs.FormCreateBanVe;
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        var p_ext = 0;
        if (record.data.drawing_path) {
            p_ext = record.data.drawing_path.lastIndexOf('.');
        }
        var urlBanVe = null;
        if (parseInt(record.get('status')) === 0) {
            urlBanVe = '/api/drawing/pdf/' + record.data.id + '?name=' + record.data.drawing_path.substring(0, p_ext) + '.pdf';
        } else if (parseInt(record.get('status')) === 2 || parseInt(record.get('status')) === 1) {
            urlBanVe = '/api/drawing/final-result/' + record.data.id;
        }
        if (urlBanVe !== null) {
            var windows_panel = Ext.create('Ext.window.Window', {
                title: 'Xem bản vẽ - NOTE: ' + record.data.note + ' - ' + record.data.lot,
                height: '90%',
                width: '95%',
                minWidth: 800,
                minHeight: 600,
                layout: 'border',
                border: false,
                maximizable: true,
                iconCls: 'far fa-file-pdf',
                modal: true,
                items: [{
                    region: 'center',
                    width: '100%',
                    xtype: 'ViewPDF',
                    layout: 'fit',
                    reference: 'ViewPDF',
                    urlPdf: urlBanVe
                }],
            }).show();
        } else {
            Ext.MessageBox.alert('Thông báo', 'Lỗi xem bản vẽ. Vui lòng liên hệ quản trị viên!', function() {});
        }
    },

    getRemoveActionClass: function(v, meta, rec) {
        var controller = this;
        if (parseInt(rec.get('status')) === 0 || rec.get('status') === null) {
            return 'fas fa-times text-blue';
        }
        if (parseInt(rec.get('status')) === 1 || parseInt(rec.get('status')) === 2) {
            return 'fas fa-times';
        }
        return "";
    },

    getSendClass: function(v, meta, rec) {
        if (parseInt(rec.get('status')) === 1 || parseInt(rec.get('status')) === 0 || rec.get('status') === null) {
            return 'far fa-paper-plane';
        } else if (parseInt(rec.get('status')) === 2) {
            return 'fas fa-paper-plane text-blue';
        }
        return "";
    },

    getSendTip: function(v, meta, rec) {
        if (parseInt(rec.get('status')) === 0 || rec.get('status') === null) {
            return 'Chưa phê duyệt bản vẽ';
        }
        if (parseInt(rec.get('status')) === 1) {
            return 'Chưa gửi sang bộ phận kế hoạch';
        }
        if (parseInt(rec.get('status')) === 2) {
            return 'Đã gửi sang bộ phận kế hoạch';
        }
        return false;
    },

    onSendClick: function(grid, rowIndex, colIndex, item, e, record) {
        e.stopEvent();
        var controller = this;
        var refs = controller.getReferences();
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        if (parseInt(record.get('status')) === 1) {
            if (ClassMain.checkPermission('quan_ly_ban_ve', 'Drawing', 'sendKHSX') === false) {
                Ext.MessageBox.alert('Thông báo', 'Bạn không có quyền gửi bản vẽ. Vui lòng liên hệ quản trị!', function() {});
            } else {
                if (parseInt(record.get('status')) === 1) {
                    Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn gửi kết quả bản vẽ sang bộ phận Kế hoạch?', function(btn, text) {
                        if (btn == "yes") {
                            Ext.Ajax.request({
                                url: '/api/drawing/send/' + record.data.id,
                                method: 'GET',
                                success: function(result, request) {
                                    try {
                                        var response = Ext.decode(result.responseText);
                                        if (response.success === true) {
                                            GridListKetQuaBanVe.getStore().load({
                                                callback: function() {}
                                            });
                                            Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ thành công', function() {});
                                        } else {
                                            if (response.message) {
                                                Ext.MessageBox.alert('Thông báo', response.message, function() {});
                                            } else {
                                                Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ không thành công. Vui lòng kiểm tra lại!', function() {});
                                            }
                                        }
                                    } catch (err) {
                                        console.log(err);
                                        Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ không thành công<br>' + err.toString(), function() {});
                                    }
                                },
                                failure: function(result, request) {
                                    Ext.MessageBox.alert('Thông báo', 'Gửi kết quả vẽ không thành công. Vui lòng kiểm tra lại!', function() {});
                                }
                            });
                        }
                    });
                } else if (parseInt(record.get('status')) === 2) {
                    Ext.MessageBox.alert('Thông báo', 'Bản vẽ đã được gửi sang bộ phận Kế hoạch', function() {});
                } else {
                    Ext.MessageBox.alert('Thông báo', 'Bản vẽ chưa được phê duyệt. Vui lòng kiểm tra lại!', function() {});
                }
            }
        } else if (parseInt(record.get('status')) === 2) {
            Ext.Msg.alert('Thông báo', 'Bản vẽ đã gửi sang bộ phận Kế hoạch', function() {});
        } else {
            Ext.Msg.alert('Thông báo', 'Bản vẽ chưa được phê duyệt. Vui lòng kiểm tra lại!', function() {});
        }
    },

    getCheckClass: function(v, meta, rec) {
        var controller = this;
        if (parseInt(rec.get('status')) === 0 || rec.get('status') === null) {
            return 'fas fa-clipboard-check';
        }
        if (parseInt(rec.get('status')) === 1 || parseInt(rec.get('status')) === 2) {
            return 'fas fa-clipboard-check text-blue';
        }
        return "";
    },

    getCheckTip: function(v, meta, rec) {
        if (parseInt(rec.get('status')) === 0 || rec.get('status') === null) {
            return 'Chưa phê duyệt bản vẽ';
        }
        if (parseInt(rec.get('status')) === 1 || parseInt(rec.get('status')) === 2) {
            return 'Đã phê duyệt bản vẽ';
        }
        return false;
    },

    onCheckClick: function(grid, rowIndex, colIndex, item, e, rec) {
        e.stopEvent();
        var controller = this;
        var refs = controller.getReferences();
        var GridListKetQuaBanVe = refs.GridListKetQuaBanVe;
        if (parseInt(rec.get('status')) === 0 || parseInt(rec.get('status')) === 1) {
            var windows_panel = Ext.create('Ext.window.Window', {
                title: 'Cập nhật bản vẽ đã phê duyệt (PDF)',
                height: 170,
                width: 400,
                layout: 'border',
                border: false,
                maximizable: false,
                iconCls: 'far fa-file-pdf',
                modal: true,
                items: [{
                    xtype: 'form',
                    layout: 'anchor',
                    region: 'center',
                    bodyPadding: 10,
                    items: [{
                        xtype: 'filefield',
                        anchor: '100%',
                        emptyText: 'Chọn File bản vẽ (PDF)',
                        fieldLabel: 'FIle bản vẽ',
                        accept: 'application/pdf',
                        name: 'drawings',
                        regex: (/.(pdf)$/i),
                    }],
                    buttons: ['->', {
                        text: 'Cập nhật bản vẽ',
                        ui: 'soft-orange',
                        iconCls: 'fas fa-file-upload',
                        handler: function(btn) {
                            var frm = windows_panel.down('form').getForm();
                            if (frm.isValid()) {
                                Ext.MessageBox.show({
                                    title: 'Xin chờ',
                                    msg: 'Đang cập nhật kết quả...',
                                    progressText: 'Đang xử lý...',
                                    width: 300,
                                    wait: {
                                        interval: 200
                                    }
                                });
                                frm.submit({
                                    url: '/api/drawing/final-result/' + rec.get('id'),
                                    clientValidation: true,
                                    method: 'POST',
                                    success: function(form, action) {
                                        if (action.result.success === true) {
                                            Ext.MessageBox.hide();
                                            windows_panel.hide();
                                            GridListKetQuaBanVe.getStore().load({
                                                callback: function() {}
                                            });
                                            if (Ext.getCmp('danhsachphancongve-TabPanel')) {
                                                Ext.getCmp('danhsachphancongve-TabPanel').down("grid").getStore().load();
                                            }
                                            Ext.Msg.alert('Thông báo', 'Cập nhật bản vẽ phê duyệt thành công', function() {});
                                        } else {
                                            Ext.MessageBox.hide();
                                            FormCreateBanVe.reset();
                                            Ext.Msg.alert('Thông báo', 'Định dạng File không đúng. Vui lòng kiểm tra lại!', function() {});
                                        }
                                    },
                                    failure: function(form, action) {
                                        Ext.MessageBox.hide();
                                        FormCreateBanVe.reset();
                                        Ext.Msg.alert('Thông báo', 'Định dạng File không đúng. Vui lòng kiểm tra lại!', function() {});
                                    },
                                })
                            } else {
                                Ext.Msg.alert('Thông báo', 'Định dạng File không đúng. Vui lòng kiểm tra lại!', function() {});
                            }
                        }
                    }]
                }],
            }).show();
        } else if (parseInt(rec.get('status')) === 2) {
            Ext.Msg.alert('Thông báo', 'Bản vẽ đã gửi sang bộ phận Kế hoạch', function() {});
        } else {
            Ext.Msg.alert('Thông báo', 'Lỗi hệ thống. Vui lòng liên hệ quản trị!', function() {});
        }
    },

});