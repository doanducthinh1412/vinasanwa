/* eslint-disable no-param-reassign */
/* eslint-disable radix */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPVe.DanhSachPhanCongVe.DanhSachPhanCongVeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.DanhSachPhanCongVe',

    onItemContextMenu(view, record, item, index, e) {
        const controller = this;
        const position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        const menu = controller.createMenu(record);
        menu.showAt(position);
    },

    createMenu(record) {
        const controller = this;
        if (record) {
            return Ext.create('Ext.menu.Menu', {
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'Kết quả vẽ',
                    iconCls: 'fas fa-drafting-compass text-dark-blue',
                    handler() {
                        controller.onViewKetQuaVeButtonClick();
                    },
                }, '-', {
                    text: 'Xem đơn đặt hàng',
                    iconCls: 'fas fa-edit text-dark-blue',
                    handler() {
                        controller.onViewDonDatHangButtonClick();
                    },
                }, {
                    text: 'Tải File - Đơn đặt hàng',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    handler() {
                        controller.onExportdDonDatHangButtonClick();
                    },
                }],
            });
        }
        return Ext.create('Ext.menu.Menu', {
            plain: true,
            mouseLeaveDelay: 10,
            items: [],
        });
    },

    onExportdDonDatHangButtonClick() {
        const controller = this;
        const refs = controller.getReferences();
        const gridPanel = refs.PhanCongTheoDoiGrid;
        const record = gridPanel.getSelectionModel().getSelection()[0];
        if (record) {
            const record_ordersheet = record.data.order_sheet;
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', (btn, text) => {
                if (btn === 'yes') {
                    const url = `api/ordersheet/download/${record_ordersheet.id}`;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btn,
                    });
                    ClassMain.saveFile(url, `${record_ordersheet.sub_no}.zip`);
                }
            });
        }
    },

    onViewDonDatHangButtonClick() {
        const controller = this;
        const refs = controller.getReferences();
        const gridPanel = refs.PhanCongTheoDoiGrid;
        const record = gridPanel.getSelectionModel().getSelection()[0];
        const record_ordersheet = record.data.order_sheet;
        const { mainView } = Ext.getCmp('mainViewPort').getReferences();
        if (record_ordersheet) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp(`ViewDonDatHang${record_ordersheet.id}-TabPanel`) == null) {
                Ext.Ajax.request({
                    url: `/api/ordersheet/get/${record_ordersheet.id}`,
                    method: 'GET',
                    success(result, request) {
                        try {
                            const response = Ext.decode(result.responseText);
                            if (response.success) {
                                const tab = mainView.add({
                                    xtype: 'ViewDonDatHang',
                                    gridView: gridPanel,
                                    dataObject: response.data,
                                    closable: true,
                                    iconCls: 'fas fa-file-invoice-dollar',
                                    title: `<b>ĐƠN HÀNG: ${record_ordersheet.sub_no.toUpperCase()}</b>`,
                                    id: `ViewDonDatHang${record_ordersheet.id}-TabPanel`,
                                    listeners: {
                                        render() {
                                            ClassMain.hideLoadingMask();
                                        },
                                    },
                                });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                        }
                    },
                    failure(result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', () => {});
                    },
                });
            } else {
                mainView.setActiveTab(Ext.getCmp(`ViewDonDatHang${record.data.id}-TabPanel`));
                ClassMain.hideLoadingMask();
            }
        }
    },

    onViewKetQuaVeButtonClick() {
        const controller = this;
        const refs = controller.getReferences();
        const { PhanCongTheoDoiGrid } = refs;
        const record = PhanCongTheoDoiGrid.getSelectionModel().getSelection()[0];
        if (record) {
            Ext.create('Ext.window.Window', {
                title: `Kết quả vẽ - Order No ${record.get('sub_no')}`,
                iconCls: 'fas fa-drafting-compass',
                width: '95%',
                height: '85%',
                layout: 'border',
                modal: true,
                viewModel: true,
                items: {
                    xtype: 'ViewKetQuaVe',
                    region: 'center',
                    recordPanel: record,
                },
            }).show();
        }
    },

    getSaveClass(v, meta, rec) {
        const controller = this;
        if (rec.get('status') === 0) {
            if (rec.get('sub_no') !== '' && rec.get('sub_no') !== null) {
                return 'fas fa-trash-alt text-blue';
            }
            return '';
        }
        if (rec.get('status') === 1) {
            return 'fas fa-drafting-compass text-blue';
        }
        if (rec.get('status') === 2) {
            return 'far fa-check-circle text-green';
        }
        if (rec.get('status') === 3) {
            return 'fas fa-drafting-compass text-blue';
        }
        if (rec.get('status') === 4) {
            return 'fas fa-paper-plane text-green';
        }
        if (rec.get('status') === -1) {
            return 'fas fa-ban text-red';
        }
        return '';
    },

    getSaveTip(v, meta, rec) {
        if (rec.get('status') === 0) {
            if (rec.get('sub_no') !== '' && rec.get('sub_no') !== null) {
                return 'Xóa phân công';
            }
            return '';
        }
        if (rec.get('status') === 1) {
            return 'Đang thực hiện vẽ';
        }
        if (rec.get('status') === 2) {
            return 'Đã hoàn thành vẽ';
        }
        if (rec.get('status') === 3) {
            return 'Đã phân công';
        }
        if (rec.get('status') === 4) {
            return 'Đã gửi sang BP kế hoạch';
        }
        if (rec.get('status') === -1) {
            return 'Đã dừng vẽ';
        }
        return false;
    },

    renderNumberColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === 0 || value === null) {
            return '';
        }
        return value;
    },

    renderDeliveryColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value) {
            if (value.frame !== '0000-00-00') {
                return Ext.Date.format(Ext.Date.parse(value, 'Y-m-d'), 'j-M');
            }
            if (value.leaf !== '0000-00-00') {
                return Ext.Date.format(Ext.Date.parse(value, 'Y-m-d'), 'j-M');
            }
            if (value.other !== '0000-00-00') {
                return Ext.Date.format(Ext.Date.parse(value, 'Y-m-d'), 'j-M');
            }
            return '';
        }
        return '';
    },

    renderStatusColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value) {
            if (parseInt(value) === 0) {
                if (rec.get('sub_no') !== '' && rec.get('sub_no') !== null) {
                    meta.style = 'background-color:red;';
                    return '<font color="#FFFFFF">Xóa phân công</font>';
                }
                return '';
            }
            if (parseInt(value) === 1) {
                meta.style = 'background-color:blue;';
                return '<font color="#FFFFFF">Đang thực hiện</font>';
            }
            if (parseInt(value) === 2) {
                meta.style = 'background-color:green;';
                return '<font color="#FFFFFF">Đã hoàn thành</font>';
            }
            if (parseInt(value) === 3) {
                meta.style = 'background-color:blue;';
                return '<font color="#FFFFFF">Phân công vẽ mới</font>';
            }
            if (parseInt(value) === 4) {
                meta.style = 'background-color:green;';
                return '<font color="#FFFFFF">Đã gửi sang BP kế hoạch</font>';
            }
            if (parseInt(value) === -1) {
                meta.style = 'background-color:red;';
                return '<font color="#FFFFFF">Đã dừng</font>';
            }
            return '';
        }
        return '';
    },

    renderDrawerColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        const controller = this;
        const danhsachnhanvienStore = controller.getViewModel().get('danhsachnhanvienStore');
        if (value) {
            if (rec.get('drawer').id) {
                if (parseInt(rec.get('drawer').id) === parseInt(value)) {
                    return rec.get('drawer').last_name;
                }
            } else {
                const record_nv = danhsachnhanvienStore.findRecord('id', value);
                if (record_nv) {
                    return record_nv.get('last_name');
                }
            }
            return '';
        }
        return '';
    },

    renderDateColumn(value, meta, rec, rowIndex, colIndex, store, view) {
        if (value === '0000-00-00' || value === null || value === '') {
            return '';
        }
        return Ext.Date.format(value, 'j-M');
    },

    onReloadButtonClick(btn) {
        const controller = this;
        const refs = controller.getReferences();
        const { PhanCongTheoDoiGrid } = refs;
        PhanCongTheoDoiGrid.getStore().load();
    },

    onSelectFilterBanVe(combo, record, eOpts) {
        const controller = this;
        const refs = controller.getReferences();
        controller.loadDataGrid();
    },

    loadDataGrid() {
        const controller = this;
        const refs = controller.getReferences();
        const gird = refs.PhanCongTheoDoiGrid;
        const { SearchField } = refs;
        const { ComboboxFilterBanVe } = refs;
        const { FilterMenu } = refs;
        const refs_FilterMenu = FilterMenu.getMenu().getReferences();
        let txt_search = '';
        const filterValue = '';
        const arr_filter = [];
        let txt_filter = '';
        if (SearchField) {
            if (SearchField.getValue() !== '' && SearchField.getValue() !== null) {
                txt_search = SearchField.getValue();
            }
        }
        if (FilterMenu) {
            refs_FilterMenu.forEach((item, key) => {
                if (item.getValue() !== '' && item.getValue() !== null && item.fieldIndex) {
                    if (['order_date', 'delivery_date', 'deadline'].indexOf(item.fieldIndex) !== -1) {
                        arr_filter.push(`${item.fieldIndex}:eq_${Ext.Date.format(item.getValue(), 'Y-m-d')}`);
                    } else {
                        arr_filter.push(`${item.fieldIndex}:eq_${item.getValue()}`);
                    }
                }
            });
        }
        if (ComboboxFilterBanVe) {
            if (ComboboxFilterBanVe.getValue() !== '' && ComboboxFilterBanVe.getValue() !== null) {
                arr_filter.push(ComboboxFilterBanVe.getValue());
            }
        }
        if (gird) {
            txt_filter = arr_filter.join(',');
            if (txt_filter !== '') {
                gird.getStore().getProxy().setExtraParam('filter', txt_filter);
            } else {
                delete gird.getStore().getProxy().extraParams.filter;
            }
            if (txt_search !== '') {
                gird.getStore().getProxy().setExtraParam('query', txt_search);
            } else {
                delete gird.getStore().getProxy().extraParams.query;
            }
            gird.getStore().load();
        }
    },
});