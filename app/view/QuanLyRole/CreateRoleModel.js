Ext.define('CMS.view.CreateRole.CreateRoleModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.CreateRole',
	requires: [
		'CMS.store.QuanLyRoleStore'
	],
	stores: {
		quanLyRoleStore: {
			type: 'QuanLyRoleStore',
			autoLoad: true,
			
		},
		
	}
});