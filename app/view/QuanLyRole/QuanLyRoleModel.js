Ext.define('CMS.view.QuanLyRole.QuanLyRoleModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.QuanLyRole',
	requires: [
		'CMS.store.RoleStore'
	],
	stores: {
		roleStore: {
			type: 'RoleStore',
			autoLoad: true,
		},
		
	}
});