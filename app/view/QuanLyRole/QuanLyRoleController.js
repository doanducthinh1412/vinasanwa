Ext.define('CMS.view.QuanLyRole.QuanLyRoleController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.QuanLyRole',
    onItemContextMenu: function(view, record, item, index, e) {
        var controller = this;
        var gridPanel = controller.view;
        var position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        gridPanel.menu.showAt(position);
    },
    onCreateButtonClick: function() {
        var panel = this.view;
        /*var windowss = Ext.create('Ext.window.Window', {
            title: 'Tạo mới role',
            height: '80%',
            width: '80%',
            layout: 'border',
            border: false,
            maximizable: true,
            glyph: 'xf055@FontAwesome',
            modal: true,
            items: [{
                xtype: 'CreateRole',
                region: 'center',
                autoScroll: true,
				gridPanel: panel
            }]
        }).show();*/
		var quanLyRoleStore = Ext.create('CMS.store.QuanLyRoleStore');
		quanLyRoleStore.load({
			callback: function(records, operation, success) {
				if (success) {
					windowss = Ext.create('Ext.window.Window', {
						title: 'Tạo mới role',
						height: '80%',
						width: '80%',
						layout: 'border',
						border: false,
						maximizable: true,
						glyph: 'xf055@FontAwesome',
						modal: true,
						items: [{
							xtype: 'CreateRole',
							region: 'center',
							autoScroll: true,
							gridPanel: panel,
							records: records
						}]
					}).show();
				}
			}
		});
    },
    onEditButtonClick: function() {
        var bl_items = [];
        main_panel = this.view;
        var selectedRow = main_panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        /*windowss = Ext.create('Ext.window.Window', {
            title: 'Chỉnh sửa thông tin role ' + rec.get('name') ,
            height: '80%',
            width: '80%',
            layout: 'border',
            glyph: 'xf044@FontAwesome',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'EditRole',
                region: 'center',
                autoScroll: true,
                rec: rec,
				gridPanel: main_panel
            }]
        }).show();*/
		var quanLyRoleStore = Ext.create('CMS.store.QuanLyRoleStore');
		quanLyRoleStore.load({
			callback: function(records, operation, success) {
				if (success) {
					windowss = Ext.create('Ext.window.Window', {
						title: 'Chỉnh sửa thông tin role ' + rec.get('name') ,
						height: '80%',
						width: '80%',
						layout: 'border',
						glyph: 'xf044@FontAwesome',
						border: false,
						maximizable: true,
						modal: true,
						items: [{
							xtype: 'EditRole',
							region: 'center',
							autoScroll: true,
							rec: rec,
							gridPanel: main_panel,
							records: records
						}]
					}).show();
				}
			}
		});
    },
	onEditButtonClickGrid: function(gridPanel, rowIndex, colIndex){
        var panel = this.view;
		var rec = gridPanel.getStore().getAt(rowIndex);
		var quanLyRoleStore = Ext.create('CMS.store.QuanLyRoleStore');
		quanLyRoleStore.load({
			callback: function(records, operation, success) {
				if (success) {
					windowss = Ext.create('Ext.window.Window', {
						title: 'Chỉnh sửa thông tin role ' + rec.get('name') ,
						height: '80%',
						width: '80%',
						layout: 'border',
						glyph: 'xf044@FontAwesome',
						border: false,
						maximizable: true,
						modal: true,
						items: [{
							xtype: 'EditRole',
							region: 'center',
							autoScroll: true,
							rec: rec,
							gridPanel: panel,
							records: records
						}]
					}).show();
				}
			}
		});
        
    },
	onAddUserButtonClick: function() {
		var bl_items = [];
        main_panel = this.view;
        var selectedRow = main_panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        windowss = Ext.create('Ext.window.Window', {
            title: 'Tạo tài khoản hệ thống cho nhân viên ' + rec.get('last_name') + ' ' + rec.get('first_name'),
            height: 290,
            width: 490,
            layout: 'border',
            glyph: 'xf044@FontAwesome',
            border: false,
            maximizable: true,
            modal: true,
            items: [{
                xtype: 'AddNguoiDung',
                region: 'center',
                autoScroll: true,
                rec: rec
            }]
        }).show();
	},
    onDeleteButtonClick: function() {
        var panel = this.view;
        var selectedRow = panel.getSelectionModel().getSelection();
        rec = selectedRow[0];
        Ext.MessageBox.confirm('Thông báo', 'Xóa role "<b>' + rec.get('name')+'</b>" ?', function(btn) {
            if (btn == "yes") {
                panel.el.mask("Đang xóa...");
                Ext.Ajax.request({
                    url: '/api/roles/delete/' + rec.get('id'),
                    success: function(response) {
                        panel.el.unmask();
                        var restext = response.responseText;
                        if (restext != "") {
                            var res = Ext.JSON.decode(restext);
                            if (res.success == false) {
                                Ext.MessageBox.alert('Thông báo', res.msg, function() {});
                            } else {
                                panel.getStore().load({
                                    callback: function() {}
                                });
                            }
                        }
                    },
                    failure: function(response) {
                        panel.el.unmask();
                    }
                });
            }
        });
    },
	onDeleteButtonClickGrid: function(gridPanel, rowIndex, colIndex) {
        var panel = this.view;
		var rec = gridPanel.getStore().getAt(rowIndex);
        Ext.MessageBox.confirm('Thông báo', 'Xóa role "<b>' + rec.get('name')+'</b>" ?', function(btn) {
            if (btn == "yes") {
                panel.el.mask("Đang xóa...");
                Ext.Ajax.request({
                    url: '/api/roles/delete/' + rec.get('id'),
                    success: function(response) {
                        panel.el.unmask();
                        var restext = response.responseText;
                        if (restext != "") {
                            var res = Ext.JSON.decode(restext);
                            if (res.success == false) {
                                Ext.MessageBox.alert('Thông báo', res.msg, function() {});
                            } else {
                                panel.getStore().load({
                                    callback: function() {}
                                });
                            }
                        }
                    },
                    failure: function(response) {
                        panel.el.unmask();
                    }
                });
            }
        });
    },
    onReloadButtonClick: function() {
        var panel = this.view;
        panel.el.mask('Đang tải...');
        panel.getStore().reload();
        panel.el.unmask();
    }
});