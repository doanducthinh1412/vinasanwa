Ext.define('CMS.view.EditRole.EditRoleController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.EditRole',
    onSaveButtonClicked: function(btn) {
		var me = this.view;
		var rec = me.getRec();
		var refs = me.getReferences();
		var resources_field = refs.resourcesField;
		var gridPanel = me.getGridPanel();
		var list = me.query('tagfield');
		var res = [];
		for (i=0; i < list.length; i ++ ) {
			var value = list[i].getValue();
			res = res.concat(value);
		}
		if (btn.up('form').getForm().isValid()) {
			btn.up('window').hide();
            var frm = btn.up('form').getForm();
			frm.findField("resources").setValue(res.toString());
            btn.up('form').getForm().submit({
                clientValidation: true,
                submitEmptyText: false,
                method: 'POST',
                url: '/api/roles/edit/'+ rec.id,
                success: function(form, action) {
                    var restext = action.response.responseText;
                    if (restext == "") {
                        btn.up('window').close();
                    }
                    var res = Ext.JSON.decode(restext);
                    if (res.success == false) {
                        Ext.MessageBox.alert('Thông báo', res.result.msg, function() {
                            btn.up('window').close();
                        });
                    } else {
                        Ext.MessageBox.alert('Thông báo', 'Chỉnh sửa role thành công', function() {
                            btn.up('window').close();
                        });
						gridPanel.getStore().reload();
						if (userid == Config.userInfo.id) {
							Config.userInfo = res.data;
							Ext.getCmp('mainViewPort').getController().getViewModel().set('MainTxtUserName', fullname);
						}
                    }
                },
                failure: function(form, action) {
                    var restext = action.response.responseText;
                    if (restext == "") {
                        Ext.MessageBox.alert('Thông báo', 'Không thể  chỉnh sửa thông tin', function() {
                            btn.up('window').close();
                        });
                    } else {
                        var res = Ext.JSON.decode(restext);
                        Ext.MessageBox.alert('Thông báo', res.msg, function() {
                            btn.up('window').show();
                        });
                    }
                },
                waitMsg: "Đang chỉnh sửa..."
            });
        } else {
			btn.up('window').hide();
			Ext.Msg.alert('Thông báo','Chỉnh sửa không thành công: Các trường bắt buộc không được để trống!',function(){
				btn.up('window').show();
			});
		}
	}
});