Ext.define('CMS.view.BPTinhGia.DanhMucNVLHangTieuChuan.DanhMucNVLHangTieuChuanModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.DanhMucNVLHangTieuChuan',
    stores: {},
    data: {
        actionMenu: Ext.create('Ext.menu.Menu', {
            plain: true,
            mouseLeaveDelay: 10,
            items: []
        })
    }
});