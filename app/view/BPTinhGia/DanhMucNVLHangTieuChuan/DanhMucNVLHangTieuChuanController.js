Ext.define('CMS.view.BPTinhGia.DanhMucNVLHangTieuChuan.DanhMucNVLHangTieuChuanController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.DanhMucNVLHangTieuChuan',
    onItemActionMenu: function(view, record, e) {
        var controller = this;
        var gridPanel = controller.view;
        var menu = controller.createMenu(record);
        controller.getViewModel().set('actionMenu', menu);
    },
    onItemContextMenu: function(view, record, item, index, e) {
        var controller = this;
        var gridPanel = controller.view;
        var position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        var menu = controller.createMenu(record);
        menu.showAt(position);
    },

    createMenu: function(record) {
        var controller = this;
        if (record) {
            return Ext.create('Ext.menu.Menu', {
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'Xem thông tin',
                    iconCls: 'fas fa-edit text-dark-blue',
                    handler: function() {
                        controller.onViewButtonClick();
                    }
                }, {
                    text: 'Tải File - Đơn đặt hàng',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    handler: function() {
                        controller.onExportdButtonClick();
                    }
                }, '-', {
                    text: 'Tính giá - Đơn đặt hàng',
                    iconCls: 'fas fa-hand-holding-usd text-dark-blue',
                    handler: function() {
                        controller.onTinhGiaButtonClick();
                    }
                }]
            });
        } else {
            return Ext.create('Ext.menu.Menu', {
                plain: true,
                mouseLeaveDelay: 10,
                items: []
            });
        }
    },

    onViewButtonClickGrid: function(gridPanel, rowIndex, colIndex) {
        var gridPanel = this.view;
        var record = gridPanel.getStore().getAt(rowIndex);
        var mainView = Ext.getCmp('mainViewPort').getReferences().mainView;
        if (record) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel') == null) {
                Ext.Ajax.request({
                    url: '/api/ordersheet/get/' + record.data.id,
                    method: 'GET',
                    success: function(result, request) {
                        try {
                            var response = Ext.decode(result.responseText);
                            if (response.success) {
                                var tab = mainView.add({
                                    xtype: 'ViewDonDatHang',
                                    recordView: record,
                                    gridView: gridPanel,
                                    dataObject: response.data,
                                    closable: true,
                                    doorType: record.data.frame_type,
                                    iconCls: 'fas fa-file-invoice-dollar',
                                    title: "<b>ĐƠN HÀNG: " + record.data.sub_no.toUpperCase() + "</b>",
                                    id: 'ViewDonDatHang' + record.data.id + '-TabPanel',
                                    listeners: {
                                        render: function() {
                                            ClassMain.hideLoadingMask();
                                        }
                                    }
                                });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function() {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function() {});
                        }
                    },
                    failure: function(result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function() {});
                    }
                });
            } else {
                mainView.setActiveTab(Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel'));
                ClassMain.hideLoadingMask();
            }
        }

    },

    onViewButtonClick: function() {
        var gridPanel = this.view;
        var record = gridPanel.getSelectionModel().getSelection()[0];
        var mainView = Ext.getCmp('mainViewPort').getReferences().mainView;
        if (record) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel') == null) {
                Ext.Ajax.request({
                    url: '/api/ordersheet/get/' + record.data.id,
                    method: 'GET',
                    success: function(result, request) {
                        try {
                            var response = Ext.decode(result.responseText);
                            if (response.success) {
                                var tab = mainView.add({
                                    xtype: 'ViewDonDatHang',
                                    recordView: record,
                                    gridView: gridPanel,
                                    dataObject: response.data,
                                    closable: true,
                                    doorType: record.data.frame_type,
                                    iconCls: 'fas fa-file-invoice-dollar',
                                    title: "<b>ĐƠN HÀNG: " + record.data.sub_no.toUpperCase() + "</b>",
                                    id: 'ViewDonDatHang' + record.data.id + '-TabPanel',
                                    listeners: {
                                        render: function() {
                                            ClassMain.hideLoadingMask();
                                        }
                                    }
                                });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function() {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function() {});
                        }
                    },
                    failure: function(result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function() {});
                    }
                });
            } else {
                mainView.setActiveTab(Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel'));
                ClassMain.hideLoadingMask();
            }
        }

    },

    onReloadButtonClick: function() {
        var controller = this;
        controller.loadDataGrid();
    },

    onExportdButtonClick: function(btn) {
        var controller = this,
            view_panel = controller.view,
            recordView = view_panel.getSelectionModel().getSelection()[0];

        if (recordView) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', function(btn, text) {
                if (btn == "yes") {
                    var url = 'api/ordersheet/download/' + recordView.data.id;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btn
                    });
                    ClassMain.saveFile(url, recordView.data.sub_no + '.zip');
                }
            });

        }
    },

    onTinhGiaButtonClick: function(btn) {
        var controller = this,
            view_panel = controller.view,
            recordView = view_panel.getSelectionModel().getSelection()[0];

        if (recordView) {
            var windowss = Ext.create('Ext.window.Window', {
                title: 'Tính giá đơn đặt hàng: ' + recordView.data.sub_no,
                height: 410,
                width: 490,
                layout: 'border',
                border: false,
                maximizable: true,
                iconCls: 'fas fa-hand-holding-usd',
                modal: true,
                items: [{
                    xtype: 'TinhGiaDonDatHang',
                    region: 'center',
                    autoScroll: true,
                    gridPanel: view_panel,
                    recordPanel: recordView
                }]
            }).show();
        }
    },

    onExportGriddButtonClick: function() {

    },

    loadDataGrid: function() {
        var controller = this;
        var refs = controller.getReferences();
        var gird = controller.getView();
        var SearchField = refs.SearchField;
        var FilterMenu = refs.FilterMenu;
        var refs_FilterMenu = FilterMenu.getMenu().getReferences();
        var txt_search = "";
        var arr_filter = [];
        var txt_filter = "";
        if (FilterMenu) {
            for (var ref in refs_FilterMenu) {
                if (refs_FilterMenu[ref].getValue() !== "" && refs_FilterMenu[ref].getValue() !== null && refs_FilterMenu[ref].fieldIndex) {
                    arr_filter.push(refs_FilterMenu[ref].fieldIndex + ":eq_" + refs_FilterMenu[ref].getValue());
                }
            }
        }
        txt_filter = arr_filter.join(",")
        if (SearchField) {
            if (SearchField.getValue() !== "" && SearchField.getValue() !== null) {
                txt_search = SearchField.getValue();
            }
        }
        if (gird.filterString) {
            txt_search = gird.filterString;
        }
        if (gird) {
            if (txt_filter !== "") {
                gird.getStore().getProxy().setExtraParam("filter", txt_filter);
            } else {
                delete gird.getStore().getProxy().extraParams["filter"];
            }
            if (txt_search !== "") {
                gird.getStore().getProxy().setExtraParam("query", txt_search);
            } else {
                delete gird.getStore().getProxy().extraParams["query"];
            }
            gird.getStore().load();
        }
    }
});