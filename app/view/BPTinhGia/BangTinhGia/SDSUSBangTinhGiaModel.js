Ext.define('CMS.view.BPTinhGia.BPTinhGia.SDSUSBangTinhGiaModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.SDSUSBangTinhGia',
    stores: {},
    data: {
        totalPrice: null,
		framePrice: null,
		leafPrice: null,
        isCalculated: false,
        isSaved: false,
        orderSheetPriceId: null,
        orderSheetPriceFilename: null
    },
	
});
