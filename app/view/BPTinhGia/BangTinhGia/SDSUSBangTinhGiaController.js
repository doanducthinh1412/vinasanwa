Ext.define('CMS.view.BPTinhGia.BangTinhGia.SDSUSBangTinhGiaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.SDSUSBangTinhGia',
	onTinhGiaButtonClick: function() {
		var me = this;
		var view = me.getView();
		view.el.mask("Đang thực hiện tính giá ...");
		var viewModel = view.getViewModel();
		var grid = view.orderSheetInformationGrid;
		var store = grid.getStore();
		var idTemplate = view.getFrameData().id;
		var templateData = [];
		var rowData = store.getRange();
		for (i=0;i<rowData.length;i++) {
			templateData.push(rowData[i].data)
		}
		Ext.Ajax.request({
			url: "/api/order-sheet-price/calculate",
			method: 'POST',
			params: {
				'excel_template': idTemplate,
				'template_data':  Ext.encode(templateData)
			},
			success: function(response) {
				view.el.unmask();
				var response = Ext.decode(response.responseText);
				if (response.success) {
					Ext.MessageBox.alert('Thông báo', 'Thực hiện tính giá thành công', function() {
						viewModel.set('totalPrice', response.data.total_price);
						var detailPrice = response.data.detail_price;
						for(i=0;i<detailPrice.length;i++){
							if (detailPrice[i].key == 'frame_total') {
								viewModel.set('framePrice',detailPrice[i].value);
							}
							if (detailPrice[i].key == 'leaf_total') {
								viewModel.set('leafPrice',detailPrice[i].value)
							}
						}
						viewModel.set('isCalculated', true);
					});
				}
			},
			failure: function(response) {
				view.el.unmask();
			}
		});
	},
	onSaveButtonClick: function() {
		var me = this;
		var view = me.getView();
		var viewModel = view.getViewModel();
		var dataPrice = viewModel.get('dataPrice');
		var grid = view.orderSheetInformationGrid;
		var store = grid.getStore();
		var idTemplate = view.getFrameData().id;
		var templateData = [];
		var rowData = store.getRange();
		for (i=0;i<rowData.length;i++) {
			templateData.push(rowData[i].data)
		}
		var formPanel = Ext.create('Ext.form.Panel',{
			bodyPadding: 10,
			layout: 'anchor',
			defaults: {
				xtype: 'textfield',
				width: '100%',
				msgTarget: 'under',
                labelAlign: 'left',
                autoFitErrors: true,
                labelStyle: 'font-weight:bold',
			},
			items: [{
				xtype: 'textfield',
				fieldLabel: 'Price (US$)',
				labelWidth: 120,
				value: viewModel.get('totalPrice'),
				name: 'total_price',
			},{
				xtype: 'textfield',
				fieldLabel: 'Frame (US$)',
				labelWidth: 120,
				value: dataPrice.detail_price.frame_total,
				submitValue:false
			},{
				xtype: 'textfield',
				fieldLabel: 'Leaf (US$)',
				labelWidth: 120,
				value: dataPrice.detail_price.leaf_total,
				submitValue:false
			},{
				xtype: 'textarea',
				fieldLabel: 'Note',
				labelWidth: 120,
				name: 'note'
			},{
				xtype: 'hiddenfield',
				value: Ext.encode(templateData),
				name: 'excel_data'
			},{
				xtype: 'hiddenfield',
				name: 'detail_price',
				value: Ext.encode(dataPrice.detail_price)
			},{
				xtype: 'hiddenfield',
				name: 'excel_template',
				value: view.getFrameData().id
			},{
				xtype: 'hiddenfield',
				name: 'order_sheet',
				value: view.getDataPanel().id
			}],
			buttons: ['->', {
				text: 'Lưu',
				glyph: 'xf00c@FontAwesome',
				ui: 'soft-orange',
				handler: function() {
					formPanel.up('window').el.mask("Đang lưu kết quả tính giá ...");
					formPanel.getForm().submit({
						url: '/api/order-sheet-price/create',
						success: function(form, action){
							if (action.result.success) {
								console.log(action.result);
								formPanel.up('window').close();
								Ext.MessageBox.alert('Thông báo', 'Lưu kết quả tính giá thành công', function() {
									//formPanel.up('window').close();
								});
								viewModel.set('isSaved', true);
								viewModel.set('orderSheetPriceId', action.result.data.order_sheet_price_id);
								viewModel.set('orderSheetPriceFilename', action.result.data.file_name);
							} else {
								Ext.MessageBox.alert('Thông báo', 'Lưu kết quả tính giá không thành công', function() {
									formPanel.up('window').el.unmask();
								});
							}
						},
						failure: function(response){
							Ext.MessageBox.alert('Thông báo', 'Lưu kết quả tính giá không thành công', function() {
								formPanel.up('window').el.unmask();
							});
						},
					});
				},
				formBind: true
			}, {
				text: 'Hủy bỏ',
				glyph: 'xf00d@FontAwesome',
				handler: function() {
					this.up('window').close();
				}
			}, '->']
		});
		var windowss = Ext.create('Ext.window.Window', {
            title: 'Xác nhận lưu thông tin tính giá',
            height: 320,
            width: 500,
            layout: 'fit',
            border: false,
            maximizable: true,
            glyph: 'xf055@FontAwesome',
            modal: true,
			items: [formPanel]
		}).show();

	},
	onDownloadButtonClick: function() {
		var me = this;
		var view = me.getView();
		var viewModel = view.getViewModel();
		var url = '/api/order-sheet-price/download-result/' + viewModel.get('orderSheetPriceId');
		ClassMain.saveFile(url,viewModel.get('orderSheetPriceFilename'));
	}
});