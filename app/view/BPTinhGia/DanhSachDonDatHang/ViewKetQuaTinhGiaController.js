Ext.define('CMS.view.BPTinhGia.DanhSachDonDatHang.ViewKetQuaTinhGiaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.ViewKetQuaTinhGia',
    onDeleteButtonClick: function() {
		var me = this;
		var view = me.getView();
		var gridPanel = view.getGridPanel();
		var tinhGiaData = view.getTinhGiaData();
		Ext.Ajax.request({
			url: '/api/order-sheet-price/delete/' + tinhGiaData.id,
			method: 'GET',
			success: function(response) {
				var jsonData = JSON.parse(response.responseText);
				if (jsonData.success) {
					Ext.MessageBox.alert('Thông báo', 'Xóa kết quả tính giá thành công', function () {
						view.up('window').close();
						gridPanel.getStore().load();
					});
				} else {
					Ext.MessageBox.alert('Thông báo', 'Xóa kết quả tính giá không thành công', function () {

					});
				}
			},
			failure: function() {
				Ext.MessageBox.alert('Thông báo', 'Xóa kết quả tính giá không thành công', function () {

				});
			}
		});
	},
	onDownloadButtonClick: function() {
		var me = this;
		var view = me.getView();
		var gridPanel = view.getGridPanel();
		var tinhGiaData = view.getTinhGiaData();
		var url = '/api/order-sheet-price/download-result/' + tinhGiaData.id;
		ClassMain.saveFile(url,tinhGiaData.excel_template_name);
	}
});
