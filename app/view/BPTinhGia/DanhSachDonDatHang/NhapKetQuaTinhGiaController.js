Ext.define('CMS.view.BPTinhGia.DanhSachDonDatHang.NhapKetQuaTinhGiaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.NhapKetQuaTinhGia',
    uploadFileTinhGia: function () {
        var controller = this;
        var view = controller.view;
        var win = view.up('window');
		var detailPrice = controller.getDetailPrice();
        var frm = view.getForm();
        if (frm.isValid()) {
            win.hide();
			frm.findField("detail_price").setValue(Ext.encode(detailPrice));
			console.log(frm.getValues());
            frm.submit({
                url: '/api/order-sheet-price/create',
                clientValidation: true,
                method: 'POST',
                success: function (form, action) {
                    Ext.MessageBox.alert('Thông báo', 'Tính giá thành công', function () {
                        win.close();
                        view.getGridPanel().getStore().load({
                            callback: function () {}
                        });
                    });
                },
                failure: function (form, action) {
                    Ext.Msg.alert('Thông báo', 'Tính giá không thành công: ', function () {
                        win.show();
                    });
                },
            })
        } else {
            win.hide();
            Ext.Msg.alert('Thông báo', 'Tính giá không thành công: Các trường bắt buộc không được để trống!', function () {
                win.show();
            });
        }
    },
    getDetailPrice: function () {
        var controller = this;
        var view = controller.view;
        var detailPanel = view.getReferences().detailPanel;
        var list_fc = detailPanel.items.items;
		var detailPrice = [];
        for (i = 0; i < list_fc.length; i++) {
			var fc = list_fc[i];
			var name = fc.getComponent('name').getValue();
			var key = controller.convertNameToKey(name);
			var value = fc.getComponent('value').getValue();
			detailPrice.push({
				'name': name,
				'key': key,
				'value': value
			})
		}
		return detailPrice
    },
    convertNameToKey: function (str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
		str = str.replace(/ {1,}/g," ");
		str = str.replace(/ /g,"_");
        return str;
    }
});
