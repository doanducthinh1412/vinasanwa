Ext.define('CMS.view.BPTinhGia.DanhSachDonDatHang.DanhSachDonDatHangModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.DanhSachDonDatHang_TinhGia',
    stores: {},
    data: {
        actionMenu: Ext.create('Ext.menu.Menu', {
            plain: true,
            mouseLeaveDelay: 10,
            items: []
        })
    }
});