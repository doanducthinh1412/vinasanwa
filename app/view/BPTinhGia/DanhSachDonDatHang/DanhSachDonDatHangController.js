Ext.define('CMS.view.BPTinhGia.DanhSachDonDatHang.DanhSachDonDatHangController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.DanhSachDonDatHang_TinhGia',
    onItemActionMenu: function (view, record, e) {
        var controller = this;
        var gridPanel = controller.view;
        var menu = controller.createMenu(record);
        controller.getViewModel().set('actionMenu', menu);
    },
    onItemContextMenu: function (view, record, item, index, e) {
        var controller = this;
        var gridPanel = controller.view;
        var position = [e.getX() - 10, e.getY() - 10];
        e.stopEvent();
        var menu = controller.createMenu(record, position);
        //menu.showAt(position);
    },

    createMenu: function (record, position) {
        var controller = this;
        var frame_type = record.data.frame_type;
        if (record) {
            var order_sheet_price_status = record.data.status.order_sheet_price;

            Ext.Ajax.request({
                url: 'api/price-templates/list/' + frame_type,
                method: 'GET',
                success: function (response) {
                    var jsonData = JSON.parse(response.responseText);
                    var is_exist_template = false;
                    if (jsonData.success) {
                        var data = jsonData.data;
                        if (data == null) {
                            data = []
                        }
                        var template_menu = [];
                        for (i = 0; i < data.length; i++) {
                            if (frame_type == data[i].frame_type) {
                                is_exist_template = true;
                            }
                            template_menu.push({
                                text: data[i].title,
                                frameData: data[i],
                                frameType: data[i].frame_type,
                                iconCls: 'fas fa-file-export text-dark-blue',
                                handler: function () {
                                    var btn = this;
                                    controller.onTinhGiaButtonClick(btn);
                                }
                            })
                        }
                        if (!is_exist_template) {
                            template_menu.push({
                                text: 'Không có file template tính giá',
                                iconCls: 'fas fa-file-export text-dark-gray',
                                handler: function () {}
                            })
                        }
                        template_menu.push('-');
                        template_menu.push({
                            text: 'Nhập kết quả tính giá',
                            iconCls: 'fas fa-file-export text-dark-blue',
                            handler: function () {
                                var btn = this;
                                controller.onNhapKetQuaTinhGiaButtonClick(record);
                            }
                        })
                        var menu = Ext.create('Ext.menu.Menu', {
                                plain: true,
                                mouseLeaveDelay: 10,
                                items: [{
                                        text: 'Xem thông tin',
                                        iconCls: 'fas fa-edit text-dark-blue',
                                        handler: function () {
                                            controller.onViewButtonClick();
                                        }
                                    }, {
                                        text: 'Tải File - Đơn đặt hàng',
                                        iconCls: 'fas fa-file-export text-dark-blue',
                                        handler: function () {
                                            controller.onExportdButtonClick();
                                        }
                                    }, '-', {
                                        text: 'Xem kết quả tính giá',
                                        iconCls: 'fas fa-hand-holding-usd text-dark-blue',
                                        hidden: !order_sheet_price_status,
                                        handler: function () {
                                            controller.onShowTinhGiaButtonClick(record)
                                        }
                                    }, {
                                        text: 'Tính giá - Đơn đặt hàng',
                                        iconCls: 'fas fa-hand-holding-usd text-dark-blue',
                                        menu: template_menu,
                                        hidden: order_sheet_price_status == 1 ? true : false
                                    }
                                ]
                            });
                        menu.showAt(position);
                    }
                },
                failure: function (response) {
                    console.log(response)
                }
            });

        } else {
            return Ext.create('Ext.menu.Menu', {
                plain: true,
                mouseLeaveDelay: 10,
                items: []
            });
        }
    },
    onNhapKetQuaTinhGiaButtonClick: function (record) {
        var me = this;
		var view = me.getView();
		var frm = Ext.create('CMS.view.BPTinhGia.DanhSachDonDatHang.NhapKetQuaTinhGia',{
			recordPanel: record,
			gridPanel: view
		});
        var windowss = Ext.create('Ext.window.Window', {
                title: 'Nhập kết quả tính giá',
                minHeight: 320,
                height: '80%',
                width: 500,
                scrollable: 'y',
                border: false,
                maximizable: true,
                glyph: 'xf055@FontAwesome',
                modal: true,
                items: [frm],
				buttons : ['->', {
					text: 'Lưu',
					glyph: 'xf00c@FontAwesome',
					ui: 'soft-orange',
					handler: function() {
						frm.getController().uploadFileTinhGia();
					}
				}, {
					text: 'Hủy bỏ',
					glyph: 'xf00d@FontAwesome',
					handler: function () {
						this.up('window').close();
					}
				}, '->']
                
            }).show();

    },
    
    onShowTinhGiaButtonClick: function (record) {
		var me = this;
		var view = me.getView();
        var order_sheet_price_id = record.data.order_sheet_price;
        Ext.Ajax.request({
            url: '/api/order-sheet-price/get/' + order_sheet_price_id,
            method: 'GET',
            success: function (response) {
                var jsonData = JSON.parse(response.responseText);
                var data = jsonData.data;
                var formPanel = Ext.create('CMS.view.BPTinhGia.DanhSachDonDatHang.ViewKetQuaTinhGia',{
					tinhGiaData: data,
					gridPanel: view
				});
                var windowss = Ext.create('Ext.window.Window', {
                        title: 'Thông tin tính giá',
                        minHeight: 320,
                        width: 500,
                        layout: 'fit',
                        border: false,
                        maximizable: true,
                        glyph: 'xf055@FontAwesome',
                        modal: true,
                        items: [formPanel]
                    }).show();
            },
            failure: function (response) {}
        });
    },
    onTinhGiaButtonClick: function (btn) {
        var gridPanel = this.view;
        var record = gridPanel.getSelectionModel().getSelection()[0];
        var frameType = btn.frameType;
        var doorText = btn.text;
        var mainView = Ext.getCmp('mainViewPort').getReferences().mainView;
        ClassMain.showLoadingMask();
        if (Ext.getCmp('SDSUSBangTinhGia' + record.data.id + '-TabPanel') == null) {
            Ext.Ajax.request({
                url: '/api/ordersheet/get/' + record.data.id,
                method: 'GET',
                success: function (result, request) {
                    try {
                        var response = Ext.decode(result.responseText);
                        if (response.success) {
                            var tab = mainView.add({
                                    xtype: 'SDSUSBangTinhGia',
                                    frameType: frameType,
                                    frameTypeDetail: btn.frame_type_detail,
                                    frameId: btn.id,
                                    orderSheetData: response.data,
                                    closable: true,
                                    iconCls: 'fas fa-plus-circle',
                                    title: "<b>Sub No " + record.data.sub_no + " - " + doorText + '</b>',
                                    id: 'SDSUSBangTinhGia' + record.data.id + '-TabPanel',
                                    dataPanel: record.data,
                                    frameData: btn.frameData,
                                    listeners: {
                                        render: function () {
                                            ClassMain.hideLoadingMask();
                                        }
                                    }
                                });
                            mainView.setActiveTab(tab);
                        } else {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function () {});
                        }
                    } catch (err) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function () {});
                    }
                },
                failure: function (result, request) {
                    ClassMain.hideLoadingMask();
                    Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function () {});
                }
            });
        } else {

            mainView.setActiveTab(Ext.getCmp('SDSUSBangTinhGia' + record.data.id + '-TabPanel'));
            ClassMain.hideLoadingMask();
        }
    },

    onViewButtonClickGrid: function (gridPanel, rowIndex, colIndex) {
        var gridPanel = this.view;
        var record = gridPanel.getStore().getAt(rowIndex);
        var mainView = Ext.getCmp('mainViewPort').getReferences().mainView;
        if (record) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel') == null) {
                Ext.Ajax.request({
                    url: '/api/ordersheet/get/' + record.data.id,
                    method: 'GET',
                    success: function (result, request) {
                        try {
                            var response = Ext.decode(result.responseText);
                            if (response.success) {
                                var tab = mainView.add({
                                        xtype: 'ViewDonDatHang',
                                        recordView: record,
                                        gridView: gridPanel,
                                        dataObject: response.data,
                                        closable: true,
                                        doorType: record.data.frame_type,
                                        iconCls: 'fas fa-file-invoice-dollar',
                                        title: "<b>ĐƠN HÀNG: " + record.data.sub_no.toUpperCase() + "</b>",
                                        id: 'ViewDonDatHang' + record.data.id + '-TabPanel',
                                        listeners: {
                                            render: function () {
                                                ClassMain.hideLoadingMask();
                                            }
                                        }
                                    });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function () {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function () {});
                        }
                    },
                    failure: function (result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function () {});
                    }
                });
            } else {
                mainView.setActiveTab(Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel'));
                ClassMain.hideLoadingMask();
            }
        }

    },

    onViewButtonClick: function () {
        var gridPanel = this.view;
        var record = gridPanel.getSelectionModel().getSelection()[0];
        var mainView = Ext.getCmp('mainViewPort').getReferences().mainView;
        if (record) {
            ClassMain.showLoadingMask();
            if (Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel') == null) {
                Ext.Ajax.request({
                    url: '/api/ordersheet/get/' + record.data.id,
                    method: 'GET',
                    success: function (result, request) {
                        try {
                            var response = Ext.decode(result.responseText);
                            if (response.success) {
                                var tab = mainView.add({
                                        xtype: 'ViewDonDatHang',
                                        recordView: record,
                                        gridView: gridPanel,
                                        dataObject: response.data,
                                        closable: true,
                                        doorType: record.data.frame_type,
                                        iconCls: 'fas fa-file-invoice-dollar',
                                        title: "<b>ĐƠN HÀNG: " + record.data.sub_no.toUpperCase() + "</b>",
                                        id: 'ViewDonDatHang' + record.data.id + '-TabPanel',
                                        listeners: {
                                            render: function () {
                                                ClassMain.hideLoadingMask();
                                            }
                                        }
                                    });
                                mainView.setActiveTab(tab);
                            } else {
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function () {});
                            }
                        } catch (err) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function () {});
                        }
                    },
                    failure: function (result, request) {
                        ClassMain.hideLoadingMask();
                        Ext.MessageBox.alert('Thông báo', 'Không tải được đơn đặt hàng', function () {});
                    }
                });
            } else {
                mainView.setActiveTab(Ext.getCmp('ViewDonDatHang' + record.data.id + '-TabPanel'));
                ClassMain.hideLoadingMask();
            }
        }

    },

    onReloadButtonClick: function () {
        var controller = this;
        controller.loadDataGrid();
    },

    onExportdButtonClick: function (btn) {
        var controller = this,
        view_panel = controller.view,
        recordView = view_panel.getSelectionModel().getSelection()[0];

        if (recordView) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn tải về?', function (btn, text) {
                if (btn == "yes") {
                    var url = 'api/ordersheet/download/' + recordView.data.id;
                    Ext.MessageBox.show({
                        title: 'Tải xuống',
                        msg: 'Đang tải xuống...',
                        progressText: 'Đang tải xuống...',
                        width: 300,
                        progress: true,
                        closable: false,
                        animateTarget: btn
                    });
                    ClassMain.saveFile(url, recordView.data.sub_no + '.zip');
                }
            });

        }
    },

    onCreateButtonClick: function (btn) {
        var controller = this,
        view_panel = controller.view,
        recordView = view_panel.getSelectionModel().getSelection()[0];
        var editable = false;
        if (recordView.data.order_sheet_price && recordView.data.order_sheet_price.length > 0) {
            editable = true;
        } else {
            editable = false;
        }
        if (recordView) {
            var windowss = Ext.create('Ext.window.Window', {
                    title: 'Tính giá đơn đặt hàng: ' + recordView.data.sub_no,
                    height: 410,
                    width: 490,
                    layout: 'border',
                    border: false,
                    maximizable: true,
                    iconCls: 'fas fa-hand-holding-usd',
                    modal: true,
                    items: [{
                            xtype: 'TinhGiaDonDatHang',
                            region: 'center',
                            autoScroll: true,
                            gridPanel: view_panel,
                            recordPanel: recordView,
                            modeedit: editable
                        }
                    ]
                }).show();
        }
    },

    onSendTinhGiaButtonClick: function () {
        var controller = this;
        var refs = controller.getReferences();
        var gird = controller.getView();
        var record = gird.getSelectionModel().getSelection()[0];
        if (record.get('order_sheet_price')) {
            Ext.MessageBox.confirm('Thông báo', 'Bạn có muốn gửi kết quả vẽ sang BP Kế hoạch?', function (btn, text) {
                if (btn == "yes") {
                    Ext.Ajax.request({
                        url: '/api/order-sheet-price/send/' + record.get('order_sheet_price')[0].id,
                        method: 'GET',
                        success: function (result, request) {
                            try {
                                var response = Ext.decode(result.responseText);
                                if (response.success) {
                                    gird.getStore().load();
                                    Ext.MessageBox.alert('Thông báo', 'Đã gửi kết quả thành công', function () {});
                                } else {
                                    Ext.MessageBox.alert('Thông báo', 'Gửi không thành công', function () {});
                                }
                            } catch (err) {
                                Ext.MessageBox.alert('Thông báo', 'Gửi không thành công', function () {});
                            }
                        },
                        failure: function (result, request) {
                            Ext.MessageBox.alert('Thông báo', 'Gửi không thành công', function () {});
                        }
                    });
                }
            });
        }
    },

    onExportGriddButtonClick: function () {},

    loadDataGrid: function () {
        var controller = this;
        var refs = controller.getReferences();
        var gird = controller.getView();
        var SearchField = refs.SearchField;
        var FilterMenu = refs.FilterMenu;
        var refs_FilterMenu = FilterMenu.getMenu().getReferences();
        var txt_search = "";
        var arr_filter = [];
        var txt_filter = "";
        if (FilterMenu) {
            for (var ref in refs_FilterMenu) {
                if (refs_FilterMenu[ref].getValue() !== "" && refs_FilterMenu[ref].getValue() !== null && refs_FilterMenu[ref].fieldIndex) {
                    arr_filter.push(refs_FilterMenu[ref].fieldIndex + ":eq_" + refs_FilterMenu[ref].getValue());
                }
            }
        }
        txt_filter = arr_filter.join(",")
            if (SearchField) {
                if (SearchField.getValue() !== "" && SearchField.getValue() !== null) {
                    txt_search = SearchField.getValue();
                }
            }
            if (gird.filterString) {
                txt_search = gird.filterString;
            }
            if (gird) {
                if (txt_filter !== "") {
                    gird.getStore().getProxy().setExtraParam("filter", txt_filter);
                } else {
                    delete gird.getStore().getProxy().extraParams["filter"];
                }
                if (txt_search !== "") {
                    gird.getStore().getProxy().setExtraParam("query", txt_search);
                } else {
                    delete gird.getStore().getProxy().extraParams["query"];
                }
                gird.getStore().load();
            }
    }
});
