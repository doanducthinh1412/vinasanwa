Ext.define('CMS.view.BPTinhGia.DanhSachDonDatHang.TinhGiaDonDatHangController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.TinhGiaDonDatHang',
    onClickTinhGiaButton: function() {
        var controller = this;
        var view = controller.view;
        var modeedit = view.getModeedit();
        var win = view.up('window');
        var frm = view.getForm();
        if (frm.isValid()) {
            win.hide();
            frm.submit({
                url: modeedit === true ? '/api/order-sheet-price/edit/' + view.getRecordPanel().data.order_sheet_price[0].id : '/api/order-sheet-price/create',
                clientValidation: true,
                method: 'POST',
                success: function(form, action) {
                    Ext.MessageBox.alert('Thông báo', 'Tính giá thành công', function() {
                        win.close();
                        view.getGridPanel().getStore().load({
                            callback: function() {}
                        });
                    });
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Thông báo', 'Tính giá không thành công: ', function() {
                        win.show();
                    });
                },
            })
        } else {
            win.hide();
            Ext.Msg.alert('Thông báo', 'Tính giá không thành công: Các trường bắt buộc không được để trống!', function() {
                win.show();
            });
        }
    }
});