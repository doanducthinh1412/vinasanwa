Ext.define("CMS.ClassMain", {
    singleton: true,
    alternateClassName: ['ClassMain'],

    checkSession: function() {
        Ext.create('CMS.view.main.Main', {
            listeners: {
                render: function() {
                    Ext.getCmp('mainViewPort').getController().redirectTo('dashboard');
                }
            }
        });
        try {
            Ext.Ajax.request({
                url: '/api/users/checkcookie',
                method: 'GET',
                timeout: 30000,
                useDefaultXhrHeader: false,
                cors: true,
                success: function(result, request) {
                    try {
                        var response = Ext.decode(result.responseText);
                        if (response.success) {
                            Config.userInfo = response.data;
                            if (!Ext.getCmp('dashboard-TabPanel')) {
                                var mainView = Ext.getCmp('mainViewPort').getReferences().mainView;
                                var tab = mainView.add({
                                    closable: false,
                                    xtype: 'Dashboard',
                                    iconCls: 'x-fa fa-desktop',
                                    title: "<b>DASHBOARD</b>",
                                    id: 'dashboard-TabPanel',
                                });
                                mainView.setActiveTab(tab);
                            }
                            Ext.getCmp('mainViewPort').initPermission();
                            Ext.getCmp('mainViewPort').getController().getViewModel().set('MainTxtUserName', Config.userInfo.last_name + " " + Config.userInfo.first_name);
                            Ext.getCmp('mainViewPort').getController().getReferences().avartaUser.setSrc('/api/users/image?_dc=' + new Date().getTime());
                            if (Config.userInfo.lang == null || Config.userInfo.lang == "") {
                                Config.defaultLanguage = 'vi'
                            } else {
                                Config.defaultLanguage = Config.userInfo.lang
                            }
                            Ext.getCmp('mainViewPort').getController().getViewModel().set('language', Config.language[Config.defaultLanguage]);
                            ClassMain.hideLoading();
                        } else {
                            ClassMain.hideLoading();
                            Ext.getCmp('mainViewPort').getController().redirectTo('login');
                        }
                    } catch (err) {
                        console.log(err);
                        ClassMain.hideLoading();
                        Ext.getCmp('mainViewPort').getController().redirectTo('login');
                    }
                },
                failure: function(result, request) {
                    ClassMain.hideLoading();
                    Ext.getCmp('mainViewPort').getController().redirectTo('login');
                }
            });
        } catch (err) {
            ClassMain.hideLoading();
            ClassMain.showErrors("Lỗi function ClassMain.checkSession", err);
            Ext.getCmp('mainViewPort').getController().redirectTo('login');
        }
    },

    checkUser: function() {
        if (Config.userInfo) {
            if (Config.userInfo.role) {
                if (Config.userInfo.role == "admin" || Config.userInfo.role == "superadmin") {
                    return false;
                }
            }
        }
        return true;
    },

    logOut: function() {
        try {
            Ext.util.Cookies.clear('PHPSESSID');
            var messageLogOut = Ext.MessageBox.show({
                msg: 'Đang đăng xuất hệ thống. Xin chờ...',
                progressText: 'Đang đăng xuất...',
                width: 300,
                wait: {
                    interval: 200
                }
            });
            Ext.Ajax.request({
                url: '/api/users/logout',
                method: 'POST',
                timeout: 30000,
                useDefaultXhrHeader: false,
                cors: true,
                scope: this,
                success: function(result, request) {
                    messageLogOut.hide();
                    location.reload();
                    //Ext.getCmp('mainViewPort').getController().redirectTo('login');
                },
                failure: function(result, request) {
                    messageLogOut.hide();
                    location.reload();
                    //Ext.getCmp('mainViewPort').getController().redirectTo('login');
                }
            });
        } catch (err) {
            location.reload();
            ClassMain.showErrors("Lỗi function ClassMain.logOut", err);
        }
    },

    hideLoading: function() {
        try {
            var hideMask = function() {
                Ext.get('loading').remove();
                Ext.fly('loading-mask').hide();
            };
            Ext.defer(hideMask, 250);
        } catch (err) {
            ClassMain.showErrors("Lỗi function ClassMain.hideLoading", err);
        }
    },

    showLoadingMask: function() {
        Ext.getBody().mask("Đang tải...");
    },

    hideLoadingMask: function() {
        Ext.getBody().unmask();
    },

    showLoadingMaskPanel: function(panel) {
        if (panel) {
            if (!panel.maskLoading) {
                panel.maskLoading = new Ext.LoadMask({
                    msg: 'Đang tải...',
                    target: panel
                });
            }
            panel.maskLoading.show();
        }
    },

    hideLoadingMaskPanel: function(panel) {
        if (panel) {
            if (panel.maskLoading) {
                panel.maskLoading.hide();
            }
        }
    },

    showErrors: function(notice, error) {
        try {
            ClassMain.hideLoading();
            console.log(notice);
            console.log(error);
            Ext.MessageBox.show({
                title: 'Thông báo',
                msg: 'Hệ thống bận. Vui lòng thử lại sau!',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
        } catch (err) {
            ClassMain.showErrors("Lỗi function ClassMain.showErrors", err);
        }
    },

    showLogger: function(notice) {
        try {
            if (Config.modeDebug) {
                console.log(notice);
            }
        } catch (err) {
            ClassMain.showErrors("Lỗi function ClassMain.showLogger", err);
        }
    },

    createViewPortMain: function() {

    },

    createTabViewPortMain: function(objPanel, nodeTree) {
        if (nodeTree == undefined) {
            nodeTree = false;
        }
        var mainView = Ext.getCmp('mainViewPort').getReferences().mainView;
        if (Ext.getCmp(objPanel.id + '-TabPanel') == null) {
            var tab = mainView.add({
                closable: true,
                xtype: objPanel.xtype,
                iconCls: objPanel.iconCls,
                title: "<b>" + objPanel.title + "</b>",
                id: objPanel.id + '-TabPanel',
                nodeTree: nodeTree
            });
            mainView.setActiveTab(tab);
        } else {
            mainView.setActiveTab(Ext.getCmp(objPanel.id + '-TabPanel'));
        }
    },

    onExpandViewPortMain: function(btn) {
        var mainView = Ext.getCmp('mainViewPort');
        if (btn.pressed) {
            mainView.down().getDockedComponent('headerBar').hide();
            mainView.down().getDockedComponent('footerBar').hide();
            btn.setIconCls('fas fa-compress-arrows-alt');
        } else {
            mainView.down().getDockedComponent('headerBar').show();
            mainView.down().getDockedComponent('footerBar').show();
            btn.setIconCls('fas fa-expand-arrows-alt');
        }
    },

    createWindowViewPortMain: function(objPanel) {
        var objPanelNew = {
            xtype: objPanel.xtype
        };
        Object.assign(objPanelNew, objPanel.data);
        Ext.create('Ext.window.Window', {
            title: objPanel.title,
            iconCls: objPanel.iconCls,
            height: '90%',
            width: '90%',
            layout: 'fit',
            modal: true,
            items: objPanelNew
        }).show();
    },

    checkPermission: function(module_, controller_, action_) {
        var permission = false;
        if (Config.userInfo.module && module_ && controller_ && action_) {
            var userPermission = Config.userInfo.module;
            if (userPermission[module_]) {
                if (userPermission[module_][controller_]) {
                    if (userPermission[module_][controller_][action_]) {
                        permission = userPermission[module_][controller_][action_];
                    }
                }
            }
        }
        return permission;
    },

    checkDepartment: function(department_) {
        var permission = false;
        if (Config.userInfo.department && department_) {
            var id_department = Config.userInfo.department;
            var id_department_check = null;
            if (department_ === "ve") {
                id_department_check = "2";
            } else if (department_ === "kehoach") {
                id_department_check = "1";
            } else if (department_ === "tinhgia") {
                id_department_check = "3";
            } else if (department_ === "muahang") {
                id_department_check = "4";
            } else if (department_ === "sanxuat") {
                id_department_check = "5";
            } else if (department_ === "kho") {
                id_department_check = "7";
            } else if (department_ === "quantri") {
                id_department_check = "0";
            }
            if (parseInt(id_department) === parseInt(id_department_check)) {
                permission = true;
            }
        }
        return permission;
    },

    arrSum: function(arr) {
        total = 0;
        arr.forEach(function(key) {
            total = total + key[1];
        });
        return total;
    },
    arrSumArray: function(arr) {
        total = 0;
        arr.forEach(function(key) {
            total = total + key;
        });
        return total;
    },
    convertNull: function(value) {
        return (value == null) ? "" : value;
    },
    bytesToSize: function(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    },
    saveFile: function(url, filename) {
        if (url) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            xhr.onload = function() {
                let a = document.createElement('a');
                a.href = window.URL.createObjectURL(xhr.response); // xhr.response is a blob
                if (filename) {
                    a.download = filename; // Set the file name.
                }
                a.style.display = 'none';
                document.body.appendChild(a);
                a.click();
                delete a;
                Ext.MessageBox.hide();
            };
            xhr.open('GET', url);
            xhr.send();
        }
    },
    clearFileUpload: function(id) {
        fileField = document.getElementById(id);
        parentNod = fileField.parentNode;
        tmpForm = document.createElement("form");
        parentNod.replaceChild(tmpForm, fileField);
        tmpForm.appendChild(fileField);
        tmpForm.reset();
        parentNod.replaceChild(fileField, tmpForm);
    }
});