/* eslint-disable radix */
/* eslint-disable eol-last */
Ext.define('CMS.model.KeHoachSanXuat', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'bending', type: 'date' },
        'bending_result',
        'cancel_reason',
        'check_result',
        'color',
        { name: 'createdAt', type: 'auto' },
        { name: 'date_delivery', type: 'date' },
        { name: 'date_drawing', type: 'date' },
        { name: 'deletedAt', type: 'auto' },
        { name: 'finish_paint', type: 'date' },
        'finish_paint_result',
        { name: 'grew_press_asembly', type: 'date' },
        'grew_press_asembly_result',
        'id',
        'incharge',
        'inner_frame',
        'note',
        'order_no',
        'other_note',
        { name: 'other_num', type: 'int' },
        { name: 'other_remain', type: 'int' },
        { name: 'num_other_done', type: 'int' },
        { name: 'cancel_other_num', type: 'int' },
        'ghichu',
        'prepare_frame',
        { name: 'prime_paint', type: 'date' },
        'prime_paint_result',
        'product_check',
        'project_name',
        'remain',
        { name: 'shearing', type: 'date' },
        'shearing_result',
        { name: 'status', type: 'int' },
        'sub_no',
        'time',
        { name: 'total', type: 'int' },
        { name: 'tpp', type: 'date' },
        'tpp_result',
        'type',
        'type_note',
        'type_num',
        { name: 'updatedAt', type: 'auto' },
        'time_type',
        {
            name: 'frame_sd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        }, {
            name: 'frame_lsd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'leaf_sd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'leaf_lsd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'leaf_wd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'leaf_susd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'frame_flsd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'frame_wd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'frame_susd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'leaf_flsd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'frame_fsd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'leaf_fsd',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'sw',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'ssjp',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        }, {
            name: 'ssaichi',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        }, {
            name: 'ssgrill',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        { name: 'ss_m2', type: 'int' },
        {
            name: 'grs',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        }, {
            name: 'srn',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        }, {
            name: 's13',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        }, {
            name: 's14',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'frame_sp',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'leaf_sp',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'frame_sld',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'leaf_sld',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'lv',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_num) {
                            return parseInt(value) - parseInt(record.data.cancel_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_num) {
                            return parseInt(value) - parseInt(record.data.stop_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        {
            name: 'other',
            type: 'int',
            convert(value, record) {
                if (value) {
                    if (record.get('status') === 2) {
                        if (record.data.cancel_other_num) {
                            return parseInt(value) - parseInt(record.data.cancel_other_num);
                        }
                    } else if (record.get('status') === 5) {
                        if (record.data.stop_other_num) {
                            return parseInt(value) - parseInt(record.data.stop_other_num);
                        }
                    }
                    return parseInt(value);
                }
                return 0;
            },
        },
        { name: 'cancel_num', type: 'int' },
    ],
});