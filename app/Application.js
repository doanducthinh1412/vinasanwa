/* eslint-disable operator-linebreak */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-syntax */
/* eslint-disable eol-last */
/* eslint-disable no-extend-native */
/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('CMS.Application', {
    extend: 'Ext.app.Application',

    name: 'CMS',

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true,
        },
    },

    stores: [
        // TODO: add global / shared stores here
    ],

    defaultToken: 'login',

    launch() {
        document.onkeydown = function stopTabEvent() {
            if (window.event && window.event.keyCode === 9) { // Capture and remap TAB
                window.event.keyCode = 9;
            }

            if (window.event && window.event.keyCode === 9) { // New action for TAB
                return false;
            }
            return true;
        };
        if (!Object.prototype.forEach) {
            Object.defineProperty(Object.prototype, 'forEach', {
                value(callback, thisArg) {
                    if (this == null) {
                        throw new TypeError('Not an object');
                    }
                    thisArg = thisArg || window;
                    for (const key in this) {
                        if (this.hasOwnProperty(key)) {
                            callback.call(thisArg, this[key], key, this);
                        }
                    }
                },
            });
        }
        Array.prototype.sum = function sum(prop) {
            let total = 0;
            for (let i = 0, len = this.length; i < len; i += 1) {
                total += parseFloat(this[i][prop]);
            }
            return total;
        };
        Date.prototype.sameDay = function sameDay(d) {
            return this.getFullYear() === d.getFullYear() &&
                this.getDate() === d.getDate() &&
                this.getMonth() === d.getMonth();
        };

        Date.prototype.isLeapYear = function isLeapYear() {
            const year = this.getFullYear();
            if ((year && 3) !== 0) return false;
            return ((year % 100) !== 0 || (year % 400) === 0);
        };

        // Get Day of Year
        Date.prototype.getDOY = function getDOY() {
            const dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
            const mn = this.getMonth();
            const dn = this.getDate();
            let dayOfYear = dayCount[mn] + dn;
            if (mn > 1 && this.isLeapYear()) dayOfYear += 1;
            return dayOfYear;
        };

        /* window.oncontextmenu = function() {
            return false;
        }; */
        ClassMain.checkSession();
    },

    onAppUpdate() {
        Ext.Msg.confirm('Thông báo', 'Có một bản cập nhật mới. Bạn có muốn cập nhật phần mềm?',
            (choice) => {
                if (choice === 'yes') {
                    window.location.reload();
                }
            });
    },
});