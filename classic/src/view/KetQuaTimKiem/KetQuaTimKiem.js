Ext.define('CMS.view.KetQuaTimKiem.KetQuaTimKiem', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.panel.Panel'
    ],
    controller: 'KetQuaTimKiem',
    viewModel: 'KetQuaTimKiem',
    xtype: 'KetQuaTimKiem',
    layout: 'border',
    reference: 'KetQuaTimKiemPanel',
    autoScroll: true,
	config: {
		searchString : null
	},
    //emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    initComponent: function() {
        var me = this;
		var searchString = me.getSearchString();
		me.items = [{
			xtype: 'DanhMucDuAn',
			reference: 'DanhMucDuAnGrid',
			filterString: searchString,
			title: '<b>Dự án</b>',
			glyph: 'f039@FontAwesome',
			ui: 'light',
			cls: 'service-type shadow',
			region: 'north',
			height: '50%',
			emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
		},{
			xtype: 'QuanLyDonDatHang',
			reference: 'QuanLyDonDatHangGrid',
			filterString: searchString,
			title: '<b>Đơn đặt hàng</b>',
			glyph: 'f039@FontAwesome',
			ui: 'light',
			cls: 'service-type shadow',
			region: 'center',
			height: '50%',
			emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
		}];
        me.callParent();
    },
    
    reloadView: function() {
        var me = this;
		var searchString = me.getSearchString();
		var refs = me.getReferences();
        me.el.mask('Đang tải...');
		me.setTitle("<b>Kết quả tìm kiếm với từ khóa: " + searchString + "</b>",);
        var DanhMucDuAnGrid = refs.DanhMucDuAnGrid;
		var QuanLyDonDatHangGrid = refs.QuanLyDonDatHangGrid;
		DanhMucDuAnGrid.setFilterString(searchString);
		QuanLyDonDatHangGrid.setFilterString(searchString);
		DanhMucDuAnGrid.reloadView();
		QuanLyDonDatHangGrid.reloadView();
        me.el.unmask();
    }
});