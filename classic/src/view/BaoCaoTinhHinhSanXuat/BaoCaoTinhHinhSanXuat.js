Ext.define('CMS.view.BaoCaoTinhHinhSanXuat.BaoCaoTinhHinhSanXuat', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.grid.Panel'
    ],
    xtype: 'BaoCaoTinhHinhSanXuat',
    layout: 'border',
    reference: 'BaoCaoTinhHinhSanXuatGrid',
    items: [{
        xtype: 'grid',
        region: 'center',
        width: '100%',
        height: '100%',
        syncRowHeight: false,
        columnLines: true,
        title: 'Spreadsheet',
        store: Ext.create('Ext.data.Store', {
            fields: [
                { name: 'year', type: 'int' },
        
                { name: 'jan', type: 'int', allowNull: true },
                { name: 'feb', type: 'int', allowNull: true },
                { name: 'mar', type: 'int', allowNull: true },
                { name: 'apr', type: 'int', allowNull: true },
                { name: 'may', type: 'int', allowNull: true },
                { name: 'jun', type: 'int', allowNull: true },
                { name: 'jul', type: 'int', allowNull: true },
                { name: 'aug', type: 'int', allowNull: true },
                { name: 'sep', type: 'int', allowNull: true },
                { name: 'oct', type: 'int', allowNull: true },
                { name: 'nov', type: 'int', allowNull: true },
                { name: 'dec', type: 'int', allowNull: true }
            ],
            data: (function () {
                var data = [],
                    thisYear = new Date().getYear() + 1900,
                    mod = 0x7fffFFFF,
                    // return integer [min,max)
                    rand = function (min, max) {
                        var r = (seed = ((seed * 214013) + 2531011) % mod) / mod; // [0, 1)
                        return Math.floor(r * (max - min)) + min;
                    },
                    seed = 13;
        
                for (var year = 1900; year <= thisYear; ++year) {
                    data.push([
                        year, // id
                        year,
                        rand(-10, 100),
                        rand(-10, 100),
                        rand(-10, 200),
                        rand(-10, 200),
                        rand(-10, 200),
                        rand(-10, 300),
                        rand(-10, 300),
                        rand(-10, 300),
                        rand(-10, 600),
                        rand(-10, 500),
                        rand(-10, 200),
                        rand(-10, 100)
                    ]);
                }
        
                return data;
            }())
        }),
        frame: true,
    
        selModel: {
            type: 'spreadsheet',
            // Disables sorting by header click, though it will be still available via menu
            columnSelect: true,
            pruneRemoved: false,
            extensible: 'y'
        },
    
        // Enable CTRL+C/X/V hot-keys to copy/cut/paste to the system clipboard.
        plugins: [
            'clipboard',
            'selectionreplicator'
        ],
    
        listeners: {
            selectionchange: 'onSelectionChange'
        },
    
        tools: [{
            type: 'refresh',
            handler: 'onRefresh',
            tooltip: 'Reload Data'
        }],
    
        tbar: [{
            xtype: 'component',
            html: 'Selectable: '
        }, {
            text: 'Rows',
            enableToggle: true,
            toggleHandler: 'toggleRowSelect',
            pressed: true
        }, {
            text: 'Cells',
            enableToggle: true,
            toggleHandler: 'toggleCellSelect',
            pressed: true
        }, {
            text: 'Columns',
            enableToggle: true,
            toggleHandler: 'toggleColumnSelect',
            pressed: true
        }, '->', {
            xtype: 'component',
            reference: 'status'
        }],
    
        columns:[
            { text: 'Year', dataIndex: 'year', width: 70, minWidth: 70, locked: true },
            { text: 'Jan',  dataIndex: 'jan', width: 100 },
            { text: 'Feb',  dataIndex: 'feb', width: 100 },
            { text: 'Mar',  dataIndex: 'mar', width: 100 },
            { text: 'Apr',  dataIndex: 'apr', width: 100 },
            { text: 'May',  dataIndex: 'may', width: 100 },
            { text: 'Jun',  dataIndex: 'jun', width: 100 },
            { text: 'Jul',  dataIndex: 'jul', width: 100 },
            { text: 'Aug',  dataIndex: 'aug', width: 100 },
            { text: 'Sep',  dataIndex: 'sep', width: 100 },
            { text: 'Oct',  dataIndex: 'oct', width: 100 },
            { text: 'Nov',  dataIndex: 'nov', width: 100 },
            { text: 'Dec',  dataIndex: 'dec', width: 100 }
        ],
    
        viewConfig: {
            columnLines: true,
            trackOver: false
        }
    }]
});