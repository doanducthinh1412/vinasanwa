/* eslint-disable no-unused-vars */
Ext.define('CMS.view.Dashboard.Dashboard', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Ext.chart.PolarChart',
        'Ext.chart.series.Pie',
        'Ext.chart.series.sprite.PieSlice',
        'Ext.chart.interactions.Rotate',
    ],
    controller: 'dashboard',
    viewModel: 'dashboard',
    xtype: 'Dashboard',
    layout: 'responsivecolumn',
    bodyStyle: {
        'background-color': '#f6f6f6;',
        padding: '5px',
    },
    autoScroll: true,
    listeners: {},
    initComponent() {
        const me = this;
        const widthPanel = (Ext.getBody().getWidth() - 320) * 0.6 - 25;
        const heightPanel = (Ext.getBody().getHeight() - 64 - 48 - 230);
        const dashboards = [];
        if (ClassMain.checkDepartment('kehoach') === true) {
            dashboards.push({
                xtype: 'panel',
                userCls: 'big-25 small-50',
                ui: 'light',
                cls: 'service-type shadow',
                height: 130,
                headerPosition: 'bottom',
                titleAlign: 'center',
                items: {
                    bind: {
                        data: '{orderSheet}',
                    },
                    viewModel: {
                        orderSheet: [],
                    },
                    xtype: 'component',
                    reference: 'newTodayPanel',
                    tpl: '<div style="text-align: center;margin-top: 10px;"><span style="color:{iconColor}" class="x-fa fa-{icon} fa-3x"></span><h1>{amount}</h1></div>',
                },
                title: 'ĐƠN ĐẶT HÀNG',
            });
            dashboards.push({
                xtype: 'panel',
                userCls: 'big-25 small-50',
                ui: 'light',
                cls: 'service-type shadow',
                height: 130,
                headerPosition: 'bottom',
                titleAlign: 'center',
                items: {
                    bind: {
                        data: '{today_production_schedule}',
                    },
                    viewModel: {
                        today_production_schedule: [],
                    },
                    xtype: 'component',
                    reference: 'returningTodayPanel',
                    tpl: '<div style="text-align: center;margin-top: 10px;"><span style="color:{iconColor}" class="x-fa fa-{icon} fa-3x"></span><h1>{amount}</h1></div>',
                },
                title: 'KẾ HOẠCH ĐANG THỰC HIỆN',
            });
            dashboards.push({
                xtype: 'panel',
                userCls: 'big-25 small-50',
                ui: 'light',
                cls: 'service-type shadow',
                height: 130,
                headerPosition: 'bottom',
                titleAlign: 'center',
                items: {
                    data: {
                        amount: 0,
                        icon: 'bell',
                        iconColor: '#167abc',
                    },
                    xtype: 'component',
                    reference: 'totalNotification',
                    tpl: '<div style="text-align: center;margin-top: 10px;"><span style="color:{iconColor}" class="x-fa fa-{icon} fa-3x"></span><h1>{amount}</h1></div>',
                },
                title: 'THÔNG BÁO CÔNG VIỆC',
            });
        }

        if (ClassMain.checkDepartment('ve') === true) {
            if (ClassMain.checkPermission('dashboard', 'WorkingDrawingDashboard', 'quanlyVe') === true) {
                dashboards.push({
                    xtype: 'panel',
                    userCls: 'big-25 small-50',
                    ui: 'light',
                    cls: 'service-type shadow',
                    height: 130,
                    headerPosition: 'bottom',
                    titleAlign: 'center',
                    items: {
                        bind: {
                            data: '{new_receive_order_sheet}',
                        },
                        viewModel: {
                            new_receive_order_sheet: [],
                        },
                        xtype: 'component',
                        reference: 'newTodayPanel',
                        tpl: '<div style="text-align: center;margin-top: 10px;"><span style="color:{iconColor}" class="x-fa fa-{icon} fa-3x"></span><h1>{amount}</h1></div>',
                    },
                    title: 'ĐƠN ĐẶT HÀNG',
                });
            }

            if (ClassMain.checkPermission('dashboard', 'WorkingDrawingDashboard', 'nhanvienVe') === true) {
                dashboards.push({
                    xtype: 'panel',
                    userCls: 'big-25 small-50',
                    ui: 'light',
                    cls: 'service-type shadow',
                    height: 130,
                    headerPosition: 'bottom',
                    titleAlign: 'center',
                    items: {
                        bind: {
                            data: '{new_assignment}',
                        },
                        viewModel: {
                            new_assignemt: [],
                        },
                        xtype: 'component',
                        reference: 'newAssignemtPanel',
                        tpl: '<div style="text-align: center;margin-top: 10px;"><span style="color:{iconColor}" class="x-fa fa-{icon} fa-3x"></span><h1>{amount}</h1></div>',
                    },
                    title: 'PHÂN CÔNG VẼ MỚI',
                });
            }

            dashboards.push({
                xtype: 'panel',
                userCls: 'big-25 small-50',
                ui: 'light',
                cls: 'service-type shadow',
                height: 130,
                headerPosition: 'bottom',
                titleAlign: 'center',
                items: {
                    bind: {
                        data: '{assignment_process}',
                    },
                    viewModel: {
                        assignment_process: [],
                    },
                    xtype: 'component',
                    reference: 'assignmentProcessPanel',
                    tpl: '<div style="text-align: center;margin-top: 10px;"><span style="color:{iconColor}" class="x-fa fa-{icon} fa-3x"></span><h1>{amount}</h1></div>',
                },
                title: 'PHÂN CÔNG VẼ ĐANG THỰC HIỆN',
            });
            dashboards.push({
                xtype: 'panel',
                userCls: 'big-25 small-50',
                ui: 'light',
                cls: 'service-type shadow',
                height: 130,
                headerPosition: 'bottom',
                titleAlign: 'center',
                items: {
                    bind: {
                        data: '{assignment_done}',
                    },
                    viewModel: {
                        assignment_done: [],
                    },
                    xtype: 'component',
                    reference: 'assignmentDonePanel',
                    tpl: '<div style="text-align: center;margin-top: 10px;"><span style="color:{iconColor}" class="x-fa fa-{icon} fa-3x"></span><h1>{amount}</h1></div>',
                },
                title: 'PHÂN CÔNG VẼ HOÀN THÀNH',
            });
        }

        dashboards.push({
            xtype: 'panel',
            userCls: 'big-25 small-50',
            ui: 'light',
            cls: 'service-type shadow',
            height: 130,
            headerPosition: 'bottom',
            titleAlign: 'center',
            items: {
                bind: {
                    data: '{total_user}',
                },
                viewModel: {
                    users: [],
                },
                xtype: 'component',
                reference: 'totalClientPanel',
                tpl: '<div style="text-align: center;margin-top: 10px;"><span style="color:{iconColor}" class="x-fa fa-{icon} fa-3x"></span><h1>{amount}</h1></div>',
            },
            title: 'THÀNH VIÊN TRONG BỘ PHẬN',
        });

        dashboards.push({
            xtype: 'grid',
            cls: 'service-type shadow',
            height: heightPanel,
            userCls: 'big-50 small-100',
            title: ' THÔNG BÁO CÔNG VIỆC',
            iconCls: 'fas fa-bell',
            ui: 'light',
            layout: 'fit',
            autoScroll: true,
            tools: [{
                type: 'refresh',
                toggleValue: false,
                tooltip: 'Làm mới dữ liệu',
                handler: '',
            }],
            emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
            store: {
                type: 'thongbao',
            },
            columnLines: true,
            columns: [{
                    xtype: 'rownumberer',
                    text: 'STT',
                    width: 60,
                    align: 'center',
                },
                {
                    text: '<i class="fas fa-bell"></i> Tên thông báo',
                    dataIndex: 'title',
                    flex: 1,
                    align: 'left',
                    renderer(value, meta, rec) {
                        return `<b>${value}</b>`;
                    },
                },
                {
                    text: 'Đơn đặt hàng',
                    dataIndex: 'saleoder',
                    flex: 0.5,
                    align: 'left',
                },
                {
                    text: '<i class="fas fa-briefcase"></i> Bộ phận',
                    dataIndex: 'department',
                    flex: 0.5,
                    align: 'left',
                    renderer(value, meta, rec) {
                        if (value === 'Vẽ') {
                            return `<font color="#4a8263">${value}</font>`;
                        }
                        if (value === 'Mua hàng') {
                            return `<font color="#cf6311">${value}</font>`;
                        }
                        if (value === 'Tính giá') {
                            return `<font color="#085fe2">${value}</font>`;
                        }
                        return value;
                    },
                },
                {
                    text: '<i class="fas fa-calendar-alt"></i> Thời gian',
                    dataIndex: 'createtime',
                    width: 130,
                    align: 'center',
                }, {
                    text: '',
                    xtype: 'actioncolumn',
                    width: 60,
                    sortable: false,
                    menuDisabled: true,
                    align: 'center',
                    items: [{
                        iconCls: 'fas fa-edit text-blue',
                        tooltip: 'Xem chi tiết',
                        handler: '',
                    }],
                },
            ],
        });

        dashboards.push({
            xtype: 'panel',
            cls: 'service-type shadow',
            bodyPadding: 15,
            height: heightPanel,
            userCls: 'big-50 small-100',
            title: ' THỐNG KÊ',
            iconCls: 'fas fa-chart-bar',
            ui: 'light',
            tools: [{
                type: 'refresh',
                toggleValue: false,
                tooltip: 'Làm mới dữ liệu',
                handler: '',
            }],
        });

        me.items = dashboards;
        me.getController().loadDataPanel();
        me.callParent();
    },
});