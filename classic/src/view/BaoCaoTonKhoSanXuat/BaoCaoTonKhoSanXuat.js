Ext.define('CMS.view.BaoCaoTonKhoSanXuat.BaoCaoTonKhoSanXuat', {
    extend: 'Ext.grid.Panel',
    requires: [
        
    ],
    xtype: 'BaoCaoTonKhoSanXuat',
    layout: 'border',
    reference: 'BaoCaoTonKhoSanXuatGrid',
	autoScroll: true,
	columnLines: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
	initComponent: function() {
        var me = this;
        me.tbar = ['->', 'US $/Quantity'];
        me.columns = [{
                text: '<b>STT</b>', 
				width: 65, 
				sortable: false, 
				fixed: true, 
				menuDisabled: true, 
				dataIndex: '', 
				renderer : function(value, meta, record, rowIndex, colIndex, store){
					var num = 0;
					store.each(function(r){
						if(r._groupId == record._groupId){
							num++;
						}
						return record != r;
					});
					return num;
				}
            },
            {
                text: '<b>Project</b>',
                dataIndex: 'project',
                flex: 1,
                align: 'center',
                renderer: function(value, meta, rec) {
                    return '<b>' + value+ '</b>';
                }
            },
            {
                text: '<b>Quantity</b>',
				columns: [
				{
					text: '<b>Frame</b>',
					dataIndex: 'frame',
					flex: 1,
					align: 'center',
				},
				{
					text: '<b>Leaf</b>',
					dataIndex: 'leaf',
					flex: 1,
					align: 'center',
				},
				{
					text: '<b>Shutter</b>',
					dataIndex: 'shutter',
					flex: 1,
					align: 'center',
				},
				{
					text: '<b>Quick save</b>',
					dataIndex: 'quicksave',
					flex: 1,
					align: 'center',
				},
				{
					text: '<b>Louver</b>',
					dataIndex: 'louver',
					flex: 1,
					align: 'center',
				},
				{
					text: '<b>Other</b>',
					dataIndex: 'other',
					flex: 1,
					align: 'center',
				}
				]
            },
        ];
		me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true
        };
        me.menu = me.buildMenu();
        me.callParent();
    },
});