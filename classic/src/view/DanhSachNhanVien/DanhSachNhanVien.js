Ext.define('CMS.view.DanhSachNhanVien.DanhSachNhanVien', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.Panel'
    ],
    controller: 'DanhSachNhanVien',
    viewModel: 'DanhSachNhanVien',
    xtype: 'DanhSachNhanVien',
    layout: 'fit',
    bind: {
		store: '{quanLyNguoiDungStore}'
	},
    reference: 'DanhSachNhanVienGrid',
    autoScroll: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    initComponent: function() {
        var me = this;
        me.tbar = [{
            xtype: 'button',
            text: 'Tạo mới',
            iconCls: 'fas fa-plus-circle',
            ui: 'soft-orange',
            handler: 'onCreateButtonClick'
        }, '-', {
            text: 'Thao tác',
            iconCls: 'fas fa-bars',
            ui: 'dark-blue',
            disabled: true,
            bind: {
                disabled: '{!DanhSachNhanVienGrid.selection}',
            },
            menu: me.buildMenu()
        }, '->', {
            xtype: 'textfield',
            emptyText: 'Nhập tìm kiếm',
            width: 250,
            triggers: {
                clear: {
                    weight: 0,
                    cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                    hidden: true,
                    handler: 'onClearClick',
                    scope: 'this'
                },
                search: {
                    weight: 1,
                    cls: Ext.baseCSSPrefix + 'form-search-trigger',
                    handler: 'onSearchClick',
                    scope: 'this'
                }
            },
            onClearClick: function() {
                this.setValue('');
                this.getTrigger('clear').hide();
                this.updateLayout();
                me.getStore().clearFilter();
            },
            onSearchClick: function() {
                var value = this.getValue();
                if (value.length > 0) {
                    this.getTrigger('clear').show();
                    me.getStore().clearFilter();

                } else {
                    this.getTrigger('clear').hide();
                    me.getStore().clearFilter();

                }
            },
            listeners: {
                'specialkey': function(f, e) {
                    if (e.getKey() == e.ENTER) {
                        this.onSearchClick();
                    }
                }
            }
        }, {
            xtype: 'button',
            text: 'Lọc theo',
            iconCls: 'fas fa-filter',
            ui: 'dark-blue'
        }, {
            text: 'Công cụ',
            iconCls: 'fas fa-briefcase',
            ui: 'dark-blue',
            menu: {
                xtype: 'menu',
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                    handler: 'onPrintButtonClick'
                }, {
                    text: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    handler: 'onExportdButtonClick'
                }, '-', {
                    text: 'Làm mới danh sách',
                    iconCls: 'fas fa-sync-alt text-dark-blue',
                    handler: 'onReloadButtonClick'
                }]
            }
        }];
        me.columns = [{
                xtype: 'rownumberer',
                text: '<b>TT</b>',
                width: 65,
                align: 'center'
            },
            {
                text: '<b>Họ và tên</b>',
                dataIndex: 'last_name',
                flex: 1,
                align: 'center',
                renderer: function(value, meta, rec) {
                    return '<b>' + rec.get('last_name') + ' ' + rec.get('first_name')+ '</b>';
                }
            },
            {
                text: '<b>Chức vụ</b>',
                dataIndex: 'position_id',
                flex: 1,
                align: 'center',
                renderer: function(value, meta, rec) {
                    var record = me.getViewModel().getStore('positionStore').findRecord('position_id',value);
					return record.get('position_description') ;
                }
            },
			{
                text: '<b>Phòng ban</b>',
                dataIndex: 'department_id',
                flex: 1,
                align: 'center',
                renderer: function(value, meta, rec) {
                    var record = me.getViewModel().getStore('departmentStore').findRecord('department_id',value);
					return record.get('department_description') ;
                }
            },
			{
                text: '<b>Số điện thoại</b>',
                dataIndex: 'phone_number',
                flex: 1,
                align: 'center',
                renderer: function(value, meta, rec) {
                    return  value;
                }
            },
			{
                text: '<b>Email</b>',
                dataIndex: 'email',
                flex: 1,
                align: 'center',
                renderer: function(value, meta, rec) {
                    return  value ;
                }
            },
        ];
		me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true
        };
        me.menu = me.buildMenu();
        me.callParent();
    },
    buildMenu: function() {
        var controller = this.getController();
        return Ext.create('Ext.menu.Menu', {
            items: [{
                    text: 'Chỉnh sửa thông tin',
                    iconCls: 'fas fa-edit text-dark-blue',
                    handler: function() {
                        controller.onEditButtonClick();
                    }
                },
                {
                    text: 'Xóa nhân viên',
                    iconCls: 'far fa-trash-alt text-dark-blue',
                    handler: function() {
                        controller.onDeleteButtonClick();
                    }
                },'-',{
                    text: 'Tạo tài khoản hệ thống',
                    iconCls: 'far fa-user text-dark-blue',
                    handler: function() {
                        controller.onAddUserButtonClick();
                    }
                }
            ]
        });
    },
    listeners: {
        'itemcontextmenu': 'onItemContextMenu',
        'itemdblclick': 'onEditButtonClick'
    },
    realoadView: function() {
        var me = this;
        me.el.mask('Đang tải...');
        me.getStore().reload();
        me.el.unmask();
    }
});