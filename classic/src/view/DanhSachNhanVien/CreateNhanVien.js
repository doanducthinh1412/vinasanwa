Ext.define('CMS.view.CreateNhanVien.CreateNhanVien', {
    extend: 'Ext.form.Panel',
    requires: [
        
    ],
    controller: 'CreateNhanVien',
    viewModel: 'CreateNhanVien',
    xtype: 'CreateNhanVien',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        gridPanel: null
    },
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'left',
        autoFitErrors: true,
        width: 420,
		labelWidth:105,
    },
    autoScroll: false,
    initComponent: function() {
        var me = this;
        me.items = [{
			xtype: 'fieldset',
            title: 'Thông tin nhân viên',
			defaultType: 'textfield',
			items: [{
				xtype: 'container',
				margin: '0 0 10 0',
				layout: {
					type: 'hbox'
				},
				padding: 0,
				defaults: {
					xtype: 'textfield',
					width: 210,
					labelWidth:105,
					emptyCls:'emptyText',
					allowBlank: false,
				},
				items: [{
					fieldLabel: 'Họ tên',
					name: 'last_name',
					emptyText: 'Họ',
				},{
					name: 'first_name',
					emptyText: 'Tên',
				},
				]
			},{
				fieldLabel: 'Email',
				name: 'email'
			},{
				fieldLabel: 'Địa chỉ',
				name: 'address'
			},{
				fieldLabel: 'Ngày sinh',
				xtype: 'datefield',
				name: 'date_of_birth',
				format: 'd/m/Y',
			}, {
				xtype      : 'fieldcontainer',
				fieldLabel : 'Giới tính',
				defaultType: 'radiofield',
				defaults: {
					width: 150
				},
				layout: 'hbox',
				items: [
				{
					boxLabel  : 'Nam',
					name      : 'sex',
					inputValue: '1',
				}, {
					boxLabel  : 'Nữ',
					name      : 'sex',
					inputValue: '0',
				}]
			},{
				xtype: 'combobox',
				name: 'position',
				fieldLabel: 'Chức vụ',
				allowBlank: false,
				bind: {
					store: '{positionStore}'
				},
				valueField: 'position_id',
				displayField: 'position_description'

			},{
				xtype: 'combobox',
				name: 'department',
				fieldLabel: 'Phòng',
				allowBlank: false,
				bind: {
					store: '{departmentStore}'
				},
				valueField: 'department_id',
				displayField: 'department_description'

			}]
		}/*,{
			xtype: 'fieldset',
            title: 'Thông tin người dùng',
			defaultType: 'textfield',
			items: [{
				fieldLabel: 'Tài khoản',
				name: 'username',
				allowBlank: false,
			},{
				xtype: 'combobox',
				allowBlank: false,
				name: 'role',
				fieldLabel: 'Phân quyền',
				bind: {
					store: '{roleStore}'
				},
				valueField: 'role_id',
				displayField: 'role_description',
				multiSelect: true

			}]
		}*/];
		me.buttons = ['->',{
			text: 'Tạo mới',
			glyph: 'xf00c@FontAwesome',
			ui: 'soft-orange',
		}, {
			text: 'Hủy bỏ',
			glyph: 'xf00d@FontAwesome',
			handler: function() {
				this.up('window').close();
			}
		},'->'];
        me.callParent();
    },

     
});