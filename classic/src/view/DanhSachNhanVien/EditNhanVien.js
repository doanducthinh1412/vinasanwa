Ext.define('CMS.view.EditNhanVien.EditNhanVien', {
    extend: 'Ext.form.Panel',
    requires: [
        
    ],
    controller: 'EditNhanVien',
    viewModel: 'EditNhanVien',
    xtype: 'EditNhanVien',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        rec: null
    },
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'left',
        autoFitErrors: true,
        width: 420,
		labelWidth:105,
		//disabled: true
    },
    autoScroll: false,
    initComponent: function() {
        var me = this;
		var record = this.getRec();
        me.items = [{
			xtype: 'fieldset',
            title: 'Thông tin nhân viên',
			defaultType: 'textfield',
			items: [{
				xtype: 'container',
				margin: '0 0 10 0',
				layout: {
					type: 'hbox'
				},
				padding: 0,
				defaults: {
					xtype: 'textfield',
					width: 210,
					labelWidth:105,
					emptyCls:'emptyText',
					allowBlank: false,
					
				},
				items: [{
					fieldLabel: 'Họ tên',
					name: 'last_name',
					emptyText: 'Họ',
					value: record.get('last_name')
				},{
					name: 'first_name',
					emptyText: 'Tên',
					value: record.get('first_name')
				},
				]
			},{
				fieldLabel: 'Email',
				name: 'email',
				value: record.get('email')
			},{
				fieldLabel: 'Địa chỉ',
				name: 'address',
				value: record.get('address')
			},{
				fieldLabel: 'Ngày sinh',
				xtype: 'datefield',
				name: 'date_of_birth',
				format: 'd/m/Y',
				altFormats: 'd,m,Y|d.m.Y|d m Y',
				value: record.get('date_of_birth')
			}, {
				xtype      : 'fieldcontainer',
				fieldLabel : 'Giới tính',
				defaultType: 'radiofield',
				defaults: {
					width: 150
				},
				layout: 'hbox',
				items: [
				{
					boxLabel  : 'Nam',
					name      : 'sex',
					inputValue: '1',
					checked: (record.get('sex') == '1')?true:false
				}, {
					boxLabel  : 'Nữ',
					name      : 'sex',
					inputValue: '0',
					checked: (record.get('sex') == '0')?true:false
				}]
			},{
				xtype      : 'fieldcontainer',
				fieldLabel : 'Trạng thái',
				defaultType: 'radiofield',
				defaults: {
					width: 150
				},
				layout: 'hbox',
				items: [
				{
					boxLabel  : 'Kích hoạt',
					name      : 'status',
					inputValue: '1',
					checked: (record.get('status') == '1')?true:false
				}, {
					boxLabel  : 'Chưa kích hoạt',
					name      : 'status',
					inputValue: '0',
					checked: (record.get('status') == '0')?true:false
				}]
			},{
				xtype: 'combobox',
				name: 'position',
				fieldLabel: 'Chức vụ',
				allowBlank: false,
				value: record.get('position_id'),
				bind: {
					store: '{positionStore}'
				},
				valueField: 'position_id',
				displayField: 'position_description'

			},{
				xtype: 'combobox',
				name: 'department',
				fieldLabel: 'Phòng',
				allowBlank: false,
				value: record.get('department_id'),
				bind: {
					store: '{departmentStore}'
				},
				valueField: 'department_id',
				displayField: 'department_description'

			}]
		}/*,{
			xtype: 'fieldset',
            title: 'Thông tin người dùng',
			defaultType: 'textfield',
			items: [{
				fieldLabel: 'Tài khoản',
				name: 'username',
				allowBlank: false,
				value: record.get('username')
			},{
				xtype: 'combobox',
				allowBlank: false,
				name: 'role',
				fieldLabel: 'Phân quyền',
				value: record.get('role_id'),
				bind: {
					store: '{roleStore}'
				},
				valueField: 'role_id',
				displayField: 'role_description',
				multiSelect: true

			}]
		}*/];
		me.buttons = ['->',{
			text: 'Lưu lại',
			glyph: 'xf00c@FontAwesome',
			ui: 'soft-orange',
		},{
			text: 'Hủy bỏ',
			glyph: 'xf00d@FontAwesome',
			handler: function() {
				this.up('window').close();
			}
		},'->'];
        me.callParent();
    },
	
     
});