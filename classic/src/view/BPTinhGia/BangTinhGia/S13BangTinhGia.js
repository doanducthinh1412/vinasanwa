Ext.define('CMS.view.BPTinhGia.BangTinhGia.S13BangTinhGia', {
	extend: 'Ext.grid.Panel',
    //controller: 'SMBangTinhGia',
    //viewModel: 'SMBangTinhGia',
    xtype: 'S13BangTinhGia',
    layout: 'fit',
	autoScroll: true,
	config: {
		dataPanel: null
	},
	bufferedRenderer: false,
    columnLines: true,
    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
	store: {
		fields: ['w','h','qty','sheet_type','window_type','window_type_color','button_switch_type','button_switch_qty','pull_switch','one_push_point','remote_control','infrared_sensor_type','infrared_sensor_qty'],
		data: [{
			'w':1000,
			'h':3000,
			'qty':1,
			'sheet_type':'Clear orange',
			'window_type':'Nothing',
			'window_type_color':'Clear',
			'button_switch_type':'',
			'button_switch_qty':'',
			'pull_switch':'',
			'one_push_point':'',
			'remote_control':'',
			'infrared_sensor_type':'DA-4700',
			'infrared_sensor_qty':'2'
		}]
	},
	initComponent: function() {
		var me = this;
		me.tbar = ['->',{
			text: 'Tính giá',
			ui: 'dark-blue',
			handler: function() {
				
			}
		}]
		me.columns = [{
			xtype: 'rownumberer',
			text: '',
			width: 65,
			align: 'center'
		},{
			text: 'W',
			dataIndex: 'w',
			width: 100,
			align: 'center',
		},{
			text: 'H',
			dataIndex: 'h',
			width: 100,
			align: 'center',
		},{
			text: 'Qty',
			dataIndex: 'qty',
			width: 100,
			align: 'center',
		},{
			text: 'Leaf steel<br>thickness',
			dataIndex: 'leaf_steel_thickness',
			width: 100,
			align: 'center',
		},{
			text: 'Type',
			dataIndex: 'type',
			width: 100,
			align: 'center',
		},{
			text: 'Painting',
			columns: [{
				text: 'Frame',
				width: 100,
				dataIndex: 'painting_frame',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'Leaf',
				width: 100,
				dataIndex: 'painting_leaf',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}]
		},{
			text: 'Jamb<br>depth: D',
			dataIndex: 'jamb_depth',
			width: 100,
			align: 'center',
		},{
			text: 'Case<br>depth: cd',
			dataIndex: 'case_depth',
			width: 100,
			align: 'center',
		},{
			text: 'Case<br>height: ch',
			dataIndex: 'case_height',
			width: 100,
			align: 'center',
		},{
			text: 'Window',
			columns: [{
				text: 'W',
				width: 100,
				dataIndex: 'window_w',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'H',
				width: 100,
				dataIndex: 'window_h',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'Qty',
				width: 100,
				dataIndex: 'window_qty',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}]
		},{
			text: 'Louver',
			columns: [{
				text: 'W',
				width: 100,
				dataIndex: 'louver_w',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'H',
				width: 100,
				dataIndex: 'louver_h',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'Type',
				width: 100,
				dataIndex: 'louver_type',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'Qty',
				width: 100,
				dataIndex: 'louver_qty',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}]
		},/*{
			text: 'Standard<br>parts<br>price',
			dataIndex: 'standard_parts_price',
			width: 100,
			align: 'center',
		},{
			text: 'Total<br>Hardware<br>Price',
			dataIndex: 'total_hardware_price',
			width: 100,
			align: 'center',
		},{
			text: 'Frame total',
			dataIndex: 'frame_total',
			width: 100,
			align: 'center',
		},{
			text: 'Leaf total',
			dataIndex: 'leaf_total',
			width: 100,
			align: 'center',
		},{
			text: 'Total<br>factory<br>price',
			dataIndex: 'total_factory_price',
			width: 100,
			align: 'center',
		}*/]
		me.callParent();
	}

});