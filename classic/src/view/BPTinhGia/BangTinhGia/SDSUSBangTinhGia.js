Ext.define('CMS.view.BPTinhGia.BangTinhGia.SDSUSBangTinhGia', {
    extend: 'Ext.form.Panel',
    controller: 'SDSUSBangTinhGia',
    viewModel: 'SDSUSBangTinhGia',
    xtype: 'SDSUSBangTinhGia',
    layout: 'fit',
    autoScroll: true,
    config: {
        dataPanel: null,
        frameData: null,
        orderSheetData: null
    },

    initComponent: function () {
        var me = this;

        var record = me.getDataPanel();
        //me.templateInformationPanel = me.createTemplateInformationPanel();
        //me.orderSheetInformationPanel = me.createOrderSheetInformationPanel();
        me.orderSheetInformationGrid = me.createOrderSheetInformationGrid();
        me.items = [/*{
            xtype: 'fieldcontainer',
            region: 'north',
            layout: 'column',
            defaults: {
            columnWidth: 0.5
            },
            items: [me.templateInformationPanel,me.orderSheetInformationPanel	]
            },*/
            me.orderSheetInformationGrid
        ];
        me.callParent();
    },
    createTemplateInformationPanel: function () {
        var me = this;
        return Ext.create('Ext.form.Panel', {
            glyph: 'f039@FontAwesome',
            ui: 'light',
            header: false,
            cls: 'service-type shadow',
            bodyPadding: 20,
            margin: '0 0 20 0',
            items: [{
                    xtype: 'fieldset',
                    title: '<b> THÔNG TIN TEMPLATE TÍNH GIÁ </b>',
                    layout: 'column',
                    defaults: {
                        padding: 10
                    },
                    items: [{
                            xtype: 'textfield',
                            fieldLabel: 'Tiêu đề',
                            labelWidth: 120,
                            editable: false,
                            columnWidth: 1,
                            value: me.getFrameData().title
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Mô tả',
                            labelWidth: 120,
                            editable: false,
                            columnWidth: 1,
                            value: me.getFrameData().description
                        }, {
                            xtype: 'fieldcontainer',
                            layout: 'column',
                            columnWidth: 1,
                            items: [{
                                    xtype: 'textfield',
                                    fieldLabel: 'File tính giá',
                                    allowBlank: false,
                                    labelWidth: 120,
                                    editable: false,
                                    columnWidth: 0.8,
                                    value: me.getFrameData().file_name
                                }, {
                                    xtype: 'button',
                                    text: 'Download',
                                    columnWidth: 0.2,
                                    handler: function () {
                                        var url = '/api/price-templates/download/' + me.getFrameData().id;
                                        ClassMain.saveFile(url, me.getFrameData().file_name);
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        });
    },
    createOrderSheetInformationPanel: function (record) {
        var me = this;
        var record = me.getDataPanel();
        return Ext.create('Ext.form.Panel', {
            glyph: 'f039@FontAwesome',
            ui: 'light',
            header: false,
            cls: 'service-type shadow',
            bodyPadding: 20,
            margin: '0 0 20 0',
            items: [{
                    xtype: 'fieldset',
                    title: '<b> THÔNG TIN ORDERSHEET </b>',
                    layout: 'column',
                    defaults: {
                        padding: 10
                    },
                    items: [{
                            xtype: 'textfield',
                            fieldLabel: 'Project name',
                            labelWidth: 120,
                            editable: false,
                            columnWidth: 1,
                            value: record.project.name
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Order No',
                            labelWidth: 120,
                            editable: false,
                            columnWidth: 1,
                            value: record.receive_no
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Sub No',
                            labelWidth: 120,
                            editable: false,
                            columnWidth: 1,
                            value: record.sub_no
                        }
                    ]
                }
            ]
        });
    },
    createOrderSheetInformationGrid: function (record) {
        var me = this;
        var controller = me.getController();
        var record = me.getDataPanel();
        var tbar = [{
                xtype: 'textfield',
                fieldLabel: 'Project name',
                labelWidth: 120,
                editable: false,
                value: record.project.name
            }, {
                xtype: 'textfield',
                fieldLabel: 'Order No',
                labelWidth: 120,
                editable: false,
                value: record.receive_no
            }, {
                xtype: 'textfield',
                fieldLabel: 'Sub No',
                labelWidth: 120,
                editable: false,
                value: record.sub_no
            }, {
                xtype: 'displayfield',
                fieldLabel: 'Tiêu đề',
                labelWidth: 120,
                editable: false,
                value: '<b>' + me.getFrameData().title + '</b>'
            }
        ];
        var bbar = [{
                xtype: 'textfield',
                fieldLabel: '<b>Price (US$)</b>',
                labelAlign: 'right',
                //labelWidth: 120,
                editable: false,
                bind: '{totalPrice}'
            }, {
                xtype: 'textfield',
                fieldLabel: '<b>Frame (US$)</b>',
                labelAlign: 'right',
                //labelWidth: 120,
                editable: false,
                bind: '{framePrice}'
            }, {
                xtype: 'textfield',
                fieldLabel: '<b>Leaf (US$)</b>',
                labelAlign: 'right',
                //labelWidth: 120,
                editable: false,
                bind: '{leafPrice}'
            }, '->', {
                xtype: 'button',
                text: 'Tính giá',
                iconCls: 'fas fa-bars',
                ui: 'soft-orange',
                handler: function () {
                    controller.onTinhGiaButtonClick();
                }
            }, {
                xtype: 'button',
                text: 'Lưu',
                iconCls: 'fas fa-bars',
                ui: 'soft-orange',
                bind: {
                    disabled: '{!isCalculated}'
                },
                handler: function () {
                    controller.onSaveButtonClick();
                }
            }, {
                xtype: 'button',
                text: 'Tải file kết quả',
                iconCls: 'fas fa-bars',
                ui: 'dark-blue',
                bind: {
                    disabled: '{!isSaved}'
                },
                handler: function () {
                    controller.onDownloadButtonClick();
                }
            }
        ];
        var fields = ["product_no", "dw", "dh", "qty", "open_type", "content_of_calculated", "sheet_thickness_frame", "sheet_thickness_leaf", "painting_frame", "painting_leaf", "assembly_at_factory", "special_packing_tape", "leaf_thickness", "frame_depth", "frame_type", "inner_rib", "sill_material", "sill_type", "vw_w", "vw_h", "vw_qty", "louver_w", "louver_h", "louver_type", "louver_qty", "insect_net", "canopy_depth", "fill_up_frame", "fill_up_leaf", "additional_price"];

        var data = me.getOrderSheetData().orderSheet2.rows[0].data;
        var rowData = [];
        for (i = 0; i < 30; i++) {
            var arr = {};
            for (j = 0; j < fields.length; j++) {
                arr[fields[j]] = "";
            }
            rowData.push(arr);
        }
        try {
            for (i = 0; i < data.length; i++) {
                rowData[i].product_no = i + 1;
                if (data[i].df_sheet_thickness && data[i].dl_sheet_thickness) {
                    rowData[i].content_of_calculated = "Frame & Leaf";
                } else if (data[i].df_sheet_thickness) {
                    rowData[i].content_of_calculated = "Frame only";
                } else if (data[i].dl_sheet_thickness) {
                    rowData[i].content_of_calculated = "Leaf only";
                }
                rowData[i].sheet_thickness_frame = "SUS " + data[i].df_sheet_thickness + "t";
                rowData[i].sheet_thickness_leaf = "SUS " + data[i].dl_sheet_thickness + "t";
                rowData[i].painting_frame = "None";
                rowData[i].painting_leaf = "None";
                rowData[i].assembly_at_factory = "None";
                rowData[i].special_packing_tape = "None";
                rowData[i].dw = data[i].f_width;
                rowData[i].dh = data[i].f_height;
                rowData[i].frame_depth = data[i].f_depth;
                rowData[i].open_type = data[i].qodl_door_type;
                rowData[i].vw_w = data[i].gw_width;
                rowData[i].vw_h = data[i].gw_height;
                rowData[i].vw_qty = data[i].gw_qty;
                rowData[i].louver_w = data[i].l_width;
                rowData[i].louver_h = data[i].l_height;
				rowData[i].louver_type = ((data[i].l_kind_of_louver == 'C') && (data[i].l_kind_of_louver != null) )?'Slope':'Blind';
                rowData[i].louver_qty = data[i].l_qty;
                rowData[i].leaf_thickness = data[i].dl_door_thickness;
            }
        } catch (err) {
            console.log(err);
        }
        var columns = [{
                xtype: 'rownumberer',
                text: '',
                width: 65,
                align: 'center',
                editor: {
                    allowBlank: true
                }
            }, {
                text: 'Product No',
                dataIndex: 'product_no',
                width: 100,
                align: 'center',
                editor: {
                    allowBlank: true
                }
            }, {
                text: 'W-DW',
                dataIndex: 'dw',
                width: 100,
                align: 'center',
                editor: {
                    allowBlank: true
                }
            }, {
                text: 'H-DH',
                dataIndex: 'dh',
                width: 100,
                align: 'center',
                editor: {
                    allowBlank: true
                }
            }, {
                text: 'Qty',
                dataIndex: 'qty',
                width: 100,
                align: 'center',
                editor: {
                    allowBlank: true
                }
            }, {
                text: 'Open<br>Type',
                dataIndex: 'open_type',
                width: 100,
                align: 'center',
                editor: {
                    xtype: 'combobox',
                    store: {
                        fields: ['display', 'value'],
                        data: [
                            ["Single", "Single"],
                            ["Double", "Double"]
                        ]
                    },
                    displayField: "display",
                    valueField: "value"
                },

            }, {
                text: 'Content of<br>calculated',
                dataIndex: 'content_of_calculated',
                width: 100,
                align: 'center',
                editor: {
                    xtype: 'combobox',
                    store: {
                        fields: ['display', 'value'],
                        data: [
                            ["Frame & Leaf", "Frame & Leaf"],
                            ["Frame only", "Frame only"],
                            ["Leaf only", "Leaf only"]
                        ]
                    },
                    displayField: "display",
                    valueField: "value"
                }
            }, {
                text: 'Sheet thickness',
                columns: [{
                        text: 'Frame',
                        width: 100,
                        dataIndex: 'sheet_thickness_frame',
                        align: 'center',
                        editor: {
                            xtype: 'combobox',
                            store: {
                                fields: ['display', 'value'],
                                data: [
                                    ["None", "None"],
                                    ["SUS 1.0t", "SUS 1.0t"],
                                    ["SUS 1.5t", "SUS 1.5t"]
                                ]
                            },
                            displayField: "display",
                            valueField: "value"
                        }
                    }, {
                        text: 'Leaf',
                        width: 100,
                        dataIndex: 'sheet_thickness_leaf',
                        align: 'center',
                        editor: {
                            xtype: 'combobox',
                            store: {
                                fields: ['display', 'value'],
                                data: [
                                    ["None", "None"],
                                    ["SUS 1.0t", "SUS 1.0t"],
                                    ["SUS 1.5t", "SUS 1.5t"]
                                ]
                            },
                            displayField: "display",
                            valueField: "value"
                        }
                    }
                ]
            }, {
                text: 'Painting',
                columns: [{
                        text: 'Frame',
                        width: 100,
                        dataIndex: 'painting_frame',
                        align: 'center',
                        editor: {
                            xtype: 'combobox',
                            store: {
                                fields: ['display', 'value'],
                                data: [
                                    ["None", "None"],
                                    ["Poly", "Poly"],
                                    ["Powder", "Powder"],
                                    ["Primer", "Primer"]
                                ]
                            },
                            displayField: "display",
                            valueField: "value"
                        }
                    }, {
                        text: 'Leaf',
                        width: 100,
                        dataIndex: 'painting_leaf',
                        align: 'center',
                        editor: {
                            xtype: 'combobox',
                            store: {
                                fields: ['display', 'value'],
                                data: [
                                    ["None", "None"],
                                    ["Poly", "Poly"],
                                    ["Powder", "Powder"],
                                    ["Primer", "Primer"]
                                ]
                            },
                            displayField: "display",
                            valueField: "value"
                        }
                    }
                ]
            }, {
                text: 'Assembly<br>frame at<br>factory',
                dataIndex: 'assembly_at_factory',
                width: 100,
                align: 'center',
                editor: {
                    xtype: 'combobox',
                    store: {
                        fields: ['display', 'value'],
                        data: [
                            ["Use", "Use"],
                            ["None", "None"],
                        ]
                    },
                    displayField: "display",
                    valueField: "value"
                }
            }, {
                text: 'Special<br>packing<br>tape',
                dataIndex: 'special_packing_tape',
                width: 100,
                align: 'center',
                editor: {
                    xtype: 'combobox',
                    store: {
                        fields: ['display', 'value'],
                        data: [
                            ["Use", "Use"],
                            ["None", "None"],
                        ]
                    },
                    displayField: "display",
                    valueField: "value"
                }
            }, {
                text: 'Leaf<br>thickness',
                dataIndex: 'leaf_thickness',
                width: 100,
                align: 'center',
                editor: {
                    xtype: 'combobox',
                    store: {
                        fields: ['display', 'value'],
                        data: [
                            ["40", "40"],
                            ["45", "45"],
                            ["50", "50"],
                            ["55", "55"],
                            ["60", "60"],
                            ["65", "65"],
                            ["70", "70"],
                            ["75", "75"],
                            ["80", "80"]
                        ]
                    },
                    displayField: "display",
                    valueField: "value"
                }
            }, {
                text: 'Frame<br>depth',
                dataIndex: 'frame_depth',
                width: 100,
                align: 'center',
                editor: {
                    allowBlank: true
                }
            }, {
                text: 'Frame<br>type',
                dataIndex: 'frame_type',
                width: 100,
                align: 'center',
                editor: {
                    xtype: 'combobox',
                    store: {
                        fields: ['display', 'value'],
                        data: [
                            ["ST type", "ST type"],
                            ["SAT type", "SAT type"],
                            ["PAT type", "PAT type"]
                        ]
                    },
                    displayField: "display",
                    valueField: "value"
                }
            }, {
                text: 'Inner<br>rib',
                dataIndex: 'inner_rib',
                width: 100,
                align: 'center',
                editor: {
                    xtype: 'combobox',
                    store: {
                        fields: ['display', 'value'],
                        data: [
                            ["1.6t", "1.6"],
                            ["2.3t", "2.3"],
                            ["3.0", "3.0"]
                        ]
                    },
                    displayField: "display",
                    valueField: "value"
                }
            }, {
                text: 'Sill',
                columns: [{
                        text: 'Material',
                        width: 100,
                        dataIndex: 'sill_material',
                        align: 'center',
                        editor: {
                            xtype: 'combobox',
                            store: {
                                fields: ['display', 'value'],
                                data: [
                                    ["None", "None"],
                                    ["SUS 1.0t", "SUS 1.0t"],
                                    ["SUS 1.5t", "SUS 1.5t"]
                                ]
                            },
                            displayField: "display",
                            valueField: "value"
                        }
                    }, {
                        text: 'Type',
                        width: 100,
                        dataIndex: 'sill_type',
                        align: 'center',
                        editor: {
                            xtype: 'combobox',
                            store: {
                                fields: ['display', 'value'],
                                data: [
                                    ["C type", "C type"],
                                    ["None", "None"],
                                    ["PAT type", "PAT type"],
                                    ["SAT type", "SAT type"],
                                    ["ST type", "ST type"]
                                ]
                            },
                            displayField: "display",
                            valueField: "value"
                        }
                    }
                ]
            }, {
                text: 'Viewing window<br>(Quantity per set)',
                columns: [{
                        text: 'W',
                        width: 100,
                        dataIndex: 'vw_w',
                        align: 'center',
                        editor: {
                            allowBlank: true
                        }
                    }, {
                        text: 'H',
                        width: 100,
                        dataIndex: 'vw_h',
                        align: 'center',
                        editor: {
                            allowBlank: true
                        }
                    }, {
                        text: 'Qty',
                        width: 100,
                        dataIndex: 'vw_qty',
                        align: 'center',
                        editor: {
                            allowBlank: true
                        }
                    }
                ]
            }, {
                text: 'Louver<br>(Quantity per set)',
                columns: [{
                        text: 'W',
                        width: 100,
                        dataIndex: 'louver_w',
                        align: 'center',
                        editor: {
                            allowBlank: true
                        }
                    }, {
                        text: 'H',
                        width: 100,
                        dataIndex: 'louver_h',
                        align: 'center',
                        editor: {
                            allowBlank: true
                        }
                    }, {
                        text: 'Type',
                        width: 100,
                        dataIndex: 'louver_type',
                        align: 'center',
                        editor: {
                            xtype: 'combobox',
                            store: {
                                fields: ['display', 'value'],
                                data: [
                                    ["Blind", "Blind"],
                                    ["Slope", "Slope"],
                                ]
                            },
                            displayField: "display",
                            valueField: "value"
                        }
                    }, {
                        text: 'Qty',
                        width: 100,
                        dataIndex: 'louver_qty',
                        align: 'center',
                        editor: {
                            allowBlank: true
                        }
                    }
                ]
            }, {
                text: 'Insect<br>net',
                dataIndex: 'insect_net',
                width: 100,
                align: 'center',
                editor: {
                    xtype: 'combobox',
                    store: {
                        fields: ['display', 'value'],
                        data: [
                            ["Bird", "Bird"],
                            ["Insect", "Insect"],
                            ["None", "None"]
                        ]
                    },
                    displayField: "display",
                    valueField: "value"
                }
            }, {
                text: 'Canopy<br>depth',
                dataIndex: 'canopy_depth',
                width: 100,
                align: 'center',
                editor: {
                    allowBlank: true
                }
            }, {
                text: 'Fill up material',
                columns: [{
                        text: 'Frame',
                        width: 100,
                        dataIndex: 'fill_up_frame',
                        align: 'center',
                        editor: {
                            xtype: 'combobox',
                            store: {
                                fields: ['display', 'value'],
                                data: [
                                    ["Glass 060kg", "Glass 060kg"],
                                    ["Glass 080kg", "Glass 080kg"],
                                    ["Glass 120kg", "Glass 120kg"],
                                    ["None", "None"],
                                    ["Rock 060kg", "Rock 060kg"],
                                    ["Rock 080kg", "Rock 080kg"],
                                    ["Rock60kg cheap", "Rock60kg cheap"],
                                    ["Rock80kg cheap", "Rock80kg cheap"],
                                    ["Rock120kg cheap", "Rock120kg cheap"]
                                ]
                            },
                            displayField: "display",
                            valueField: "value"
                        }
                    }, {
                        text: 'Leaf',
                        width: 100,
                        dataIndex: 'fill_up_leaf',
                        align: 'center',
                        editor: {
                            xtype: 'combobox',
                            store: {
                                fields: ['display', 'value'],
                                data: [
                                    ["Glass 060kg", "Glass 060kg"],
                                    ["Glass 080kg", "Glass 080kg"],
                                    ["Glass 120kg", "Glass 120kg"],
                                    ["None", "None"],
                                    ["Rock 060kg", "Rock 060kg"],
                                    ["Rock 080kg", "Rock 080kg"],
                                    ["Rock60kg cheap", "Rock60kg cheap"],
                                    ["Rock80kg cheap", "Rock80kg cheap"],
                                    ["Rock120kg cheap", "Rock120kg cheap"]
                                ]
                            },
                            displayField: "display",
                            valueField: "value"
                        }
                    }
                ]
            }, {
                text: 'Additional<br>price',
                dataIndex: 'additional_price',
                width: 100,
                align: 'center',
                editor: {
                    allowBlank: true
                }
            }
        ];

        //var columns = [];
        return Ext.create('Ext.grid.Panel', {
            //tbar: tbar,
            tbar: bbar,
            columnLines: true,
            //region: 'center',
            bufferedRenderer: false,
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
            columns: columns,
            store: {
                fields: fields,
                data: rowData
            },
            plugins: {
                ptype: 'cellediting',
                clicksToEdit: 1
            }
        });

    }
});
