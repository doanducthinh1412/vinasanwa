Ext.define('CMS.view.BPTinhGia.BangTinhGia.SMBangTinhGia', {
	extend: 'Ext.grid.Panel',
    //controller: 'SMBangTinhGia',
    //viewModel: 'SMBangTinhGia',
    xtype: 'SMBangTinhGia',
    layout: 'fit',
	autoScroll: true,
	config: {
		dataPanel: null
	},
	bufferedRenderer: false,
    columnLines: true,
    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
	store: {
		fields: ['w','h','qty','leaf_steel_thickness','type','painting_frame','painting_leaf','jamb_depth','case_depth','case_height','window_w','window_h','window_qty','louver_w','louver_h','louver_type','louver_qty','standard_parts_price','total_hardware_price','frame_total','leaf_total','total_factory_price'],
		data: [{
			'w':700,
			'h':1200,
			'qty':1,
			'leaf_steel_thickness':'0.8t',
			'type':'Single',
			'painting_frame':'Powder',
			'painting_leaf':'Powder',
			'jamb_depth':195,
			'case_depth':117,
			'case_height':200,
			'window_w':100,
			'window_h':600,
			'window_qty':1,
			'louver_w':100,
			'louver_h':600,
			'louver_type':'Blind',
			'louver_qty':1,
			'standard_parts_price':'US$60.45',
			'total_hardware_price':'US$60.45',
			'frame_total':'US$109.665',
			'leaf_total':'US$220.51',
			'total_factory_price':'US$390.62',
		}]
	},
	initComponent: function() {
		var me = this;
		me.tbar = ['->',{
			text: 'Tính giá',
			ui: 'dark-blue',
			handler: function() {
				
			}
		}]
		me.columns = [{
			xtype: 'rownumberer',
			text: '',
			width: 65,
			align: 'center'
		},{
			text: 'W',
			dataIndex: 'w',
			width: 100,
			align: 'center',
		},{
			text: 'H',
			dataIndex: 'h',
			width: 100,
			align: 'center',
		},{
			text: 'Qty',
			dataIndex: 'qty',
			width: 100,
			align: 'center',
		},{
			text: 'Leaf steel<br>thickness',
			dataIndex: 'leaf_steel_thickness',
			width: 100,
			align: 'center',
		},{
			text: 'Type',
			dataIndex: 'type',
			width: 100,
			align: 'center',
		},{
			text: 'Painting',
			columns: [{
				text: 'Frame',
				width: 100,
				dataIndex: 'painting_frame',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'Leaf',
				width: 100,
				dataIndex: 'painting_leaf',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}]
		},{
			text: 'Jamb<br>depth: D',
			dataIndex: 'jamb_depth',
			width: 100,
			align: 'center',
		},{
			text: 'Case<br>depth: cd',
			dataIndex: 'case_depth',
			width: 100,
			align: 'center',
		},{
			text: 'Case<br>height: ch',
			dataIndex: 'case_height',
			width: 100,
			align: 'center',
		},{
			text: 'Window',
			columns: [{
				text: 'W',
				width: 100,
				dataIndex: 'window_w',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'H',
				width: 100,
				dataIndex: 'window_h',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'Qty',
				width: 100,
				dataIndex: 'window_qty',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}]
		},{
			text: 'Louver',
			columns: [{
				text: 'W',
				width: 100,
				dataIndex: 'louver_w',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'H',
				width: 100,
				dataIndex: 'louver_h',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'Type',
				width: 100,
				dataIndex: 'louver_type',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}, {
				text: 'Qty',
				width: 100,
				dataIndex: 'louver_qty',
				align: 'center',
				editor: {
					allowBlank: true
				}
			}]
		},/*{
			text: 'Standard<br>parts<br>price',
			dataIndex: 'standard_parts_price',
			width: 100,
			align: 'center',
		},{
			text: 'Total<br>Hardware<br>Price',
			dataIndex: 'total_hardware_price',
			width: 100,
			align: 'center',
		},{
			text: 'Frame total',
			dataIndex: 'frame_total',
			width: 100,
			align: 'center',
		},{
			text: 'Leaf total',
			dataIndex: 'leaf_total',
			width: 100,
			align: 'center',
		},{
			text: 'Total<br>factory<br>price',
			dataIndex: 'total_factory_price',
			width: 100,
			align: 'center',
		}*/]
		me.callParent();
	}

});