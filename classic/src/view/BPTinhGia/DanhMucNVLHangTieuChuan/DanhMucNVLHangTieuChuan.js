Ext.define('CMS.view.BPTinhGia.DanhMucNVLHangTieuChuan.DanhMucNVLHangTieuChuan', {
    extend: 'Ext.grid.Panel',
    controller: 'DanhMucNVLHangTieuChuan',
    viewModel: 'DanhMucNVLHangTieuChuan',
    xtype: 'DanhMucNVLHangTieuChuan',
    layout: 'fit',
    store: {
        model: 'CMS.model.DanhSachDonDatHang',
        autoLoad: false,
        groupField: 'project_name',
        proxy: {
            type: 'ajax',
            url: '/api/ordersheet?filter=dp:eq_3',
            reader: {
                type: 'json',
                rootProperty: 'data',
                totalProperty: 'total'
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    if (response.status === 302) {
                        window.location = "/index.html#logout";
                    }
                }
            }
        }
    },
    reference: 'DanhMucNVLHangTieuChuanGrid',
    autoScroll: true,
    columnLines: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    config: {
        filterString: null
    },
    features: [{
        ftype: 'grouping',
        startCollapsed: false,
        groupHeaderTpl: '<font color="blue"><b>Dự án:</b> {name} ({rows.length} đơn đặt hàng)</font>'
    }],
    initComponent: function() {
        var me = this;
        var tbar = [{
            text: 'Thao tác',
            iconCls: 'fas fa-bars',
            ui: 'dark-blue',
            disabled: true,
            bind: {
                disabled: '{!DanhMucNVLHangTieuChuanGiaGrid.selection}',
                menu: '{actionMenu}'
            },
        }, '->', {
            xtype: 'textfield',
            emptyText: 'Nhập tìm kiếm',
            width: 250,
            reference: 'SearchField',
            triggers: {
                clear: {
                    weight: 0,
                    cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                    hidden: true,
                    handler: 'onClearClick',
                    scope: 'this'
                },
                search: {
                    weight: 1,
                    cls: Ext.baseCSSPrefix + 'form-search-trigger',
                    handler: 'onSearchClick',
                    scope: 'this'
                }
            },
            onClearClick: function() {
                this.setValue('');
                this.getTrigger('clear').hide();
                this.updateLayout();
                me.getController().loadDataGrid();
            },
            onSearchClick: function() {
                var value = this.getValue();
                if (value.length > 0) {
                    this.getTrigger('clear').show();
                    this.updateLayout();
                } else {
                    this.getTrigger('clear').hide();
                }
                me.getController().loadDataGrid();
            },
            listeners: {
                'specialkey': function(f, e) {
                    if (e.getKey() == e.ENTER) {
                        this.onSearchClick();
                    }
                }
            }
        }, {
            xtype: 'button',
            text: 'Lọc theo',
            iconCls: 'fas fa-filter',
            ui: 'dark-blue',
            menuAlign: 'tr-br',
            reference: 'FilterMenu',
            menu: Ext.create('Ext.menu.Menu', {
                style: {
                    overflow: 'visible'
                },
                mouseLeaveDelay: 10,
                width: 370,
                margin: '10 10 10 10',
                referenceHolder: true,
                buttons: [{
                    text: 'Lọc',
                    ui: 'soft-orange',
                    handler: function() {
                        me.getController().loadDataGrid();
                    }
                }, {
                    text: 'Xóa bộ lọc',
                    handler: function(btn) {
                        var refs = btn.up().up().getReferences();
                        for (var ref in refs) {
                            refs[ref].clearValue();
                            refs[ref].getTrigger('clear').hide();
                        }
                        btn.up().up().hide();
                        me.getController().loadDataGrid();
                    }
                }],
                items: [{
                    xtype: 'combobox',
                    fieldLabel: '<b>Loại đơn</b>',
                    labelAlign: 'left',
                    store: {
                        type: 'LoaiDonDatHangStore'
                    },
                    queryMode: 'local',
                    editable: false,
                    labelWidth: 80,
                    reference: 'ordertypefilter',
                    margin: '10 10 10 10',
                    displayField: 'name',
                    valueField: 'val',
                    fieldIndex: 'order_type',
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            weight: -1,
                            hidden: true,
                            scope: 'this',
                            handler: function(combo) {
                                combo.clearValue();
                                combo.getTrigger('clear').hide();
                                me.getController().loadDataGrid();
                            }
                        }
                    },
                    listeners: {
                        'select': function(combo) {
                            combo.getTrigger('clear').show();
                            combo.updateLayout();
                        }
                    }
                }, {
                    xtype: 'combobox',
                    labelAlign: 'left',
                    fieldLabel: '<b>Độ ưu tiên</b>',
                    store: {
                        type: 'DoUuTienDonDatHangStore'
                    },
                    queryMode: 'local',
                    reference: 'priorityfilter',
                    editable: false,
                    labelWidth: 80,
                    margin: '10 10 10 10',
                    displayField: 'name',
                    fieldIndex: 'priority',
                    valueField: 'val',
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            weight: -1,
                            hidden: true,
                            scope: 'this',
                            handler: function(combo) {
                                combo.clearValue();
                                combo.getTrigger('clear').hide();
                                me.getController().loadDataGrid();
                            }
                        }
                    },
                    listeners: {
                        'select': function(combo) {
                            combo.getTrigger('clear').show();
                            combo.updateLayout();
                        }
                    }
                }]
            })
        }, {
            text: 'Công cụ',
            iconCls: 'fas fa-briefcase',
            ui: 'dark-blue',
            menu: {
                xtype: 'menu',
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                    handler: 'onPrintButtonClick'
                }, {
                    text: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    handler: 'onExportGriddButtonClick'
                }, '-', {
                    text: 'Làm mới danh sách',
                    iconCls: 'fas fa-sync-alt text-dark-blue',
                    handler: 'onReloadButtonClick'
                }]
            }
        }];

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            hidden: (me.getFilterString()) ? true : false,
            items: tbar
        }];
        me.columns = [{
                xtype: 'rownumberer',
                text: '<b>TT</b>',
                width: 65,
                align: 'center'
            },
            {
                text: '<b>Project Name</b>',
                dataIndex: 'project_name',
                sortable: false,
                width: 130,
                locked: true,
                align: 'center'
            },
            {
                text: '<b>Receive Number</b>',
                dataIndex: 'receive_no',
                width: 110,
                locked: true,
                align: 'center'
            },
            {
                text: '<b>Oder Number</b>',
                dataIndex: 'sub_no',
                width: 130,
                locked: true,
                align: 'center'
            },
            {
                text: '<b>Building Type</b>',
                dataIndex: 'building_type',
                width: 110,
                align: 'center'
            },
            {
                text: '<b>Customer Name</b>',
                dataIndex: 'cus_name',
                width: 130,
                align: 'center'
            },
            {
                text: '<b>Frame</b>',
                dataIndex: 'frame',
                width: 80,
                align: 'center'
            },
            {
                text: '<b>Leaf</b>',
                dataIndex: 'leaf',
                width: 80,
                align: 'center'
            },
            {
                text: '<b>Other</b>',
                align: 'center',
                columns: [{
                    text: 'Type',
                    width: 80,
                    align: 'center',
                    dataIndex: 'frame_type',
                    renderer: function(value, meta, rec) {
                        return value.toUpperCase();
                    }
                }, {
                    text: 'Quality',
                    width: 80,
                    align: 'center',
                    dataIndex: 'frame_total'
                }]
            },
            {
                text: '<b>FINISHING PAINT</b>',
                align: 'center',
                columns: [{
                    text: 'Apply',
                    align: 'center',
                    width: 120,
                    dataIndex: 'paint_apply'
                }, {
                    text: 'Color',
                    align: 'center',
                    width: 120,
                    dataIndex: 'paint_color'
                }]
            },
            {
                text: '<b>Frame Factory Out</b>',
                dataIndex: 'frame_factory_out',
                width: 120,
                align: 'center'
            },
            {
                text: '<b>Leaf Factory Out</b>',
                dataIndex: 'leaf_factory_out',
                width: 120,
                align: 'center'
            },
            {
                text: '<b>Oder Type</b>',
                dataIndex: 'order_type',
                width: 130,
                align: 'center',
                renderer: function(value, meta, rec) {
                    if (value === 1) {
                        return 'Đơn đặt hàng mới';
                    } else if (value === 2) {
                        return 'Đơn đặt hàng chỉnh sửa';
                    } else if (value === 3) {
                        return 'Đơn đặt hàng lại';
                    } else if (value === 4) {
                        return 'Đơn đặt hàng mua phụ kiện';
                    } else if (value === -1) {
                        return 'Đơn đặt hàng bị hủy';
                    }
                    return value;
                }
            },
            {
                text: '<b>Priority</b>',
                dataIndex: 'priority',
                width: 130,
                align: 'center',
                renderer: function(value, meta, rec) {
                    if (value === 1) {
                        return 'Bình thường';
                    } else if (value === 2) {
                        return 'Gấp';
                    }
                    return value;
                }
            },
            {
                text: '<b>Status</b>',
                dataIndex: 'status',
                width: 130,
                align: 'center',
                renderer: function(value, meta, rec) {
                    var stt = value.production_schedule;
                    if (stt === 0) {
                        return '<font color="red">Chưa lập KH</font>';
                    } else if (stt === 1) {
                        return '<font color="blue">Đang lập KH</font>';
                    } else if (stt === 2) {
                        return '<font color="green">Hoàn thành lập KH';
                    }
                }
            }
        ];
        me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true
        };
        me.callParent();
    },

    listeners: {
        'itemcontextmenu': 'onItemContextMenu',
        'select': 'onItemActionMenu',
        'itemdblclick': 'onViewButtonClick',
        'render': function(panel) {
            panel.getController().loadDataGrid();
        }
    },
    reloadView: function() {
        var me = this;
        panel.getController().loadDataGrid();
    }
});