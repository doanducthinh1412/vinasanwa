Ext.define('CMS.view.BPTinhGia.DanhSachDonDatHang.TinhGiaDonDatHang', {
    extend: 'Ext.form.Panel',
    controller: 'TinhGiaDonDatHang',
    viewModel: 'TinhGiaDonDatHang',
    xtype: 'TinhGiaDonDatHang',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        gridPanel: null,
        recordPanel: null,
        modeedit: null
    },
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'left',
        autoFitErrors: true,
        width: '100%',
        labelWidth: 105,
    },
    autoScroll: false,
    initComponent: function() {
        var me = this;
        var recordPanel = me.getRecordPanel();
        var modeedit = me.getModeedit();
        var order_sheet = null;
        if (recordPanel) {
            order_sheet = recordPanel.data.id;
        }
        me.items = [{
            xtype: 'fieldset',
            title: 'Thông tin tính giá',
            defaultType: 'textfield',
            items: [{
                fieldLabel: 'Giá Frame',
                name: 'frame_price',
                allowDecimals: true,
                xtype: 'numberfield',
                value: modeedit === true ? recordPanel.data.order_sheet_price[0].frame_price : null
            }, {
                fieldLabel: 'Giá Leaf',
                name: 'leaf_price',
                allowDecimals: true,
                xtype: 'numberfield',
                value: modeedit === true ? recordPanel.data.order_sheet_price[0].leaf_price : null
            }, {
                fieldLabel: 'Giá Other',
                name: 'other_price',
                allowDecimals: true,
                xtype: 'numberfield',
                value: modeedit === true ? recordPanel.data.order_sheet_price[0].other_price : null
            }, {
                fieldLabel: 'Ghi chú',
                xtype: 'textareafield',
                name: 'note',
                value: modeedit === true ? recordPanel.data.order_sheet_price[0].note : null
            }, {
                xtype: 'hiddenfield',
                name: 'order_sheet',
                value: order_sheet
            }]
        }];
        me.buttons = ['->', {
            text: 'Tính giá',
            glyph: 'xf00c@FontAwesome',
            ui: 'soft-orange',
            handler: 'onClickTinhGiaButton',
            formBind: true
        }, {
            text: 'Hủy bỏ',
            glyph: 'xf00d@FontAwesome',
            handler: function() {
                this.up('window').close();
            }
        }, '->'];
        me.callParent();
    },


});