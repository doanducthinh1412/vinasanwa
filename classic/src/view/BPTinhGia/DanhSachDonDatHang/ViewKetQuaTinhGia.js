Ext.define('CMS.view.BPTinhGia.DanhSachDonDatHang.ViewKetQuaTinhGia', {
    extend: 'Ext.form.Panel',
    controller: 'ViewKetQuaTinhGia',
    //viewModel: 'NhapKetQuaTinhGia',
    xtype: 'ViewKetQuaTinhGia',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        tinhGiaData: null,
		gridPanel: null
    },
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'left',
        autoFitErrors: true,
        width: '100%',
        labelWidth: 105,
    },
    autoScroll: false,
    initComponent: function () {
        var me = this;
		var controller = me.getController();
		var data = me.getTinhGiaData();
        var detailItems = [];
		//console.log(data);
        var detailPrice = (data.detail_price != null)? data.detail_price:[];
        for (i=0;i<detailPrice.length;i++) {
            detailItems.push({
                xtype: 'fieldcontainer',
                width: '100%',
                layout: 'column',
                defaults: {
                    xtype: 'textfield',
                    labelWidth: 0,

                },
                items: [{
                        emptyText: 'Detail Title',
                        value: detailPrice[i].name,
                        columnWidth: 0.5

                    }, {
                        emptyText: 'Detail Price',
                        value: detailPrice[i].value,
                        columnWidth: 0.5
                    }
                ]
            })
        }
        me.items = [{
                title: 'Total Price (US$)',
                xtype: 'panel',
                glyph: 'f039@FontAwesome',
                //cls: 'service-type shadow',
                ui: 'light',
                bodyPadding: 16,
                margin: '0 0 10 0',
                items: [{
                        xtype: 'textfield',
                        labelWidth: 0,
                        value: data.total_price,
                        name: 'total_price',
                        width: '100%'
                    }
                ]
            }, {
                xtype: 'panel',
                title: 'Detail Price (US$)',
                glyph: 'f039@FontAwesome',
                //cls: 'service-type shadow',
                bodyPadding: 16,
                margin: '0 0 10 0',
                ui: 'light',
                layout: 'vbox',
                items: detailItems
            }, {
                xtype: 'panel',
                title: 'Note',
                glyph: 'f039@FontAwesome',
                //cls: 'service-type shadow',
                ui: 'light',
                bodyPadding: 16,
                items: [{
                        xtype: 'textarea',
                        labelWidth: 0,
                        name: 'note',
                        value: data.note,
                        width: '100%'
                    }
                ]
            }
        ];

        me.buttons = ['->', {
                text: 'Xem chi tiết',
                glyph: 'xf00d@FontAwesome',
                handler: function () {
                    this.up('window').close();
                },
                hidden: (data.excel_data == null) ? true : false
            }, {
                text: 'Tải file tính giá',
                glyph: 'xf00d@FontAwesome',
                handler: function () {
                    controller.onDownloadButtonClick();
                }
            }, {
                text: 'Xóa',
                glyph: 'xf00d@FontAwesome',
                handler: function () {
                    controller.onDeleteButtonClick();
                }
            }, {
                text: 'Hủy bỏ',
                glyph: 'xf00d@FontAwesome',
                handler: function () {
                    this.up('window').close();
                }
            }, '->'];
        me.callParent();
    }
});
