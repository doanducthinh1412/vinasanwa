Ext.define('CMS.view.BPTinhGia.DanhSachDonDatHang.NhapKetQuaTinhGia', {
    extend: 'Ext.form.Panel',
    controller: 'NhapKetQuaTinhGia',
    //viewModel: 'NhapKetQuaTinhGia',
    xtype: 'NhapKetQuaTinhGia',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        gridPanel: null,
        recordPanel: null,
        modeedit: null
    },
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'left',
        autoFitErrors: true,
        width: '100%',
        labelWidth: 105,
    },
    autoScroll: false,
    initComponent: function () {
        var me = this;
        var recordPanel = me.getRecordPanel();
        var modeedit = me.getModeedit();
        var order_sheet = null;
        me.items = [{
                xtype: 'hiddenfield',
                name: 'order_sheet',
                value: recordPanel.data.id
            }, {
                xtype: 'hiddenfield',
                name: 'detail_price',
				itemId: 'detail_price'
            }, {
                title: 'Total Price (US$) <span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>',
                xtype: 'panel',
                glyph: 'f039@FontAwesome',
                ui: 'light',
                bodyPadding: 16,
                //margin: '0 0 10 0',
                items: [{
                        xtype: 'textfield',
                        labelWidth: 0,
                        name: 'total_price',
                        width: '100%',
                        allowBlank: false,
                        maskRe: /[0-9.]/,

                    }
                ]
            }, {
                xtype: 'panel',
                reference: 'detailPanel',
                title: 'Detail Price (US$)',
                glyph: 'f039@FontAwesome',
                bodyPadding: 16,
                ui: 'light',
                layout: 'vbox',
                header: {
                    titlePosition: 0,
                    items: [{
                            xtype: 'button',
                            iconCls: 'fas fa-plus-circle',
                            ui: 'round',
                            width: 34,
                            height: 34,
                            handler: function () {
                                var panel = this.up('panel');
                                panel.add(me.createDetailPriceContainer());
                            }
                        }
                    ],
                },
                items: []
            }, {
                xtype: 'panel',
                title: 'Note',
                glyph: 'f039@FontAwesome',
                //cls: 'service-type shadow',
                ui: 'light',
                bodyPadding: 16,
                items: [{
                        xtype: 'textarea',
                        labelWidth: 0,
                        name: 'note',
                        width: '100%'
                    }
                ]
            }, {
                xtype: 'panel',
                title: 'File Tính giá  <span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>',
                glyph: 'f039@FontAwesome',
                //cls: 'service-type shadow',
                ui: 'light',
                bodyPadding: 16,
                items: [{
                        xtype: 'fileuploadfield',
                        labelWidth: 0,
                        name: 'custom_price_file',
                        width: '100%',
                        allowBlank: false,
                        afterLabelTextTpl: [
                            '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                        ],
                    }
                ]
            }
        ];

        me.callParent();
    },
    createDetailPriceContainer: function () {
        var fc = {
            xtype: 'fieldcontainer',
            width: '100%',
            layout: 'column',
            defaults: {
                xtype: 'textfield',
                labelWidth: 0,
                submitValue: false
            },
            items: [{
                    emptyText: 'Detail Title',
                    itemId: 'name',
                    columnWidth: 0.5
                }, {
                    emptyText: 'Detail Price',
                    itemId: 'value',
                    columnWidth: 0.5,
                    maskRe: /[0-9.]/
                }, {
                    xtype: 'button',
                    iconCls: 'fas fa-minus-circle',
                    ui: 'round',
                    width: 34,
                    height: 34,
                    handler: function () {
                        this.up('fieldcontainer').destroy();
                    }
                }
            ],

        };
        return fc
    },

});
