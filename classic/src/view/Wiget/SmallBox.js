Ext.define('CMS.view.Wiget.SmallBox', {
    extend: 'Ext.Component',
    xtype: 'SmallBox',
    cls: 'weather-panel shadow',
    baseCls: 'weather-panel',
    userCls: 'big-33 small-100',
    border: false,
    height: 80,
    data: {
        icon: 'ion ion-ios-analytics',
        number: '',
        title: '',
        background: '#82d9EA',
        width: '150px'
    },
    tpl: '<div class="weather-image-container" style="background: {background};"><i class="{icon}"></i></div>' +
        '<div class="weather-details-container">' +
        '<div class="mumber-box">{number}</div>' +
        '<div style="display: flex;width: {width};"><div class="title-box">{title}</div></div>' +
        '</div>'
});