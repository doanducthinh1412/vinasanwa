Ext.define('CMS.view.Wiget.SmallBox2', {
    extend: 'Ext.Component',
    xtype: 'SmallBox2',
    userCls: 'big-25 small-50',
    ui: 'light',
    cls: 'service-type shadow',
    height: 120,
    headerPosition: 'bottom',
    titleAlign: 'center',
    items: {
        xtype: 'component',
        data: {
            amount: 0,
            icon: 'user-plus',
            iconColor: '#00a65a'
        },
        tpl: '<div style="text-align: center;"><span style="color:{iconColor}" class="x-fa fa-{icon} fa-3x"></span><h1>{amount}</h1></div>',
    }
});