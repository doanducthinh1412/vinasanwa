Ext.define('CMS.view.Wiget.WindowFullPanel', {
    extend: 'Ext.window.Window',
    xtype: 'WindowFullPanel',
    scrollable: true,
    width: '100%',
    height: '100%',
    layout: 'fit',
    plain: true,
    header: false,
    border: false,
    bodyBorder: false,
    draggable: false,
    resizable: false,
    style: 'padding: 0; border-width: 0;',
    title: '',
    frame: false,
    config: {
        titleWindow: null
    },
    initComponent: function() {
        var me = this;
        me.tbar = {
            cls: 'shadow',
            style: {
                background: '#32404e',
            },
            items: ['->', {
                xtype: 'container',
                html: Ext.String.format('<h1 style="color:#FFFFFF;font-size:16px;"><i class="{0}" aria-hidden="true"></i> {1}</h1>', me.getIconCls(), me.getTitleWindow())
            }, '->', {
                text: 'Thoát',
                ui: 'soft-red',
                glyph: 'xf00d@FontAwesome',
                handler: function() {
                    this.up().up().close();
                }
            }]
        };
        me.callParent();
    }
});