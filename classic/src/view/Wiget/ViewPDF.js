Ext.define('CMS.view.Wiget.ViewPDF', {
    extend: 'Ext.panel.Panel',
    xtype: 'ViewPDF',
    layout: 'fit',
    config: {
        urlPdf: ''
    },
    initComponent: function() {
        var me = this;
        var url = 'lib/pdfviewer/index.html?path=' + encodeURIComponent(me.urlPdf);
        me.html = '<iframe id=\"apprun\" style=\"border:0px solid #000000\" width=\"100%\" height=\"100%\" src=\"' + url + '\"></iframe>';
        me.callParent();
    },
    changeUrlPdf: function(new_url) {
        var me = this;
        if (new_url) {
            var url = 'lib/pdfviewer/index.html?path=' + encodeURIComponent(new_url);
            me.setUrlPdf(new_url);
            me.update('<iframe id=\"apprun\" style=\"border:0px solid #000000\" width=\"100%\" height=\"100%\" src=\"' + url + '\"></iframe>');
            me.updateLayout();
        }
    }
});