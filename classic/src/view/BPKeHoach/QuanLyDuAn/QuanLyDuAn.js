Ext.define('CMS.view.BPKeHoach.QuanLyDuAn.QuanLyDuAn', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.Panel'
    ],
    controller: 'QuanLyDuAn',
    viewModel: 'QuanLyDuAn',
    xtype: 'QuanLyDuAn',
    layout: 'fit',
    bind: {
        store: '{DuAnStore}'
    },
    reference: 'QuanLyDuAnGrid',
    columnLines: true,
    autoScroll: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    config: {
        filterString: null
    },
    features: [{
        ftype: 'grouping',
        startCollapsed: false,
        groupHeaderTpl: '<font color="blue"><b>Dự án:</b> {name} ({rows.length} đơn đặt hàng)</font>'
    }],
    initComponent: function() {
        var me = this;
        var filterString = me.getFilterString();
        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            hidden: (filterString) ? true : false,
            items: [
                '->',
                {
                    xtype: 'textfield',
                    emptyText: 'Nhập tìm kiếm',
                    width: 250,
                    reference: 'SearchField',
                    triggers: {
                        clear: {
                            weight: 0,
                            cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                            hidden: true,
                            handler: 'onClearClick',
                            scope: 'this'
                        },
                        search: {
                            weight: 1,
                            cls: Ext.baseCSSPrefix + 'form-search-trigger',
                            handler: 'onSearchClick',
                            scope: 'this'
                        }
                    },
                    onClearClick: function() {
                        this.setValue('');
                        this.getTrigger('clear').hide();
                        this.updateLayout();
                        me.getController().loadDataGrid();
                    },
                    onSearchClick: function() {
                        var value = this.getValue();
                        if (value.length > 0) {
                            this.getTrigger('clear').show();
                            this.updateLayout();
                        } else {
                            this.getTrigger('clear').hide();
                        }
                        me.getController().loadDataGrid();
                    },
                    listeners: {
                        'specialkey': function(f, e) {
                            if (e.getKey() == e.ENTER) {
                                this.onSearchClick();
                            }
                        }
                    }
                },
                {
                    text: 'Công cụ',
                    iconCls: 'fas fa-briefcase',
                    ui: 'dark-blue',
                    menu: {
                        xtype: 'menu',
                        plain: true,
                        mouseLeaveDelay: 10,
                        items: [{
                            text: 'In ấn',
                            iconCls: 'fas fa-print text-dark-blue',
                            handler: 'onPrintButtonClick'
                        }, {
                            text: 'Xuất Excel',
                            iconCls: 'fas fa-file-export text-dark-blue',
                            handler: 'onExportdButtonClick'
                        }, '-', {
                            text: 'Làm mới danh sách',
                            iconCls: 'fas fa-sync-alt text-dark-blue',
                            handler: 'onReloadButtonClick'
                        }]
                    }
                }
            ]
        }];

        me.columns = [{
                xtype: 'rownumberer',
                text: '<b>TT</b>',
                width: 65,
                locked: true,
                align: 'center'
            },
            {
                text: '<b>Name Project</b>',
                dataIndex: 'project_name',
                width: 200,
                locked: true,
                align: 'center',
                renderer: function(value, meta, rec) {
                    return '<b>' + value + '</b>';
                }
            },
            {
                text: '<b>MO Subject</b>',
                dataIndex: 'receive_no',
                width: 130,
                locked: true,
                align: 'center'
            },
            {
                text: '<b>No/Yes</b>',
                dataIndex: '',
                width: 80,
                align: 'center'
            },
            {
                text: '<b>Comecial value</b>',
                dataIndex: '',
                width: 100,
                align: 'center'
            }, {
                text: '<br><b>VN</b><br>',
                columns: [{
                    text: '<br><b>EXPORT</b><br>',
                    dataIndex: 'building_type',
                    width: 80,
                    align: 'center'
                }]
            },
            {
                text: '<br><b>HCMC</b><br>',
                columns: [{
                    text: '<br><b>HANOI</b><br>',
                    dataIndex: 'receive_no',
                    width: 80,
                    align: 'center',
                    renderer: function(value, meta, rec) {
                        if (value.toUpperCase().indexOf("HN") !== -1) {
                            return "HANOI"
                        } else {
                            return "HCMC"
                        }
                    }
                }]
            },
            {
                text: '<b>Sub No</b>',
                dataIndex: 'sub_no',
                locked: true,
                width: 120,
                align: 'center',
            },
            {
                text: '<b>Order Sheet come date</b>',
                dataIndex: '',
                width: 80,
                align: 'center',
            }, {
                text: '<b>Date of Change in order sheet</b>',
                columns: [{
                    text: '<b></b>',
                    dataIndex: 'change_1',
                    width: 80,
                    align: 'center',
                }, {
                    text: '<b></b>',
                    dataIndex: 'change_2',
                    width: 80,
                    align: 'center',
                }, {
                    text: '<b></b>',
                    dataIndex: 'change_3',
                    width: 80,
                    align: 'center',
                }, {
                    text: '<b></b>',
                    dataIndex: 'change_4',
                    width: 80,
                    align: 'center',
                }, {
                    text: '<b></b>',
                    dataIndex: 'change_5',
                    width: 80,
                    align: 'center',
                }]
            }, {
                text: 'Delivery request date',
                columns: [{
                    text: '<b>Frame</b>',
                    columns: [{
                        text: '<b>SD</b>',
                        dataIndex: 'frame_factory_out',
                        width: 80,
                        align: 'center',
                    }, {
                        text: '<b>LSD</b>',
                        dataIndex: 'frame_factory_out',
                        width: 80,
                        align: 'center',
                    }]
                }, {
                    text: '<b>Leaf</b>',
                    columns: [{
                        text: '<b>SD</b>',
                        dataIndex: 'leaf_factory_out',
                        width: 80,
                        align: 'center',
                    }, {
                        text: '<b>LSD</b>',
                        dataIndex: 'leaf_factory_out',
                        width: 80,
                        align: 'center',
                    }]
                }, {
                    text: '<b>SMOOD DOOR</b>',
                    columns: [{
                        text: '<b>Frame</b>',
                        dataIndex: 'frame_factory_out',
                        width: 80,
                        align: 'center',
                    }, {
                        text: '<b>Leaf</b>',
                        dataIndex: 'leaf_factory_out',
                        width: 80,
                        align: 'center',
                    }]
                }, {
                    text: '<b>SP HANGER</b>',
                    columns: [{
                        text: '<b>Frame</b>',
                        dataIndex: 'frame_factory_out',
                        width: 80,
                        align: 'center',
                    }, {
                        text: '<b>Leaf</b>',
                        dataIndex: 'leaf_factory_out',
                        width: 80,
                        align: 'center',
                    }]
                }, {
                    text: '<b>SHUTTERS</b>',
                    columns: [{
                        text: '<b>JP</b>',
                        dataIndex: 'cus_name',
                        width: 80,
                        align: 'center',
                    }, {
                        text: '<b>TW</b>',
                        dataIndex: 'cus_name',
                        width: 80,
                        align: 'center',
                    }, {
                        text: '<b>Grill</b>',
                        dataIndex: 'cus_name',
                        width: 80,
                        align: 'center',
                    }]
                }, {
                    text: '<b>QUICK</b>',
                    columns: [{
                        text: '<b>GR-S</b>',
                        dataIndex: 'cus_name',
                        width: 80,
                        align: 'center',
                    }, {
                        text: '<b>S13</b>',
                        dataIndex: 'cus_name',
                        width: 80,
                        align: 'center',
                    }, {
                        text: '<b>S14</b>',
                        dataIndex: 'cus_name',
                        width: 80,
                        align: 'center',
                    }]
                }, {
                    text: '<b>LOUVER</b>',
                    columns: [{
                        text: '<b>Louver</b>',
                        dataIndex: 'cus_name',
                        width: 80,
                        align: 'center',
                    }]
                }, {
                    text: '<b>Other</b>',
                    dataIndex: 'other_factory_out',
                    width: 80,
                    align: 'center',
                    renderer: 'renderDateColumn'
                }]
            }, {
                text: 'Quality',
                columns: [{
                    text: '<b>Frame</b>',
                    columns: [{
                        text: '<b>SD</b>',
                        dataIndex: 'frame_sd',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>LSD</b>',
                        dataIndex: 'frame_lsd',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>FSD</b>',
                        dataIndex: 'frame_fsd',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>FLSD</b>',
                        dataIndex: 'frame_flsd',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }]
                }, {
                    text: '<b>Leaf</b>',
                    columns: [{
                        text: '<b>SD</b>',
                        dataIndex: 'leaf_sd',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>LSD</b>',
                        dataIndex: 'leaf_lsd',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>FSD</b>',
                        dataIndex: 'leaf_fsd',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>FLSD</b>',
                        dataIndex: 'leaf_flsd',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }]
                }, {
                    text: '<b>SMOOD DOOR</b>',
                    columns: [{
                        text: '<b>Frame</b>',
                        dataIndex: 'frame_sld',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>Leaf</b>',
                        dataIndex: 'leaf_sld',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }]
                }, {
                    text: '<b>SP HANGER</b>',
                    columns: [{
                        text: '<b>Frame</b>',
                        dataIndex: 'frame_sp',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>Leaf</b>',
                        dataIndex: 'leaf_sp',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }]
                }, {
                    text: '<b>SHUTTERS</b>',
                    columns: [{
                        text: '<b>JP</b>',
                        dataIndex: 'ssjp',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>Aichi</b>',
                        dataIndex: 'ssaichi',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>Grill</b>',
                        dataIndex: 'ssgrill',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }]
                }, {
                    text: '<b>QUICKSAVE</b>',
                    columns: [{
                        text: '<b>SRN</b>',
                        dataIndex: 'srn',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>GR-S</b>',
                        dataIndex: 'grs',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>S13</b>',
                        dataIndex: 's13',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }, {
                        text: '<b>S14</b>',
                        dataIndex: 's14',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }]
                }, {
                    text: '<b>SW</b>',
                    columns: [{
                        text: '<b>SW</b>',
                        dataIndex: 'sw',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }]
                }, {
                    text: '<b>LOUVER</b>',
                    columns: [{
                        text: '<b>Louver</b>',
                        dataIndex: 'lv',
                        width: 80,
                        align: 'center',
                        renderer: 'renderNumberColumn'
                    }]
                }, {
                    text: '<b>Other</b>',
                    dataIndex: 'other',
                    width: 80,
                    align: 'center',
                    renderer: 'renderNumberColumn'
                }]
            }, {
                text: '<b>set/pcs</b>',
                dataIndex: 'frame_unit',
                width: 80,
                align: 'center',
            }, {
                text: '<b>COLOR PAINT</b>',
                dataIndex: 'paint_color',
                width: 150,
                align: 'center',
            }
        ];
        me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true
        };
        me.getViewModel().getStore('DuAnStore').filter('deletedAt', null);
        if (filterString) {
            me.getViewModel().getStore('DuAnStore').filterBy(function(rec, id) {
                var name = rec.get('name');
                name = name.toLowerCase();
                filterString = filterString.toLowerCase();
                if (name.indexOf(filterString) != -1) {
                    return true;
                } else {
                    return false;
                }
            });
        }
        me.callParent();
    },
    listeners: {
        //'itemcontextmenu': 'onItemContextMenu',
        'itemdblclick': 'onViewButtonClick',
    },

    reloadView: function() {
        var me = this;
        me.el.mask('Đang tải...');
        var filterString = me.getFilterString();
        me.getViewModel().getStore('DuAnStore').clearFilter();
        me.getViewModel().getStore('DuAnStore').filter('deletedAt', null);
        if (filterString) {
            me.getViewModel().getStore('DuAnStore').filterBy(function(rec, id) {
                var name = rec.get('name');
                name = name.toLowerCase();
                filterString = filterString.toLowerCase();
                if (name.indexOf(filterString) != -1) {
                    return true;
                } else {
                    return false;
                }
            });
        }
        me.el.unmask();
    }
});