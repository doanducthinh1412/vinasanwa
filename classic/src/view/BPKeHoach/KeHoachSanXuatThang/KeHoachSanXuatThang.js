Ext.define('CMS.view.BPKeHoach.KeHoachSanXuatThang.KeHoachSanXuatThang', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.grid.feature.Grouping',
        'Ext.grid.plugin.CellEditing',
    ],
    controller: 'KeHoachSanXuatThang',
    xtype: 'KeHoachSanXuatThang',
    viewModel: 'KeHoachSanXuatThang',
    bodyPadding: 10,
    layout: 'hbox',
    //layout: 'fit',
    scrollable: 'x',
    config: {
        gridPanel: '',
        storeParent: '',
        relationComponent: '',
        relationData: ''
    },
    //autoScroll: true,
    bodyStyle: {
        'background-color': '#f6f6f6;',
        'padding': '5px'
    },

    initComponent: function() {
        var me = this;

        var years = [];
        var y = 2000;
        var current_year = (new Date()).getFullYear();
        //years.push([0,'------']);
        while (current_year >= y) {
            years.push([current_year, current_year]);
            current_year--;
        }
        var current_year = (new Date()).getFullYear();
        var current_month = (new Date()).getMonth() + 1;
        var yearStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'display'],
            data: years
        });
        var months = [];
        var month = 1;
        while (month <= 12) {
            months.push([month, 'Tháng ' + month.toString()]);
            month++;
        }
        months.push(['fullyear', 'Cả năm']);
        var monthStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'display'],
            data: months
        });
        me.tbar = [{
                xtype: 'combo',
                fieldLabel: 'Chọn năm',
                emptyText: '----------',
                labelWidth: 100,
                name: 'yearCmb',
                store: yearStore,
                queryMode: 'local',
                displayField: 'display',
                valueField: 'value',
                width: 250,
                reference: 'yearCmb',
                value: current_year,
                listeners: {
                    change: function(combo, newValue, oldValue) {
                        var refs = me.getReferences(),
                            startdate = refs.startdate,
                            enddate = refs.enddate,
                            year = refs.yearCmb.getValue(),
                            month = refs.monthCmb.getValue();
                        if (year && month) {
                            startdate.setValue(new Date(year, month - 1).toLocaleDateString('en-GB'));
                            enddate.setValue(new Date(year, month, 0).toLocaleDateString('en-GB'))
                        }

                    }
                }
            }, '-',
            {
                xtype: 'combo',
                fieldLabel: 'Chọn tháng',
                //emptyText  	: '----------',
                labelWidth: 100,
                name: 'monthCmb',
                store: monthStore,
                queryMode: 'local',
                displayField: 'display',
                valueField: 'value',
                width: 250,
                reference: 'monthCmb',
                value: current_month,
                //multiSelect	: true,
                listeners: {
                    change: function(combo, newValue, oldValue) {
                        var refs = me.getReferences(),
                            startdate = refs.startdate,
                            enddate = refs.enddate,
                            year = refs.yearCmb.getValue(),
                            month = refs.monthCmb.getValue();
                        if (year && month) {
                            startdate.setValue(new Date(year, month - 1).toLocaleDateString('en-GB'));
                            startdate.setMaxValue(new Date(year, month, 0).toLocaleDateString('en-GB'));
                            startdate.setMinValue(new Date(year, month - 1).toLocaleDateString('en-GB'));
                            enddate.setValue(new Date(year, month, 0).toLocaleDateString('en-GB'));
                            enddate.setMaxValue(new Date(year, month, 0).toLocaleDateString('en-GB'));
                            enddate.setMinValue(new Date(year, month - 1).toLocaleDateString('en-GB'));
                        }

                    }
                }
            }, '-',
            {
                xtype: 'datefield',
                fieldLabel: 'Từ ngày',
                labelWidth: 70,
                width: 200,
                reference: 'startdate',
                format: 'd/m/Y',
                submitFormat: 'Y/m/d',
                value: new Date(current_year, current_month - 1).toLocaleDateString('en-GB'),
                minValue: new Date(current_year, current_month - 1).toLocaleDateString('en-GB'),
                maxValue: new Date(current_year, current_month, 0).toLocaleDateString('en-GB')
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Đến ngày',
                labelWidth: 70,
                width: 200,
                reference: 'enddate',
                format: 'd/m/Y',
                submitFormat: 'Y/m/d',
                value: new Date(current_year, current_month, 0).toLocaleDateString('en-GB'),
                minValue: new Date(current_year, current_month - 1).toLocaleDateString('en-GB'),
                maxValue: new Date(current_year, current_month, 0).toLocaleDateString('en-GB')
            }, '-',
            {
                text: 'Xem',
                ui: 'dark-blue',
                handler: function() {
                    me.reloadGrid();
                }
            }, '->',
            {
                text: 'Công cụ',
                iconCls: 'fas fa-briefcase',
                ui: 'dark-blue',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    mouseLeaveDelay: 10,
                    items: [{
                        text: 'In ấn',
                        iconCls: 'fas fa-print text-dark-blue',
                        handler: 'onPrintButtonClick'
                    }, {
                        text: 'Xuất Excel',
                        iconCls: 'fas fa-file-export text-dark-blue',
                        handler: 'onExportButtonClick'
                    }, '-', {
                        text: 'Làm mới danh sách',
                        iconCls: 'fas fa-sync-alt text-dark-blue',
                        handler: 'onReloadButtonClick'
                    }]
                }
            }
        ];



        me.items = [{
                xtype: 'panel',
                layout: 'border',
                height: '100%',
                width: 1325,
                bodyPadding: 0,
                items: [{
                    xtype: 'grid',
                    columnLines: true,
                    //width: 1305,
                    region: 'center',
                    title: '<b>Cummulative Total Grid</b>',
                    reference: 'cummulativeTotalGrid',
                    ui: 'light',
                    cls: 'service-type shadow',
                    style: 'border-left: solid Black 2px',
                    bind: {
                        store: '{cummulativeTotalStore}'
                    },
                    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                    plugins: {
                        ptype: 'cellediting',
                        clicksToEdit: 2,
                        listeners: {
                            edit: 'onEditPlanCell',
                            //beforeedit: 'onBeforeEditCell'
                            beforeEdit: function(editor, context) {
                                if (context.record.get("date") != 'Plan') {
                                    return false;
                                }

                            },
                        }
                    },
                    autoScroll: true,
                    listeners: {
                        'itemClick': 'onItemClick',
                        'afterrender': function() {
                            //var grid = this;
                            var refs = me.getReferences();
                            var grid = refs.cummulativeTotalGrid;
                            grid.view.on('scroll', function(e, f) {

                            })
                        }
                    },
                    columns: {
                        defaults: {
                            flex: 1
                        },
                        items: [{
                                text: '<b>Date</b>',
                                dataIndex: 'date',
                                height: 100,
                                align: 'center',
                                renderer: 'renderDateColumn'
                            },
                            {
                                text: '<b>Frame</b>',
                                columns: [{
                                    text: 'SD',
                                    dataIndex: 'frame_sd',
                                    align: 'center',
                                    renderer: 'renderMergeColumn',
                                    editor: {
                                        xtype: 'numberfield',
                                        selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true
                                    },
                                }, {
                                    text: 'LSD',
                                    dataIndex: 'frame_lsd',
                                    align: 'center',
                                    renderer: 'renderPlanColumn',
                                    editor: {
                                        xtype: 'numberfield',
                                        selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true
                                    },
                                }]

                            },
                            {
                                text: '<b>Leaf</b>',
                                align: 'center',
                                columns: [{
                                    text: 'SD',
                                    dataIndex: 'leaf_sd',
                                    align: 'center',
                                    renderer: 'renderMergeColumn',
                                    /*editor: {
                                    	xtype: 'numberfield',
                                    	selectOnFocus: true,
                                    	hideTrigger: true,
                                    	minValue: 0,
                                    	allowBlank: true
                                    },*/

                                }, {
                                    text: 'LSD',
                                    dataIndex: 'leaf_lsd',
                                    align: 'center',
                                    renderer: 'renderPlanColumn',
                                    editor: {
                                        xtype: 'numberfield',
                                        selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true
                                    },
                                }]
                            },
                            {
                                text: '<b>SS</b>',
                                align: 'center',
                                columns: [{
                                    text: 'QTY',
                                    dataIndex: 'ss',
                                    align: 'center',
                                    renderer: 'renderMergeColumn',
                                    editor: {
                                        xtype: 'numberfield',
                                        selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true
                                    },
                                }, {
                                    text: 'M2',
                                    dataIndex: 'ss_m2',
                                    align: 'center',
                                    renderer: 'renderPlanColumn',
                                    /*editor: {
                                    	xtype: 'numberfield',
                                    	selectOnFocus: true,
                                    	hideTrigger: true,
                                    	minValue: 0,
                                    	allowBlank: true
                                    },*/
                                }]
                            },
                            {
                                text: '<b>QSAVE</b>',
                                dataIndex: 'qs',
                                align: 'center',
                                renderer: 'renderPlanColumn',
                                editor: {
                                    xtype: 'numberfield',
                                    selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true
                                },
                            },
                            {
                                text: '<b>SP HANGER</b>',
                                align: 'center',
                                columns: [{
                                    text: 'Frame',
                                    dataIndex: 'frame_sp',
                                    align: 'center',
                                    renderer: 'renderMergeColumn',
                                    /*editor: {
                                    	xtype: 'numberfield',
                                    	selectOnFocus: true,
                                    	hideTrigger: true,
                                    	minValue: 0,
                                    	allowBlank: true
                                    },*/
                                }, {
                                    text: 'Leaf',
                                    dataIndex: 'leaf_sp',
                                    align: 'center',
                                    renderer: 'renderPlanColumn',
                                    editor: {
                                        xtype: 'numberfield',
                                        selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true
                                    },
                                }]
                            },
                            {
                                text: '<b>SMOOD DOOR</b>',
                                align: 'center',
                                columns: [{
                                    text: 'Frame',
                                    dataIndex: 'frame_sld',
                                    align: 'center',
                                    renderer: 'renderMergeColumn',
                                    /*editor: {
                                    	xtype: 'numberfield',
                                    	selectOnFocus: true,
                                    	hideTrigger: true,
                                    	minValue: 0,
                                    	allowBlank: true
                                    },*/
                                }, {
                                    text: 'Leaf',
                                    dataIndex: 'leaf_sld',
                                    align: 'center',
                                    renderer: 'renderPlanColumn',
                                    editor: {
                                        xtype: 'numberfield',
                                        selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true
                                    },
                                }]
                            },
                            {
                                text: '<b>LOUVER</b>',
                                dataIndex: 'lv',
                                align: 'center',
                                renderer: 'renderPlanColumn',
                                editor: {
                                    xtype: 'numberfield',
                                    selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true
                                },
                            },
                            {
                                text: '<b>OTHER</b>',
                                dataIndex: 'other',
                                align: 'center',
                                renderer: 'renderPlanColumn',
                                editor: {
                                    xtype: 'numberfield',
                                    selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true
                                },
                            }
                        ]
                    }
                }]
            },
            {
                xtype: 'panel',
                layout: 'border',
                height: '100%',
                width: 1625,
                bodyPadding: 0,
                items: [{
                    xtype: 'grid',
                    columnLines: true,
                    region: 'center',
                    //width: 1505,
                    title: '<b>US $/QUANTITY</b>',
                    reference: 'usQuantityGrid',
                    ui: 'light',
                    cls: 'service-type shadow',
                    style: 'border-left: solid Black 2px',
                    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                    bind: {
                        store: '{usQuantityStore}'
                    },
                    listeners: {
                        'itemClick': 'onItemClick'
                    },
                    autoScroll: true,
                    plugins: {
                        ptype: 'cellediting',
                        clicksToEdit: 2,
                        listeners: {
                            //edit: 'onEditCell',
                            //beforeedit: 'onBeforeEditCell'
                        }
                    },
                    columns: {
                        defaults: {
                            flex: 1
                        },
                        items: [{
                                text: '<b>Date</b>',
                                height: 100,
                                dataIndex: 'date',
                                align: 'center',
                                renderer: 'renderDateColumn'
                            },
                            {
                                text: '<b>Amount</b>',
                                align: 'center',
                                columns: [{
                                        text: '<b>Frame</b>',
                                        columns: [{
                                            text: 'SD',
                                            align: 'center',
                                            height: 50,
                                            columns: [{
                                                bind: {
                                                    text: '<b>{priceData.frame_sd}</b>',
                                                },
                                                dataIndex: 'frame_sd',
                                                align: 'center',
                                                renderer: 'renderPlanColumn'
                                            }]
                                        }, {
                                            text: 'LSD',
                                            align: 'center',
                                            height: 50,
                                            columns: [{
                                                bind: {
                                                    text: '<b>{priceData.frame_lsd}</b>',
                                                },
                                                dataIndex: 'frame_lsd',
                                                align: 'center',
                                                renderer: 'renderPlanColumn'
                                            }]
                                        }]

                                    },
                                    {
                                        text: '<b>Leaf</b>',
                                        align: 'center',
                                        columns: [{
                                            text: 'SD',
                                            align: 'center',
                                            columns: [{
                                                //text: '<b>121.94</b>',
                                                bind: {
                                                    text: '<b>{priceData.leaf_sd}</b>',
                                                },
                                                dataIndex: 'leaf_sd',
                                                align: 'center',
                                                renderer: 'renderMergeColumn'
                                            }]
                                        }, {
                                            text: 'LSD',
                                            align: 'center',
                                            columns: [{
                                                bind: {
                                                    text: '<b>{priceData.leaf_lsd}</b>',
                                                },
                                                dataIndex: 'leaf_lsd',
                                                align: 'center',
                                                renderer: 'renderPlanColumn'
                                            }]
                                        }]
                                    },
                                    {
                                        text: '<b>SHUTTER</b>',
                                        align: 'center',
                                        height: 50,
                                        columns: [{
                                            bind: {
                                                text: '<b>{priceData.ss}</b>',
                                            },
                                            dataIndex: 'ss',
                                            align: 'center',
                                            renderer: 'renderPlanColumn'
                                        }]
                                    },
                                    {
                                        text: '<b>QSAVE</b>',
                                        align: 'center',
                                        height: 50,
                                        columns: [{
                                            bind: {
                                                text: '<b>{priceData.qs}</b>',
                                            },
                                            dataIndex: 'qs',
                                            align: 'center',
                                            renderer: 'renderPlanColumn'
                                        }]
                                    },
                                    {
                                        text: '<b>SP HANGER</b>',
                                        align: 'center',
                                        columns: [{
                                                text: 'Frame',
                                                align: 'center',
                                                columns: [{
                                                    bind: {
                                                        text: '<b>{priceData.frame_sp}</b>',
                                                    },
                                                    dataIndex: 'frame_sp',
                                                    align: 'center',
                                                    renderer: 'renderMergeColumn'
                                                }]
                                            },
                                            {
                                                text: 'Leaf',
                                                align: 'center',
                                                columns: [{
                                                    bind: {
                                                        text: '<b>{priceData.leaf_sp}</b>',
                                                    },
                                                    dataIndex: 'leaf_sp',
                                                    align: 'center',
                                                    renderer: 'renderPlanColumn'
                                                }]
                                            }
                                        ]
                                    },
                                    {
                                        text: '<b>SMOOD DOOR</b>',
                                        align: 'center',
                                        columns: [{
                                            text: 'Frame',
                                            align: 'center',
                                            columns: [{
                                                bind: {
                                                    text: '<b>{priceData.frame_sld}</b>',
                                                },
                                                dataIndex: 'frame_sld',
                                                align: 'center',
                                                renderer: 'renderMergeColumn'
                                            }]
                                        }, {
                                            text: 'Leaf',
                                            align: 'center',
                                            columns: [{
                                                bind: {
                                                    text: '<b>{priceData.leaf_sld}</b>',
                                                },
                                                dataIndex: 'leaf_sld',
                                                align: 'center',
                                                renderer: 'renderPlanColumn'
                                            }]
                                        }]
                                    },
                                    {
                                        text: '<b>LOUVER</b>',
                                        align: 'center',
                                        height: 50,
                                        columns: [{
                                            bind: {
                                                text: '<b>{priceData.lv}</b>',
                                            },
                                            dataIndex: 'lv',
                                            align: 'center',
                                            renderer: 'renderPlanColumn'
                                        }]
                                    },
                                    {
                                        text: '<b>OTHER</b>',

                                        align: 'center',
                                        columns: [{
                                            bind: {
                                                text: '<b>{priceData.other}</b>',
                                            },
                                            dataIndex: 'other',
                                            align: 'center',
                                            renderer: 'renderPlanColumn'
                                        }]
                                    },
                                    {
                                        text: '<b>Total</b>',
                                        align: 'center',
                                        dataIndex: 'total',
                                        renderer: function(value, metadata, record) {
                                            //var total =  me.getTotalValue(record);
                                            if (value) {
                                                return '<b>' + value + '</b>'
                                            } else {
                                                return '-'
                                            }
                                        }
                                    },
                                    {
                                        text: '<b>Ratio(%)</b>',
                                        align: 'center',
                                        dataIndex: 'ratio',
                                        renderer: 'renderMergeColumn'
                                    },
                                    {
                                        text: '<b>Plan</b>',
                                        align: 'center',
                                        dataIndex: 'plan',
                                        renderer: 'renderMergeColumn'
                                            /*editor: {
                                            	xtype: 'numberfield',
                                            	selectOnFocus: true,
                                            	hideTrigger: true,
                                            	minValue: 0,
                                            	allowBlank: true
                                            },*/
                                    }
                                ]
                            },
                        ]
                    },
                }]
            },
            {
                xtype: 'panel',
                layout: 'border',
                height: '100%',
                width: 1325,
                bodyPadding: 0,
                items: [{
                    xtype: 'grid',
                    columnLines: true,
                    region: 'center',
                    //width: 1305,
                    title: '<b>Each Day</b>',
                    reference: 'eachDayGrid',
                    ui: 'light',
                    cls: 'service-type shadow',
                    style: 'border-left: solid Black 2px',
                    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                    bind: {
                        store: '{eachDayStore}'
                    },
                    listeners: {
                        'itemClick': 'onItemClick'
                    },
                    autoScroll: true,
                    plugins: {
                        ptype: 'cellediting',
                        clicksToEdit: 2,
                        listeners: {
                            edit: 'onEditM2Cell',
                            //beforeedit: 'onBeforeEditCell'
                            beforeEdit: function(editor, context) {
                                if (isNaN(context.record.get("id"))) {
                                    return false;
                                }
                            },
                        }
                    },
                    columns: {
                        defaults: {
                            flex: 1
                        },
                        items: [{
                                text: '<b>Date</b>',
                                dataIndex: 'date',
                                height: 100,
                                align: 'center',
                                renderer: 'renderDateColumn'
                            },
                            {
                                text: '<b>Frame</b>',
                                columns: [{
                                    text: 'SD',
                                    dataIndex: 'frame_sd',
                                    align: 'center',
                                    renderer: 'renderPlanColumn'
                                }, {
                                    text: 'LSD',
                                    dataIndex: 'frame_lsd',
                                    align: 'center',
                                    renderer: 'renderPlanColumn'
                                }]

                            },
                            {
                                text: '<b>Leaf</b>',
                                align: 'center',
                                columns: [{
                                    text: 'SD',
                                    dataIndex: 'leaf_sd',
                                    align: 'center',
                                    renderer: 'renderPlanColumn'

                                }, {
                                    text: 'LSD',
                                    dataIndex: 'leaf_lsd',
                                    align: 'center',
                                    renderer: 'renderPlanColumn'
                                }]
                            },
                            {
                                text: '<b>SS</b>',
                                align: 'center',
                                columns: [{
                                    text: 'QTY',
                                    dataIndex: 'ss',
                                    align: 'center',
                                    renderer: 'renderPlanColumn'
                                }, {
                                    text: 'M2',
                                    dataIndex: 'ss_m2',
                                    align: 'center',
                                    editor: {
                                        xtype: 'numberfield',
                                        selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true
                                    },
                                    renderer: 'renderPlanColumn'
                                }]
                            },
                            {
                                text: '<b>QSAVE</b>',
                                dataIndex: 'qs',
                                align: 'center',
                                renderer: 'renderPlanColumn'
                            },
                            {
                                text: '<b>SP HANGER</b>',
                                align: 'center',
                                columns: [{
                                    text: 'Frame',
                                    dataIndex: 'frame_sp',
                                    align: 'center',
                                    renderer: 'renderPlanColumn'
                                }, {
                                    text: 'Leaf',
                                    dataIndex: 'leaf_sp',
                                    align: 'center',
                                    renderer: 'renderPlanColumn'
                                }]
                            },
                            {
                                text: '<b>SMOOD DOOR</b>',
                                align: 'center',
                                columns: [{
                                    text: 'Frame',
                                    dataIndex: 'frame_sld',
                                    align: 'center',
                                    renderer: 'renderPlanColumn'
                                }, {
                                    text: 'Leaf',
                                    dataIndex: 'leaf_sld',
                                    align: 'center',
                                    renderer: 'renderPlanColumn'
                                }]
                            },
                            {
                                text: '<b>LOUVER</b>',
                                dataIndex: 'lv',
                                align: 'center',
                                renderer: 'renderPlanColumn'
                            },
                            {
                                text: '<b>OTHER</b>',
                                dataIndex: 'other',
                                align: 'center',
                                renderer: 'renderPlanColumn'
                            }
                        ]
                    }
                }]
            }
        ];
        me.callParent();
        me.reloadGrid();
    },
    createEachDayStoreData: function(data, year, month) {
        var me = this;
        var eachDayStoreData = [];
        var list_month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var day = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28'];
        if (((year % 4) == 0) && (month == 2)) {
            day.push('29');
        } else if ([1, 3, 5, 7, 8, 10, 12].indexOf(month) != -1) {
            day.push('29');
            day.push('30');
            day.push('31');
        } else if ([4, 6, 9, 11].indexOf(month) != -1) {
            day.push('29');
            day.push('30');
        }
        for (i = 0; i < day.length; i++) {
            var month2 = Array(2 - Math.floor(Math.log10(month))).join('0') + month;
            date = year + '-' + month2 + '-' + day[i];
            var exist = false;
            for (j = 0; j < data.length; j++) {
                if (data[j].date == date) {
                    exist = true;
                    eachDayStoreData.push(data[j]);
                }
            }
            if (!exist) {
                eachDayStoreData.push({
                    "id": null,
                    "date": date,
                    "frame_sd": null,
                    "frame_lsd": null,
                    "leaf_sd": null,
                    "leaf_lsd": null,
                    "ss": null,
                    "ss_m2": null,
                    "qs": null,
                    "frame_sp": null,
                    "leaf_sp": null,
                    "frame_sld": null,
                    "leaf_sld": null,
                    "lv": null,
                    "other": null
                });
            }
        }
        me.getViewModel().set('eachDayStoreData', eachDayStoreData);
        return eachDayStoreData
    },
    createCummulativeTotalStoreData: function(data, plan, year, month) {
        var me = this;
        var cummulativeTotalStoreData = [];
        var total = data[0];
        cummulativeTotalStoreData.push(data[0]);
        for (i = 1; i < data.length; i++) {
            total = {
                "id": null,
                "date": data[i].date,
                "frame_sd": me.summaryValue(total.frame_sd, data[i].frame_sd),
                "frame_lsd": me.summaryValue(total.frame_lsd, data[i].frame_lsd),
                "leaf_sd": me.summaryValue(total.leaf_sd, data[i].leaf_sd),
                "leaf_lsd": me.summaryValue(total.leaf_lsd, data[i].leaf_lsd),
                "ss": me.summaryValue(total.ss, data[i].ss),
                "ss_m2": me.summaryValue(total.ss_m2, data[i].ss_m2),
                "qs": me.summaryValue(total.qs, data[i].qs),
                "frame_sp": me.summaryValue(total.frame_sp, data[i].frame_sp),
                "leaf_sp": me.summaryValue(total.leaf_sp, data[i].leaf_sp),
                "frame_sld": me.summaryValue(total.frame_sld, data[i].frame_sld),
                "leaf_sld": me.summaryValue(total.leaf_sld, data[i].leaf_sld),
                "lv": me.summaryValue(total.lv, data[i].lv),
                "other": me.summaryValue(total.other, data[i].other),
            };
            var arr = total;
            cummulativeTotalStoreData.push(arr);
        }
        var planData = {};
        for (i = 0; i < plan.length; i++) {
            planData[plan[i].product_code] = plan[i].plan_num;
        }
        if (plan.length) {
            if (plan[0].month == month) {
                cummulativeTotalStoreData.push({
                    "id": null,
                    "date": 'Plan',
                    "year": plan[0].year,
                    "month": plan[0].month,
                    //"frame": planData.frame,
                    "frame_sd": me.summaryValue(planData.frame_sd, planData.frame_lsd),
                    "frame_lsd": me.summaryValue(planData.frame_sd, planData.frame_lsd),
                    "leaf_sd": me.summaryValue(planData.leaf_sd, planData.leaf_lsd),
                    "leaf_lsd": me.summaryValue(planData.leaf_sd, planData.leaf_lsd),
                    "ss": planData.ss,
                    "ss_m2": planData.ss_m2,
                    "qs": planData.qs,
                    "frame_sp": planData.frame_sp,
                    "leaf_sp": planData.leaf_sp,
                    "frame_sld": planData.frame_sld,
                    "leaf_sld": planData.leaf_sld,
                    "lv": planData.lv,
                    "other": planData.other,
                });
            } else {
                cummulativeTotalStoreData.push({
                    "id": null,
                    "date": 'Plan',
                    "year": year,
                    "month": month,
                    //"frame": null,
                    "frame_sd": null,
                    "frame_lsd": null,
                    "leaf_sd": null,
                    "leaf_lsd": null,
                    "ss": null,
                    "ss_m2": null,
                    "qs": null,
                    "frame_sp": null,
                    "leaf_sp": null,
                    "frame_sld": null,
                    "leaf_sld": null,
                    "lv": null,
                    "other": null,
                });
            }
        } else {
            cummulativeTotalStoreData.push({
                "id": null,
                "date": 'Plan',
                "year": year,
                "month": month,
                //"frame": null,
                "frame_sd": null,
                "frame_lsd": null,
                "leaf_sd": null,
                "leaf_lsd": null,
                "ss": null,
                "ss_m2": null,
                "qs": null,
                "frame_sp": null,
                "leaf_sp": null,
                "frame_sld": null,
                "leaf_sld": null,
                "lv": null,
                "other": null,
            });
        }
        me.getViewModel().set('cummulativeTotalStoreData', cummulativeTotalStoreData);
        return cummulativeTotalStoreData
    },
    summaryValue: function(a, b) {
        if (a == null) {
            return b
        } else if (b == null) {
            return a
        } else {
            return (parseInt(a) + parseInt(b)).toString()
        }
    },
    createUsQuantityStoreData: function(data) {
        var me = this;
        Ext.Ajax.request({
            url: 'api/product',
            success: function(response) {
                var res = Ext.decode(response.responseText);
                var priceData = {};
                for (i = 0; i < res.data.length; i++) {
                    priceData[res.data[i].code] = res.data[i].price
                }
                me.getViewModel().set('priceData', priceData)
                var usQuantityStoreData = [];
                var day_number = data.length - 1;
                var plan = {
                    "id": data[day_number].id,
                    "date": data[day_number].date,
                    "frame_sd": me.multipleValue(data[day_number].frame_sd, priceData.frame_sd),
                    "frame_lsd": me.multipleValue(data[day_number].frame_lsd, priceData.frame_lsd),
                    "leaf_sd": me.multipleValue(data[day_number].leaf_sd, priceData.leaf_sd),
                    "leaf_lsd": me.multipleValue(data[day_number].leaf_lsd, priceData.leaf_lsd),
                    "ss": me.multipleValue(data[day_number].ss, priceData.ss),
                    "qs": me.multipleValue(data[day_number].qs, priceData.qs),
                    "frame_sp": me.multipleValue(data[day_number].frame_sp, priceData.frame_sp),
                    "leaf_sp": me.multipleValue(data[day_number].leaf_sp, priceData.leaf_sp),
                    "frame_sld": me.multipleValue(data[day_number].frame_sld, priceData.frame_sld),
                    "leaf_sld": me.multipleValue(data[day_number].leaf_sld, priceData.leaf_sld),
                    "lv": me.multipleValue(data[day_number].lv, priceData.lv),
                    "other": me.multipleValue(data[day_number].other, priceData.other),
                };
                var total = parseFloat(me.getTotalValue(plan));
                var step = total / day_number;
                for (i = 0; i < data.length; i++) {
                    var record = {
                        "id": data[i].id,
                        "date": data[i].date,
                        "frame_sd": me.multipleValue(data[i].frame_sd, priceData.frame_sd),
                        "frame_lsd": me.multipleValue(data[i].frame_lsd, priceData.frame_lsd),
                        "leaf_sd": me.multipleValue(data[i].leaf_sd, priceData.leaf_sd),
                        "leaf_lsd": me.multipleValue(data[i].leaf_lsd, priceData.leaf_lsd),
                        "ss": me.multipleValue(data[i].ss, priceData.ss),
                        "qs": me.multipleValue(data[i].qs, priceData.qs),
                        "frame_sp": me.multipleValue(data[i].frame_sp, priceData.frame_sp),
                        "leaf_sp": me.multipleValue(data[i].leaf_sp, priceData.leaf_sp),
                        "frame_sld": me.multipleValue(data[i].frame_sld, priceData.frame_sld),
                        "leaf_sld": me.multipleValue(data[i].leaf_sld, priceData.leaf_sld),
                        "lv": me.multipleValue(data[i].lv, priceData.lv),
                        "other": me.multipleValue(data[i].other, priceData.other),
                    };
                    record["total"] = me.getTotalValue(record);
                    record["plan"] = parseFloat(step * (i + 1)).toFixed(1);
                    record["ratio"] = parseInt(parseFloat(record["total"]) * 100 / record["plan"]);
                    usQuantityStoreData.push(record);
                }
                me.getViewModel().set('usQuantityStoreData', usQuantityStoreData);
                me.getViewModel().getStore('usQuantityStore').loadData(usQuantityStoreData, false);
            },
            failure: function(response) {

            }
        });
    },
    multipleValue: function(a, b) {
        if (a == null) {
            return null
        } else {
            return (parseInt(a) * parseFloat(b)).toString()
        }
    },
    getValue: function(a) {
        if (a == null) {
            return 0
        } else {
            return parseFloat(a)
        }
    },
    getTotalValue: function(record) {
        var me = this;
        return me.getValue(record.frame_sd) + me.getValue(record.frame_lsd) + me.getValue(record.leaf_sd) + me.getValue(record.leaf_lsd) + me.getValue(record.ss) + me.getValue(record.qs) + me.getValue(record.frame_sp) + me.getValue(record.leaf_sp) + me.getValue(record.frame_sld) + me.getValue(record.leaf_sld) + me.getValue(record.lv) + me.getValue(record.other)
    },
    reloadGrid: function() {
        var me = this;
        var current_year = (new Date()).getFullYear();
        var current_month = (new Date()).getMonth() + 1;
        var refs = me.getReferences();
        var year = refs.yearCmb.getValue() ? refs.yearCmb.getValue() : current_year;
        var month = refs.monthCmb.getValue() ? refs.monthCmb.getValue() : current_month;
        var start = Ext.Date.format(refs.startdate.getValue(), 'Y-m-d');
        var end = Ext.Date.format(refs.enddate.getValue(), 'Y-m-d');
        Ext.Ajax.request({
            url: 'api/production-schedule/report',
            params: {
                start: start,
                end: end
            },
            method: 'GET',
            success: function(response) {
                var res = Ext.decode(response.responseText);
                var eachDayStoreData = me.createEachDayStoreData(res.data.each_day, year, month);
                var cummulativeTotalStoreData = me.createCummulativeTotalStoreData(eachDayStoreData, res.data.plan, year, month);
                var usQuantityStoreData = me.createUsQuantityStoreData(cummulativeTotalStoreData);

                me.getViewModel().getStore('eachDayStore').loadData(eachDayStoreData, false);
                me.getViewModel().getStore('cummulativeTotalStore').loadData(cummulativeTotalStoreData, false);
            },
            failure: function(response) {

            }
        });
    }

});