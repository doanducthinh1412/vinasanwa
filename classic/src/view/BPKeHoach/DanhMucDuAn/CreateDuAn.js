Ext.define('CMS.view.BPKeHoach.CreateDuAn.CreateDuAn', {
    extend: 'Ext.form.Panel',
    requires: [

    ],
    controller: 'CreateDuAn',
    xtype: 'CreateDuAn',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        gridPanel: null,
        storeParent: null,
        relationComponent: null,
        relationData: null
    },
    reference: 'CreateDuAnForm',
    autoScroll: false,
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'fieldset',
            title: 'Thông tin dự án',
            fieldDefaults: {
                msgTarget: 'under',
                labelAlign: 'left',
                autoFitErrors: true,
                labelStyle: 'font-weight:bold',
                width: '100%',
                labelWidth: 200,
            },
            defaultType: 'textfield',
            defaults: {
                allowBlank: true
            },
            items: [{
                fieldLabel: 'Tên dự án',
                name: 'name',
                allowBlank: false,
                blankText: 'Trường bắt buộc nhập thông tin',
                afterLabelTextTpl: [
                    '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                ],
                value: me.getRelationData() ? me.getRelationData().name : ""
            }, {
                fieldLabel: 'Mã dự án',
                name: 'code',
                allowBlank: false,
                blankText: 'Trường bắt buộc nhập thông tin',
                afterLabelTextTpl: [
                    '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                ],
                value: me.getRelationData() ? me.getRelationData().code : ""
            }, {
                fieldLabel: 'Người quản lý dự án',
                name: 'incharge'
            }, {
                fieldLabel: 'Địa chỉ khách hàng',
                name: 'cus_address'
            }, {
                fieldLabel: 'Tên khách hàng',
                name: 'cus_name'
            }, {
                fieldLabel: 'Người liên hệ khách hàng',
                name: 'cus_incharge'
            }, {
                fieldLabel: 'Số điện thoại khách hàng',
                name: 'cus_phone_number'
            }, {
                fieldLabel: 'Email khách hàng',
                name: 'cus_email',
                vtype: 'email'
            }]
        }];
        me.buttons = ['->', {
            text: 'Tạo mới',
            glyph: 'xf00c@FontAwesome',
            ui: 'soft-orange',
            handler: function() {
                me.getController().createProject();
            },
            formBind: true
        }, {
            text: 'Hủy bỏ',
            glyph: 'xf00d@FontAwesome',
            handler: function() {
                this.up('window').close();
            }
        }, '->'];
        me.callParent();
    },


});