Ext.define('CMS.view.BPKeHoach.EditDuAn.EditDuAn', {
    extend: 'Ext.form.Panel',
    requires: [

    ],
    controller: 'EditDuAn',
    //viewModel: 'EditDuAn',
    xtype: 'EditDuAn',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        rec: null,
        gridPanel: null
    },
    autoScroll: false,
    initComponent: function() {
        var me = this;
        var rec = this.getRec();
        me.items = [{
            xtype: 'fieldset',
            title: 'Thông tin dự án',
            fieldDefaults: {
                msgTarget: 'under',
                labelAlign: 'left',
                autoFitErrors: true,
                labelStyle: 'font-weight:bold',
                width: '100%',
                labelWidth: 200,
            },
            defaultType: 'textfield',
            defaults: {
                allowBlank: false
            },
            items: [{
                fieldLabel: 'Tên dự án',
                name: 'name',
                value: rec.get('name')
            }, {
                fieldLabel: 'Mã dự án',
                name: 'code',
                value: rec.get('code')
            }, {
                fieldLabel: 'Người quản lý dự án',
                name: 'incharge',
                value: rec.get('incharge')
            }, {
                fieldLabel: 'Địa chỉ khách hàng',
                name: 'cus_address',
                value: rec.get('cus_address')
            }, {
                fieldLabel: 'Tên khách hàng',
                name: 'cus_name',
                value: rec.get('cus_name')
            }, {
                fieldLabel: 'Người liên hệ khách hàng',
                name: 'cus_incharge',
                value: rec.get('cus_incharge')
            }, {
                fieldLabel: 'Số điện thoại khách hàng',
                name: 'cus_phone_number',
                value: rec.get('cus_phone_number')
            }, {
                fieldLabel: 'Email khách hàng',
                name: 'cus_email',
                value: rec.get('cus_email'),
                vtype: 'email'
            }, ]
        }];
        me.buttons = ['->', {
            text: 'Lưu lại',
            glyph: 'xf00c@FontAwesome',
            ui: 'soft-orange',
            hidden: ClassMain.checkPermission('quan_ly_du_an', 'Project', 'edit') === true ? false : true,
            handler: function() {
                me.getController().editProject();
            },
            formBind: true
        }, {
            text: 'Hủy bỏ',
            glyph: 'xf00d@FontAwesome',
            handler: function() {
                this.up('window').close();
            }
        }, '->'];
        me.callParent();
    },


});