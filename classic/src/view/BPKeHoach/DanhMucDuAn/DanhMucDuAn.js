Ext.define('CMS.view.BPKeHoach.DanhMucDuAn.DanhMucDuAn', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.Panel'
    ],
    controller: 'DanhMucDuAn',
    viewModel: 'DanhMucDuAn',
    xtype: 'DanhMucDuAn',
    layout: 'fit',
    bind: {
        store: '{danhMucDuAnStore}'
    },
    reference: 'DanhMucDuAnGrid',
    columnLines: true,
    autoScroll: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    config: {
        filterString: null
    },
    initComponent: function() {
        var me = this;
        var filterString = me.getFilterString();
        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            hidden: (filterString) ? true : false,
            items: [{
                    xtype: 'button',
                    text: 'Tạo mới',
                    iconCls: 'fas fa-plus-circle',
                    ui: 'soft-orange',
                    hidden: ClassMain.checkPermission('quan_ly_du_an', 'Project', 'create') === true ? false : true,
                    handler: 'onCreateButtonClick'
                },
                '-',
                {
                    text: 'Thao tác',
                    iconCls: 'fas fa-bars',
                    ui: 'dark-blue',
                    disabled: true,
                    bind: {
                        disabled: '{!DanhMucDuAnGrid.selection}',
                    },
                    menu: me.buildMenu()
                },
                '->',
                {
                    xtype: 'textfield',
                    emptyText: 'Nhập tìm kiếm',
                    width: 250,
                    triggers: {
                        clear: {
                            weight: 0,
                            cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                            hidden: true,
                            handler: 'onClearClick',
                            scope: 'this'
                        },
                        search: {
                            weight: 1,
                            cls: Ext.baseCSSPrefix + 'form-search-trigger',
                            handler: 'onSearchClick',
                            scope: 'this'
                        }
                    },
                    onClearClick: function() {
                        this.setValue('');
                        this.getTrigger('clear').hide();
                        this.updateLayout();
                        me.getStore().clearFilter();
                    },
                    onSearchClick: function() {
                        var value = this.getValue();
                        if (value.length > 0) {
                            this.getTrigger('clear').show();
                            me.getStore().clearFilter();
                            me.getStore().filterBy(function(rec, id) {
                                var name = rec.get('name');
                                name = name.toLowerCase();
                                value = value.toLowerCase();
                                if (name.indexOf(value) != -1) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                        } else {
                            this.getTrigger('clear').hide();
                            me.getStore().clearFilter();

                        }
                    },
                    listeners: {
                        'specialkey': function(f, e) {
                            if (e.getKey() == e.ENTER) {
                                this.onSearchClick();
                            }
                        }
                    }
                },
                /*{
                	xtype: 'button',
                	text: 'Lọc theo',
                	iconCls: 'fas fa-filter',
                	ui: 'dark-blue',
                	
                }, 
                */
                {
                    text: 'Công cụ',
                    iconCls: 'fas fa-briefcase',
                    ui: 'dark-blue',
                    menu: {
                        xtype: 'menu',
                        plain: true,
                        mouseLeaveDelay: 10,
                        items: [{
                            text: 'In ấn',
                            iconCls: 'fas fa-print text-dark-blue',
                            handler: 'onPrintButtonClick'
                        }, {
                            text: 'Xuất Excel',
                            iconCls: 'fas fa-file-export text-dark-blue',
                            handler: 'onExportdButtonClick'
                        }, '-', {
                            text: 'Làm mới danh sách',
                            iconCls: 'fas fa-sync-alt text-dark-blue',
                            handler: 'onReloadButtonClick'
                        }]
                    }
                }
            ]
        }];

        me.columns = [{
                xtype: 'rownumberer',
                text: '<b>TT</b>',
                width: 65,
                align: 'center'
            },
            {
                text: '<b>Tên dự án</b>',
                dataIndex: 'name',
                width: 250,
                align: 'center',
                renderer: function(value, meta, rec) {

                    return '<b>' + value + '</b>';
                }
            },
            {
                text: '<b>Mã dự án</b>',
                dataIndex: 'code',
                flex: 1,
                align: 'center',
            },
            {
                text: '<b>Người quản lý dự án</b>',
                dataIndex: 'incharge',
                flex: 1,
                align: 'center',
            },
            {
                text: '<b>Địa chỉ khách hàng</b>',
                dataIndex: 'cus_address',
                flex: 1,
                align: 'center',
            },
            {
                text: '<b>Tên khách hàng</b>',
                dataIndex: 'cus_name',
                flex: 1,
                align: 'center',
            },
            {
                text: '<b>Người liên hệ khách hàng</b>',
                dataIndex: 'cus_incharge',
                flex: 1,
                align: 'center',
            },
            {
                text: '<b>Số điện thoại khách hàng</b>',
                dataIndex: 'cus_phone_number',
                flex: 1,
                align: 'center',
            },
            {
                text: '<b>Email khách hàng</b>',
                dataIndex: 'cus_email',
                flex: 1,
                align: 'center',
            }, {
                text: '<b>Thao tác</b>',
                xtype: 'actioncolumn',
                width: 100,
                sortable: false,
                menuDisabled: true,
                align: 'center',
                items: [{
                    iconCls: 'fas fa-info-circle text-blue',
                    tooltip: 'Thông tin chi tiết',
                    handler: 'onEditButtonClickGrid'
                }, {
                    width: 10,
                    hidden: ClassMain.checkPermission('quan_ly_du_an', 'Project', 'delete') === true ? false : true
                }, {
                    iconCls: 'fas fa-trash-alt text-blue',
                    tooltip: 'Xóa dự án',
                    hidden: ClassMain.checkPermission('quan_ly_du_an', 'Project', 'delete') === true ? false : true,
                    handler: 'onDeleteButtonClickGrid'
                }]
            }
        ];
        me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true
        };
        me.menu = me.buildMenu();
        me.getViewModel().getStore('danhMucDuAnStore').filter('deletedAt', null);
        if (filterString) {
            me.getViewModel().getStore('danhMucDuAnStore').filterBy(function(rec, id) {
                var name = rec.get('name');
                name = name.toLowerCase();
                filterString = filterString.toLowerCase();
                if (name.indexOf(filterString) != -1) {
                    return true;
                } else {
                    return false;
                }
            });
        }
        me.callParent();
    },
    buildMenu: function() {
        var controller = this.getController();
        return Ext.create('Ext.menu.Menu', {
            items: [{
                    text: 'Thông tin chi tiết',
                    iconCls: 'fas fa-info-circle text-dark-blue',
                    handler: function() {
                        controller.onEditButtonClick();
                    }
                },
                /*{
                    text: 'Sửa dự án',
                    iconCls: 'fas fa-edit text-dark-blue',
                    handler: function() {
                        controller.onEditButtonClick();
                    }
                },*/
                {
                    text: 'Xóa dự án',
                    iconCls: 'fas fa-trash-alt text-dark-blue',
                    hidden: ClassMain.checkPermission('quan_ly_du_an', 'Project', 'delete') === true ? false : true,
                    handler: function() {
                        controller.onDeleteButtonClick();
                    }
                }, '-', {
                    text: 'Xem danh sách OrderSheet',
                    iconCls: 'fas fa-clipboard-list text-dark-blue',
                    handler: function() {
                        controller.onAddUserButtonClick();
                    }
                }
            ]
        });
    },
    listeners: {
        'itemcontextmenu': 'onItemContextMenu',
        'itemdblclick': 'onEditButtonClick',
    },

    reloadView: function() {
        var me = this;
        me.el.mask('Đang tải...');
        var filterString = me.getFilterString();
        me.getViewModel().getStore('danhMucDuAnStore').clearFilter();
        me.getViewModel().getStore('danhMucDuAnStore').filter('deletedAt', null);
        if (filterString) {
            me.getViewModel().getStore('danhMucDuAnStore').filterBy(function(rec, id) {
                var name = rec.get('name');
                name = name.toLowerCase();
                filterString = filterString.toLowerCase();
                if (name.indexOf(filterString) != -1) {
                    return true;
                } else {
                    return false;
                }
            });
        }
        me.el.unmask();
    }
});