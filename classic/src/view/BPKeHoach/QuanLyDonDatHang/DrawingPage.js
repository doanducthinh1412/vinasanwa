Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.ViewDrawingPage', {
    extend: 'Ext.panel.Panel',
    controller: 'ViewDrawingPage',
    viewModel: 'ViewDrawingPage',
    xtype: 'ViewDrawingPage',
    initComponent: function() {
        var me = this;
        me.items = [];
        me.callParents();
    }
});