Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.SMDetailPage', {
    extend: 'Ext.grid.Panel',
    xtype: 'SMDetailPage',
    bodyBorder: true,
    autoScroll: true,
    layout: 'fit',
    config: {
        dataPanel: null
    },
    bufferedRenderer: false,
    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true
    },
    columnLines: true,
    selModel: {
        type: 'spreadsheet',
        columnSelect: true,
        pruneRemoved: false,
        extensible: 'y'
    },
    forceFit: true,
    viewConfig: {
        columnLines: true,
        trackOver: false
    },
    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
    columns: [{
        text: 'ORDER SHEET',
        columns: [{
            text: 'NO',
            width: 80,
            dataIndex: 'no',
            align: 'center',
            editor: false
        }, {
            text: 'SUB NO',
            width: 120,
            dataIndex: 'sub_no',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'MARK',
        columns: [{
            width: 120,
            text: 'NAME',
            dataIndex: 'mark',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'FRAME',
        columns: [{
            text: 'W',
            columns: [{
                text: 'mm',
                width: 80,
                dataIndex: 'f_width',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'H',
            columns: [{
                text: 'mm',
                width: 80,
                dataIndex: 'f_height',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'DEPTH',
            columns: [{
                text: 'mm',
                width: 80,
                dataIndex: 'f_depth',
                align: 'center',
                editor: false
            }]
        }]
    }, {
        text: 'QUANTITY',
        columns: [{
            text: 'LH',
            columns: [{
                text: 'LHA',
                width: 80,
                dataIndex: 'q_lh',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'RH',
            columns: [{
                text: 'RHA',
                width: 80,
                dataIndex: 'q_rh',
                align: 'center',
                editor: false
            }]
        }]
    }, {
        text: 'QUANTITY OF DOOR LEAF',
        columns: [{
            text: 'QUANTITY PER SET',
            width: 100,
            dataIndex: 'qodl_quantity_per_set',
            align: 'center',
            editor: false
        }, {
            text: 'DOOR TYPE',
            width: 100,
            dataIndex: 'qodl_door_type',
            align: 'center',
            editor: false
        }, {
            text: 'TOTAL',
            width: 100,
            dataIndex: 'qodl_total',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'FRAME',
        columns: [{
            text: 'KIND OF',
            columns: [{
                text: 'FRAME JOINT',
                width: 100,
                dataIndex: 'df_kind_of_frame_joint',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'MATERIAL',
            width: 100,
            dataIndex: 'df_material',
            align: 'center',
            editor: false
        }, {
            text: 'SHEET',
            columns: [{
                text: 'THICKNESS',
                width: 100,
                dataIndex: 'df_sheet_thickness',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'PAINT COLOR',
            columns: [{
                text: 'PRIMER PAINT',
                width: 80,
                dataIndex: 'f_primer_paint',
                align: 'center',
                editor: false
            }, {
                text: 'FINISH PAINT',
                width: 80,
                dataIndex: 'f_finish_paint',
                align: 'center',
                editor: false
            }]
        }]
    }, {
        text: 'DOOR LEAF',
        columns: [{
            text: 'DOOR THICKNESS',
            width: 100,
            dataIndex: 'dl_door_thickness',
            align: 'center',
            editor: false
        }, {
            text: 'MATERIAL',
            width: 100,
            dataIndex: 'dl_material',
            align: 'center',
            editor: false
        }, {
            text: 'SHEET THICKNESS',
            width: 100,
            dataIndex: 'dl_sheet_thickness',
            align: 'center',
            editor: false
        }, {
            text: 'PAINT COLOR',
            columns: [{
                text: 'PRIMER PAINT',
                width: 80,
                dataIndex: 'dl_primer_paint',
                align: 'center',
                editor: false
            }, {
                text: 'FINISH PAINT',
                width: 80,
                dataIndex: 'dl_finish_paint',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'HONEY CORE',
            width: 100,
            dataIndex: 'dl_honey_core',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'RAIL',
        columns: [{
            text: 'SWSL-1-WA<br>L=2500/3100',
            columns: [{
                hideHeader: true,
                dataIndex: "rail_length",
                editor: false
            }, {
                hideHeader: true,
                dataIndex: "rail_qty",
                editor: false
            }]
        }]
    }, {
        text: 'RAIL SCREW',
        columns: [{
            text: 'SWSL-1-WA2',
            columns: [{
                text: 'M5x14, 9PCS',
                dataIndex: "rail_crew",
                editor: false
            }]
        }]

    }, {
        text: 'RACK UNIT',
        columns: [{
            text: 'SWSL-1-WA3',
            columns: [{
                text: 'M5x14, 9PCS',
                dataIndex: "rack_unit",
                editor: false
            }]
        }]
    }, {
        text: 'HANGER ROLLER',
        columns: [{
            text: 'SWSL-1-TO',
            columns: [{
                text: '2PCS/LEAF/SET',
                dataIndex: "hanger_roller",
                editor: false
            }]
        }]
    }, {
        text: 'DOOR END STOP RUBBER',
        columns: [{
            text: 'TAG30',
            columns: [{
                text: '2PCS/LEAF',
                dataIndex: "door_end_stop_rubber",
                editor: false
            }]
        }]
    }, {
        text: 'FRONT EDGE RUBBER (M)',
        columns: [{
            text: 'TK-A',
            columns: [{
                text: 'L=2200',
                dataIndex: "front_edge_rubber_m",
                editor: false
            }]
        }]
    }, {
        text: 'FRONT EDGE RUBBER (F)',
        columns: [{
            text: 'TR-B',
            columns: [{
                text: 'L=2200',
                dataIndex: "front_edge_rubber_f",
                editor: false
            }]
        }]
    }, {
        text: 'PINCH LESS RUBBER',
        columns: [{
            text: 'TY-A',
            columns: [{
                text: 'L=2200',
                dataIndex: "pinch_less_rubber",
                editor: false
            }]
        }]
    }, {
        text: 'PINCH LESS RUBBER PLATE',
        columns: [{
            text: 'TY-B',
            columns: [{
                text: 'L=2200',
                dataIndex: "pinch_less_rubber_plate",
                editor: false
            }]
        }]
    }, {
        text: 'GUARD RUBBER A',
        columns: [{
            text: 'GG-A',
            columns: [{
                text: '1PC FOR TR-A',
                dataIndex: "guard_rubber_a",
                editor: false
            }]
        }]
    }, {
        text: 'GUARD RUBBER B',
        columns: [{
            text: 'GG-B',
            columns: [{
                text: '1PC FOR TR-B',
                dataIndex: "guard_rubber_b",
                editor: false
            }]
        }]
    }, {
        text: 'BRAKING UNIT',
        columns: [{
            text: 'SWSL-1-GE',
            columns: [{
                text: '1SET/LEAF',
                dataIndex: "braking_unit",
                editor: false
            }]
        }]
    }, {
        text: 'SPECIAL BRAKING UNIT FOR OVER SIDE',
        columns: [{
            text: 'SWSL-2W-GE2G',
            columns: [{
                text: '1 SET/LEAF',
                dataIndex: "special_braking_unit",
                editor: false
            }]
        }]
    }, {
        text: 'SPACER',
        columns: [{
            text: 'SWSL-1-TOSP',
            columns: [{
                text: '30PCS/LEAF',
                dataIndex: "spacer",
                editor: false
            }]
        }]
    }, {
        text: 'END STOPPER',
        columns: [{
            text: 'SWST-01',
            columns: [{
                text: '1SET/LEAF',
                dataIndex: "end_stopper",
                editor: false
            }]
        }]
    }, {
        text: 'FLOOR GUIDE',
        columns: [{
            text: 'SWGT-20V2',
            columns: [{
                text: '1SET/LEAF',
                dataIndex: "floor_guide",
                editor: false
            }]
        }]
    }, {
        text: 'YURIA SCREW',
        columns: [{
            text: 'M5x8',
            columns: [{
                text: '2PCS/LEAF',
                dataIndex: "yuria_crew",
                editor: false
            }]
        }]
    }, {
        text: 'PULL HANDLE',
        columns: [{
            text: 'UNION',
            columns: [{
                text: 'L= 452',
                dataIndex: "pull_handle",
                editor: false
            }]
        }]
    }, {
        text: 'LOCK SET',
        columns: [{
            text: 'KIND OF LOCK SET',
            width: 100,
            dataIndex: 'ls_lock_set_body',
            align: 'center',
            editor: false
        }, {
            text: 'BS',
            width: 80,
            dataIndex: 'ls_bs',
            align: 'center',
            editor: false
        }, {
            text: 'DT',
            width: 80,
            dataIndex: 'ls_dt',
            align: 'center',
            editor: false
        }, {
            text: 'QTY',
            width: 80,
            dataIndex: 'ls_qty',
            align: 'center',
            editor: false
        }, {
            text: 'MASTER KEY',
            width: 80,
            dataIndex: 'ls_master_key',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'LOUVER',
        columns: [{
            text: 'KIND OF LOUVER',
            width: 100,
            dataIndex: 'l_kind_of_louver',
            align: 'center',
            editor: false
        }, {
            text: 'W(mm)',
            width: 80,
            dataIndex: 'l_width',
            align: 'center',
            editor: false
        }, {
            text: 'H(mm)',
            width: 80,
            dataIndex: 'l_height',
            align: 'center',
            editor: false
        }, {
            text: 'QTy.',
            width: 80,
            dataIndex: 'l_qty',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'GLASS WINDOW',
        columns: [{
            text: 'KIND OF GLASS',
            width: 100,
            dataIndex: 'gw_kind_of_glass',
            align: 'center',
            editor: false
        }, {
            text: 'W(mm)',
            width: 80,
            dataIndex: 'gw_width',
            align: 'center',
            editor: false
        }, {
            text: 'H(mm)',
            width: 80,
            dataIndex: 'gw_height',
            align: 'center',
            editor: false
        }, {
            text: 'QTy.',
            width: 80,
            dataIndex: 'gw_qty',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'KICK PLATE',
        columns: [{
            text: 'METERIAL',
            width: 100,
            dataIndex: 'kp_meterial',
            align: 'center',
            editor: false
        }, {
            text: 'W(mm)',
            width: 80,
            dataIndex: 'kp_width',
            align: 'center',
            editor: false
        }, {
            text: 'H(mm)',
            width: 80,
            dataIndex: 'kp_height',
            align: 'center',
            editor: false
        }, {
            text: 'QTy.',
            width: 80,
            dataIndex: 'kp_qty',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'SILICON',
        columns: [{
            text: 'KIND OF SILICON',
            width: 100,
            dataIndex: 's_kind_of_silicon',
            align: 'center',
            editor: false
        }, {
            text: 'COLOR',
            width: 80,
            dataIndex: 's_color',
            align: 'center',
            editor: false
        }, {
            text: 'BOTTLE',
            width: 80,
            dataIndex: 's_bottle',
            align: 'center',
            editor: false
        }, {
            text: 'QTY',
            width: 80,
            dataIndex: 's_qty',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'WELDING ROD',
        columns: [{
            text: 'KIND',
            width: 100,
            dataIndex: 'wr_kind_of_welding_rod',
            align: 'center',
            editor: false
        }, {
            text: 'UNIT',
            width: 80,
            dataIndex: 'wr_unit',
            align: 'center',
            editor: false
        }, {
            text: 'QTY',
            width: 80,
            dataIndex: 'wr_qty',
            align: 'center',
            editor: false
        }]
    }, {
        text: 'ANCHOR BOLT',
        columns: [{
            text: 'KIND',
            width: 100,
            dataIndex: 'ab_kind_of_anchor_bolt',
            align: 'center',
            editor: false
        }, {
            text: 'UNIT',
            width: 80,
            dataIndex: 'ab_unit',
            align: 'center',
            editor: false
        }, {
            text: 'QTY',
            width: 80,
            dataIndex: 'ab_qty',
            align: 'center',
            editor: false
        }]
    }],
    initComponent: function() {
        var me = this;
        var fields = ["no", "sub_no", "mark", "f_width", "f_height", "f_depth", "q_lh", "q_rh", "qodl_quantity_per_set", "qodl_door_type", "qodl_total", "f_kind_of_frame_joint", "f_material", "f_sheet_thickness", "f_primer_paint", "f_finish_paint", "dl_door_thickness", "dl_material", "dl_sheet_thickness", "dl_primer_paint", "dl_finish_paint", "dl_honey_core", "rail_length", "rail_qty", "rail_crew", "rack_unit", "hanger_roller", "door_end_stop_rubber", "front_edge_rubber_m", "front_edge_rubber_f", "pinch_less_rubber", "pinch_less_rubber_plate", "guard_rubber_a", "guard_rubber_b", "braking_unit", "special_braking_unit", "spacer", "end_stopper", "floor_guide", "yuria_crew", "pull_handle", "ls_kind_of_lock_set", "ls_bs", "ls_dt", "ls_qty", "ls_master_key", "l_kind_of_louver", "l_width", "l_height", "l_qty", "gw_kind_of_glass", "gw_width", "gw_height", "gw_qty", "kp_meterial", "kp_width", "kp_height", "kp_qty", "s_kind_of_silicon", "s_color", "s_bottle", "ds_unit", "wr_kind", "wr_unit", "wr_qty", "ab_kind", "ab_unit", "ab_qty"];
        me.store = {
            fields: fields,
            autoLoad: true,
            data: me.getDataPanel()
        };
        me.callParent();
    }
});