Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.LVDetailPage', {
    extend: 'Ext.grid.Panel',
    xtype: 'LVDetailPage',
    bodyBorder: true,
    autoScroll: true,
    config: {
        dataPanel: null
    },
    bufferedRenderer: false,
    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true
    },
    columnLines: true,
    selModel: {
        type: 'spreadsheet',
        columnSelect: true,
        pruneRemoved: false,
        extensible: 'y'
    },
    forceFit: true,
    viewConfig: {
        columnLines: true,
        trackOver: false
    },
    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
    columns: [{
        text: 'ORDER SHEET',
        columns: [{
            text: 'NO',
            width: 80,
            dataIndex: 'no',
            align: 'center',
            editor: false
                /*editor: {
                    allowBlank: true
                }*/
        }, {
            text: 'SUB NO',
            width: 80,
            dataIndex: 'sub_no',
            align: 'center',
            editor: false
                /*editor: {
                    allowBlank: true
                }*/
        }]
    }, {
        text: 'MARK',
        dataIndex: 'mark',
        align: 'center',
        editor: false
    }, {
        text: 'W (mm)',
        dataIndex: 'width',
        align: 'center',
        editor: false
    }, {
        text: 'H',
        dataIndex: 'height',
        align: 'center',
        editor: false
    }, {
        text: 'FRAME DEPTH',
        dataIndex: 'frame_deep',
        align: 'center',
        editor: false
    }, {
        text: 'FRAME THICKNESS',
        dataIndex: 'frame_thickness',
        align: 'center',
        editor: false
    }, {
        text: 'LOUVER PLATE THICKNESS',
        dataIndex: 'louver_plate_thickness',
        align: 'center',
        editor: false
    }, {
        text: 'PRIMER PAINT',
        dataIndex: 'primer_paint',
        align: 'center',
        editor: false
    }, {
        text: 'FINISHING PAINT',
        dataIndex: 'finishing_paint',
        align: 'center',
        editor: false
    }, {
        text: 'INSECT NET',
        dataIndex: 'insect_net',
        align: 'center',
        editor: false
    }, {
        text: 'QUANTITY ORDER',
        dataIndex: 'quantity_order',
        align: 'center',
        editor: false
    }],
    initComponent: function() {
        var me = this;
        me.store = {
            fields: ['finishing_paint', 'frame_deep', 'frame_thickness', 'height', 'insect_net', 'louver_plate_thickness', 'mark', 'no', 'primer_paint', 'quantity_order', 'sub_no', 'width'],
            autoLoad: true,
            data: me.getDataPanel()
        };
        me.callParent();
    }
});