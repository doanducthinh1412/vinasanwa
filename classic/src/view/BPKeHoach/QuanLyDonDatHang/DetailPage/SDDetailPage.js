Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.SDDetailPage', {
    extend: 'Ext.grid.Panel',
    controller: 'SDDetailPage',
    viewModel: 'SDDetailPage',
    xtype: 'SDDetailPage',
    autoScroll: true,
    layout: 'fit',
    config: {
        dataPanel: null
    },
    bufferedRenderer: false,
    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true
    },
    columnLines: true,
    selModel: {
        type: 'spreadsheet',
        columnSelect: true,
        pruneRemoved: false,
        extensible: 'y'
    },
    forceFit: true,
    viewConfig: {
        columnLines: true,
        trackOver: false
    },
    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
    columns: [{
            text: 'ORDER SHEET',
            columns: [{
                text: 'NO',
                width: 80,
                dataIndex: 'no',
                align: 'center',
                editor: false
            }, {
                text: 'SUB NO',
                width: 80,
                dataIndex: 'sub_no',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'MARK',
            columns: [{
                width: 120,
                text: 'NAME',
                dataIndex: 'mark',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'FRAME',
            columns: [{
                text: 'W',
                columns: [{
                    text: 'mm',
                    width: 80,
                    dataIndex: 'f_width',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'H',
                columns: [{
                    text: 'mm',
                    width: 80,
                    dataIndex: 'f_height',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'DEPTH',
                columns: [{
                    text: 'mm',
                    width: 80,
                    dataIndex: 'f_depth',
                    align: 'center',
                    editor: false
                }]
            }]
        }, {
            text: 'QUANTITY',
            columns: [{
                text: 'RH',
                columns: [{
                    text: 'RHA',
                    width: 80,
                    dataIndex: 'q_rh',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'LHR',
                columns: [{
                    text: 'LHRA',
                    width: 80,
                    dataIndex: 'q_lhr',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'LH',
                columns: [{
                    text: 'LHA',
                    width: 80,
                    dataIndex: 'q_lh',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'RHR',
                columns: [{
                    text: 'RHRA',
                    width: 80,
                    dataIndex: 'q_rhr',
                    align: 'center',
                    editor: false
                }]
            }]
        }, {
            text: 'QUANTITY OF DOOR LEAF',
            columns: [{
                text: 'QUANTITY PER SET',
                width: 100,
                dataIndex: 'qodl_quantity_per_set',
                align: 'center',
                editor: false
            }, {
                text: 'DOOR TYPE',
                width: 100,
                dataIndex: 'qodl_door_type',
                align: 'center',
                editor: false
            }, {
                text: 'TOTAL',
                width: 100,
                dataIndex: 'qodl_total',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'DOOR FRAME',
            columns: [{
                text: 'FRAME SHAPE',
                width: 100,
                dataIndex: 'df_frame_shape',
                align: 'center',
                editor: false
            }, {
                text: 'KIND OF',
                columns: [{
                    text: 'FRAME JOINT',
                    width: 100,
                    dataIndex: 'df_kind_of_frame_joint',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'THCKNESS OF',
                columns: [{
                    text: 'FRONT FRAME',
                    width: 100,
                    dataIndex: 'df_thickness_of_front_frame',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'MATERIAL',
                width: 100,
                dataIndex: 'df_material',
                align: 'center',
                editor: false
            }, {
                text: 'SHEET',
                columns: [{
                    text: 'THICKNESS',
                    width: 100,
                    dataIndex: 'df_sheet_thickness',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'PAINT COLOR',
                columns: [{
                    text: 'BASE PAINT',
                    width: 80,
                    dataIndex: 'df_base_paint',
                    align: 'center',
                    editor: false
                }, {
                    text: 'FINISH PAINT',
                    width: 80,
                    dataIndex: 'df_finish_paint',
                    align: 'center',
                    editor: false
                }]
            }]
        }, {
            text: 'DOOR LEAF',
            columns: [{
                text: 'DOOR THICKNESS',
                width: 100,
                dataIndex: 'dl_door_thickness',
                align: 'center',
                editor: false
            }, {
                text: 'MATERIAL',
                width: 100,
                dataIndex: 'dl_material',
                align: 'center',
                editor: false
            }, {
                text: 'SHEET THICKNESS',
                width: 100,
                dataIndex: 'dl_sheet_thickness',
                align: 'center',
                editor: false
            }, {
                text: 'PAINT COLOR',
                columns: [{
                    text: 'BASE PAINT',
                    width: 80,
                    dataIndex: 'dl_base_paint',
                    align: 'center',
                    editor: false
                }, {
                    text: 'FINISH PAINT',
                    width: 80,
                    dataIndex: 'dl_finish_paint',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'HONEY CORE',
                width: 100,
                dataIndex: 'dl_honey_core',
                align: 'center',
                editor: false
            }, {
                text: 'ROCK WOOL',
                width: 100,
                dataIndex: 'dl_rock_wool',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'HINGE',
            columns: [{
                text: 'KIND OF HINGE',
                width: 100,
                dataIndex: 'h_kind_of_hinge',
                align: 'center',
                editor: false
            }, {
                text: 'QUANTITY  PER EACH SIDE',
                width: 100,
                dataIndex: 'h_quantity_per_each_side',
                align: 'center',
                editor: false
            }, {
                text: 'QUANTITY/Each door',
                columns: [{
                    text: 'R',
                    width: 80,
                    dataIndex: 'h_quantity_each_door_r',
                    align: 'center',
                    editor: false
                }, {
                    text: 'L',
                    width: 80,
                    dataIndex: 'h_quantity_each_door_l',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'TOTAL',
                width: 100,
                dataIndex: 'h_total',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'LOCK SET',
            columns: [{
                text: 'LOCK SET BODY',
                width: 100,
                dataIndex: 'ls_lock_set_body',
                align: 'center',
                editor: false
            }, {
                text: 'LEVER HANDLE',
                width: 100,
                dataIndex: 'ls_lever_handle',
                align: 'center',
                editor: false
            }, {
                text: 'CYLINDER',
                width: 100,
                dataIndex: 'ls_cylinder',
                align: 'center',
                editor: false
            }, {
                text: 'BS',
                width: 80,
                dataIndex: 'ls_bs',
                align: 'center',
                editor: false
            }, {
                text: 'DT',
                width: 80,
                dataIndex: 'ls_dt',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'ls_qty',
                align: 'center',
                editor: false
            }]
        },
        /*{
               text: 'DOOR STOP',
               columns: [{
                   text: 'TYPE OF DOOR STOP',
                   width: 100,
                   dataIndex: 'ds_kind_of_door_stop',
                   align: 'center',
                   editor: {
                       allowBlank: true
                   }
               }, {
                   text: 'QUAN',
                   width: 100,
                   dataIndex: 'ds_quan',
                   align: 'center',
                   editor: {
                       allowBlank: true
                   }
               }]
           }, */
        {
            text: 'DOOR CLOSER',
            columns: [{
                text: 'TYPE OF CLOSER',
                width: 100,
                dataIndex: 'dc_type_of_closer',
                align: 'center',
                editor: false
            }, {
                text: 'SET',
                width: 80,
                dataIndex: 'dc_set',
                align: 'center',
                editor: false
            }, {
                text: 'RH',
                columns: [{
                    text: 'RHA',
                    width: 80,
                    dataIndex: 'dc_rh',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'LH',
                columns: [{
                    text: 'LHA',
                    width: 80,
                    dataIndex: 'dc_lh',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'TOTAL',
                width: 100,
                dataIndex: 'dc_total',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'PANIC BAR',
            columns: [{
                text: 'KIND OF PANIC BAR',
                width: 100,
                dataIndex: 'pb_kind_of_panic_bar',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'pb_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'pb_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'LEVER HANDLE',
            columns: [{
                text: 'KIND OF LEVER HANDLE',
                width: 100,
                dataIndex: 'lh_kind_of_lever_handle',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'lh_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'lh_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'PULL HANDLE',
            columns: [{
                text: 'KIND OF PULL HANDLE',
                width: 100,
                dataIndex: 'kind_of_pull_handle',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'unit_pull_handle',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'qty_pull_handle',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'PUSH HANDLE',
            columns: [{
                text: 'KIND OF PUSH HANDLE',
                width: 100,
                dataIndex: 'psh_kind_of_push_handle',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'psh_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'psh_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'DOOR EYE',
            columns: [{
                text: 'KIND OF DOOR EYE',
                width: 100,
                dataIndex: 'de_kind_of_door_eye',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'de_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'de_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'DOOR GUARD',
            columns: [{
                text: 'KIND OF DOOR GUARD',
                width: 100,
                dataIndex: 'dg_kind_of_door_guard',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'dg_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'dg_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'DOOR SELECTOR',
            columns: [{
                text: 'KIND OF DOOR SELECTOR',
                width: 100,
                dataIndex: 'ds_kind_of_door_selector',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'ds_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'ds_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'FLUSH BOLT',
            columns: [{
                text: 'KIND OF FLUSH BOLT',
                width: 100,
                dataIndex: 'fb_kind_of_flush_bolt',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'fb_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'fb_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'TOP FLUSH BOLT STRIKE',
            columns: [{
                text: 'KIND OF TOP FLUSH BOLT STRIKE',
                width: 100,
                dataIndex: 'tfbs_kind_of_top_flush_bolt_strike',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'tfbs_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'tfbs_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'BOTTOM FLUSH BOLT STRIKE',
            columns: [{
                text: 'KIND OF BOTTOM FLUSH BOLT STRIKE',
                width: 100,
                dataIndex: 'bfbs_kind_of_bottom_flush_bolt_strike',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'bfbs_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'bfbs_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'DOOR STOPPER',
            columns: [{
                text: 'KIND OF DOOR STOPPER',
                width: 100,
                dataIndex: 'dst_kind_of_door_stopper',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'dst_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'dst_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'AUTO SEAL',
            columns: [{
                text: 'KIND OF AUTO SEAL',
                width: 100,
                dataIndex: 'as_kind_of_auto_seal',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'as_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'as_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'RUBBER',
            columns: [{
                text: 'KIND OF RUBBER',
                width: 100,
                dataIndex: 'r_kind_of_rubber',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'r_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'r_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'LOUVER',
            columns: [{
                text: 'KIND OF LOUVER',
                width: 100,
                dataIndex: 'l_kind_of_louver',
                align: 'center',
                editor: false
            }, {
                text: 'W(mm)',
                width: 80,
                dataIndex: 'l_width',
                align: 'center',
                editor: false
            }, {
                text: 'H(mm)',
                width: 80,
                dataIndex: 'l_height',
                align: 'center',
                editor: false
            }, {
                text: 'QTy.',
                width: 80,
                dataIndex: 'l_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'GLASS WINDOW',
            columns: [{
                text: 'KIND OF GLASS',
                width: 100,
                dataIndex: 'gw_kind_of_glass',
                align: 'center',
                editor: false
            }, {
                text: 'W(mm)',
                width: 80,
                dataIndex: 'gw_width',
                align: 'center',
                editor: false
            }, {
                text: 'H(mm)',
                width: 80,
                dataIndex: 'gw_height',
                align: 'center',
                editor: false
            }, {
                text: 'QTy.',
                width: 80,
                dataIndex: 'gw_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'CANOPY',
            columns: [{
                text: 'MATERIAL',
                width: 100,
                dataIndex: 'c_material',
                align: 'center',
                editor: false
            }, {
                text: 'W(mm)',
                width: 80,
                dataIndex: 'c_width',
                align: 'center',
                editor: false
            }, {
                text: 'QTy.',
                width: 80,
                dataIndex: 'c_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'KICK PLATE',
            columns: [{
                text: 'METERIAL',
                width: 100,
                dataIndex: 'kp_meterial',
                align: 'center',
                editor: false
            }, {
                text: 'W(mm)',
                width: 80,
                dataIndex: 'kp_width',
                align: 'center',
                editor: false
            }, {
                text: 'H(mm)',
                width: 80,
                dataIndex: 'kp_height',
                align: 'center',
                editor: false
            }, {
                text: 'QTy.',
                width: 80,
                dataIndex: 'kp_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'SILICON',
            columns: [{
                text: 'KIND OF SILICON',
                width: 100,
                dataIndex: 's_kind_of_silicon',
                align: 'center',
                editor: false
            }, {
                text: 'COLOR',
                width: 80,
                dataIndex: 's_color',
                align: 'center',
                editor: false
            }, {
                text: 'BOTTLE',
                width: 80,
                dataIndex: 's_bottle',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 's_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'WELDING ROD',
            columns: [{
                text: 'KIND',
                width: 100,
                dataIndex: 'wr_kind_of_welding_rod',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'wr_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'wr_qty',
                align: 'center',
                editor: false
            }]
        }, {
            text: 'ANCHOR BOLT',
            columns: [{
                text: 'KIND',
                width: 100,
                dataIndex: 'ab_kind_of_anchor_bolt',
                align: 'center',
                editor: false
            }, {
                text: 'UNIT',
                width: 80,
                dataIndex: 'ab_unit',
                align: 'center',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'ab_qty',
                align: 'center',
                editor: false
            }]
        }
    ],
    initComponent: function() {
        var me = this;
        me.store = {
            type: 'DrawingStore',
            autoLoad: true,
            data: me.getDataPanel()
        };
        me.callParent();
    }
});