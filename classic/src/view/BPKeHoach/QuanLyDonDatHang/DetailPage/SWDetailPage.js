Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.SWDetailPage', {
    extend: 'Ext.grid.Panel',
    //controller: 'SWDetailPage',
    //viewModel: 'SWDetailPage',
    xtype: 'SWDetailPage',
    autoScroll: true,
    layout: 'fit',
    config: {
        dataPanel: null
    },
    bufferedRenderer: false,
    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true
    },
    columnLines: true,
    selModel: {
        type: 'spreadsheet',
        columnSelect: true,
        pruneRemoved: false,
        extensible: 'y'
    },
    forceFit: true,
    viewConfig: {
        columnLines: true,
        trackOver: false
    },
    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
    columns: [
		{
            text: 'ORDER SHEET',
            columns: [{
                text: 'NO',
                width: 80,
                dataIndex: 'no',
                align: 'center',
                editor: false
            }, {
                text: 'SUB NO',
                width: 80,
                dataIndex: 'sub_no',
                align: 'center',
                editor: false
            }]
        }, 
		{
            text: 'MARK',
            columns: [{
                width: 120,
                text: 'NAME',
                dataIndex: 'mark',
                align: 'center',
                editor: false
            }]
        }, 
		{
            text: 'STEEL WINDOW FRAME',
            columns: [{
                text: 'W',
                columns: [{
                    text: 'mm',
                    width: 80,
                    dataIndex: 'swf_width',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'H',
                columns: [{
                    text: 'mm',
                    width: 80,
                    dataIndex: 'swf_height',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'DEPTH',
                columns: [{
                    text: 'mm',
                    width: 80,
                    dataIndex: 'swf_depth',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'QTY',
				width: 80,
				dataIndex: 'swf_qty',
				align: 'center',
				editor: false
            }, {
                text: 'MATERIAL',
                width: 100,
                dataIndex: 'swf_material',
                align: 'center',
                editor: false
            }, {
                text: 'SHEET',
                columns: [{
                    text: 'THICKNESS',
                    width: 100,
                    dataIndex: 'swf_sheet_thickness',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'PAINT COLOR',
                columns: [{
                    text: 'BASE PAINT',
                    width: 80,
                    dataIndex: 'swf_base_paint',
                    align: 'center',
                    editor: false
                }, {
                    text: 'FINISH PAINT',
                    width: 80,
                    dataIndex: 'swf_finish_paint',
                    align: 'center',
                    editor: false
                }]
            }]
        }, 
		{
            text: 'GLASS',
            columns: [{
                text: 'KIND OF GLASS',
                width: 100,
                dataIndex: 'g_kind_of_glass',
                align: 'center',
                editor: false
            }, {
                text: 'W(mm)',
                width: 80,
                dataIndex: 'g_width',
                align: 'center',
                editor: false
            }, {
                text: 'H(mm)',
                width: 80,
                dataIndex: 'g_height',
                align: 'center',
                editor: false
            }, {
                text: 'QTy.',
                width: 80,
                dataIndex: 'g_qty',
                align: 'center',
                editor: false
            }]
        }
    ],
    initComponent: function() {
        var me = this;
        me.store = {
            fields: ['g_height', 'g_kind_of_glass', 'g_qty', 'g_width', 'mark', 'no', 'sub_no', 'swf_base_paint','swf_depth', 'swf_finish_paint', 'swf_height', 'swf_material', 'swf_qty', 'swf_sheet_thickness', 'swf_width'],
            autoLoad: true,
            data: me.getDataPanel()
        };
        me.callParent();
    }
});