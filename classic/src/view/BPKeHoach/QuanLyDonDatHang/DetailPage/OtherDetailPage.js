Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.OtherDetailPage', {
    extend: 'Ext.panel.Panel',
    //controller: 'OtherDetailPage',
    //viewModel: 'OtherDetailPage',
    xtype: 'OtherDetailPage',
    bodyBorder: true,
    autoScroll: false,
    config: {
        dataPanel: null
    },
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});