Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.SSGrillDetailPage', {
    extend: 'Ext.panel.Panel',
    //controller: 'SSGrillDetailPage',
    //viewModel: 'SSGrillDetailPage',
    xtype: 'SSGrillDetailPage',
    autoScroll: true,
    layout: 'anchor',
    bodyPadding: 10,
    bodyStyle: {
        'background-color': '#f6f6f6;',
        'padding': '5px'
    },
    config: {
        dataPanel: null,
        doorType: null,
        projectName: null,
    },
    initComponent: function() {
        var me = this;
        var items = [];
        items = me.createInforGrid();
        items.push(me.createForFactoryUseGrid());
        me.items = items;
        me.callParent();
    },

    createInforGrid: function() {
        var me = this;
        var data = me.getDataPanel();
        data['pj_name'] = me.getProjectName();
        var fields = ['bottom_rubber', 'cd_type1', 'cd_type2', 'checked_by', 'co_h', 'co_height', 'co_width', 'ex_factory_date', 'finishing_color', 'finishing_type', 'frp', 'fs_guide_rail', 'gr_mat', 'gr_type', 'l_or_r', 'location', 'prepared_by', 'qty', 'remarks', 'sb_type', 'shutter_no', 'slat_mat', 'slat_type', 'sub_no', 'pj_name'];
        var store = Ext.create('Ext.data.Store', {
            fields: fields,
            data: data
        });
        var item1 = Ext.create('Ext.grid.Panel', {
            region: 'north',
            forceFit: true,
            store: store,
            columnLines: true,
            border: true,
            ui: 'light',
            margin: '0 0 15 0',
            columns: [{
                text: 'SUB NO',
                dataIndex: 'sub_no',
                width: 100,
                editor: false
            }, {
                text: 'PROJECT NAME',
                dataIndex: 'pj_name',
                editor: false
            }, {
                text: 'EX-FACTORY DATE',
                dataIndex: 'ex_factory_date',
                editor: false
            }, {
                text: 'PREPARED BY',
                dataIndex: 'prepared_by',
                editor: false
            }, {
                text: 'CHECKED BY',
                dataIndex: 'checked_by',
                editor: false
            }]
        });
        var item2 = Ext.create('Ext.grid.Panel', {
            region: 'north',
            forceFit: true,
            store: store,
            columnLines: true,
            border: true,
            ui: 'light',
            margin: '0 0 15 0',
            columns: [{
                text: 'SHUTTER NO',
                width: 100,
                dataIndex: 'shutter_no',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'qty',
                editor: false
            }, {
                text: 'CLEAR OPENING',
                columns: [{
                    text: 'WIDTH',
                    width: 80,
                    dataIndex: 'co_width',
                    editor: false
                }, {
                    text: 'HEIGHT',
                    width: 80,
                    dataIndex: 'co_height',
                    editor: false
                }, {
                    text: 'h',
                    width: 80,
                    dataIndex: 'co_h',
                    editor: false
                }]
            }, {
                text: 'LOCATION',
                width: 100,
                dataIndex: 'location',
                editor: false
            }, {
                text: 'L or R',
                width: 80,
                dataIndex: 'l_or_r',
                editor: false
            }, {
                text: 'SLAT TYPE',
                columns: [{
                    text: 'TYPE',
                    width: 100,
                    dataIndex: 'slat_type',
                    editor: false
                }, {
                    text: 'MAT',
                    width: 100,
                    dataIndex: 'slat_mat',
                    editor: false
                }]

            }, {
                text: 'BOTTOM RUBBER',
                width: 100,
                dataIndex: 'bottom_rubber',
                editor: false
            }, {
                text: 'GUIDE RAIL',
                columns: [{
                    text: 'TYPE',
                    width: 80,
                    dataIndex: 'gr_type',
                    align: 'center',
                    editor: false
                }, {
                    text: 'MAT',
                    width: 80,
                    dataIndex: 'gr_mat',
                    align: 'center',
                    editor: false
                }]
            }, {
                text: 'SWITCH BOX TYPE',
                width: 100,
                dataIndex: 'sb_type',
                editor: false
            }, {
                text: 'FINISHING',
                columns: [{
                    text: 'TYPE',
                    width: 80,
                    dataIndex: 'finishing_type',
                    align: 'center',
                    editor: false
                }, {
                    text: 'COLOR',
                    width: 80,
                    dataIndex: 'finishing_color',
                    align: 'center',
                    editor: false
                }]
            }]
        });
        var item3 = Ext.create('Ext.grid.Panel', {
            region: 'north',
            store: store,
            forceFit: true,
            border: true,
            ui: 'light',
            columnLines: true,
            margin: '0 0 15 0',
            columns: [{
                text: 'F.R.P',
                //width: 80,
                dataIndex: 'frp',
                align: 'center',
                editor: false
            }, {
                text: 'CLOSING DEVICE',
                columns: [{
                    text: 'TYPE',
                    //width: 80,
                    dataIndex: 'cd_type1',
                    align: 'center',
                    editor: false
                }, {
                    text: 'TYPE',
                    //width: 80,
                    dataIndex: 'cd_type2',
                    align: 'center',
                    editor: false
                }]

            }, {
                text: 'REMARKS',
                //width: 100,
                dataIndex: 'remarks',
                editor: false
            }]
        });
        var item = [item1, item2, item3];
        return item
    },

    createForFactoryUseGrid: function() {
        var me = this;
        var data = me.getDataPanel().for_factory_use_only;
        var gridData = [];
        var current = '';
        if (data) {
            data.forEach(function(row) {
                var new_row = row;
                new_row.qty_l = row.qty.l;
                new_row.qty_r = row.qty.r;
                new_row.qty_total = row.qty.total;
                new_row.type0 = row.type[0];
                new_row.type1 = row.type[1];
                new_row.type2 = row.type[2];
                new_row.type3 = row.type[3];
                if (row.name == current) {
                    new_row.name = ''
                } else {
                    current = row.name
                }
                gridData.push(new_row);
            });
        }
        var fields = ['name', 'color', 'finishing', 'length', 'qty_l', 'qty_r', 'qty_total', 'type0', 'type1', 'type2', 'type3'];
        var store = Ext.create('Ext.data.Store', {
            fields: fields,
            data: gridData
        });
        var columns = [{
            hideHeader: true,
            dataIndex: 'name',
            width: 150,
            renderer: function(value) {
                return '<b>' + value + '</b>'
            },
            editor: false
        }, {
            text: 'TYPE',
            columns: [{
                hideHeader: true,
                dataIndex: 'type0',
                editor: false
            }, {
                hideHeader: true,
                dataIndex: 'type1',
                editor: false
            }, {
                hideHeader: true,
                dataIndex: 'type2',
                editor: false
            }, {
                hideHeader: true,
                dataIndex: 'type3',
                editor: false
            }]
        }, {
            text: 'LENGTH',
            dataIndex: 'length',
            editor: false
        }, {
            text: 'QTY',
            columns: [{
                text: '(L)',
                dataIndex: 'qty_l',
                editor: false
            }, {
                text: '(R)',
                dataIndex: 'qty_r',
                editor: false
            }, {
                text: 'TOTAL',
                dataIndex: 'qty_total',
                editor: false
            }]
        }, {
            text: 'FINISHING',
            dataIndex: 'length',
            editor: false
        }, {
            text: 'COLOR',
            dataIndex: 'length',
            editor: false
        }];
        var item = Ext.create('Ext.grid.Panel', {
            region: 'center',
            title: '<b> FOR FACTORY USE ONLY </b>',
            glyph: 'f039@FontAwesome',
            border: true,
            ui: 'light',
            store: store,
            columns: columns,
            autoScroll: true,
            selModel: {
                type: 'spreadsheet',
                columnSelect: true,
                pruneRemoved: false,
                extensible: 'y'
            },
            forceFit: true,
            viewConfig: {
                columnLines: true,
                trackOver: false
            },
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
        });
        return item
    }
});