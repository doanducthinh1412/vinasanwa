Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.QSDetailPage', {
    extend: 'Ext.panel.Panel',
    //controller: 'QSDetailPage',
    //viewModel: 'QSDetailPage',
    xtype: 'QSDetailPage',
    autoScroll: true,
    layout: 'anchor',
    bodyPadding: 10,
    bodyStyle: {
        'background-color': '#f6f6f6;',
        'padding': '5px'
    },
    config: {
        dataPanel: null,
        doorType: null,
        projectName: null,
    },
    initComponent: function() {
        var me = this;
        var items = [];
        items = me.createInforGrid();
        items.push(Ext.create('Ext.panel.Panel', {
            layout: 'column',
            margin: '0 0 15 0',
            region: 'north',
            items: [me.createOperationGrid(), me.createOptionGrid()]
        }));
        //items.push(me.createOperationGrid());
        //items.push(me.createOptionGrid());
        items.push(me.createForFactoryUseGrid());
        me.items = items;
        me.callParent();
    },

    createInforGrid: function() {
        var me = this;
        var data = me.getDataPanel();
        data['pj_name'] = me.getProjectName();
        var fields = [
            'checked_by',
            'co_height',
            'co_width',
            'control_panel',
            'ex_factory_date',
            'paint_finish',
            'paint_under_coat',
            'prepared_by',
            'pull_switch_stand',
            'qty',
            'remarks',
            'sheet_type',
            'shutter_no',
            'sub_no',
            'tsf_jpn_type',
            'type',
            'pj_name',
            'vision_window'
        ];
        var store = Ext.create('Ext.data.Store', {
            fields: fields,
            data: data
        });
        var item1 = Ext.create('Ext.grid.Panel', {
            region: 'north',
            forceFit: true,
            store: store,
            columnLines: true,
            border: true,
            ui: 'light',
            margin: '0 0 15 0',
            columns: [{
                text: 'SUB NO',
                dataIndex: 'sub_no',
                height: 50,
                width: 100,
            }, {
                text: 'PROJECT NAME',
                dataIndex: 'pj_name',
            }, {
                text: 'EX-FACTORY DATE',
                dataIndex: 'ex_factory_date'
            }, {
                text: 'PREPARED BY',
                dataIndex: 'prepared_by'
            }, {
                text: 'CHECKED BY',
                dataIndex: 'checked_by'
            }]
        });
        var item2 = Ext.create('Ext.grid.Panel', {
            region: 'north',
            forceFit: true,
            store: store,
            columnLines: true,
            border: true,
            ui: 'light',
            margin: '0 0 15 0',
            columns: [{
                text: 'SHUTTER NO',
                width: 100,
                dataIndex: 'shutter_no',
                editor: false
            }, {
                text: 'QTY',
                width: 80,
                dataIndex: 'qty',
                editor: false
            }, {
                text: 'CLEAR OPENING',
                columns: [{
                    text: 'WIDTH',
                    width: 80,
                    dataIndex: 'co_width',
                    editor: false
                }, {
                    text: 'HEIGHT',
                    width: 80,
                    dataIndex: 'co_height',
                    editor: false
                }]
            }, {
                text: 'SHEET TYPE',
                width: 100,
                dataIndex: 'sheet_type',
                editor: false
            }, {
                text: 'VISION WINDOW',
                width: 80,
                dataIndex: 'vision_window',
                editor: false
            }, {
                text: 'CONTROL PANEL',
                dataIndex: 'control_panel',
                editor: false
            }, {
                text: 'TRANSFORMER for JPN TYPE',
                width: 150,
                dataIndex: 'tsf_jpn_type',
                editor: false
            }, {
                text: 'PAINT',
                columns: [{
                    text: 'UNDER COAT',
                    width: 80,
                    dataIndex: 'paint_under_coat',
                    align: 'center',
                    editor: false
                }, {
                    text: 'FINISH',
                    width: 80,
                    dataIndex: 'paint_finish',
                    align: 'center',
                    editor: false
                }]
            }]
        });
        var item3 = Ext.create('Ext.grid.Panel', {
            region: 'north',
            store: store,
            forceFit: true,
            border: true,
            ui: 'light',
            columnLines: true,
            margin: '0 0 15 0',
            columns: [{
                text: 'TYPE',
                height: 50,
                dataIndex: 'type',
                align: 'center',
                editor: false
            }, {
                text: 'REMARKS',
                //width: 100,
                dataIndex: 'remarks',
                editor: false
            }]
        });
        var item = [item1, item2, item3];
        return item
    },

    createOperationGrid: function() {
        var me = this;
        var data = me.getDataPanel().operation;
        var fields = ['name', 'type', 'qty'];
        var store = Ext.create('Ext.data.Store', {
            fields: fields,
            data: data
        });
        var columns = [{
            text: 'OPERATION',
            columns: [{
                hideHeader: true,
                dataIndex: 'name',
                flex: 2,
                renderer: function(value) {
                    return '<b>' + value + '</b>';
                },
                editor: false
            }, {
                hideHeader: true,
                flex: 2,
                dataIndex: 'type',
                editor: false
            }]
        }, {
            text: 'QTY',
            flex: 1,
            dataIndex: 'qty',
            editor: false
        }];
        var item = Ext.create('Ext.grid.Panel', {
            columnWidth: 0.5,
            margin: '0 10 0 0',
            border: true,
            ui: 'light',
            store: store,
            columns: columns,
            autoScroll: true,
            selModel: {
                type: 'spreadsheet',
                columnSelect: true,
                pruneRemoved: false,
                extensible: 'y'
            },
            forceFit: true,
            viewConfig: {
                columnLines: true,
                trackOver: false
            },
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
        });
        return item
    },

    createOptionGrid: function() {
        var me = this;
        var data = me.getDataPanel().option;
        var fields = ['name', 'qty'];
        var store = Ext.create('Ext.data.Store', {
            fields: fields,
            data: data
        });
        var columns = [{
            text: 'OPTION',
            height: 50,
            dataIndex: 'name',
            flex: 2,
            renderer: function(value) {
                return '<b>' + value + '</b>';
            },
            editor: false
        }, {
            text: 'QTY',
            flex: 1,
            dataIndex: 'qty',
            editor: false
        }];
        var item = Ext.create('Ext.grid.Panel', {
            columnWidth: 0.5,
            border: true,
            ui: 'light',
            store: store,
            columns: columns,
            autoScroll: true,
            selModel: {
                type: 'spreadsheet',
                columnSelect: true,
                pruneRemoved: false,
                extensible: 'y'
            },
            forceFit: true,
            viewConfig: {
                columnLines: true,
                trackOver: false
            },
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
        });
        return item
    },

    createForFactoryUseGrid: function() {
        var me = this;
        var data = me.getDataPanel().for_factory_use_only;
        var gridData = [];
        var current = '';
        if (data) {
            data.forEach(function(row) {
                if (row.name === null) {
                    return
                } else {
                    var new_row = row;
                    new_row.qty_l = row.qty.l;
                    new_row.qty_r = row.qty.r;
                    new_row.qty = row.qty.total;
                    if (row.name == current) {
                        new_row.name = ''
                    } else {
                        current = row.name
                    }
                    gridData.push(new_row);
                }
            });
        }
        var fields = ['name', 'color', 'finishing', 'length', 'qty_l', 'qty_r', 'qty_total', 'type'];
        var store = Ext.create('Ext.data.Store', {
            fields: fields,
            data: gridData
        });
        var columns = [{
            hideHeader: true,
            dataIndex: 'name',
            width: 150,
            renderer: function(value) {
                return '<b>' + value + '</b>'
            },
            editor: false
        }, {
            text: 'TYPE',
            dataIndex: 'type',
            editor: false
        }, {
            text: 'QTY',
            dataIndex: 'qty_total',
            editor: false
        }, {
            text: 'QTY',
            columns: [{
                text: '(L)',
                dataIndex: 'qty_l',
                editor: false
            }, {
                text: '(R)',
                dataIndex: 'qty_r',
                editor: false
            }]
        }, {
            text: 'FINISHING',
            dataIndex: 'finishing',
            editor: false
        }, {
            text: 'COLOR',
            dataIndex: 'color',
            editor: false
        }];
        var item = Ext.create('Ext.grid.Panel', {
            region: 'center',
            title: '<b> FOR FACTORY USE ONLY </b>',
            glyph: 'f039@FontAwesome',
            border: true,
            ui: 'light',
            //margin: '15 0 0 0',
            store: store,
            columns: columns,
            autoScroll: true,
            selModel: {
                type: 'spreadsheet',
                columnSelect: true,
                pruneRemoved: false,
                extensible: 'y'
            },
            forceFit: true,
            viewConfig: {
                columnLines: true,
                trackOver: false
            },
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
        });
        return item
    }
});