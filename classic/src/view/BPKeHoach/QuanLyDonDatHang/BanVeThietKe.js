Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.BanVeThietKe', {
    extend: 'Ext.panel.Panel',
    xtype: 'BanVeThietKe',
    reference: 'BanVeThietKe',
    controller: 'BanVeThietKe',
    viewModel: 'BanVeThietKe',
    layout: 'fit',
    reference: 'BanVeThietKeGrid',
    autoScroll: true,
    config: {
        recordData: null,
        dataFile: null,
        dataObject: null,
        onlineFile: true
    },
    initComponent: function() {
        var me = this;
        var dataFile = me.getDataFile();
        var onlineFile = me.getOnlineFile();
        var recordData = me.getRecordData();
        var dataObject = me.getDataObject();
        var banvethietke = [];
        if (onlineFile === false) {
            if (dataFile) {
                dataFile.each(function(record) {
                    if (record.data.type === "banve") {
                        banvethietke.push({
                            'filename': record.data.name,
                            'file': record.data.file,
                            'filepath': URL.createObjectURL(record.data.file)
                        });
                    }
                });
            }
        } else {
            if (dataFile) {
                for (i = 0; i < dataFile.length; i++) {
                    if (dataObject) {
                        banvethietke.push({
                            'filename': dataFile[i].file_name,
                            'filepath': '/api/ordersheet/banvethietke/' + dataObject.order_sheet_id + '?name=' + dataFile[i].file_name
                        });
                    }
                }
            }
        }
        me.items = [{
            xtype: 'ViewPDF',
            reference: 'ViewPDF',
            flex: 1,
            urlPdf: banvethietke !== [] ? banvethietke[0].filepath : '',
            tbar: [{
                xtype: 'combobox',
                reference: 'listBanVeThietKe',
                fieldLabel: '<b>Danh sách bản vẽ</b>',
                displayField: 'filename',
                valueField: 'filename',
                width: '50%',
                labelWidth: 130,
                value: banvethietke !== [] ? banvethietke[0].filename : null,
                store: Ext.create('Ext.data.Store', {
                    fields: ['filename', 'id'],
                    data: banvethietke
                }),
                queryMode: 'local',
                editable: false,
                listeners: {
                    select: 'selectionBanVe'
                }
            }, {
                xtype: 'displayfield',
                value: "<b>Số lượng: " + banvethietke.length + " (bản vẽ)</b>"
            }]
        }];

        me.callParent();
    }
});