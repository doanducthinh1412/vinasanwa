/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.UploadDonDatHang', {
    extend: 'Ext.window.Window',
    xtype: 'UploadDonDatHang',
    reference: 'UploadDonDatHang',
    controller: 'UploadDonDatHang',
    viewModel: 'UploadDonDatHang',
    autoShow: true,
    height: 600,
    width: '60%',
    minWidth: 600,
    minHeight: 600,
    layout: 'border',
    iconCls: 'fas fa-exclamation-circle',
    modal: true,
    config: {
        doorType: null,
        gridView: null,
        recordView: null,
    },
    initComponent() {
        const me = this;
        let mode = 1;
        const doorType = me.getDoorType();
        const recordView = me.getRecordView();
        if (doorType === 'sd' || doorType === 'fsd' || doorType === 'lsd' || doorType === 'flsd' || doorType === 'sw' || doorType === 'sld' || doorType === 'sp' || doorType === 'susd' || doorType === 'wd') {
            mode = 1;
        } else {
            mode = 2;
        }
        me.buttons = ['->', {
                text: 'Nhập liệu Excel',
                ui: 'soft-orange',
                iconCls: 'fas fa-file-upload',
                handler: 'onClickButtonImportFile',
            },
            {
                text: 'Thoát',
                glyph: 'xf00d@FontAwesome',
                handler() {
                    me.close();
                },
            },
            '->',
        ];
        me.items = [{
            region: 'north',
            height: 150,
            layout: {
                type: 'vbox',
                align: 'stretch',
            },
            bodyPadding: 15,
            xtype: 'form',
            defaultType: 'textfield',
            fieldDefaults: {
                labelWidth: 80,
            },
            margin: '0 0 5 0',
            items: [{
                xtype: 'combobox',
                fieldLabel: '<b>Loại đơn</b>',
                labelAlign: 'left',
                reference: 'ComboboxLoaiDon',
                store: {
                    type: 'LoaiDonDatHangStore',
                },
                queryMode: 'local',
                editable: false,
                labelWidth: 80,
                displayField: 'name',
                valueField: 'val',
                allowBlank: false,
                value: recordView === null ? '1' : recordView.get('order_type'),
                listeners: {
                    // select: 'onSelectLoaiDon'
                },
            }, {
                xtype: 'combobox',
                labelAlign: 'left',
                fieldLabel: '<b>Độ ưu tiên</b>',
                store: {
                    type: 'DoUuTienDonDatHangStore',
                },
                queryMode: 'local',
                editable: false,
                labelWidth: 80,
                displayField: 'name',
                reference: 'ComboboxDoUuTien',
                valueField: 'val',
                value: recordView === null ? '1' : recordView.get('priority'),
                allowBlank: false,
            }, {
                xtype: 'combobox',
                labelAlign: 'left',
                fieldLabel: '<b>Ghi chú</b>',
                store: Ext.create('Ext.data.Store', {
                    fields: ['val'],
                    data: [
                        { val: 'Cửa nhập khẩu' },
                        { val: 'Cửa màu' },
                        { val: 'Cửa Test' },
                    ],
                }),
                queryMode: 'local',
                editable: true,
                labelWidth: 80,
                displayField: 'val',
                reference: 'ComboboxGhiChu',
                valueField: 'val',
                minChars: 0,
                typeAhead: true,
                value: recordView === null ? '' : recordView.get('note'),
                allowBlank: true,
            }],
        }, {
            region: 'west',
            width: 160,
            layout: 'vbox',
            bodyPadding: 5,
            margin: '0 5 0 0',
            items: [{
                xtype: 'filefield',
                buttonOnly: true,
                hidden: mode !== 1,
                buttonConfig: {
                    text: 'Tải trang<br>phủ + chi tiết<br>(Excel)',
                    scale: 'large',
                    height: 100,
                    width: '100%',
                    iconCls: 'fas fa-file-excel',
                    ui: 'soft-orange',
                },
                multiple: false,
                typeFile: 'phu+chitiet',
                reference: 'btnUploadPhuChiTiet',
                accept: 'application/vnd.ms-excel',
                listeners: {
                    change: 'onChangeFile',
                },
            }, {
                xtype: 'filefield',
                buttonOnly: true,
                hidden: mode !== 2,
                buttonConfig: {
                    text: 'Tải trang<br>phủ (Excel)',
                    scale: 'large',
                    height: 100,
                    width: '100%',
                    iconCls: 'fas fa-file-excel',
                    ui: 'soft-orange',
                },
                multiple: false,
                typeFile: 'phu',
                reference: 'btnUploadPhu',
                accept: 'application/vnd.ms-excel',
                listeners: {
                    change: 'onChangeFile',
                },
            }, {
                xtype: 'filefield',
                buttonOnly: true,
                hidden: mode !== 2,
                buttonConfig: {
                    text: 'Tải trang<br>chi tiết<br>(Excel)',
                    scale: 'large',
                    height: 100,
                    width: '100%',
                    iconCls: 'fas fa-file-excel',
                    ui: 'facebook',
                },
                multiple: true,
                typeFile: 'chitiet',
                reference: 'btnUploadChiTiet',
                accept: 'application/vnd.ms-excel',
                listeners: {
                    change: 'onChangeFile',
                },
            }, {
                xtype: 'filefield',
                buttonOnly: true,
                buttonConfig: {
                    text: 'Tải bản vẽ<br>thiết kế <br>(Pdf)',
                    scale: 'large',
                    height: 100,
                    width: '100%',
                    iconCls: 'fas fa-file-pdf',
                    ui: 'gray',
                },
                multiple: true,
                typeFile: 'banve',
                accept: 'application/pdf',
                regex: (/.(pdf)$/i),
                reference: 'btnUploadBanVe',
                regexText: 'Định dạng không được hỗ trợ',
                listeners: {
                    change: 'onChangeFile',
                },
            }],
        }, {
            region: 'center',
            xtype: 'grid',
            columnLines: true,
            height: '100%',
            layout: 'fit',
            reference: 'GridListFile',
            emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
            store: {
                fields: ['name', 'type', 'filetype', { name: 'file', type: 'auto' }, 'size'],
                data: [],
            },
            columns: [{
                xtype: 'rownumberer',
                text: '<b>STT</b>',
                width: 65,
                align: 'center',
            }, {
                text: '<br><i class="fas fa-file-alt"></i> Tên File<br><br>',
                flex: 1,
                align: 'left',
                dataIndex: 'name',
            }, {
                text: 'Loại<br>',
                flex: 0.5,
                align: 'center',
                dataIndex: 'type',
                renderer(value, meta, rec) {
                    if (value === 'phu') {
                        return 'Tờ phủ';
                    }
                    if (value === 'chitiet') {
                        return 'Tờ chi tiết';
                    }
                    if (value === 'phu+chitiet') {
                        return 'Order Sheet';
                    }
                    if (value === 'banve') {
                        return 'Bản vẽ thiết kế';
                    }
                    return '';
                },
            }, {
                text: 'Kích thước<br>',
                flex: 0.5,
                align: 'center',
                dataIndex: 'size',
                renderer(value, meta, rec) {
                    return ClassMain.bytesToSize(value);
                },
            }, {
                text: '',
                xtype: 'actioncolumn',
                width: 60,
                sortable: false,
                menuDisabled: true,
                align: 'center',
                items: [{
                    iconCls: 'fas fa-times text-blue',
                    tooltip: 'Xóa File',
                    handler: 'onClickButtonRemoveFile',
                }],
            }],
        }];
        me.callParent();
    },
});