/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.QuanLyDonDatHang', {
    extend: 'Ext.grid.Panel',
    controller: 'QuanLyDonDatHang',
    viewModel: 'QuanLyDonDatHang',
    xtype: 'QuanLyDonDatHang',
    layout: 'fit',
    store: {
        type: 'QuanLyDonDatHangStore',
        autoLoad: true,
    },
    reference: 'QuanLyDonDatHangGrid',
    autoScroll: true,
    columnLines: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    config: {
        filterString: null,
    },
    features: [{
        ftype: 'grouping',
        startCollapsed: false,
        groupHeaderTpl: '<font color="blue"><b>Dự án:</b> {name} ({rows.length} đơn đặt hàng)</font>',
    }],
    cls: 'custom-grid',
    initComponent() {
        const me = this;
        const tbar = [{
            xtype: 'button',
            text: 'Tạo mới',
            iconCls: 'fas fa-plus-circle',
            ui: 'soft-orange',
            hidden: !((ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'create') === true && ClassMain.checkPermission('quan_ly_du_an', 'Project', 'create') === true)),
            menu: me.buildCreateMenu(),
        }, '-', {
            text: 'Thao tác',
            iconCls: 'fas fa-bars',
            ui: 'dark-blue',
            disabled: true,
            bind: {
                disabled: '{!QuanLyDonDatHangGrid.selection}',
                menu: '{actionMenu}',
            },
        }, '->', {
            xtype: 'textfield',
            emptyText: 'Nhập tìm kiếm',
            width: 250,
            reference: 'SearchField',
            triggers: {
                clear: {
                    weight: 0,
                    cls: `${Ext.baseCSSPrefix}form-clear-trigger`,
                    hidden: true,
                    handler: 'onClearClick',
                    scope: 'this',
                },
                search: {
                    weight: 1,
                    cls: `${Ext.baseCSSPrefix}form-search-trigger`,
                    handler: 'onSearchClick',
                    scope: 'this',
                },
            },
            onClearClick() {
                this.setValue('');
                this.getTrigger('clear').hide();
                this.updateLayout();
                me.getController().loadDataGrid();
            },
            onSearchClick() {
                const value = this.getValue();
                if (value.length > 0) {
                    this.getTrigger('clear').show();
                    this.updateLayout();
                } else {
                    this.getTrigger('clear').hide();
                }
                me.getController().loadDataGrid();
            },
            listeners: {
                specialkey(f, e) {
                    if (e.getKey() === e.ENTER) {
                        this.onSearchClick();
                    }
                },
            },
        }, {
            xtype: 'button',
            text: 'Lọc theo',
            iconCls: 'fas fa-filter',
            ui: 'dark-blue',
            menuAlign: 'tr-br',
            reference: 'FilterMenu',
            menu: Ext.create('Ext.menu.Menu', {
                style: {
                    overflow: 'visible',
                },
                mouseLeaveDelay: 10,
                width: 370,
                margin: '10 10 10 10',
                referenceHolder: true,
                buttons: [{
                    text: 'Lọc',
                    ui: 'soft-orange',
                    handler() {
                        me.getController().loadDataGrid();
                    },
                }, {
                    text: 'Xóa bộ lọc',
                    handler(btn) {
                        const refs = btn.up().up().getReferences();
                        refs.forEach((item, key) => {
                            item.clearValue();
                            item.getTrigger('clear').hide();
                        });
                        btn.up().up().hide();
                        me.getController().loadDataGrid();
                    },
                }],
                items: [{
                    xtype: 'combobox',
                    fieldLabel: '<b>Loại đơn</b>',
                    labelAlign: 'left',
                    store: {
                        type: 'LoaiDonDatHangStore',
                    },
                    queryMode: 'local',
                    editable: false,
                    labelWidth: 80,
                    reference: 'ordertypefilter',
                    margin: '10 10 10 10',
                    displayField: 'name',
                    valueField: 'val',
                    fieldIndex: 'order_type',
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            weight: -1,
                            hidden: true,
                            scope: 'this',
                            handler(combo) {
                                combo.clearValue();
                                combo.getTrigger('clear').hide();
                                me.getController().loadDataGrid();
                            },
                        },
                    },
                    listeners: {
                        select(combo) {
                            combo.getTrigger('clear').show();
                            combo.updateLayout();
                        },
                    },
                }, {
                    xtype: 'combobox',
                    labelAlign: 'left',
                    fieldLabel: '<b>Độ ưu tiên</b>',
                    store: {
                        type: 'DoUuTienDonDatHangStore',
                    },
                    queryMode: 'local',
                    reference: 'priorityfilter',
                    editable: false,
                    labelWidth: 80,
                    margin: '10 10 10 10',
                    displayField: 'name',
                    fieldIndex: 'priority',
                    valueField: 'val',
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            weight: -1,
                            hidden: true,
                            scope: 'this',
                            handler(combo) {
                                combo.clearValue();
                                combo.getTrigger('clear').hide();
                                me.getController().loadDataGrid();
                            },
                        },
                    },
                    listeners: {
                        select(combo) {
                            combo.getTrigger('clear').show();
                            combo.updateLayout();
                        },
                    },
                }],
            }),
        }, {
            text: 'Công cụ',
            iconCls: 'fas fa-briefcase',
            ui: 'dark-blue',
            menu: {
                xtype: 'menu',
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                    handler: 'onPrintButtonClick',
                }, {
                    text: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    handler: 'onExportGriddButtonClick',
                }, '-', {
                    text: 'Làm mới danh sách',
                    iconCls: 'fas fa-sync-alt text-dark-blue',
                    handler: 'onReloadButtonClick',
                }],
            },
        }, '-', {
            iconCls: 'fas fa-stream',
            tooltip: 'Xem danh sách theo dự án',
            handler: 'onChangeModeViewByGroup',
        }, {
            iconCls: 'far fa-calendar-alt',
            tooltip: 'Xem danh sách theo thời gian',
            handler: 'onChangeModeViewByTime',
        }];

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            hidden: !!(me.getFilterString()),
            items: tbar,
        }];
        me.columns = [{
                xtype: 'rownumberer',
                text: '<b>TT</b>',
                width: 55,
                align: 'center',
            }, {
                text: '<b>Thao tác</b>',
                xtype: 'actioncolumn',
                width: 80,
                sortable: false,
                menuDisabled: true,
                align: 'center',
                locked: true,
                items: [{
                    iconCls: 'fas fa-info-circle text-blue',
                    tooltip: 'Xem thông tin đơn hàng',
                    handler: 'onViewButtonClickGrid',
                }, {
                    width: 3,
                }, {
                    tooltip: 'Chuyển xử lý',
                    getClass: 'getSendClass',
                    handler: 'onSendButtonClickGrid',
                }],
            },
            {
                text: '<b>Project Name</b>',
                dataIndex: 'project_name',
                sortable: false,
                width: 125,
                locked: true,
                align: 'center',
            },
            {
                text: '<b>Receive Number</b>',
                dataIndex: 'receive_no',
                width: 100,
                locked: true,
                align: 'center',
            },
            {
                text: '<b>Oder Number</b>',
                dataIndex: 'sub_no',
                width: 110,
                locked: true,
                align: 'center',
            },
            {
                text: '<b>Status</b>',
                dataIndex: 'status',
                width: 190,
                align: 'left',
                renderer(value, meta, rec) {
                    let productionSchedule = 0;
                    let orderSheet = 0;
                    let drawingDepartment = 0;
                    let orderSheetPrice = 0;
                    if (value.production_schedule) {
                        productionSchedule = parseInt(value.production_schedule, 10);
                    }
                    if (value.order_sheet) {
                        orderSheet = parseInt(value.order_sheet, 10);
                    }
                    if (value.drawing_department) {
                        drawingDepartment = parseInt(value.drawing_department, 10);
                    }
                    if (value.order_sheet_price) {
                        orderSheetPrice = parseInt(value.order_sheet_price, 10);
                    }
                    if (orderSheet === 0) {
                        return '<font color="red">BP KH: Chưa chuyển xử lý</font>';
                    }
                    const status = [];
                    if (drawingDepartment === 0) {
                        status.push('<font color="red">BP Vẽ: Chưa phân công</font>');
                    } else if (drawingDepartment === 1) {
                        status.push('<font color="blue">BP Vẽ: Đang thực hiện</font>');
                    } else if (drawingDepartment === 2) {
                        status.push('<font color="green">BP Vẽ: Hoàn thành vẽ</font>');
                    } else if (drawingDepartment === 3) {
                        status.push('<font color="green">BP Vẽ: Đã chuyển kết quả</font>');
                    }

                    if (orderSheetPrice === 0) {
                        status.push('<font color="red">BP Tính giá: Đang tính giá</font>');
                    } else if (orderSheetPrice === 1) {
                        status.push('<font color="green">BP Tính giá: Hoàn thành</font>');
                    } else if (orderSheetPrice === 2) {
                        status.push('<font color="green">BP Tính giá: Đã chuyển kết quả</font>');
                    }

                    if (productionSchedule === 0) {
                        status.push('<font color="red">BP KH: Chưa lập KH</font>');
                    } else if (productionSchedule === 1) {
                        status.push('<font color="blue">BP KH: Đang lập KH</font>');
                    } else if (productionSchedule === 2) {
                        status.push('<font color="green">BP KH: Hoàn thành lập KH');
                    }
                    return status.join('<br>');
                },
            },
            {
                text: '<b>Building Type</b>',
                dataIndex: 'building_type',
                width: 110,
                align: 'center',
            },
            {
                text: '<b>Customer Name</b>',
                dataIndex: 'cus_name',
                width: 130,
                align: 'center',
            },
            {
                text: '<b>Frame</b>',
                dataIndex: 'frame',
                width: 70,
                align: 'center',
            },
            {
                text: '<b>Leaf</b>',
                dataIndex: 'leaf',
                width: 70,
                align: 'center',
            },
            {
                text: '<b>Other</b>',
                align: 'center',
                columns: [{
                    text: 'Type',
                    width: 80,
                    align: 'center',
                    dataIndex: 'frame_type',
                    renderer(value, meta, rec) {
                        return value.toUpperCase();
                    },
                }, {
                    text: 'Quality',
                    width: 70,
                    align: 'center',
                    dataIndex: 'frame_total',
                }],
            },
            {
                text: '<b>FINISHING PAINT</b>',
                align: 'center',
                columns: [{
                    text: 'Apply',
                    align: 'center',
                    width: 100,
                    dataIndex: 'paint_apply',
                }, {
                    text: 'Color',
                    align: 'center',
                    width: 100,
                    dataIndex: 'paint_color',
                }],
            },
            {
                text: '<b>Frame Factory Out</b>',
                dataIndex: 'frame_factory_out',
                width: 100,
                align: 'center',
            },
            {
                text: '<b>Leaf Factory Out</b>',
                dataIndex: 'leaf_factory_out',
                width: 100,
                align: 'center',
            },
            {
                text: '<b>Oder Type</b>',
                dataIndex: 'order_type',
                width: 130,
                align: 'center',
                renderer(value, meta, rec) {
                    if (value === 1) {
                        return 'Đơn đặt hàng mới';
                    }
                    if (value === 2) {
                        return 'Đơn đặt hàng chỉnh sửa';
                    }
                    if (value === 3) {
                        return 'Đơn đặt hàng lại';
                    }
                    if (value === 4) {
                        return 'Đơn đặt hàng mua phụ kiện';
                    }
                    if (value === -1) {
                        return 'Đơn đặt hàng bị hủy';
                    }
                    return value;
                },
            },
            {
                text: '<b>Priority</b>',
                dataIndex: 'priority',
                width: 120,
                align: 'center',
                renderer(value, meta, rec) {
                    if (value === 1) {
                        return 'Bình thường';
                    }
                    if (value === 2) {
                        return 'Gấp';
                    }
                    return value;
                },
            },
            {
                text: '<b>Ghi chú</b>',
                dataIndex: 'note',
                width: 130,
            },
        ];
        me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true,
        };
        me.callParent();
    },
    buildCreateMenu() {
        const controller = this.getController();
        return Ext.create('Ext.menu.Menu', {
            plain: true,
            mouseLeaveDelay: 10,
            items: [{
                    text: 'Cửa SD',
                    doortype: 'sd',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa FSD',
                    doortype: 'fsd',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa LSD',
                    doortype: 'lsd',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa FLSD',
                    doortype: 'flsd',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa SS (Aichi)',
                    doortype: 'ssaichi',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa SS (JP)',
                    doortype: 'ssjp',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa SS (Grill)',
                    doortype: 'ssgrill',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa GR-S',
                    doortype: 'grs',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                },
                /* {
                                   text: 'Cửa SR-N',
                                   doortype: 'srn',
                                   iconCls: 'fas fa-door-open text-dark-blue',
                                   handler(btn) {
                                       controller.onCreateDoorButtonClick(btn);
                                   },
                               }, */
                {
                    text: 'Cửa S-13',
                    doortype: 's13',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa S-14',
                    doortype: 's14',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa LV',
                    doortype: 'lv',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa SW',
                    doortype: 'sw',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa SLD',
                    doortype: 'sld',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa SP',
                    doortype: 'sp',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa SUSD',
                    doortype: 'susd',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Cửa WD',
                    doortype: 'wd',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                }, {
                    text: 'Other',
                    doortype: 'other',
                    iconCls: 'fas fa-door-open text-dark-blue',
                    handler(btn) {
                        controller.onCreateDoorButtonClick(btn);
                    },
                },

            ],
        });
    },
    listeners: {
        itemcontextmenu: 'onItemContextMenu',
        select: 'onItemActionMenu',
        itemdblclick: 'onViewButtonClick',
        render(panel) {
            panel.getController().loadDataGrid();
        },
    },
    reloadView() {
        const me = this;
        me.getController().loadDataGrid();
    },
});