Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.CancelDonDatHang', {
    extend: 'Ext.window.Window',
    xtype: 'CancelDonDatHang',
    reference: 'CancelDonDatHang',
    title: 'Bạn có muốn hủy đơn đặt hàng?',
    autoShow: true,
    height: 250,
    width: 400,
    minWidth: 300,
    minHeight: 250,
    layout: 'fit',
    border: false,
    iconCls: 'fas fa-exclamation-circle',
    modal: true,
    buttons: ['->', {
        text: 'Đồng ý',
        ui: 'soft-orange'
    }, {
        text: 'Thoát',
        glyph: 'xf00d@FontAwesome',
        handler: function() {
            this.up('window').close();
        }
    }, '->'],
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'panel',
            layout: 'hbox',
            items: [{
                xtype: 'panel',
                width: 100,
                html: '<div style="text-align: center;margin-top: 40px;"><i style="color:#167abc;font-size:60px;" class="fas fa-ban"></i></div>'
            }, {
                flex: 1,
                xtype: 'form',
                bodyPadding: 15,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaultType: 'textareafield',
                items: [{
                    fieldLabel: '<b>Lý do hủy đơn đặt hàng</b>',
                    labelAlign: 'top',
                    empemptyText: 'Nhập lý do hủy đơn đặt hàng',
                }]
            }]
        }];
        me.callParent();
    }
});