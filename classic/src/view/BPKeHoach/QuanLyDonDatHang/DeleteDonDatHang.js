Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.DeleteDonDatHang', {
    extend: 'Ext.window.Window',
    xtype: 'DeleteDonDatHang',
    reference: 'DeleteDonDatHang',
    autoShow: true,
    title: 'Bạn có muốn xóa đơn đặt hàng?',
    height: 300,
    width: 400,
    minWidth: 300,
    minHeight: 300,
    layout: 'fit',
    border: false,
    iconCls: 'fas fa-exclamation-circle',
    modal: true,
    config: {
        recordView: null,
        gridView: null
    },
    initComponent: function() {
        var me = this;
        me.buttons = ['->', {
                text: 'Đồng ý',
                ui: 'soft-orange',
                iconCls: 'far fa-trash-alt',
                handler: function(btn) {
                    ClassMain.showLoadingMask();
                    Ext.Ajax.request({
                        url: '/api/ordersheet/delete/' + me.recordView.data.id,
                        method: 'GET',
                        success: function(result, request) {
                            try {
                                var response = Ext.decode(result.responseText);
                                if (response.success) {
                                    if (me.gridView) {
                                        me.gridView.getStore().load(function(records, operation, success) {
                                            ClassMain.hideLoadingMask();
                                            me.gridView.updateLayout();
                                            me.gridView.getSelectionModel().select(0);
                                        });
                                    }
                                } else {
                                    ClassMain.hideLoadingMask();
                                    Ext.MessageBox.alert('Thông báo', 'Không xóa được đơn đặt hàng', function() {});
                                }
                            } catch (err) {
                                console.log(err);
                                ClassMain.hideLoadingMask();
                                Ext.MessageBox.alert('Thông báo', 'Không xóa được đơn đặt hàng', function() {});
                            }
                        },
                        failure: function(result, request) {
                            ClassMain.hideLoadingMask();
                            Ext.MessageBox.alert('Thông báo', 'Không xóa được đơn đặt hàng', function() {});
                        }
                    });
                    me.hide();
                }
            },
            {
                text: 'Thoát',
                glyph: 'xf00d@FontAwesome',
                handler: function() {
                    me.close();
                }
            },
            '->'
        ];
        me.items = [{
            xtype: 'panel',
            layout: 'hbox',
            items: [{
                xtype: 'panel',
                width: 100,
                html: '<div style="text-align: center;margin-top: 50px;"><i style="color:#167abc;font-size:60px;" class="far fa-trash-alt"></i></div>'
            }, {
                flex: 1,
                xtype: 'form',
                fieldLabel: '<b>Bạn có muốn xóa đơn đặt hàng</b>',
                hideLabel: true,
                bodyPadding: 15,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaultType: 'textfield',
                items: [{
                    fieldLabel: '<b>Receive No</b>',
                    flex: 0.8,
                    value: me.recordView ? me.recordView.data.receive_no : ""
                }, {
                    fieldLabel: '<b>Building type</b>',
                    flex: 0.8,
                    value: me.recordView ? me.recordView.data.building_type : ""
                }, {
                    fieldLabel: '<b>Sub No</b>',
                    flex: 0.8,
                    value: me.recordView ? me.recordView.data.sub_no : ""
                }, {
                    fieldLabel: '<b>Project Name</b>',
                    flex: 0.8,
                    value: me.recordView ? me.recordView.data.project_name : ""
                }]
            }]
        }];
        me.callParent();
    }
});