/* eslint-disable eol-last */
Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.CoverPage', {
    extend: 'Ext.panel.Panel',
    controller: 'CoverPage',
    viewModel: 'CoverPage',
    xtype: 'CoverPage',
    config: {
        createOrderSheet: true,
    },
    initComponent() {
        const me = this;
        me.items = [{
            title: '<b> THÔNG TIN CHI TIẾT </b>',
            glyph: 'f039@FontAwesome',
            ui: 'light',
            header: false,
            cls: 'service-type shadow',
            xtype: 'form',
            bodyPadding: 20,
            margin: '0 0 20 0',
            items: [{
                xtype: 'fieldset',
                title: 'REQUEST FOR DRAWING & FABRICATION',
                defaultType: 'textfield',
                layout: 'column',
                defaults: {
                    xtype: 'container',
                    defaultType: 'textfield',
                    style: 'width: 50%',
                    padding: 10,
                },
                fieldDefaults: {
                    readOnly: true,
                },
                items: [{
                    items: [{
                            fieldLabel: '<b>Receive No.</b>',
                            blankText: 'Trường bắt buộc nhập thông tin',
                            afterLabelTextTpl: [
                                '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>',
                            ],
                            allowBlank: false,
                            labelWidth: 120,
                            editable: false,
                            width: '100%',
                            name: 'receive_no',
                            bind: '{dataPanel.orderSheet1.generalInfo.receive_no}',
                        },
                        {
                            fieldLabel: '<b>Building type</b>',
                            labelWidth: 120,
                            editable: false,
                            width: '100%',
                            name: 'building_type',
                            bind: '{dataPanel.orderSheet1.generalInfo.building_type}',
                        },
                        {
                            fieldLabel: '<b>Sub No.</b>',
                            blankText: 'Trường bắt buộc nhập thông tin',
                            afterLabelTextTpl: [
                                '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>',
                            ],
                            allowBlank: false,
                            labelWidth: 120,
                            editable: false,
                            width: '100%',
                            name: 'sub_no',
                            bind: '{dataPanel.orderSheet1.generalInfo.sub_no}',
                        },
                        {
                            fieldLabel: '<b>Project Name</b>',
                            labelWidth: 120,
                            width: '100%',
                            xtype: 'combobox',
                            allowBlank: false,
                            editable: false,
                            blankText: 'Trường bắt buộc nhập thông tin',
                            afterLabelTextTpl: [
                                '<span style="color:red;font-weight:bold" data-qtip="Bắt buộc"> (*)</span>',
                            ],
                            anyMatch: true,
                            reference: 'comboboxDuAn',
                            readOnly: !((ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'edit') === true && ClassMain.checkPermission('quan_ly_du_an', 'Project', 'create') === true)),
                            store: {
                                type: 'DanhMucDuAnStore',
                                autoLoad: true,
                                listeners: {
                                    load: me.getCreateOrderSheet() === true ? 'checkProjectExist' : 'projectExist',
                                    scope: me.getController(),
                                },
                            },
                            bind: {
                                value: '{dataPanel.orderSheet1.generalInfo.project_id}',
                            },
                            valueField: 'id',
                            displayField: 'name',
                            triggers: {
                                search: {
                                    weight: 1,
                                    cls: 'fas fa-plus',
                                    hidden: !((ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'edit') === true && ClassMain.checkPermission('quan_ly_du_an', 'Project', 'create') === true)),
                                    handler: 'onSearchProjectClick',
                                },
                            },
                        },
                    ],
                }, {
                    items: [{
                            fieldLabel: '<b>Project Incharge</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.generalInfo.pj_incharge}',
                        },
                        {
                            fieldLabel: '<b>Design Manager</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.generalInfo.dgm}',
                        },
                        {
                            fieldLabel: '<b>Designer</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.generalInfo.designer}',
                        },
                        {
                            fieldLabel: '<b>Sale Manager</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.generalInfo.sale_manager}',
                        },
                        {
                            fieldLabel: '<b>Sale Incharge</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.generalInfo.sale_incharge}',
                        },
                    ],
                }],
            }, {
                xtype: 'fieldset',
                title: 'DELIVERY INFO',
                defaultType: 'textfield',
                layout: 'column',
                defaults: {
                    xtype: 'container',
                    defaultType: 'textfield',
                    style: 'width: 50%',
                    padding: 10,
                },
                fieldDefaults: {
                    readOnly: true,
                },
                items: [{
                    items: [{
                            fieldLabel: '<b>Frame</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.deliveryInfo.frame} ({dataPanel.orderSheet1.deliveryInfo.frame_unit})',
                        },
                        {
                            fieldLabel: '<b>Leaf</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.deliveryInfo.leaf} ({dataPanel.orderSheet1.deliveryInfo.leaf_unit})',
                        },
                        {
                            fieldLabel: '<b>Other</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.deliveryInfo.other}',
                        },
                        {
                            fieldLabel: '<b>Delivery Place</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.deliveryInfo.delivery_place}',
                        },
                    ],
                }, {
                    items: [{
                            fieldLabel: '<b>Factory Out</b>',
                            editable: false,
                            xtype: 'datefield',
                            format: 'd-m-Y',
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.deliveryInfo.frame_factory_out}',
                        },
                        {
                            fieldLabel: '<b>Factory Out</b>',
                            editable: false,
                            xtype: 'datefield',
                            format: 'd-m-Y',
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.deliveryInfo.leaf_factory_out}',
                        },
                        {
                            fieldLabel: '<b>Factory Out</b>',
                            editable: false,
                            xtype: 'datefield',
                            format: 'd-m-Y',
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.deliveryInfo.other_factory_out}',
                        },
                        {
                            fieldLabel: '<b>Delivery By</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.deliveryInfo.delivery_by}',
                        },
                    ],
                }],
            }, {
                xtype: 'fieldset',
                title: 'REMARKS',
                defaultType: 'textfield',
                layout: 'anchor',
                items: [{
                    xtype: 'container',
                    anchor: '100%',
                    defaultType: 'textfield',
                    layout: 'column',
                    defaults: {
                        xtype: 'container',
                        defaultType: 'textfield',
                        style: 'width: 50%',
                        padding: 10,
                    },
                    fieldDefaults: {
                        readOnly: true,
                    },
                    items: [{
                        items: [{
                                fieldLabel: '<b>Address</b>',
                                editable: false,
                                labelWidth: 120,
                                width: '100%',
                                bind: '{dataPanel.orderSheet1.remarks.cus_address}',
                            },
                            {
                                fieldLabel: '<b>Customer Name</b>',
                                editable: false,
                                labelWidth: 120,
                                width: '100%',
                                bind: '{dataPanel.orderSheet1.remarks.cus_name}',
                            },
                            {
                                fieldLabel: '<b>Incharge</b>',
                                editable: false,
                                labelWidth: 120,
                                width: '100%',
                                bind: '{dataPanel.orderSheet1.remarks.cus_incharge}',
                            },
                        ],
                    }, {
                        items: [{
                                fieldLabel: '<b>TEL</b>',
                                editable: false,
                                labelWidth: 120,
                                width: '100%',
                                bind: '{dataPanel.orderSheet1.remarks.cus_tel}',
                            },
                            {
                                fieldLabel: '<b>Email</b>',
                                editable: false,
                                labelWidth: 120,
                                width: '100%',
                                bind: '{dataPanel.orderSheet1.remarks.cus_email}',
                            },
                        ],
                    }],
                }, {
                    fieldLabel: '<b>FINISHING PAINT</b>',
                    xtype: 'fieldcontainer',
                    labelAlign: 'top',
                    labelStyle: 'font-weight:bold;',
                    layout: 'column',
                    anchor: '100%',
                    defaults: {
                        xtype: 'container',
                        defaultType: 'textfield',
                        style: 'width: 50%',
                        padding: 10,
                    },
                    fieldDefaults: {
                        readOnly: true,
                    },
                    defaultType: 'textfield',
                    items: [{
                        items: [{
                            fieldLabel: '<b>Apply</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.remarks.paint_apply}',
                        }],
                    }, {
                        items: [{
                            fieldLabel: '<b>Color</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.remarks.paint_color}',
                        }],
                    }],
                }],
            }, {
                xtype: 'fieldset',
                title: 'DESIGNER',
                defaultType: 'textfield',
                layout: 'column',
                defaults: {
                    xtype: 'container',
                    defaultType: 'textfield',
                    style: 'width: 50%',
                    padding: 10,
                },
                fieldDefaults: {
                    readOnly: true,
                },
                items: [{
                    items: [{
                            fieldLabel: '<b>Item No.</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.noteDesign.des_item_no}',
                        },
                        {
                            fieldLabel: '<b>Change date</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.noteDesign.des_change_date}',
                        },
                        {
                            fieldLabel: '<b>Drawing No.</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.noteDesign.des_drawing_no}',
                        },
                    ],
                }, {
                    items: [{
                            fieldLabel: '<b>Times</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.noteDesign.des_version_order_sheet}',
                        },
                        {
                            fieldLabel: '<b>Date created</b>',
                            editable: false,
                            labelWidth: 120,
                            width: '100%',
                            bind: '{dataPanel.orderSheet1.noteDesign.des_date_create_time}',
                        },
                    ],
                }],
            }, {
                xtype: 'fieldset',
                title: 'NOTE(DESIGNER)',
                layout: 'anchor',
                items: [{
                    xtype: 'textareafield',
                    anchor: '100%',
                    emptyText: 'Nhập thông tin ghi chú từ bộ phận thiết kế',
                    name: 'note_designer',
                    fieldLabel: '',
                    hideLabel: true,
                    readOnly: true,
                    editable: false,
                    bind: '{dataPanel.orderSheet1.noteDesign.des_note}',
                }],
            }],
        }, {
            title: '<b> SỐ LƯỢNG CỬA ĐẶT HÀNG </b>',
            glyph: 'f039@FontAwesome',
            ui: 'light',
            cls: 'service-type shadow',
            xtype: 'grid',
            margin: '0 0 15 0',
            reference: 'soluongcuadathangGrid',
            columnLines: true,
            /* plugins: {
                ptype: 'cellediting',
                clicksToEdit: 2
            }, */
            selModel: {
                type: 'spreadsheet',
                columnSelect: true,
                pruneRemoved: false,
                extensible: 'y',
            },
            forceFit: true,
            viewConfig: {
                columnLines: true,
                trackOver: false,
            },
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
            bind: {
                store: '{SoLuongCuaDatHang}',
            },
            listeners: {
                cellkeydown: 'onItemKeydown',
            },
            columns: [{
                    text: 'TYPE',
                    flex: 1,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'type',
                    renderer(value) {
                        return `<b>${value.toUpperCase()}</b>`;
                    },
                    editor: false,
                }, {
                    text: 'SD',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'sd',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'FSD',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'fsd',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'LSD',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'lsd',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'FLSD',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'flsd',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'SS (Aichi)',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'ssaichi',
                    renderer(val) {
                        if (val === null) {
                            return '';
                        }
                        return val;
                    },
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'SS (JP)',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'ssjp',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'SS (GRILL)',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'ssgrill',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'GR-S',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'grs',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                },
                /* {
                               text: 'SR-N',
                               flex: 0.7,
                               sortable: false,
                               dataIndex: 'srn',
                               editor: false,
                               editor: {
                                   xtype: 'numberfield',
                                   selectOnFocus: true,
                                   hideTrigger: true,
                                   minValue: 0,
                                   allowBlank: false
                               }
                           }, */
                {
                    text: 'S-13',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 's13',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'S-14',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 's14',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'LV',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'lv',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'SW',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'sw',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'SLD',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'sld',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'SP',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'sp',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'WD',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'wd',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                }, {
                    text: 'SUSD',
                    flex: 0.7,
                    sortable: false,
                    dataIndex: 'susd',
                    editor: false,
                    /* editor: {
                        xtype: 'numberfield',
                        selectOnFocus: true,
                        hideTrigger: true,
                        minValue: 0,
                        allowBlank: false
                    } */
                },
            ],
        }, {
            title: '<b> HW TYPE </b>',
            glyph: 'f039@FontAwesome',
            ui: 'light',
            cls: 'service-type shadow',
            xtype: 'grid',
            reference: 'hwtypeGrid',
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
            margin: '0 0 15 0',
            columnLines: true,
            /* plugins: {
                ptype: 'cellediting',
                clicksToEdit: 2
            }, */
            listeners: {
                cellkeydown: 'onItemKeydown',
            },
            selModel: {
                type: 'spreadsheet',
                columnSelect: true,
                pruneRemoved: false,
                extensible: 'y',
            },
            forceFit: true,
            viewConfig: {
                columnLines: true,
                trackOver: false,
            },
            bind: {
                store: '{hwtype}',
            },
            columns: [{
                text: 'HW TYPE',
                width: 270,
                sortable: false,
                align: 'center',
                dataIndex: 'hw_type_name',
                renderer(value) {
                    return `<b>${value.toUpperCase()}</b>`;
                },
                editor: false,
            }, {
                text: 'SUPPLY BY',
                flex: 0.7,
                sortable: false,
                align: 'center',
                dataIndex: 'supply_name',
                editor: false,
                /* editor: {
                    allowBlank: true
                } */
            }, {
                text: 'SAMPLE',
                flex: 0.7,
                sortable: false,
                align: 'center',
                dataIndex: 'sample',
                editor: false,
                /* editor: {
                    allowBlank: true
                } */
            }],
        }, {
            title: '<b> CHANGE HISTORY </b>',
            glyph: 'f039@FontAwesome',
            ui: 'light',
            cls: 'service-type shadow',
            xtype: 'grid',
            reference: 'changeHistoryGrid',
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
            margin: '0 0 15 0',
            columnLines: true,
            /* plugins: {
                ptype: 'cellediting',
                clicksToEdit: 2
            }, */
            listeners: {
                cellkeydown: 'onItemKeydown',
            },
            selModel: {
                type: 'spreadsheet',
                columnSelect: true,
                pruneRemoved: false,
                extensible: 'y',
            },
            forceFit: true,
            viewConfig: {
                columnLines: true,
                trackOver: false,
            },
            // height: 300,
            autoScroll: true,
            bind: {
                store: '{changeHistory}',
            },
            columns: [{
                text: 'Item No.',
                width: 200,
                sortable: false,
                align: 'center',
                dataIndex: 'item_no',
                editor: false,
                /* editor: {
                    allowBlank: true
                } */
            }, {
                text: 'Date',
                flex: 0.7,
                sortable: false,
                dataIndex: 'date',
                editor: false,
                /* editor: {
                    xtype: 'datefield'
                } */
            }, {
                text: 'Change Before',
                flex: 0.7,
                sortable: false,
                dataIndex: 'change_before',
                editor: false,
                /* editor: {
                    allowBlank: true
                } */
            }, {
                text: 'Change After',
                flex: 0.7,
                sortable: false,
                dataIndex: 'change_after',
                editor: false,
                /* editor: {
                    allowBlank: true
                } */
            }, {
                text: 'Reasons',
                flex: 0.7,
                sortable: false,
                dataIndex: 'reasons',
                editor: false,
                /* editor: {
                    allowBlank: true
                } */
            }],
        }, {
            title: '<b> NOTE (SALES) </b>',
            glyph: 'f039@FontAwesome',
            ui: 'light',
            cls: 'service-type shadow',
            xtype: 'form',
            bodyPadding: 10,
            margin: '0 0 15 0',
            items: [{
                xtype: 'textareafield',
                emptyText: 'Nhập thông tin ghi chú từ bộ phận Sales',
                name: 'note_sales',
                fieldLabel: '',
                hideLabel: true,
                readOnly: true,
                editable: false,
                bind: '{dataPanel.orderSheet1.noteSale.sale_note}',
            }],
        }];
        me.callParent();
    },
});