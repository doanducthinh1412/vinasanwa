/* eslint-disable prefer-const */
Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.QuanLyPhienBan', {
    extend: 'Ext.grid.Panel',
    controller: 'BPKH_QuanLyPhienBan',
    viewModel: 'BPKH_QuanLyPhienBan',
    xtype: 'BPKH_QuanLyPhienBan',
    columnLines: true,
    height: '100%',
    layout: 'fit',
    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
    config: {
        dataObject: null,
    },
    initComponent() {
        let me = this;
        let dataObject = me.getDataObject();
        if (dataObject !== null) {
            if (dataObject.order_sheet_id) {
                me.store = {
                    type: 'BPKH_QuanLyPhienBanStore',
                    proxy: {
                        url: `/api/ordersheet/history/${dataObject.order_sheet_id}`,
                    },
                    autoLoad: true,
                };
            } else {
                me.store = {
                    type: 'BPKH_QuanLyPhienBanStore',
                    autoLoad: true,
                    data: [],
                };
            }
        } else {
            me.store = {
                type: 'BPKH_QuanLyPhienBanStore',
                autoLoad: true,
                data: [],
            };
        }

        me.actions = {
            restore: {
                iconCls: 'fas fa-history text-blue',
                handler: 'onRestoreClick',
                scope: me.getController(),
            },
            view: {
                iconCls: 'far fa-eye text-blue',
                tooltip: 'Xem bản vẽ',
                handler: 'onViewClick',
                scope: me.getController(),
            },
            download: {
                iconCls: 'fas fa-download text-blue',
                tooltip: 'Tải bản vẽ',
                handler: 'onDownloadClick',
                scope: me.getController(),
            },
        };
        me.columns = [{
            xtype: 'rownumberer',
            text: '<br><b>STT</b><br><br>',
            width: 65,
            align: 'center',
        }, {
            text: '<b>Thao tác</b>',
            xtype: 'actioncolumn',
            width: 80,
            sortable: false,
            menuDisabled: true,
            align: 'center',
            items: [{
                iconCls: 'far fa-file-pdf text-blue',
                tooltip: 'Xem File phiên bản đơn đặt hàng',
                handler: 'onViewClick',
            }, {
                width: 3,
            }, {
                iconCls: 'fas fa-download text-blue',
                tooltip: 'Tải File phiên bản đơn đặt hàng',
                handler: 'onDownloadClick',
            }],
        }, {
            menuDisabled: true,
            sortable: false,
            text: '<br>Phiên bản OrderSheet<br><br>',
            flex: 0.5,
            align: 'center',
            dataIndex: 'created_at',
            renderer: 'renderVersionColumn',
        }, {
            menuDisabled: true,
            sortable: false,
            text: '<br>Thời gian cập nhật<br><br>',
            flex: 0.5,
            align: 'center',
            dataIndex: 'created_at',
            renderer: 'renderDateColumn',
        }, {
            menuDisabled: true,
            sortable: false,
            text: '<br>Người cập nhật<br><br>',
            flex: 0.5,
            align: 'center',
            dataIndex: 'created_by',
        }, {
            menuDisabled: true,
            sortable: false,
            text: '<br>Nội dung cập nhật<br><br>',
            width: 195,
            align: 'center',
            xtype: 'widgetcolumn',
            widget: {
                xtype: 'button',
                ui: 'soft-orange',
                text: 'Xem thông tin thay đổi',
                iconCls: 'fas fa-history',
                handler: 'onButtonHistoryClick',
            },
        }];
        me.callParent();
    },
});
