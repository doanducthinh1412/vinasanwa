/* eslint-disable eol-last */
/* eslint-disable consistent-return */
/* eslint-disable no-console */
/* eslint-disable radix */
/* eslint-disable camelcase */
Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.ViewDonDatHang', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.grid.selection.SpreadsheetModel',
        'Ext.grid.plugin.Clipboard',
        'Ext.layout.container.Absolute',
    ],
    controller: 'ViewDonDatHang',
    viewModel: 'ViewDonDatHang',
    xtype: 'ViewDonDatHang',
    defaultType: 'textfield',
    bodyBorder: true,
    config: {
        gridView: null,
        dataObject: null,
        doorType: null,
        modeEdit: false,
        modeVersion: false,
    },
    layout: 'fit',
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'right',
        autoFitErrors: true,
        width: '100%',
    },
    autoScroll: false,
    initComponent() {
        const me = this;
        const dataPanel = me.getDataObject();
        let listDetails = [];
        const viewModel = me.getController().getViewModel();
        const modeVersion = me.getModeVersion();
        if (dataPanel) {
            if (dataPanel.orderSheet2) {
                listDetails = dataPanel.orderSheet2.rows;
            }
            if (dataPanel.status) {
                const status_order = dataPanel.status;
                if (status_order.drawing_department) {
                    const drawing_department = parseInt(status_order.drawing_department);
                    if (drawing_department === 0) {
                        viewModel.set('workflow.ve', {
                            code: 0,
                            status: 'Chưa phân công',
                            color: '#e13345',
                            time: '',
                        });
                    } else if (drawing_department === 1) {
                        viewModel.set('workflow.ve', {
                            code: 1,
                            status: 'Đang thực hiện',
                            color: '#126398',
                            time: '',
                        });
                    } else if (drawing_department === 2) {
                        viewModel.set('workflow.ve', {
                            code: 2,
                            status: 'Đã hoàn thành',
                            color: '#90b962',
                            time: '',
                        });
                    } else if (drawing_department === 3) {
                        viewModel.set('workflow.ve', {
                            code: 3,
                            status: 'Đã gửi kết quả vẽ',
                            color: '#90b962',
                            time: '',
                        });
                    }
                }
                if (status_order.order_sheet_price) {
                    const order_sheet_price = parseInt(status_order.order_sheet_price);
                    if (order_sheet_price === 0) {
                        viewModel.set('workflow.tinhgia', {
                            code: 0,
                            status: 'Chưa tính giá',
                            color: '#e13345',
                            time: '',
                        });
                    } else if (order_sheet_price === 1) {
                        viewModel.set('workflow.tinhgia', {
                            code: 1,
                            status: 'Hoàn thành tính giá',
                            color: '#90b962',
                            time: '',
                        });
                    } else if (order_sheet_price === 2) {
                        viewModel.set('workflow.tinhgia', {
                            code: 2,
                            status: 'Đã gửi kết quả tính giá',
                            color: '#90b962',
                            time: '',
                        });
                    }
                }
                if (status_order.production_schedule) {
                    const production_schedule = parseInt(status_order.production_schedule);
                    if (production_schedule === 0) {
                        viewModel.set('workflow.kehoach', {
                            code: 0,
                            status: 'Chưa lập kế hoạch',
                            color: '#e13345',
                            time: '',
                        });
                    } else if (production_schedule === 1) {
                        viewModel.set('workflow.kehoach', {
                            code: 1,
                            status: 'Đang lập kế hoạch',
                            color: '#126398',
                            time: '',
                        });
                    } else if (production_schedule === 2) {
                        viewModel.set('workflow.kehoach', {
                            code: 2,
                            status: 'Đã hoàn thành',
                            color: '#90b962',
                            time: '',
                        });
                    }
                }
            }
        }
        me.getController().getViewModel().set('dataPanel', dataPanel);
        const listItems = [{
                title: '<b>THÔNG TIN<br>CHUNG</b>',
                iconCls: 'fas fa-info-circle',
                autoScroll: true,
                iconAlign: 'top',
                itemId: 'ThongTinChungTab',
                items: me.createTabThongTinChung(),
            },
            {
                title: '<b>THEO DÕI<br>ĐƠN ĐẶT HÀNG</b>',
                iconCls: 'fas fa-share-alt',
                iconAlign: 'top',
                layout: 'fit',
                itemId: 'TheoDoiDonDatHangTab',
                hidden: modeVersion === true,
                items: me.createTabTheoDoiDonDatHang(),
            },
            {
                title: '<b>BẢN VẼ<br>THIẾT KẾ</b>',
                iconCls: 'fas fa-drafting-compass',
                autoScroll: true,
                layout: 'fit',
                iconAlign: 'top',
                itemId: 'BanVeThietKeTab',
                items: me.createTabBanVeThietKe(),
            },
        ];
        for (let i = 0; i < listDetails.length; i += 1) {
            listItems.push({
                title: '<b>THÔNG TIN<br>CHI TIẾT</b>',
                iconCls: 'fas fa-tasks',
                iconAlign: 'top',
                layout: 'fit',
                itemId: 'SoLuongChiTietTab',
                items: me.createTabThongTinChiTiet(listDetails[i].data),
            });
        }

        listItems.push({
            title: '<b>QUẢN LÝ<br>PHIÊN BẢN</b>',
            iconCls: 'fas fa-history',
            iconAlign: 'top',
            layout: 'fit',
            itemId: 'QuanLyPhienBanTab',
            hidden: modeVersion === true,
            items: me.createTabQuanLyPhienBan(),
        });

        me.items = {
            xtype: 'tabpanel',
            layout: 'fit',
            tabPosition: 'left',
            tabRotation: 0,
            cls: 'tabxemchitiet',
            plain: true,
            style: {
                'background-image': 'url(resources/images/square.gif)',
            },
            listeners: {
                /* beforetabchange(tabPanel, newCard, oldCard, eOpts) {
                    ClassMain.showLoadingMask();
                    var task = new Ext.util.DelayedTask(function() {
                        if (newCard.itemId === "SoLuongChiTietTab") {
                            newCard.add(me.createTabSoLuongChiTiet());
                        } else if (newCard.itemId === "ThongTinChungTab") {
                            newCard.add(me.createTabThongTinChung());
                        } else if (newCard.itemId === "BanVeThietKeTab") {
                            newCard.add(me.createTabBanVeThietKe());
                        } else if (newCard.itemId === "TheoDoiDonDatHangTab") {
                            newCard.add(me.createTabTheoDoiDonDatHang());
                        } else {
                            ClassMain.hideLoadingMask();
                        }
                    });
                    task.delay(100);
                },
                tabchange(tabPanel, newCard, oldCard, eOpts) {
                    newCard.updateLayout();
                    oldCard.removeAll();
                    oldCard.updateLayout();
                }, */
            },
            items: listItems,
        };
        me.tbar = [{
                xtype: 'combobox',
                fieldLabel: '<b>Loại đơn</b>',
                labelAlign: 'left',
                reference: 'ComboboxLoaiDon',
                store: {
                    type: 'LoaiDonDatHangStore',
                },
                queryMode: 'local',
                editable: false,
                labelWidth: 80,
                width: 300,
                displayField: 'name',
                bind: '{dataPanel.orderSheet1.generalInfo.order_type}',
                valueField: 'val',
                allowBlank: false,
                readOnly: modeVersion === true,
                listeners: {
                    select: 'onSelectLoaiDon',
                },
            }, {
                xtype: 'combobox',
                labelAlign: 'left',
                reference: 'ComboboxDoUuTien',
                fieldLabel: '<b>Độ ưu tiên</b>',
                store: {
                    type: 'DoUuTienDonDatHangStore',
                },
                queryMode: 'local',
                editable: false,
                labelWidth: 80,
                width: 300,
                displayField: 'name',
                bind: '{dataPanel.orderSheet1.generalInfo.priority}',
                allowBlank: false,
                valueField: 'val',
                readOnly: modeVersion === true,
                listeners: {
                    select: 'onSelectDoUuTien',
                },
            }, {
                xtype: 'combobox',
                labelAlign: 'left',
                fieldLabel: '<b>Ghi chú</b>',
                store: Ext.create('Ext.data.Store', {
                    fields: ['val'],
                    data: [
                        { val: 'Cửa nhập khẩu' },
                        { val: 'Cửa màu' },
                        { val: 'Cửa Test' },
                    ],
                }),
                queryMode: 'local',
                editable: true,
                labelWidth: 60,
                width: 250,
                displayField: 'val',
                reference: 'ComboboxGhiChu',
                bind: '{dataPanel.orderSheet1.generalInfo.note}',
                valueField: 'val',
                typeAhead: true,
                allowBlank: true,
                enableKeyEvents: true,
                readOnly: modeVersion === true,
                listeners: {
                    keypress: 'onEnterGhiChu',
                    select: 'onSelectGhiChu',
                },
            }, '->', {
                text: 'Cập nhật đơn đặt hàng',
                glyph: 'xf0c7@FontAwesome',
                ui: 'soft-orange',
                hidden: !((parseInt(dataPanel.status.order_sheet) === 0 && ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'edit') === true && ClassMain.checkPermission('quan_ly_du_an', 'Project', 'create') === true) && modeVersion !== true),
                handler: 'onEditButtonClick',
            }, '-',
            {
                text: 'Công cụ',
                iconCls: 'fas fa-briefcase',
                ui: 'dark-blue',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    mouseLeaveDelay: 10,
                    items: [{
                        text: 'Tải File - Đơn đặt hàng',
                        iconCls: 'fas fa-file-export text-dark-blue',
                        handler: 'onExportdButtonClick',
                    }, {
                        text: 'Xem File - Đơn đặt hàng',
                        iconCls: 'far fa-file-pdf text-dark-blue',
                        handler: 'onViewFileButtonClick',
                    }],
                },
            },
        ];
        me.callParent();
    },
    createTabThongTinChung() {
        const coverPagePanel = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.CoverPage', {
            createOrderSheet: false,
        });
        return coverPagePanel;
    },

    createTabThongTinChiTiet(data) {
        try {
            const me = this;
            let doorType = null;
            const dataPanel = me.getDataObject();
            if (dataPanel.frame_type) {
                doorType = dataPanel.frame_type;
            }
            if (doorType) {
                const { pj_name } = me.getDataObject().orderSheet1.generalInfo;
                let item = [];
                if ((doorType === 'sd') || (doorType === 'fsd') || (doorType === 'lsd') || (doorType === 'flsd')) {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SDDetailPage', {
                        dataPanel: data,
                    });
                } else if ((doorType === 'ssaichi') || (doorType === 'ssjp')) {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SSDetailPage', {
                        dataPanel: data,
                        doorType,
                        projectName: pj_name,
                    });
                } else if (doorType === 'ssgrill') {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SSGrillDetailPage', {
                        dataPanel: data,
                        doorType,
                        projectName: pj_name,
                    });
                } else if ((doorType === 'grs') || (doorType === 'srn') || (doorType === 's13') || (doorType === 's14')) {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.QSDetailPage', {
                        dataPanel: data,
                        doorType,
                        projectName: pj_name,
                    });
                } else if (doorType === 'lv') {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.LVDetailPage', {
                        dataPanel: data,
                        // doorType: doorType,
                        // projectName: pj_name
                    });
                } else if (doorType === 'sld') {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SMDetailPage', {
                        dataPanel: data,
                        // doorType: doorType,
                        // projectName: pj_name
                    });
                } else if (doorType === 'sp') {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SPDetailPage', {
                        dataPanel: data,
                        // doorType: doorType,
                        projectName: pj_name,
                    });
                } else if (doorType === 'sw') {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SWDetailPage', {
                        dataPanel: data,
                        // doorType: doorType,
                        projectName: pj_name,
                    });
                } else if (doorType === 'susd') {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SUSDDetailPage', {
                        dataPanel: data,
                        // doorType: doorType,
                        projectName: pj_name,
                    });
                } else if (doorType === 'wd') {
                    item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SUSDDetailPage', {
                        dataPanel: data,
                        // doorType: doorType,
                        projectName: pj_name,
                    });
                }
                return item;
            }
            return [];
        } catch (err) {
            console.log(err);
            return [];
        }
    },

    createTabBanVeThietKe() {
        const me = this;
        if (me.getController().getViewModel().get('dataPanel').banvethietke) {
            const item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.BanVeThietKe', {
                dataObject: me.getDataObject(),
                onlineFile: true,
                dataFile: me.getController().getViewModel().get('dataPanel').banvethietke,
            });
            return item;
        }
        return {
            xtype: 'panel',
            html: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có bản vẽ</center></div>',
        };
    },

    createTabQuanLyPhienBan() {
        try {
            const me = this;
            const dataPanel = me.getDataObject();
            const item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.QuanLyPhienBan', {
                dataObject: dataPanel,
            });
            return item;
        } catch (err) {
            console.log(err);
            return [];
        }
    },

    createTabTheoDoiDonDatHang() {
        try {
            return {
                listeners: {
                    render() {
                        ClassMain.hideLoadingMask();
                    },
                },
                width: 1500,
                height: 520,
                autoScroll: true,
                layout: 'absolute',
                plain: true,
                border: false,
                bodyBorder: false,
                bodyStyle: {
                    'background-color': '#e2e2e2',
                },
                tbar: ['->', {
                    text: '<b>ĐÃ HOÀN THÀNH</b>',
                    ui: 'soft-green',
                    width: 135,
                }, {
                    text: '<b>ĐANG THỰC HIỆN</b>',
                    ui: 'soft-blue',
                    width: 135,
                }, {
                    text: '<b>CHƯA THỰC HIỆN</b>',
                    ui: 'soft-red',
                    width: 135,
                }, '->'],
                defaults: {
                    bodyPadding: 5,
                    height: 130,
                    width: 200,
                    ui: 'light',
                    titleAlign: 'center',
                    cls: 'service-type shadow',
                    xtype: 'panel',
                    listeners: {
                        render: 'renderPanelWorkFlow',
                    },
                },
                items: [{
                    xtype: 'draw',
                    width: 1500,
                    height: 520,
                    bodyBorder: false,
                    border: false,
                    frame: false,
                    bodyStyle: {
                        'background-color': '#e2e2e2',
                    },
                    sprites: [{
                        type: 'image',
                        src: Ext.getResourcePath('images/arrow.png'),
                        x: 220,
                        y: 120,
                        width: 130,
                        height: 30,
                        rotation: {
                            degrees: 50,
                        },
                    }, {
                        type: 'image',
                        src: Ext.getResourcePath('images/arrow.png'),
                        x: 220,
                        y: 250,
                        width: 100,
                        height: 30,
                    }, {
                        type: 'image',
                        src: Ext.getResourcePath('images/arrow.png'),
                        x: 220,
                        y: 380,
                        width: 130,
                        height: 30,
                        rotation: {
                            degrees: -50,
                        },
                    }, {
                        type: 'image',
                        src: Ext.getResourcePath('images/arrow.png'),
                        x: 540,
                        y: 250,
                        width: 100,
                        height: 30,
                    }, {
                        type: 'image',
                        src: Ext.getResourcePath('images/arrow.png'),
                        x: 820,
                        y: 250,
                        width: 100,
                        height: 30,
                    }, {
                        type: 'image',
                        src: Ext.getResourcePath('images/arrow.png'),
                        x: 1100,
                        y: 250,
                        width: 100,
                        height: 30,
                    }],
                }, {
                    x: 50,
                    y: 20,
                    title: '<font style="font-size:14px;">BỘ PHẬN VẼ</font>',
                    itemId: 'workflowVe',
                    iconCls: 'fas fa-drafting-compass',
                    bind: {
                        html: '<div class="div_workflow"><p>+ Trạng thái: {workflow.ve.status}</p><p>+ Thời gian: {workflow.ve.time}</p></div>',
                        bodyStyle: {
                            'background-color': '{workflow.ve.color}',
                            cursor: 'pointer',
                        },
                    },
                }, {
                    x: 50,
                    y: 200,
                    title: '<font style="font-size:14px;">BỘ PHẬN TÍNH GIÁ</font>',
                    itemId: 'workflowTinhGia',
                    iconCls: 'fas fa-hand-holding-usd',
                    bind: {
                        html: '<div class="div_workflow"><p>+ Trạng thái: {workflow.tinhgia.status}</p><p>+ Thời gian: {workflow.tinhgia.time}</p></div>',
                        bodyStyle: {
                            'background-color': '{workflow.tinhgia.color}',
                            cursor: 'pointer',
                        },
                    },
                }, {
                    x: 50,
                    y: 380,
                    title: '<font style="font-size:14px;">BỘ PHẬN MUA HÀNG</font>',
                    itemId: 'workflowMuaHang',
                    iconCls: 'fas fa-cart-plus',
                    bind: {
                        html: '<div class="div_workflow"><p>+ Trạng thái: {workflow.muahang.status}</p><p>+ Thời gian: {workflow.muahang.time}</p></div>',
                        bodyStyle: {
                            'background-color': '{workflow.muahang.color}',
                            cursor: 'pointer',
                        },
                    },
                }, {
                    x: 320,
                    y: 200,
                    width: 240,
                    title: '<font style="font-size:14px;">LẬP KẾ HOẠCH SẢN XUẤT</font>',
                    itemId: 'workflowLapKeHoachSanXuat',
                    iconCls: 'fas fa-clipboard-list',
                    bind: {
                        html: '<div class="div_workflow"><p>+ Trạng thái: {workflow.kehoach.status}</p><p>+ Thời gian: {workflow.kehoach.time}</p></div>',
                        bodyStyle: {
                            'background-color': '{workflow.kehoach.color}',
                            cursor: 'pointer',
                        },
                    },
                }, {
                    x: 640,
                    y: 200,
                    title: '<font style="font-size:14px;">BỘ PHẬN SẢN XUẤT</font>',
                    itemId: 'workflowSanXuat',
                    iconCls: 'fas fa-wrench',
                    bind: {
                        html: '<div class="div_workflow"><p>+ Trạng thái: {workflow.sanxuat.status}</p><p>+ Thời gian: {workflow.sanxuat.time}</p></div>',
                        bodyStyle: {
                            'background-color': '{workflow.sanxuat.color}',
                            cursor: 'pointer',
                        },
                    },
                }, {
                    x: 920,
                    y: 200,
                    title: '<font style="font-size:14px;">BỘ PHẬN KCS</font>',
                    itemId: 'workflowKCS',
                    iconCls: 'fas fa-clipboard-check',
                    bind: {
                        html: '<div class="div_workflow"><p>+ Trạng thái: {workflow.kcs.status}</p><p>+ Thời gian: {workflow.kcs.time}</p></div>',
                        bodyStyle: {
                            'background-color': '{workflow.kcs.color}',
                            cursor: 'pointer',
                        },
                    },
                }, {
                    x: 1200,
                    y: 200,
                    title: '<font style="font-size:14px;">BỘ PHẬN GIAO HÀNG</font>',
                    itemId: 'workflowGiaoHang',
                    iconCls: 'fas fa-truck',
                    bind: {
                        html: '<div class="div_workflow"><p>+ Trạng thái: {workflow.giaohang.status}</p><p>+ Thời gian: {workflow.giaohang.time}</p></div>',
                        bodyStyle: {
                            'background-color': '{workflow.giaohang.color}',
                            cursor: 'pointer',
                        },
                    },
                }, {
                    x: 920,
                    y: 20,
                    title: '<font style="font-size:14px;">BỘ PHẬN KHO</font>',
                    itemId: 'workflowKho',
                    iconCls: 'fas fa-warehouse',
                    bind: {
                        html: '<div class="div_workflow"><p>+ Trạng thái: {workflow.kho.status}</p><p>+ Thời gian: {workflow.kho.time}</p></div>',
                        bodyStyle: {
                            'background-color': '{workflow.kho.color}',
                            cursor: 'pointer',
                        },
                    },
                }],
            };
        } catch (err) {
            console.log(err);
        }
    },
});