/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPKeHoach.CreateDonDatHang.CreateDonDatHang', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.grid.selection.SpreadsheetModel',
        'Ext.grid.plugin.Clipboard',
        'Ext.grid.plugin.CellEditing',
    ],
    controller: 'CreateDonDatHang',
    viewModel: 'CreateDonDatHang',
    xtype: 'CreateDonDatHang',
    defaultType: 'textfield',
    bodyBorder: true,
    config: {
        gridView: null,
        dataOrderSheet: null,
        dataFile: null,
        dataPanel: null,
        doorType: null,
        recordView: null,
        mode: null,
    },
    layout: 'fit',
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'right',
        autoFitErrors: true,
        width: '100%',
    },
    autoScroll: false,
    initComponent() {
        const me = this;
        const mode = me.getMode();
        const dataPanel = me.getDataPanel();
        const recordView = me.getRecordView();
        let listDetails = [];
        if (dataPanel) {
            if (dataPanel.orderSheet2) {
                listDetails = dataPanel.orderSheet2.rows;
            }
        }
        // console.log(me.getDataOrderSheet());
        me.getController().getViewModel().set('dataPanel', dataPanel);
        me.getController().getViewModel().set('dataPanel.orderSheet1.generalInfo.order_type', me.getDataOrderSheet() !== null ? me.getDataOrderSheet().loaidon : null);
        me.getController().getViewModel().set('dataPanel.orderSheet1.generalInfo.priority', me.getDataOrderSheet() !== null ? me.getDataOrderSheet().douutien : null);
        me.getController().getViewModel().set('dataPanel.orderSheet1.generalInfo.note', me.getDataOrderSheet() !== null ? me.getDataOrderSheet().note : null);
        // mode = 1: loại cửa chỉ có 1 file cover + detail
        // mode = 2: loại cửa có 1 file cover và nhiều file detail
        const listItems = [{
            title: '<b>THÔNG TIN<br>CHUNG</b>',
            iconCls: 'fas fa-info-circle',
            autoScroll: true,
            iconAlign: 'top',
            reference: 'ThongTinChungTab',
            items: me.createTabThongTinChung(),
        }];

        listItems.push({
            title: '<b>BẢN VẼ<br>THIẾT KẾ</b>',
            iconCls: 'fas fa-file-pdf',
            iconAlign: 'top',
            layout: 'fit',
            reference: 'BanVeThietKeTab',
            items: me.createTabBanVeThietKe(),
        });
        // console.log(listDetails);
        for (let i = 0; i < listDetails.length; i += 1) {
            listItems.push({
                title: '<b>THÔNG TIN<br>CHI TIẾT</b>',
                iconCls: 'fas fa-tasks',
                iconAlign: 'top',
                layout: 'fit',
                items: me.createTabDetail(listDetails[i].data),
            });
        }
        me.items = {
            xtype: 'tabpanel',
            layout: 'fit',
            tabPosition: 'left',
            tabRotation: 0,
            cls: 'tabxemchitiet',
            plain: true,
            style: {
                'background-image': 'url(resources/images/square.gif)',
            },
            listeners: {
                beforetabchange(tabPanel, newCard, oldCard, eOpts) {
                    /* ClassMain.showLoadingMask();
                    if (newCard.reference === "SoLuongChiTietTab") {
                        var task = new Ext.util.DelayedTask(function() {
                            newCard.add(me.createTabSoLuongChiTiet());
                        });
                        task.delay(100);
                    } else if (newCard.reference === "ThongTinChungTab") {
                        //newCard.add(me.createTabThongTinChung());
                        ClassMain.hideLoadingMask();
                    } */
                },
                tabchange(tabPanel, newCard, oldCard, eOpts) {
                    /* if (oldCard.reference !== "ThongTinChungTab") {
                        oldCard.removeAll();
                        oldCard.updateLayout();
                    }
                    newCard.updateLayout(); */
                },
            },
            items: listItems,
        };


        me.tbar = [{
            xtype: 'combobox',
            fieldLabel: '<b>Loại đơn</b>',
            labelAlign: 'left',
            reference: 'ComboboxLoaiDon',
            store: {
                type: 'LoaiDonDatHangStore',
            },
            queryMode: 'local',
            editable: false,
            labelWidth: 80,
            width: 300,
            displayField: 'name',
            bind: '{dataPanel.orderSheet1.generalInfo.order_type}',
            valueField: 'val',
            allowBlank: false,
            listeners: {
                select: 'onSelectLoaiDon',
            },
        }, {
            xtype: 'combobox',
            labelAlign: 'left',
            fieldLabel: '<b>Độ ưu tiên</b>',
            store: {
                type: 'DoUuTienDonDatHangStore',
            },
            queryMode: 'local',
            editable: false,
            labelWidth: 80,
            width: 300,
            displayField: 'name',
            reference: 'ComboboxDoUuTien',
            bind: '{dataPanel.orderSheet1.generalInfo.priority}',
            valueField: 'val',
            allowBlank: false,
        }, {
            xtype: 'combobox',
            labelAlign: 'left',
            fieldLabel: '<b>Ghi chú</b>',
            store: Ext.create('Ext.data.Store', {
                fields: ['val'],
                data: [
                    { val: 'Cửa nhập khẩu' },
                    { val: 'Cửa màu' },
                    { val: 'Cửa Test' },
                ],
            }),
            queryMode: 'local',
            editable: true,
            labelWidth: 60,
            width: 250,
            displayField: 'val',
            reference: 'ComboboxGhiChu',
            bind: '{dataPanel.orderSheet1.generalInfo.note}',
            valueField: 'val',
            typeAhead: true,
            allowBlank: true,
        }, '->', {
            text: recordView === null ? 'Tạo mới' : 'Cập nhật đơn đặt hàng',
            glyph: 'xf0c7@FontAwesome',
            ui: 'soft-orange',
            disabled: true,
            formBind: true,
            handler: 'onCreateButtonClick',
        }];
        me.callParent();
    },

    createTabThongTinChung() {
        const me = this;
        const dataPanel = me.getDataPanel();
        const coverPage = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.CoverPage', {});
        return coverPage;
    },

    createTabDetail(data) {
        const me = this;
        const { pj_name } = me.getDataPanel().orderSheet1.generalInfo;
        const doorType = me.getDoorType();
        let item = [];
        if ((doorType === 'sd') || (doorType === 'fsd') || (doorType === 'lsd') || (doorType === 'flsd') || (doorType === 'susd') || (doorType === 'wd')) {
            item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SDDetailPage', {
                dataPanel: data,
            });
        } else if ((doorType === 'ssaichi') || (doorType === 'ssjp')) {
            item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SSDetailPage', {
                dataPanel: data,
                doorType,
                projectName: pj_name,
            });
        } else if (doorType === 'ssgrill') {
            item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SSGrillDetailPage', {
                dataPanel: data,
                doorType,
                projectName: pj_name,
            });
        } else if ((doorType === 'grs') || (doorType === 'srn') || (doorType === 's13') || (doorType === 's14')) {
            item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.QSDetailPage', {
                dataPanel: data,
                doorType,
                projectName: pj_name,
            });
        } else if (doorType === 'lv') {
            item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.LVDetailPage', {
                dataPanel: data,
                // doorType: doorType,
                // projectName: pj_name
            });
        } else if (doorType === 'sld') {
            item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SMDetailPage', {
                dataPanel: data,
                // doorType: doorType,
                // projectName: pj_name
            });
        } else if (doorType === 'sp') {
            item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SPDetailPage', {
                dataPanel: data,
                // doorType: doorType,
                // projectName: pj_name
            });
        } else if (doorType === 'sw') {
            item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.SWDetailPage', {
                dataPanel: data,
                // doorType: doorType,
                // projectName: pj_name
            });
        }
        return item;
    },
    createTabBanVeThietKe() {
        const me = this;
        const item = Ext.create('CMS.view.BPKeHoach.QuanLyDonDatHang.BanVeThietKe', {
            onlineFile: false,
            dataFile: me.getDataFile(),
        });
        return item;
    },
});