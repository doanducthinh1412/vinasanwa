Ext.define('CMS.view.BPKeHoach.QuanLyDonDatHang.SendDonDatHang', {
    extend: 'Ext.window.Window',
    xtype: 'SendDonDatHang',
    reference: 'SendDonDatHang',
    controller: 'SendDonDatHang',
    viewModel: 'SendDonDatHang',
    title: 'Chuyển xử lý đơn đặt hàng',
    height: 250,
    width: 400,
    minWidth: 300,
    minHeight: 250,
    layout: 'fit',
    config: {
        recordData: null,
        gridPanel: null
    },
    border: false,
    iconCls: 'fas fa-paper-plane',
    modal: true,
    autoShow: true,
    buttons: ['->', {
        text: 'Đồng ý',
        ui: 'soft-orange',
        handler: 'onClickSendDonDatHangButton'
    }, {
        text: 'Thoát',
        glyph: 'xf00d@FontAwesome',
        handler: function() {
            this.up('window').close();
        }
    }, '->'],
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            layout: 'hbox',
            items: [{
                xtype: 'panel',
                width: 120,
                html: '<div style="text-align: center;margin-top: 40px;"><i style="color:#167abc;font-size:60px;" class="fas fa-paper-plane"></i></div>'
            }, {
                flex: 1,
                xtype: 'checkboxgroup',
                defaultType: 'checkbox',
                hideLabel: true,
                margin: '15 0 0 0',
                reference: 'ChuyenXuLyCheckBox',
                columns: 1,
                items: [{
                    boxLabel: 'Bộ phận tính giá',
                    name: '3',
                    checked: true
                }, {
                    boxLabel: 'Bộ phận vẽ',
                    checked: true,
                    name: '2'
                }, {
                    checked: true,
                    boxLabel: 'Bộ phận mua hàng',
                    name: '4'
                }]
            }]
        }];
        me.callParent();
    }
});