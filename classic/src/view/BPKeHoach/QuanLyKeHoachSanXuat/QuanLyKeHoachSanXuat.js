/* eslint-disable eol-last */
/* eslint-disable consistent-return */
/* eslint-disable camelcase */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-unused-vars */
Ext.define('CMS.view.BPKeHoach.QuanLyKeHoachSanXuat.QuanLyKeHoachSanXuat', {
    extend: 'Ext.panel.Panel',
    controller: 'QuanLyKeHoachSanXuat',
    viewModel: 'QuanLyKeHoachSanXuat',
    xtype: 'QuanLyKeHoachSanXuat',
    layout: 'fit',
    autoScroll: false,
    config: {
        maxDateKeHoach: new Date(),
    },
    requires: [
        'Ext.selection.CellModel',
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'Ext.grid.selection.SpreadsheetModel',
        'Ext.grid.plugin.Clipboard',
        'Ext.grid.plugin.CellEditing',
        'Ext.grid.selection.Replicator',
        'Ext.grid.feature.Grouping',
    ],
    reference: 'QuanLyKeHoachSanXuatPanel',
    initComponent() {
        const me = this;
        me.SoLuongBanVeStore = Ext.create('CMS.store.SoLuongBanVe', {
            type: 'SoLuongBanVeStore',
            pageSize: 0,
            proxy: {
                type: 'ajax',
                url: '/api/drawing/remain?number=gt_0',
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    totalProperty: 'total',
                },
            },
        });
        me.items = {
            xtype: 'tabpanel',
            layout: 'fit',
            tabPosition: 'left',
            tabRotation: 0,
            cls: 'tabxemchitiet',
            plain: true,
            style: {
                'background-image': 'url(resources/images/square.gif)',
            },
            listeners: {
                beforetabchange(tabPanel, newCard, oldCard, eOpts) {},
                tabchange(tabPanel, newCard, oldCard, eOpts) {
                    if (newCard.itemId === 'SoLuongBanVeTab') {
                        newCard.down('grid').getController().loadDataBanVe();
                    }
                    newCard.updateLayout();
                    oldCard.updateLayout();
                },
            },
            items: [{
                title: '<b>KẾ HOẠCH<br>SẢN XUẤT</b>',
                iconCls: 'fas fa-calendar-alt',
                autoScroll: false,
                layout: 'fit',
                iconAlign: 'top',
                itemId: 'KeHoachSanXuatTab',
                items: me.createTabKeHoachSanXuat(),
            }, {
                title: '<b>SỐ LƯỢNG<br>BẢN VẼ</b>',
                iconCls: 'fas fa-clipboard-list',
                iconAlign: 'top',
                autoScroll: false,
                layout: 'fit',
                itemId: 'SoLuongBanVeTab',
                items: {
                    xtype: 'QuanLyBanVeSanXuat',
                },
            }],
        };
        me.callParent();
    },

    createTabKeHoachSanXuat() {
        const me = this;
        return {
            xtype: 'panel',
            layout: 'border',
            bodyBorder: true,
            tbar: [{
                    xtype: 'segmentedbutton',
                    items: [{
                        text: 'Bảng sản xuất',
                        reference: 'modeDangSanXuat',
                        ui: 'soft-orange',
                        pressed: true,
                        handler: 'onBtnViewDangSanXuat',
                    }, {
                        text: 'Xem theo ngày',
                        reference: 'modeXemTheoNgay',
                        ui: 'soft-orange',
                        bind: {
                            pressed: '{modeViewDayPressed}',
                        },
                    }],
                }, '-',
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: '<b>Xem theo ngày</b>',
                    hideLabel: true,
                    labelWidth: 110,
                    hidden: true,
                    bind: {
                        hidden: '{showBtnSanXuat}',
                    },
                    layout: 'hbox',
                    items: [{
                        xtype: 'datefield',
                        name: 'startDate',
                        reference: 'StartDateField',
                        fieldLabel: 'Từ',
                        labelWidth: 30,
                        bind: {
                            value: '{startdate}',
                        },
                        format: 'd/m/Y',
                        submitFormat: 'Y-m-d',
                        width: 170,
                        margin: '0 5 0 0',
                    }, {
                        xtype: 'datefield',
                        name: 'endDate',
                        reference: 'EndDateField',
                        labelWidth: 30,
                        width: 170,
                        minValue: new Date(),
                        bind: {
                            minValue: '{startdate}',
                            value: '{enddate}',
                        },
                        format: 'd/m/Y',
                        submitFormat: 'Y-m-d',
                        margin: '0 5 0 0',
                        fieldLabel: 'đến',
                    }, {
                        xtype: 'button',
                        text: 'Tra cứu',
                        ui: 'dark-blue',
                        iconCls: 'fas fa-search',
                        handler: 'onBtnSearchRangeDate',
                    }],
                }, '->', {
                    xtype: 'textfield',
                    emptyText: 'Nhập tìm kiếm',
                    width: 250,
                    reference: 'SearchField',
                    triggers: {
                        clear: {
                            weight: 0,
                            cls: `${Ext.baseCSSPrefix}form-clear-trigger`,
                            hidden: true,
                            handler: 'onClearClick',
                            scope: 'this',
                        },
                        search: {
                            weight: 1,
                            cls: `${Ext.baseCSSPrefix}form-search-trigger`,
                            handler: 'onSearchClick',
                            scope: 'this',
                        },
                    },
                    onClearClick() {
                        this.setValue('');
                        this.getTrigger('clear').hide();
                        this.updateLayout();
                        me.getController().loadDataGrid();
                    },
                    onSearchClick() {
                        const value = this.getValue();
                        if (value.length > 0) {
                            this.getTrigger('clear').show();
                            me.getController().loadDataGrid();
                        } else {
                            this.getTrigger('clear').hide();
                            me.getController().loadDataGrid();
                        }
                    },
                    listeners: {
                        specialkey(f, e) {
                            if (e.getKey() === e.ENTER) {
                                this.onSearchClick();
                            }
                        },
                    },
                }, {
                    xtype: 'button',
                    text: 'Lọc theo',
                    iconCls: 'fas fa-filter',
                    ui: 'dark-blue',
                    menuAlign: 'tr-br',
                    reference: 'FilterMenu',
                    menu: Ext.create('Ext.menu.Menu', {
                        style: {
                            overflow: 'visible',
                        },
                        mouseLeaveDelay: 10,
                        width: 370,
                        margin: '10 10 10 10',
                        referenceHolder: true,
                        buttons: [{
                            text: 'Lọc',
                            ui: 'soft-orange',
                            handler() {
                                me.getController().loadDataGrid();
                            },
                        }, {
                            text: 'Xóa bộ lọc',
                            handler(btn) {
                                const refs = btn.up().up().getReferences();
                                for (const ref in refs) {
                                    refs[ref].reset();
                                    refs[ref].getTrigger('clear').hide();
                                }
                                btn.up().up().hide();
                                me.getController().loadDataGrid();
                            },
                        }],
                        items: [{
                            xtype: 'datefield',
                            fieldLabel: '<b>Ngày giao  hàng</b>',
                            labelAlign: 'left',
                            labelWidth: 120,
                            reference: 'datedeliveryfilter',
                            margin: '10 10 10 10',
                            fieldIndex: 'date_delivery',
                            format: 'd/m/Y',
                            altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                            enableKeyEvents: true,
                            triggers: {
                                clear: {
                                    cls: 'x-form-clear-trigger',
                                    weight: -1,
                                    hidden: true,
                                    scope: 'this',
                                    handler(combo) {
                                        combo.reset();
                                        combo.getTrigger('clear').hide();
                                        me.getController().loadDataGrid();
                                    },
                                },
                            },
                            listeners: {
                                change(combo, newval) {
                                    combo.getTrigger('clear').show();
                                    combo.updateLayout();
                                },
                                keypress(field, event) {
                                    if (event.getKey() === event.ENTER) {
                                        me.getController().loadDataGrid();
                                    }
                                },
                            },
                        }],
                    }),
                }, {
                    text: 'Công cụ',
                    iconCls: 'fas fa-briefcase',
                    ui: 'dark-blue',
                    menu: {
                        xtype: 'menu',
                        plain: true,
                        mouseLeaveDelay: 10,
                        items: [{
                            text: 'In ấn',
                            iconCls: 'fas fa-print text-dark-blue',
                            handler: 'onPrintButtonClick',
                        }, {
                            text: 'Xuất Excel',
                            iconCls: 'fas fa-file-export text-dark-blue',
                            handler: 'onExportdButtonClick',
                        }, '-', {
                            text: 'Làm mới danh sách',
                            iconCls: 'fas fa-sync-alt text-dark-blue',
                            handler: 'onReloadButtonClick',
                        }],
                    },
                }, {
                    iconCls: 'fas fa-clipboard-list',
                    ui: 'soft-orange',
                    text: 'Xem bản vẽ',
                    reference: 'btnViewDanhSachBanVe',
                    hidden: true,
                    enableToggle: true,
                }, {
                    iconCls: 'fas fa-expand-arrows-alt',
                    handler: ClassMain.onExpandViewPortMain,
                    hidden: true,
                    enableToggle: true,
                },
            ],
            items: [{
                    xtype: 'grid',
                    autoScroll: true,
                    region: 'center',
                    collapsible: false,
                    listeners: {
                        render(grid) {
                            ClassMain.hideLoadingMask();
                        },
                        viewready: 'onViewGridReady',
                        destroy() {
                            this.tips = Ext.destroy(this.tip);
                        },
                        cellcontextmenu: 'onCellContextMenuGrid',
                        cellkeydown: 'onItemKeydown',
                        celldblclick: 'onCellDBClick',
                    },
                    reference: 'QuanLyKeHoachSanXuatGrid',
                    bufferedRenderer: false,
                    cls: 'grid-kehoachsanxuat',
                    store: {
                        type: 'QuanLyKeHoachSanXuatStore',
                        storeId: 'KeHoachSanXuatStore',
                        grouper: {
                            groupFn(item) {
                                const datenow = new Date();
                                const time = Ext.Date.parse(item.get('time'), 'Y-m-d');
                                const time_type = item.get('time_type').toUpperCase();
                                let btn_guikehoach = '';
                                if (time) {
                                    if (ClassMain.checkPermission('quan_ly_ke_hoach_san_xuat', 'ProductSchedule', 'create') === true) {
                                        btn_guikehoach = `<a class="button_sanxuat" href="#" onclick="SendKeHoach('${Ext.Date.format(time, 'Y-m-d')}');return false;"><i class="fas fa-play"></i> Gửi kế hoạch</a>`;
                                    }
                                    if (item.get('time') === Ext.Date.format(datenow, 'Y-m-d')) {
                                        if (time_type === 'AM') {
                                            return `<i class="far fa-calendar-alt"></i> <font color="blue" style="font-size:11px"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')} - ${time_type} (Today) </font> ${btn_guikehoach}`;
                                        }
                                        return `<i class="far fa-calendar-alt"></i> <font color="blue" style="font-size:11px"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')} - ${time_type} (Today) </font>`;
                                    }
                                    if (Ext.Date.diff(time, datenow, Ext.Date.DAY) <= -1) {
                                        if (time_type === 'AM') {
                                            return `<i class="far fa-calendar-alt"></i> <font color="green" style="font-size:11px"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')} - ${time_type}</font> ${btn_guikehoach}`;
                                        }
                                        return `<i class="far fa-calendar-alt"></i> <font color="green" style="font-size:11px"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')} - ${time_type}</font>`;
                                    }
                                    return `<i class="far fa-calendar-alt"></i> <font color="black" style="font-size:11px"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')} - ${time_type}</font>`;
                                }
                            },
                        },
                        listeners: {
                            load: me.getController().onLoadGridKeHoachSanXuat,
                            update: me.getController().onUpdateGridKeHoach,
                            scope: me.getController(),
                        },
                    },
                    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
                    plugins: {
                        ptype: 'cellediting',
                        clicksToEdit: 2,
                        listeners: {
                            edit: 'onEditCell',
                            beforeedit: 'onBeforeEditCell',
                        },
                    },
                    columnLines: true,
                    // selType: 'cellmodel',
                    selModel: {
                        type: 'spreadsheet',
                        columnSelect: false,
                        checkboxSelect: false,
                        rowSelect: false,
                        checkboxHeaderWidth: 40,
                        pruneRemoved: false,
                        extensible: true,
                        // mode: 'SINGLE',
                    },

                    syncRowHeight: false,
                    forceFit: true,
                    viewConfig: {
                        columnLines: true,
                        trackOver: false,
                        // markDirty: false,
                        stripeRows: true,
                    },
                    features: [{
                        ftype: 'summary',
                        // dock: 'bottom',
                        // showSummaryRow: false
                    }, {
                        id: 'group',
                        ftype: 'groupingsummary',
                        groupHeaderTpl: '<b>{name}</b>',
                        hideGroupedHeader: false,
                        enableGroupingMenu: false,
                        showSummaryRow: false,
                        collapsible: false,
                    }],
                    actions: {
                        save: {
                            getClass: 'getSaveClass',
                            getTip: 'getSaveTip',
                            handler: 'onSaveClick',
                        },
                    },
                    bbar: [{
                        xtype: 'button',
                        text: 'Thêm ngày',
                        ui: 'dark-blue',
                        iconCls: 'fas fa-plus-circle',
                        bind: {
                            hidden: ClassMain.checkPermission('quan_ly_ke_hoach_san_xuat', 'ProductSchedule', 'create') === true ? '{!showBtnSanXuat}' : true,
                        },
                        handler: 'onClickAddDayButton',
                    }, '->', {
                        xtype: 'button',
                        text: 'Làm mới danh sách',
                        iconCls: 'fas fa-sync-alt text-dark-blue',
                        handler: 'onReloadButtonClick',
                    }],
                    columns: [{
                            text: '<font style="font-size:11px">INFO</font>',
                            locked: true,
                            columns: [{
                                    xtype: 'rownumberer',
                                    text: '<font style="font-size:10px"><b>TT</b></font>',
                                    width: 45,
                                    locked: true,
                                    sortable: false,
                                    align: 'center',
                                }, {
                                    menuDisabled: true,
                                    sortable: false,
                                    xtype: 'actioncolumn',
                                    width: 50,
                                    locked: true,
                                    align: 'center',
                                    items: ['@save'],
                                }, {
                                    text: '<font style="font-size:10px"><b>Note</b></font>',
                                    dataIndex: 'note',
                                    locked: true,
                                    width: 55,
                                    sortable: false,
                                    align: 'center',
                                    /* editor: {
                                        //xtype: 'numberfield',
                                        //selectOnFocus: true,
                                        hideTrigger: true,
                                        //minValue: 0,
                                        allowBlank: false
                                    }, */
                                    editor: {
                                        xtype: 'combo',
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        queryMode: 'local',
                                        displayField: 'note',
                                        valueField: 'note',
                                        // selectOnFocus: true,
                                        anyMatch: true,
                                        // forceSelection: true,
                                        store: me.SoLuongBanVeStore,
                                        listConfig: {
                                            itemTpl: [
                                                '<div data-qtip="{type}: {number}"><b>Note: {note}</b> ({project_name} - {order_no})</div>',
                                            ],
                                        },
                                        listeners: {
                                            select(combo, record, eOpts) {
                                                combo.ownerCt.completeEdit();
                                            },
                                            afterrender(field) {
                                                field.bodyEl.setWidth(280);
                                            },
                                        },
                                    },
                                    renderer(value) {
                                        return `<b>${value}<b>`;
                                    },
                                },
                                {
                                    text: '<font style="font-size:10px"><b>Project Name</b></font>',
                                    dataIndex: 'project_name',
                                    locked: true,
                                    width: 90,
                                    sortable: false,
                                    align: 'center',
                                    summaryType(records, values) {
                                        let i = 0;
                                        const { length } = records;
                                        let total = 0;
                                        let record;
                                        for (; i < length; i += 1) {
                                            record = records[i];
                                            if (record.data.project_name !== '') {
                                                total += 1;
                                            }
                                        }
                                        return total;
                                    },
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return '<b>TOTAL</b>';
                                    },
                                    editor: {
                                        allowBlank: true,
                                    },
                                },
                                {
                                    text: '<font style="font-size:10px"><b>Order No</b></font>',
                                    dataIndex: 'order_no',
                                    locked: true,
                                    width: 75,
                                    align: 'center',
                                    sortable: false,
                                    editor: {
                                        allowBlank: true,
                                    },
                                },
                                {
                                    text: '<font style="font-size:10px"><b>Sub No</b></font>',
                                    dataIndex: 'sub_no',
                                    locked: true,
                                    width: 70,
                                    sortable: false,
                                    align: 'center',
                                    editor: {
                                        allowBlank: true,
                                    },
                                },
                            ],
                        },
                        {
                            text: '<font style="font-size:11px"><b>QUANTITY</b></font>',
                            columns: [{
                                text: '<font style="font-size:10px"><br>FRAME<br></font>',
                                columns: [{
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">SD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'frame_sd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px;">LSD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'frame_lsd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">FSD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'frame_fsd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">FLSD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'frame_flsd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">WD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'frame_wd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">SUSD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'frame_susd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }],
                            }, {
                                text: '<font style="font-size:9px"><br>LEAF<br></font>',
                                columns: [{
                                    text: '<font style="font-size:10px">SD</font>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'leaf_sd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">LSD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'leaf_lsd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    tdCls: 'tip',
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">FSD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'leaf_fsd',
                                    summaryType: 'sum',
                                    tdCls: 'tip',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">FLSD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'leaf_flsd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    tdCls: 'tip',
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">WD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'leaf_wd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    tdCls: 'tip',
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">SUSD</font></p>',
                                    width: 35,
                                    align: 'center',
                                    dataIndex: 'leaf_susd',
                                    summaryType: 'sum',
                                    sortable: false,
                                    tdCls: 'tip',
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }],
                            }, {
                                text: '<font style="font-size:10px">SMOOD<br> DOOR</font>',
                                columns: [{
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">Frame</font></p>',
                                    dataIndex: 'frame_sld',
                                    width: 35,
                                    align: 'center',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:8px;word-break: break-all;"><font style="font-size:9px">Leaf</font></p>',
                                    dataIndex: 'leaf_sld',
                                    width: 35,
                                    align: 'center',
                                    sortable: false,
                                    summaryType: 'sum',
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }],
                            }, {
                                text: '<font style="font-size:9px">SP<br> HANGER</font>',
                                columns: [{
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">Frame</font></p>',
                                    dataIndex: 'frame_sp',
                                    width: 35,
                                    align: 'center',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:8px;word-break: break-all;"><font style="font-size:9px">Leaf</font></p>',
                                    dataIndex: 'leaf_sp',
                                    width: 35,
                                    align: 'center',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }],
                            }, {
                                text: '<font style="font-size:10px"><br>SHUTTER<br></font>',
                                columns: [{
                                    text: '<font style="font-size:9px">JP</font>',
                                    dataIndex: 'ssjp',
                                    width: 35,
                                    align: 'center',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:8px;word-break: break-all;"><font style="font-size:9px">Aichi</font></p>',
                                    dataIndex: 'ssaichi',
                                    width: 35,
                                    align: 'center',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<p style="width:8px;word-break: break-all;"><font style="font-size:9px">Grill</font></p>',
                                    dataIndex: 'ssgrill',
                                    width: 35,
                                    align: 'center',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }],
                            }, {
                                text: '<font style="font-size:10px"><br>QSAVE<br></font>',
                                columns: [{
                                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">GR-S</font></p>',
                                    dataIndex: 'grs',
                                    width: 35,
                                    align: 'center',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<font style="font-size:9px">S-13</font>',
                                    dataIndex: 's13',
                                    width: 35,
                                    align: 'center',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }, {
                                    text: '<font style="font-size:9px">S-14</font>',
                                    dataIndex: 's14',
                                    width: 35,
                                    align: 'center',
                                    summaryType: 'sum',
                                    sortable: false,
                                    renderer: 'renderNumberColumn',
                                    summaryRenderer(value, summaryData, dataIndex, metaData) {
                                        return `<b>${value}</b>`;
                                    },
                                    editor: {
                                        xtype: 'numberfield',
                                        // selectOnFocus: true,
                                        hideTrigger: true,
                                        minValue: 0,
                                        allowBlank: true,
                                        listeners: {
                                            afterrender(field) {
                                                field.bodyEl.setWidth(90);
                                            },
                                        },
                                    },
                                }],
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">LOUVER</font></p>',
                                dataIndex: 'lv',
                                width: 35,
                                align: 'center',
                                summaryType: 'sum',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                                summaryRenderer(value, summaryData, dataIndex, metaData) {
                                    return `<b>${value}</b>`;
                                },
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(90);
                                        },
                                    },
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:9px">SW</font></p>',
                                dataIndex: 'sw',
                                width: 35,
                                align: 'center',
                                sortable: false,
                                summaryType: 'sum',
                                renderer: 'renderNumberColumn',
                                summaryRenderer(value, summaryData, dataIndex, metaData) {
                                    return `<b>${value}</b>`;
                                },
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(90);
                                        },
                                    },
                                },
                            }, {
                                text: '<p style="width:9px;word-break: break-all;"><font style="font-size:9px">Other</font></p>',
                                dataIndex: 'other_num',
                                width: 35,
                                summaryType: 'sum',
                                sortable: false,
                                renderer: 'renderNumberColumnOther',
                                summaryRenderer(value, summaryData, dataIndex, metaData) {
                                    return `<b>${value}</b>`;
                                },
                                align: 'center',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(90);
                                        },
                                    },
                                },
                            }],
                        }, {
                            text: '<font style="font-size:11px">DATE</font>',
                            columns: [{
                                text: '<font style="font-size:8px">Drawing</font>',
                                dataIndex: 'date_drawing',
                                width: 55,
                                align: 'center',
                                xtype: 'datecolumn',
                                format: 'j-M',
                                sortable: false,
                                renderer: 'renderDrawingColumn',
                            }, {
                                text: '<font style="font-size:8px">Delivery</font>',
                                width: 55,
                                align: 'center',
                                dataIndex: 'date_delivery',
                                xtype: 'datecolumn',
                                format: 'j-M',
                                sortable: false,
                                editor: {
                                    xtype: 'datefield',
                                    minValue: new Date(),
                                    emptyText: 'dd/mm/YY',
                                    format: 'd/m/Y',
                                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(130);
                                        },
                                    },
                                },
                                renderer: 'renderDeliveryColumn',
                            }],
                        }, {
                            text: '<font style="font-size:11px">PRODUCTION SCHEDULE</font>',
                            columns: [{
                                text: '<font style="font-size:8px;">Shearing<br>(cắt)</font>',
                                dataIndex: 'shearing',
                                width: 55,
                                align: 'center',
                                xtype: 'datecolumn',
                                format: 'j-M',
                                sortable: false,
                                editor: {
                                    xtype: 'datefield',
                                    minValue: new Date(),
                                    emptyText: 'dd/mm/YY',
                                    format: 'd/m/Y',
                                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(130);
                                        },
                                    },
                                },
                                renderer: 'renderShearingColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px;">Result</font></p>',
                                dataIndex: 'shearing_result',
                                width: 28,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderResultColumn',
                            }, {
                                text: '<font style="font-size:9px">T.P.P (đột)</font>',
                                dataIndex: 'tpp',
                                width: 55,
                                align: 'center',
                                sortable: false,
                                xtype: 'datecolumn',
                                format: 'j-M',
                                editor: {
                                    xtype: 'datefield',
                                    minValue: new Date(),
                                    emptyText: 'dd/mm/YY',
                                    format: 'd/m/Y',
                                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(130);
                                        },
                                    },
                                },
                                renderer: 'renderTPPColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px;">Result</font></p>',
                                dataIndex: 'tpp_result',
                                width: 28,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderResultColumn',
                            }, {
                                text: '<font style="font-size:9px">Bending (uốn)</font>',
                                dataIndex: 'bending',
                                width: 55,
                                align: 'center',
                                xtype: 'datecolumn',
                                format: 'j-M',
                                sortable: false,
                                editor: {
                                    xtype: 'datefield',
                                    minValue: new Date(),
                                    emptyText: 'dd/mm/YY',
                                    format: 'd/m/Y',
                                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(130);
                                        },
                                    },
                                },
                                renderer: 'renderBendingColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px;">Result</font></p>',
                                dataIndex: 'bending_result',
                                width: 28,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderResultColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Prepare</font></p>',
                                dataIndex: 'prepare_frame',
                                width: 28,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderResultColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Inner</font></p>',
                                dataIndex: 'inner_frame',
                                width: 28,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderResultColumn',
                            }, {
                                text: '<font style="font-size:9px">Grew press & Asembly (hàn)</font>',
                                dataIndex: 'grew_press_asembly',
                                width: 55,
                                align: 'center',
                                xtype: 'datecolumn',
                                sortable: false,
                                format: 'j-M',
                                editor: {
                                    xtype: 'datefield',
                                    minValue: new Date(),
                                    emptyText: 'dd/mm/YY',
                                    format: 'd/m/Y',
                                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(130);
                                        },
                                    },
                                },
                                renderer: 'renderGrewPressColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px;">Result</font></p>',
                                dataIndex: 'grew_press_asembly_result',
                                width: 28,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderResultColumn',
                            }, {
                                text: '<font style="font-size:9px">Prime paint</font>',
                                dataIndex: 'prime_paint',
                                width: 55,
                                align: 'center',
                                xtype: 'datecolumn',
                                format: 'j-M',
                                sortable: false,
                                editor: {
                                    xtype: 'datefield',
                                    minValue: new Date(),
                                    emptyText: 'dd/mm/YY',
                                    format: 'd/m/Y',
                                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(130);
                                        },
                                    },
                                },
                                renderer: 'renderPrimePaintColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px;">Result</font></p>',
                                dataIndex: 'prime_paint_result',
                                width: 28,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderResultColumn',
                            }, {
                                text: '<font style="font-size:9px">Finish paint</font>',
                                dataIndex: 'finish_paint',
                                width: 55,
                                align: 'center',
                                sortable: false,
                                xtype: 'datecolumn',
                                format: 'j-M',
                                editor: {
                                    xtype: 'datefield',
                                    minValue: new Date(),
                                    emptyText: 'dd/mm/YY',
                                    format: 'd/m/Y',
                                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(130);
                                        },
                                    },
                                },
                                renderer: 'renderFinishPaintColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px;">Result</font></p>',
                                dataIndex: 'finish_paint_result',
                                width: 28,
                                sortable: false,
                                align: 'center',
                                renderer: 'renderResultColumn',
                            }, {
                                text: '<font style="font-size:9px">Check</font>',
                                dataIndex: 'product_check',
                                width: 55,
                                align: 'center',
                                sortable: false,
                                xtype: 'datecolumn',
                                format: 'j-M',
                                editor: {
                                    xtype: 'datefield',
                                    minValue: new Date(),
                                    emptyText: 'dd/mm/YY',
                                    format: 'd/m/Y',
                                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                                    listeners: {
                                        afterrender(field) {
                                            field.bodyEl.setWidth(130);
                                        },
                                    },
                                },
                                renderer: 'renderCheckColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px;">Result</font></p>',
                                dataIndex: 'check_result',
                                width: 28,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderResultColumn',
                            }, {
                                text: '<font style="font-size:9px">Colour</font>',
                                dataIndex: 'color',
                                width: 55,
                                align: 'center',
                                sortable: false,
                                editor: {
                                    allowBlank: true,
                                },
                            }, {
                                text: '<font style="font-size:10px">Note</font>',
                                dataIndex: 'ghichu',
                                width: 55,
                                sortable: false,
                                align: 'center',
                                editor: {
                                    allowBlank: true,
                                },
                            }, {
                                text: '<font style="font-size:10px">Người theo dõi</font>',
                                dataIndex: 'incharge',
                                width: 55,
                                sortable: false,
                                align: 'center',
                                editor: {
                                    allowBlank: true,
                                },
                            }],
                        },
                    ],
                },
                {
                    xtype: 'grid',
                    region: 'east',
                    width: 280,
                    collapsible: false,
                    bind: {
                        hidden: '{!btnViewDanhSachBanVe.pressed}',
                    },
                    header: false,
                    hidden: true,
                    split: false,
                    margin: '0 0 0 7',
                    layout: 'fit',
                    columnLines: true,
                    reference: 'gridDanhSachBanVeChuaLap',
                    cls: 'grid-kehoachsanxuat',
                    emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                    store: me.SoLuongBanVeStore,
                    bbar: [{
                        text: 'Ẩn',
                        iconCls: 'fas fa-eye text-dark-blue',
                        handler: 'onBtnHideDanhSachBanVe',
                    }, '->', {
                        iconCls: 'fas fa-sync-alt text-dark-blue',
                        handler: 'onBtnReloadDanhSachBanVe',
                    }],
                    columns: [{
                        text: 'DANH SÁCH BẢN VẼ CHƯA LẬP KH',
                        columns: [{
                            text: '<font style="font-size:9px">Note</font>',
                            width: 50,
                            dataIndex: 'note',
                            align: 'center',
                            summaryRenderer(value, summaryData, dataIndex, metaData) {
                                return '<b>BẢN VẼ</b>';
                            },
                        }, {
                            text: '<font style="font-size:10px"><br>Order<br>No<br></font>',
                            width: 70,
                            dataIndex: 'order_no',
                            summaryType: 'count',
                            align: 'center',
                            summaryRenderer(value, summaryData, dataIndex, metaData) {
                                return `<b>${value}</b>`;
                            },
                        }, {
                            text: '<font style="font-size:10px">Quatity</font>',
                            columns: [{
                                text: '<font style="font-size:10px">Type</font>',
                                width: 85,
                                dataIndex: 'type',
                                align: 'center',
                                renderer(value, meta, rec, rowIndex, colIndex, store, view) {
                                    if (rec.get('other_num') > 0) {
                                        return `${value.toUpperCase()} / Other`;
                                    }
                                    return value.toUpperCase();
                                },
                            }, {
                                text: '<font style="font-size:10px">Quatity</font>',
                                flex: 1,
                                dataIndex: 'remain',
                                align: 'center',
                                renderer(value, meta, rec, rowIndex, colIndex, store, view) {
                                    if (rec.get('other_num') > 0) {
                                        return `${value.toString()} / ${rec.get('other_remain').toString()}`;
                                    }
                                    return value.toString();
                                },
                            }],
                        }],
                    }],
                },
            ],
        };
    },
});

function SendKeHoach(time) {
    if (Ext.getCmp('mainViewPort').getController().getReferences().QuanLyKeHoachSanXuatPanel) {
        Ext.getCmp('mainViewPort').getController().getReferences().QuanLyKeHoachSanXuatPanel.getController().onClickLapKeHoachButton(time);
    }
}