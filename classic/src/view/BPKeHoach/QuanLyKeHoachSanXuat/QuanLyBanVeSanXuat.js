/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPKeHoach.QuanLyKeHoachSanXuat.QuanLyBanVeSanXuat', {
    extend: 'Ext.grid.Panel',
    requires: [

    ],
    controller: 'QuanLyBanVeSanXuat',
    viewModel: 'QuanLyBanVeSanXuat',
    xtype: 'QuanLyBanVeSanXuat',
    autoScroll: true,
    layout: 'fit',
    listeners: {
        render(panel) {
            ClassMain.hideLoadingMask();
        },
    },
    bufferedRenderer: false,
    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true,
    },
    columnLines: true,
    /* selModel: {
        type: 'spreadsheet',
        columnSelect: true,
        pruneRemoved: false,
        extensible: 'y'
    }, */
    syncRowHeight: false,
    forceFit: true,
    viewConfig: {
        columnLines: true,
        trackOver: false,
        markDirty: false,
        stripeRows: true,
    },
    config: {
        modeFilter: 1,
    },
    store: {
        type: 'SoLuongBanVeStore',
        storeId: 'SoLuongBanVeStore',
    },
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    initComponent() {
        const me = this;
        me.tbar = [{
            xtype: 'combobox',
            fieldLabel: '<i class="far fa-calendar-alt"></i> <b>Lọc theo</b>',
            labelAlign: 'left',
            store: {
                field: ['name', 'val'],
                data: [{
                    name: 'Xem tất cả',
                    val: 1,
                }, {
                    name: 'Chưa lập kế hoạch sản xuất',
                    val: 2,
                }, {
                    name: 'Đã lập kế hoạch sản xuất',
                    val: 3,
                }],
            },
            queryMode: 'local',
            editable: false,
            labelWidth: 80,
            width: 350,
            displayField: 'name',
            value: me.getModeFilter(),
            valueField: 'val',
            reference: 'ComboboxFilterBanVe',
            listeners: {
                select: 'onSelectFilterBanVe',
            },
        }, '->', {
            xtype: 'textfield',
            emptyText: 'Nhập tìm kiếm',
            width: 250,
            reference: 'SearchBanVe',
            triggers: {
                clear: {
                    weight: 0,
                    cls: `${Ext.baseCSSPrefix}form-clear-trigger`,
                    hidden: true,
                    handler: 'onClearClick',
                    scope: 'this',
                },
                search: {
                    weight: 1,
                    cls: `${Ext.baseCSSPrefix}form-search-trigger`,
                    handler: 'onSearchClick',
                    scope: 'this',
                },
            },
            onClearClick() {
                this.setValue('');
                this.getTrigger('clear').hide();
                this.updateLayout();
                me.getController().loadDataBanVe();
            },
            onSearchClick() {
                const value = this.getValue();
                if (value.length > 0) {
                    this.getTrigger('clear').show();
                    this.updateLayout();
                    me.getController().loadDataBanVe();
                } else {
                    this.getTrigger('clear').hide();
                    me.getController().loadDataBanVe();
                }
            },
            listeners: {
                specialkey(f, e) {
                    if (e.getKey() === e.ENTER) {
                        this.onSearchClick();
                    }
                },
            },
        }, {
            text: 'Công cụ',
            iconCls: 'fas fa-briefcase',
            ui: 'dark-blue',
            menu: {
                xtype: 'menu',
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                    // handler: 'onPrintButtonClick'
                }, {
                    text: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    // handler: 'onExportdButtonClick'
                }, '-', {
                    text: 'Làm mới danh sách',
                    iconCls: 'fas fa-sync-alt text-dark-blue',
                    handler: 'onReloadButtonClick',
                }],
            },
        }];
        me.columns = [{
            text: 'Note',
            width: 70,
            dataIndex: 'note',
            align: 'center',
            locked: true,
            renderer(value) {
                return `<b>${value}</b>`;
            },
        }, {
            text: 'Project Name',
            width: 120,
            locked: true,
            dataIndex: 'project_name',
            align: 'center',
        }, {
            text: 'Order No',
            width: 110,
            locked: true,
            dataIndex: 'order_no',
            align: 'center',
        }, {
            text: 'Sub No',
            width: 80,
            locked: true,
            dataIndex: 'sub_no',
            align: 'center',
        }, {
            text: 'Quatity',
            columns: [{
                text: 'FRAME',
                columns: [{
                    text: 'SD',
                    width: 90,
                    dataIndex: 'frame_sd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'LSD',
                    width: 90,
                    dataIndex: 'frame_lsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'FSD',
                    width: 90,
                    dataIndex: 'frame_fsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'FLSD',
                    width: 90,
                    dataIndex: 'frame_flsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'WD',
                    width: 90,
                    dataIndex: 'frame_wd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'SUSD',
                    width: 90,
                    dataIndex: 'frame_susd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'LEAF',
                columns: [{
                    text: 'SD',
                    width: 90,
                    dataIndex: 'leaf_sd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'LSD',
                    width: 90,
                    dataIndex: 'leaf_lsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'FSD',
                    width: 90,
                    dataIndex: 'leaf_fsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'FLSD',
                    width: 90,
                    dataIndex: 'leaf_flsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'WD',
                    width: 90,
                    dataIndex: 'leaf_wd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'SUSD',
                    width: 90,
                    dataIndex: 'leaf_susd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'SMOOD DOOR',
                columns: [{
                    text: 'Frame',
                    width: 90,
                    dataIndex: 'frame_sld',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'Leaf',
                    width: 90,
                    dataIndex: 'leaf_sld',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'SP HANGER',
                columns: [{
                    text: 'Frame',
                    width: 90,
                    dataIndex: 'frame_sp',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'Leaf',
                    width: 90,
                    dataIndex: 'leaf_sp',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'SHUTTER',
                columns: [{
                    text: 'JP',
                    width: 90,
                    dataIndex: 'ssjp',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'Aichi',
                    width: 90,
                    dataIndex: 'ssaichi',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'Grill',
                    width: 90,
                    dataIndex: 'ssgrill',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'QSAVE',
                columns: [{
                    text: 'GR-S',
                    width: 90,
                    dataIndex: 'grs',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'S-13',
                    width: 90,
                    dataIndex: 's13',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'S-14',
                    width: 90,
                    dataIndex: 's14',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'SW',
                width: 90,
                dataIndex: 'sw',
                renderer: 'renderNumberColumnOther',
                align: 'center',
            }, {
                text: 'LOUVER',
                width: 90,
                dataIndex: 'lv',
                renderer: 'renderNumberColumnOther',
                align: 'center',
            }, {
                text: 'Other',
                width: 90,
                dataIndex: 'other_num',
                renderer: 'renderNumberColumnOther',
                align: 'center',
            }],
        }];
        me.callParent();
    },
});