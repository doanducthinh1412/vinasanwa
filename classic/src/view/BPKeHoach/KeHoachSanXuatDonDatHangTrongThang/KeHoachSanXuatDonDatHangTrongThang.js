Ext.define('CMS.view.BPKeHoach.KeHoachSanXuatDonDatHangTrongThang.KeHoachSanXuatDonDatHangTrongThang', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.grid.feature.Grouping'
    ],
    controller: 'KeHoachSanXuatDonDatHangTrongThang',
    xtype: 'KeHoachSanXuatDonDatHangTrongThang',
    viewModel: 'KeHoachSanXuatDonDatHangTrongThang',
    bodyPadding: 10,
    layout: 'hbox',
    config: {
        gridPanel: '',
        storeParent: '',
        relationComponent: '',
        relationData: ''
    },
    autoScroll: true,
    bodyStyle: {
        'background-color': '#f6f6f6;',
        'padding': '5px'
    },
    initComponent: function() {
        var me = this;

        var years = [];
        var y = 2000;
        var current_year = (new Date()).getFullYear();
        //years.push([0,'------']);
        while (current_year >= y) {
            years.push([current_year, current_year]);
            current_year--;
        }
        var current_year = (new Date()).getFullYear();
        var yearStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'display'],
            data: years
        });
        var months = [];
        var month = 1;
        while (month <= 12) {
            months.push([month, 'Tháng ' + month.toString()]);
            month++;
        }
        months.push(['fullyear', 'Cả năm']);
        var monthStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'display'],
            data: months
        });
        me.tbar = [{
                xtype: 'combo',
                fieldLabel: 'Chọn năm',
                emptyText: '----------',
                labelWidth: 100,
                name: 'yearCmb',
                store: yearStore,
                queryMode: 'local',
                displayField: 'display',
                valueField: 'value',
                width: 250,
                reference: 'yearCmb',
                value: current_year
            }, '-',
            {
                xtype: 'combo',
                fieldLabel: 'Chọn tháng',
                //emptyText  	: '----------',
                labelWidth: 100,
                name: 'monthCmb',
                store: monthStore,
                queryMode: 'local',
                displayField: 'display',
                valueField: 'value',
                width: 500,
                reference: 'monthCmb',
                multiSelect: true
            }, '-',
            {
                text: 'Xem',
                ui: 'dark-blue',
                handler: function() {
                    var refs = me.getReferences();
                    var store = me.getStore();
                    var year = refs.yearCmb.getValue();
                    if (year) {
                        store.clearFilter();
                        store.filter('year', year);
                        var months = refs.monthCmb.getValue();
                        if (months) {
                            if (months.indexOf('fullyear') == -1) {
                                store.filterBy(function(record) {
                                    var month = record.get('month');
                                    if (month) {
                                        return (months.indexOf(parseInt(month)) != -1)
                                    }
                                });
                            }
                        }
                    }
                }
            }, '->',
            {
                text: 'Công cụ',
                iconCls: 'fas fa-briefcase',
                ui: 'dark-blue',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    mouseLeaveDelay: 10,
                    items: [{
                        text: 'In ấn',
                        iconCls: 'fas fa-print text-dark-blue',
                        handler: 'onPrintButtonClick'
                    }, {
                        text: 'Xuất Excel',
                        iconCls: 'fas fa-file-export text-dark-blue',
                        handler: 'onExportdButtonClick'
                    }, '-', {
                        text: 'Làm mới danh sách',
                        iconCls: 'fas fa-sync-alt text-dark-blue',
                        handler: 'onReloadButtonClick'
                    }]
                }
            }
        ];
        me.items = [

            {
                xtype: 'grid',
                columnLines: true,
                width: 1305,
                title: '<b>Cummulative Total Grid</b>',
                reference: 'cummulativeTotalGrid',
                ui: 'light',
                cls: 'service-type shadow',
                style: 'border-left: solid Black 2px',
                bind: {
                    store: '{cummulativeTotalStore}'
                },
                emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                features: [{
                    ftype: 'summary',
                    dock: 'bottom'
                }],
                columns: {
                    items: [{
                            text: '<b>Date</b>',
                            dataIndex: 'date',
                            height: 100,
                            align: 'center',
                            summaryType: 'count',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>Plan</b>';
                            },
                            renderer: function(value, record) {
                                return '<b>' + value + '</b>'
                            }
                        },
                        {
                            text: '<b>Frame</b>',
                            dataIndex: 'frame',
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            renderer: function(value, record) {
                                if (value) {
                                    return value
                                } else {
                                    return '-'
                                }
                            }
                        },
                        {
                            text: '<b>Leaf</b>',
                            align: 'center',
                            columns: [{
                                text: 'SD',
                                dataIndex: 'leaf_sd',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }

                            }, {
                                text: 'LSD',
                                dataIndex: 'leaf_lsd',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }]
                        },
                        {
                            text: '<b>SS</b>',
                            align: 'center',
                            columns: [{
                                text: 'QTY',
                                dataIndex: 'ss_qty',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }, {
                                text: 'M2',
                                dataIndex: 'ss_m2',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }]
                        },
                        {
                            text: '<b>QSAVE</b>',
                            dataIndex: 'qsave',
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            renderer: function(value, record) {
                                if (value) {
                                    return value
                                } else {
                                    return '-'
                                }
                            }
                        },
                        {
                            text: '<b>SP HANGER</b>',
                            align: 'center',
                            columns: [{
                                text: 'Frame',
                                dataIndex: 'sp_hanger_frame',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }, {
                                text: 'Leaf',
                                dataIndex: 'sp_hanger_leaf',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }]
                        },
                        {
                            text: '<b>SMOOD DOOR</b>',
                            align: 'center',
                            columns: [{
                                text: 'Frame',
                                dataIndex: 'smood_door_frame',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }, {
                                text: 'Leaf',
                                dataIndex: 'smood_door_leaf',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }]
                        },
                        {
                            text: '<b>LOUVER</b>',
                            dataIndex: 'louver',
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            renderer: function(value, record) {
                                if (value) {
                                    return value
                                } else {
                                    return '-'
                                }
                            }
                        },
                        {
                            text: '<b>OTHER</b>',
                            dataIndex: 'other',
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            renderer: function(value, record) {
                                if (value) {
                                    return value
                                } else {
                                    return '-'
                                }
                            }
                        }
                    ]
                }
            },
            {
                xtype: 'grid',
                columnLines: true,
                width: 1505,
                title: '<b>US $/QUANTITY</b>',
                reference: 'usQuantityGrid',
                ui: 'light',
                cls: 'service-type shadow',
                style: 'border-left: solid Black 2px',
                emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                bind: {
                    store: '{cummulativeTotalStore}'
                },
                features: [{
                    ftype: 'summary',
                    dock: 'bottom'
                }],
                columns: {
                    items: [{
                            text: '<b>Date</b>',
                            height: 100,
                            dataIndex: 'date',
                            align: 'center',
                            summaryType: 'count',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>Plan</b>';
                            },
                            renderer: function(value, record) {
                                return '<b>' + value + '</b>'
                            }
                        },
                        {
                            text: '<b>Amount</b>',
                            align: 'center',
                            columns: [{
                                    text: '<b>Frame</b>',
                                    align: 'center',
                                    columns: [{
                                        text: '<b>34.70</b>',
                                        dataIndex: 'frame',
                                        align: 'center',
                                        summaryType: 'sum',
                                        summaryRenderer: function(value, summaryData, dataIndex) {
                                            return '<b>' + value + '</b>';
                                        },
                                        renderer: function(value, record) {
                                            if (value) {
                                                return value
                                            } else {
                                                return '-'
                                            }
                                        }
                                    }]
                                },
                                {
                                    text: '<b>Leaf</b>',
                                    align: 'center',
                                    columns: [{
                                        text: 'SD',
                                        align: 'center',
                                        columns: [{
                                            text: '<b>121.94</b>',
                                            dataIndex: 'leaf_sd',
                                            align: 'center',
                                            summaryType: 'sum',
                                            summaryRenderer: function(value, summaryData, dataIndex) {
                                                return '<b>' + value + '</b>';
                                            },
                                            renderer: function(value, record) {
                                                if (value) {
                                                    return value
                                                } else {
                                                    return '-'
                                                }
                                            }
                                        }]
                                    }, {
                                        text: 'LSD',
                                        align: 'center',
                                        columns: [{
                                            text: '<b>121.94</b>',
                                            dataIndex: 'leaf_lsd',
                                            align: 'center',
                                            summaryType: 'sum',
                                            summaryRenderer: function(value, summaryData, dataIndex) {
                                                return '<b>' + value + '</b>';
                                            },
                                            renderer: function(value, record) {
                                                if (value) {
                                                    return value
                                                } else {
                                                    return '-'
                                                }
                                            }
                                        }]
                                    }]
                                },
                                {
                                    text: '<b>SHUTTER</b>',
                                    align: 'center',
                                    columns: [{
                                        text: '<b>1177.86</b>',
                                        dataIndex: 'shutter',
                                        align: 'center',
                                        summaryType: 'sum',
                                        summaryRenderer: function(value, summaryData, dataIndex) {
                                            return '<b>' + value + '</b>';
                                        },
                                        renderer: function(value, record) {
                                            if (value) {
                                                return value
                                            } else {
                                                return '-'
                                            }
                                        }
                                    }]
                                },
                                {
                                    text: '<b>QSAVE</b>',
                                    align: 'center',
                                    columns: [{
                                        text: '<b>2526.70</b>',
                                        dataIndex: 'qsave',
                                        align: 'center',
                                        summaryType: 'sum',
                                        summaryRenderer: function(value, summaryData, dataIndex) {
                                            return '<b>' + value + '</b>';
                                        },
                                        renderer: function(value, record) {
                                            if (value) {
                                                return value
                                            } else {
                                                return '-'
                                            }
                                        }
                                    }]
                                },
                                {
                                    text: '<b>SP HANGER</b>',
                                    align: 'center',
                                    columns: [{
                                            text: 'Frame',
                                            align: 'center',
                                            columns: [{
                                                text: '<b>157.50</b>',
                                                dataIndex: 'sp_hanger_frame',
                                                align: 'center',
                                                summaryType: 'sum',
                                                summaryRenderer: function(value, summaryData, dataIndex) {
                                                    return '<b>' + value + '</b>';
                                                },
                                                renderer: function(value, record) {
                                                    if (value) {
                                                        return value
                                                    } else {
                                                        return '-'
                                                    }
                                                }
                                            }]
                                        },
                                        {
                                            text: 'Leaf',
                                            align: 'center',
                                            columns: [{
                                                text: '<b>340.50</b>',
                                                dataIndex: 'sp_hanger_leaf',
                                                align: 'center',
                                                summaryType: 'sum',
                                                summaryRenderer: function(value, summaryData, dataIndex) {
                                                    return '<b>' + value + '</b>';
                                                },
                                                renderer: function(value, record) {
                                                    if (value) {
                                                        return value
                                                    } else {
                                                        return '-'
                                                    }
                                                }
                                            }]
                                        }
                                    ]
                                },
                                {
                                    text: '<b>SMOOD DOOR</b>',
                                    align: 'center',
                                    columns: [{
                                        text: 'Frame',
                                        align: 'center',
                                        columns: [{
                                            text: '<b>174.40</b>',
                                            dataIndex: 'smood_door_frame',
                                            align: 'center',
                                            summaryType: 'sum',
                                            summaryRenderer: function(value, summaryData, dataIndex) {
                                                return '<b>' + value + '</b>';
                                            },
                                            renderer: function(value, record) {
                                                if (value) {
                                                    return value
                                                } else {
                                                    return '-'
                                                }
                                            }
                                        }]
                                    }, {
                                        text: 'Leaf',
                                        align: 'center',
                                        columns: [{
                                            text: '<b>294.00</b>',
                                            dataIndex: 'smood_door_leaf',
                                            align: 'center',
                                            summaryType: 'sum',
                                            summaryRenderer: function(value, summaryData, dataIndex) {
                                                return '<b>' + value + '</b>';
                                            },
                                            renderer: function(value, record) {
                                                if (value) {
                                                    return value
                                                } else {
                                                    return '-'
                                                }
                                            }
                                        }]
                                    }]
                                },
                                {
                                    text: '<b>LOUVER</b>',
                                    align: 'center',
                                    columns: [{
                                        text: '<b>441.60</b>',
                                        dataIndex: 'louver',
                                        align: 'center',
                                        summaryType: 'sum',
                                        summaryRenderer: function(value, summaryData, dataIndex) {
                                            return '<b>' + value + '</b>';
                                        },
                                        renderer: function(value, record) {
                                            if (value) {
                                                return value
                                            } else {
                                                return '-'
                                            }
                                        }
                                    }]
                                },
                                {
                                    text: '<b>OTHER</b>',
                                    align: 'center',
                                    columns: [{
                                        text: '<b>10.41</b>',
                                        dataIndex: 'louver',
                                        align: 'center',
                                        summaryType: 'sum',
                                        summaryRenderer: function(value, summaryData, dataIndex) {
                                            return '<b>' + value + '</b>';
                                        },
                                        renderer: function(value, record) {
                                            if (value) {
                                                return value
                                            } else {
                                                return '-'
                                            }
                                        }
                                    }]
                                },
                                {
                                    text: '<b>Total</b>',
                                    align: 'center',
                                    dataIndex: 'louver',
                                    summaryType: 'sum',
                                    summaryRenderer: function(value, summaryData, dataIndex) {
                                        return '<b>' + value + '</b>';
                                    },
                                    renderer: function(value, record) {
                                        if (value) {
                                            return value
                                        } else {
                                            return '-'
                                        }
                                    }
                                },
                                {
                                    text: '<b>Ratio(%)</b>',
                                    align: 'center',
                                    dataIndex: 'ratio',
                                    summaryType: 'sum',
                                    summaryRenderer: function(value, summaryData, dataIndex) {
                                        return '<b>' + value + '</b>';
                                    },
                                    renderer: function(value, record) {
                                        if (value) {
                                            return value
                                        } else {
                                            return '-'
                                        }
                                    }
                                },
                                {
                                    text: '<b>Plan</b>',
                                    align: 'center',
                                    dataIndex: 'plan',
                                    summaryType: 'sum',
                                    summaryRenderer: function(value, summaryData, dataIndex) {
                                        return '<b>' + value + '</b>';
                                    },
                                    renderer: function(value, record) {
                                        if (value) {
                                            return value
                                        } else {
                                            return '-'
                                        }
                                    }
                                }
                            ]
                        }

                    ]
                }
            },
            {
                xtype: 'grid',
                columnLines: true,
                width: 1305,
                title: '<b>Each Day</b>',
                reference: 'eachDayGrid',
                ui: 'light',
                cls: 'service-type shadow',
                style: 'border-left: solid Black 2px',
                emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                bind: {
                    store: '{cummulativeTotalStore}'
                },
                columns: {
                    items: [{
                            text: '<b>Date</b>',
                            dataIndex: 'date',
                            height: 100,
                            align: 'center',
                            summaryType: 'count',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>Plan</b>';
                            },
                            renderer: function(value, record) {
                                return '<b>' + value + '</b>'
                            }
                        },
                        {
                            text: '<b>Frame</b>',
                            dataIndex: 'frame',
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            renderer: function(value, record) {
                                if (value) {
                                    return value
                                } else {
                                    return '-'
                                }
                            }
                        },
                        {
                            text: '<b>Leaf</b>',
                            align: 'center',
                            columns: [{
                                text: 'SD',
                                dataIndex: 'leaf_sd',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }

                            }, {
                                text: 'LSD',
                                dataIndex: 'leaf_lsd',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }]
                        },
                        {
                            text: '<b>SS</b>',
                            align: 'center',
                            columns: [{
                                text: 'QTY',
                                dataIndex: 'ss_qty',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }, {
                                text: 'M2',
                                dataIndex: 'ss_m2',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }]
                        },
                        {
                            text: '<b>QSAVE</b>',
                            dataIndex: 'qsave',
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            renderer: function(value, record) {
                                if (value) {
                                    return value
                                } else {
                                    return '-'
                                }
                            }
                        },
                        {
                            text: '<b>SP HANGER</b>',
                            align: 'center',
                            columns: [{
                                text: 'Frame',
                                dataIndex: 'sp_hanger_frame',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }, {
                                text: 'Leaf',
                                dataIndex: 'sp_hanger_leaf',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }]
                        },
                        {
                            text: '<b>SMOOD DOOR</b>',
                            align: 'center',
                            columns: [{
                                text: 'Frame',
                                dataIndex: 'smood_door_frame',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }, {
                                text: 'Leaf',
                                dataIndex: 'smood_door_leaf',
                                align: 'center',
                                summaryType: 'sum',
                                summaryRenderer: function(value, summaryData, dataIndex) {
                                    return '<b>' + value + '</b>';
                                },
                                renderer: function(value, record) {
                                    if (value) {
                                        return value
                                    } else {
                                        return '-'
                                    }
                                }
                            }]
                        },
                        {
                            text: '<b>LOUVER</b>',
                            dataIndex: 'louver',
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            renderer: function(value, record) {
                                if (value) {
                                    return value
                                } else {
                                    return '-'
                                }
                            }
                        },
                        {
                            text: '<b>OTHER</b>',
                            dataIndex: 'other',
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            renderer: function(value, record) {
                                if (value) {
                                    return value
                                } else {
                                    return '-'
                                }
                            }
                        }
                    ]
                }
            }
        ];
        me.callParent();
    },


});