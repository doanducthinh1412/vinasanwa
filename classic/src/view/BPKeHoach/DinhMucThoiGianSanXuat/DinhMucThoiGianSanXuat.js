Ext.define('CMS.view.BPKeHoach.DinhMucThoiGianSanXuat.DinhMucThoiGianSanXuat', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.grid.feature.Grouping'
    ],
    controller: 'DinhMucThoiGianSanXuat',
    xtype: 'DinhMucThoiGianSanXuat',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        gridPanel: null,
        storeParent: null,
        relationComponent: null,
        relationData: null
    },
    autoScroll: true,
    bodyStyle: {
        'background-color': '#f6f6f6;',
        'padding': '5px'
    },
    initComponent: function() {
        var me = this;
        /*me.tbar = ['->',{
        	text: 'Công cụ',
        	iconCls: 'fas fa-briefcase',
        	ui: 'dark-blue',
        	menu: {
        		xtype: 'menu',
        		plain: true,
        		mouseLeaveDelay: 10,
        		items: [{
        			text: 'In ấn',
        			iconCls: 'fas fa-print text-dark-blue',
        			handler: 'onPrintButtonClick'
        		}, {
        			text: 'Xuất Excel',
        			iconCls: 'fas fa-file-export text-dark-blue',
        			handler: 'onExportdButtonClick'
        		}]
        	}
        }];*/
        me.items = [{
                title: '<b>Định mức sản xuất các tổ Đột, Cắt, Uốn</b>',
                glyph: 'f039@FontAwesome',
                ui: 'light',
                region: 'north',
                cls: 'service-type shadow',
                xtype: 'grid',
                reference: 'dinhMucChungGrid',
                emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                margin: '0 0 15 0',
                columnLines: true,
                tools: [{
                    tooltip: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                }, {
                    tooltip: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                }],
                store: Ext.create('Ext.data.Store', {
                    fields: ['product_name', 'shearing_time_per_product', 'shearing_people_per_machine', 'punching_time_per_product', 'punching_people_per_machine', 'bending_time_per_product', 'bending_people_per_machine'],
                    data: [{
                        'product_name': 'Khung etai / Frame',
                        'shearing_time_per_product': '15',
                        'shearing_people_per_machine': '2',
                        'punching_time_per_product': '10',
                        'punching_people_per_machine': '2',
                        'bending_time_per_product': '45',
                        'bending_people_per_machine': '2'
                    }, {
                        'product_name': 'Cánh LSD / LSD leaf',
                        'shearing_time_per_product': '15',
                        'shearing_people_per_machine': '2',
                        'punching_time_per_product': '7',
                        'punching_people_per_machine': '2',
                        'bending_time_per_product': '20',
                        'bending_people_per_machine': '2'
                    }, {
                        'product_name': 'Cánh SD / SD Leaf',
                        'shearing_time_per_product': '30',
                        'shearing_people_per_machine': '2',
                        'punching_time_per_product': '8',
                        'punching_people_per_machine': '2',
                        'bending_time_per_product': '15',
                        'bending_people_per_machine': '2'
                    }]
                }),
                columns: [{
                    xtype: 'rownumberer',
                    text: '<b>TT</b>',
                    width: 65,
                    align: 'center'
                }, {
                    text: 'Tên sản phẩm',
                    width: 270,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'product_name',
                    renderer: function(value) {
                        return '<b>' + value + '</b>'
                    },
                }, {
                    text: 'Thời gian cắt<br>(phút/sản phẩm)',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'shearing_time_per_product',
                }, {
                    text: 'Số người/máy',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'shearing_people_per_machine',
                }, {
                    text: 'Thời gian đột<br>(phút/sản phẩm)',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'punching_time_per_product',
                }, {
                    text: 'Số người/máy',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'punching_people_per_machine',
                }, {
                    text: 'Thời gian uốn<br>(phút/sản phẩm)',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'bending_time_per_product',
                }, {
                    text: 'Số người/máy',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'bending_people_per_machine',
                }]
            },
            {
                title: '<b>Định mức Hàn Khung</b>',
                glyph: 'f039@FontAwesome',
                region: 'center',
                ui: 'light',
                cls: 'service-type shadow custom-grid',
                xtype: 'grid',
                reference: 'dinhMucHanKhungGrid',
                emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                margin: '0 0 15 0',
                columnLines: true,
                tools: [{
                    tooltip: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                }, {
                    tooltip: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                }],
                store: Ext.create('Ext.data.Store', {
                    fields: ['product_name', 'producing_process', 'worker_per_process', 'manufacture_time', 'average_time_per_product', 'frame_per_day', 'total_frame'],
                    data: [{
                        'product_name': 'Khung',
                        'producing_process': 'Lắp ráp chi tiết tiêu chuẩn, ke chống cháy',
                        'worker_per_process': '12 người (5040 phút/ngày)',
                        'manufacture_time': '700 phút/ngày',
                        'average_time_per_product': '5 phút/cửa ',
                        'frame_per_day': '140',
                        'total_frame': 'Cả dây chuyền làm hoàn thiện được 120 khung/ngày'
                    }, {
                        'product_name': 'Khung',
                        'producing_process': 'Bắn vít',
                        'worker_per_process': '12 người (5040 phút/ngày)',
                        'manufacture_time': '1120 phút/ngày',
                        'average_time_per_product': '12 phút/cửa',
                        'frame_per_day': '140',
                        'total_frame': 'Cả dây chuyền làm hoàn thiện được 120 khung/ngày'
                    }, {
                        'product_name': 'Khung',
                        'producing_process': 'Lắp ráp',
                        'worker_per_process': '12 người (5040 phút/ngày)',
                        'manufacture_time': '3220 phút/ngày',
                        'average_time_per_product': '26 phút/cửa',
                        'frame_per_day': '120',
                        'total_frame': 'Cả dây chuyền làm hoàn thiện được 120 khung/ngày'
                    }]
                }),
                columns: [{
                    text: 'Loại cửa',
                    flex: 1,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'product_name',
                    renderer: function(value, meta, record, rowIndex, colIndex, store) {
                        var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('product_name'),
                            last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('product_name');
                        meta.css += 'row-span' + (first ? ' row-span-first' : '') + (last ? ' row-span-last' : '');
                        if (first) {
                            var i = rowIndex + 1;
                            while (i < store.getCount() && value === store.getAt(i).get('product_name')) {
                                i++;
                            }
                            var rowHeight = 20,
                                padding = 6,
                                height = (rowHeight * (i - rowIndex) - padding) + 'px';
                            meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
                        }
                        return first ? value : '';
                    }
                }, {
                    text: 'Công đoạn sản xuất',
                    flex: 2,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'producing_process',
                }, {
                    text: 'Số người trong một công đoạn',
                    flex: 2,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'worker_per_process',
                    renderer: function(value, meta, record, rowIndex, colIndex, store) {
                        var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('worker_per_process'),
                            last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('worker_per_process');
                        meta.css += 'row-span' + (first ? ' row-span-first' : '') + (last ? ' row-span-last' : '');
                        if (first) {
                            var i = rowIndex + 1;
                            while (i < store.getCount() && value === store.getAt(i).get('worker_per_process')) {
                                i++;
                            }
                            var rowHeight = 20,
                                padding = 6,
                                height = (rowHeight * (i - rowIndex) - padding) + 'px';
                            meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
                        }
                        return first ? value : '';
                    }
                }, {
                    text: 'Thời gian sản xuất',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'manufacture_time',
                }, {
                    text: 'Thời gian trung bình sản phẩm',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'average_time_per_product',
                }, {
                    text: 'Số khung thực hiện/ngày',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'frame_per_day',
                }, {
                    text: 'Tổng cộng',
                    flex: 0.7,
                    sortable: false,
                    align: 'center',
                    dataIndex: 'total_frame',
                    renderer: function(value, meta, record, rowIndex, colIndex, store) {
                        var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('total_frame'),
                            last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('total_frame');
                        meta.css += 'row-span' + (first ? ' row-span-first' : '') + (last ? ' row-span-last' : '');
                        if (first) {
                            var i = rowIndex + 1;
                            while (i < store.getCount() && value === store.getAt(i).get('total_frame')) {
                                i++;
                            }
                            var rowHeight = 20,
                                padding = 6,
                                height = (rowHeight * (i - rowIndex) - padding) + 'px';
                            meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
                        }
                        return first ? value : '';
                    }
                }],

            },
            {
                title: '<b>Định mức Hàn Cánh</b>',
                glyph: 'f039@FontAwesome',
                region: 'center',
                ui: 'light',
                cls: 'service-type shadow',
                xtype: 'grid',
                reference: 'dinhMucHanCanhGrid',
                emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                margin: '0 0 15 0',
                columnLines: true,
                features: [{
                    ftype: 'grouping',
                    //startCollapsed: true,
                    groupHeaderTpl: '<b>{name}</b>'
                }],
                tools: [{
                    tooltip: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                }, {
                    tooltip: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                }],
                store: Ext.create('Ext.data.Store', {
                    fields: ['product_type', 'product_name', 'total_product'],
                    groupField: 'product_type',
                    sorter: {
                        property: 'product_type',
                        direction: 'DESC'
                    },
                    data: [{
                        'product_type': 'Cửa SD',
                        'product_name': 'Cửa đơn, không có ô kính hay louver',
                        'total_product': 'Cả dây chuyền làm hoàn thiện được 115 cánh/ngày',
                    }, {
                        'product_type': 'Cửa SD',
                        'product_name': 'Cửa đôi, không có ô kính hay louver',
                        'total_product': 'Cả dây chuyền làm hoàn thiện được 75 cánh/ngày + 40 cánh ép chờ hoàn thiện',
                    }, {
                        'product_type': 'Cửa SD',
                        'product_name': 'Cửa đơn, có ô kính hay louver',
                        'total_product': 'Cả dây chuyền làm hoàn thiện được 90 cánh/ngày',
                    }, {
                        'product_type': 'Cửa SD',
                        'product_name': 'Cửa đôi, có ô kính hay louver',
                        'total_product': 'Cả dây chuyền làm hoàn thiện được 60 cánh/ngày + 60 cánh ép chờ hoàn thiện',
                    }, {
                        'product_type': 'Cửa LSD',
                        'product_name': 'Cửa đơn, không có ô kính hay louver',
                        'total_product': 'Cả dây chuyền làm hoàn thiện được 100 cánh/ngày',
                    }, {
                        'product_type': 'Cửa LSD',
                        'product_name': 'Cửa đôi, không có ô kính hay louver',
                        'total_product': 'Cả dây chuyền làm hoàn thiện được 80 cánh/ngày + 20 cánh ép chờ hoàn thiện<br> hoặc <br>Cả dây chuyền làm hoàn thiện được 90 cánh/ngày + 10 cánh ép chờ hoàn thiện',
                    }, {
                        'product_type': 'Cửa LSD',
                        'product_name': 'Cửa đơn, có ô kính hay louver',
                        'total_product': 'Cả dây chuyền làm hoàn thiện được 75 cánh/ngày',
                    }, {
                        'product_type': 'Cửa LSD',
                        'product_name': 'Cửa đôi, có ô kính hay louver',
                        'total_product': 'Cả dây chuyền làm hoàn thiện được 60 cánh/ngày + 15 cánh ép chờ hoàn thiện',
                    }]
                }),
                columns: [{
                        xtype: 'rownumberer',
                        text: '<b>TT</b>',
                        width: 65,
                        align: 'center'
                    },
                    {
                        text: 'Loại cửa',
                        align: 'center',
                        dataIndex: 'product_name',
                        flex: 1,
                        renderer: function(value) {
                            return '<b>' + value + '</b>'
                        }
                    }, {
                        text: 'Tổng cộng',
                        align: 'center',
                        dataIndex: 'total_product',
                        flex: 1
                    }
                ]
            }
        ];
        me.callParent();
    },


});