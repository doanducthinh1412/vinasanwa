Ext.define('CMS.view.BPKeHoach.ThongKeLoaiCua.ThongKeLoaiCua', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.panel.Panel'
    ],
    controller: 'ThongKeLoaiCua',
    viewModel: 'ThongKeLoaiCua',
    xtype: 'ThongKeLoaiCua',
    layout: 'border',
    autoScroll: true,
    columnLines: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    initComponent: function() {
        var me = this;
        var years = [];
        var y = 2000;
        var current_year = (new Date()).getFullYear();
        //years.push([0,'------']);
        while (current_year >= y) {
            years.push([current_year, current_year]);
            current_year--;
        }
        var current_year = (new Date()).getFullYear();
        var yearStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'display'],
            data: years
        });
        var current_month = (new Date()).getMonth() + 1;
        me.getViewModel().set('month', current_month);
        me.getViewModel().set('year', current_year);

        /*
        var gridData = [];
        for (i= 1; i < current_month+1; i++ ) {
        	var new_row = {};
        	new_row.year = current_year;
        	new_row.month = i;
        	new_row.wood_grain_frame = null;
        	new_row.wood_grain_leaf = null;
        	new_row.standard_door_frame = null;
        	new_row.smoor_door_frame = null;
        	new_row.smoor_door_leaf = null;
        	new_row.sp_hanger_frame = null;
        	new_row.sp_hanger_leaf = null;
        	new_row.quicksave_grs = null;
        	new_row.quicksave_srn = null;
        	new_row.quicksave_s13 = null;
        	new_row.quicksave_s14 = null;
        	new_row.shutter_grill = null;
        	new_row.shutter_typhoon_hook = null;
        	gridData.push(new_row);
        }
        Ext.Ajax.request({
        	url: '/api/ordersheet/statistic-door?year=' + current_year,
        	success: function(response) {
        		var res = Ext.decode(response.responseText);
        		if (res.success) {
        			var data = res.data;
        			if (data) {
        				data.forEach(function(row) {
        					if (row.frame_type == 'sld') {
        						gridData[row.month-1].smoor_door_frame = row.frame_total;
        						gridData[row.month-1].smoor_door_leaf = row.leaf_total;
        					} else if (row.frame_type == 'sp') { 
        						gridData[row.month-1].sp_hanger_frame = row.frame_total;
        						gridData[row.month-1].sp_hanger_leaf = row.leaf_total;
        					} else if (row.frame_type == 'grs') { 
        						gridData[row.month-1].quicksave_grs = row.type_total;
        					} else if (row.frame_type == 'srn') { 
        						gridData[row.month-1].quicksave_srn = row.type_total;
        					} else if (row.frame_type == 's13') { 
        						gridData[row.month-1].quicksave_s13 = row.type_total;
        					} else if (row.frame_type == 's14') { 
        						gridData[row.month-1].quicksave_s14 = row.type_total;
        					} else if (row.frame_type == 'ssgrill') { 
        						gridData[row.month-1].shutter_grill = row.type_total;
        					}
        				});
        			}
        			me.getViewModel().getStore('thongKeLoaiCuaStore').loadData(gridData,true);
        		}
        	},
        });
        */
        me.tbar = [{
                xtype: 'combo',
                fieldLabel: 'Chọn năm',
                //emptyText  	: '----------',
                labelWidth: 100,
                name: 'yearCmb',
                store: yearStore,
                queryMode: 'local',
                displayField: 'display',
                valueField: 'value',
                width: 250,
                reference: 'yearCmb',
                value: current_year
            }, '-',
            {
                text: 'Xem',
                ui: 'dark-blue',
                handler: function() {
                    var refs = me.getReferences();
                    var year = refs.yearCmb.getValue();

                    if (year) {
                        me.getViewModel().set('year', year);
                        me.getViewModel().set('month', 1);
                        me.getViewModel().getStore('thongKeLoaiCuaStore').getProxy().url = '/api/ordersheet/statistic-door?year=' + year;
                        me.getViewModel().getStore('thongKeLoaiCuaStore').reload();
                        me.getViewModel().getStore('thongKeLoaiCuaMonthStore').getProxy().url = '/api/ordersheet/statistic-door?year=' + year + '&&month=1';
                        me.getViewModel().getStore('thongKeLoaiCuaMonthStore').reload();
                        /*
                        refs.ThongKeLoaiCuaGrid.getStore().reload({
                        	params: {
                        		year: year
                        	}
                        });
                        refs.ThongKeLoaiCuaMonthGrid.getStore().reload({
                        	params: {
                        		year: year,
                        		month: (year==current_year)? current_month : 1
                        	}
                        });
                        */
                    }
                }
            },
            '->',
            {
                text: 'Công cụ',
                iconCls: 'fas fa-briefcase',
                ui: 'dark-blue',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    mouseLeaveDelay: 10,
                    items: [{
                        text: 'In ấn',
                        iconCls: 'fas fa-print text-dark-blue',
                        handler: 'onPrintButtonClick'
                    }, {
                        text: 'Xuất Excel',
                        iconCls: 'fas fa-file-export text-dark-blue',
                        handler: 'onExportdButtonClick'
                    }, '-', {
                        text: 'Làm mới danh sách',
                        iconCls: 'fas fa-sync-alt text-dark-blue',
                        handler: 'onReloadButtonClick'
                    }]
                }
            }
        ];
        //console.log(me.getViewModel());
        me.items = [{
                xtype: 'grid',
                region: 'north',
                height: '50%',
                autoScroll: true,
                columnLines: true,
                bind: {
                    store: '{thongKeLoaiCuaStore}'
                },
                reference: 'ThongKeLoaiCuaGrid',
                listeners: {
                    'itemdblclick': 'onItemDblClick'
                },
                columns: [{
                        xtype: 'rownumberer',
                        text: '<b>TT</b>',
                        width: 65,
                        align: 'center'
                    },
                    {
                        text: '<b>PROJECT</b>',
                        dataIndex: 'name',
                        flex: 2,
                        align: 'left',
                        renderer: function(value, meta, rec) {
                            //value = Array(2 - Math.floor(Math.log10(value))).join('0') + value;
                            return '<b>Tháng ' + value + '/' + me.getViewModel().get('year') + '</b>';
                            //return value
                        },
                        summaryType: 'count',
                        summaryRenderer: function(value, summaryData, dataIndex) {
                            return '<b>Total</b>';
                        }
                    },
                    {
                        text: '<b>ORDER</b>',
                        dataIndex: null,
                        width: 100,
                        align: 'center',
                    },
                    {
                        text: '<b>WOOD GRAIN<br>(Tôn vân gỗ)</b>',
                        flex: 2,
                        columns: [{
                            text: 'Frame',
                            dataIndex: 'wood_grain_frame',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }, {
                            text: 'Leaf',
                            dataIndex: 'wood_grain_leaf',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }]
                    },
                    {
                        text: '<b>STANDARD DOOR<br>(Cửa tiêu chuẩn)</b>',
                        flex: 2,
                        columns: [{
                            text: 'Frame',
                            dataIndex: 'standard_door_frame',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }, {
                            text: 'Leaf',
                            dataIndex: 'standard_door_leaf',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }]
                    },
                    {
                        text: '<b>SMOOR DOOR</b>',
                        flex: 2,
                        columns: [{
                            text: 'Frame',
                            dataIndex: 'smoor_door_frame',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }, {
                            text: 'Leaf',
                            dataIndex: 'smoor_door_leaf',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }]
                    },
                    {
                        text: '<b>SP HANGER</b>',
                        flex: 2,
                        columns: [{
                            text: 'Frame',
                            dataIndex: 'sp_hanger_frame',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }, {
                            text: 'Leaf',
                            dataIndex: 'sp_hanger_leaf',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }]
                    },
                    {
                        text: '<b>QUICK SAVE</b>',
                        flex: 4,
                        columns: [{
                            text: 'GR-S',
                            dataIndex: 'quicksave_grs',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }, {
                            text: 'SR-N',
                            dataIndex: 'quicksave_srn',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }, {
                            text: 'S13',
                            dataIndex: 'quicksave_s13',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }, {
                            text: 'S14',
                            dataIndex: 'quicksave_s14',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }]
                    },
                    {
                        text: '<b>SHUTTER</b>',
                        flex: 2,
                        columns: [{
                            text: 'Grill',
                            dataIndex: 'shutter_grill',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }, {
                            text: 'Typhoon Hook',
                            dataIndex: 'shutter_typhoon_hook',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            }
                        }]
                    }
                ],
                features: [{
                    ftype: 'summary',
                    dock: 'bottom'
                }]
            },
            {
                xtype: 'grid',
                glyph: 'f039@FontAwesome',
                ui: 'light',
                cls: 'service-type shadow',
                region: 'center',
                reference: 'ThongKeLoaiCuaMonthGrid',
                height: '50%',
                autoScroll: true,
                emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
                columnLines: true,
                bind: {
                    store: '{thongKeLoaiCuaMonthStore}',
                    title: '<b>Tháng {month}/{year}</b>',
                },
                plugins: {
                    ptype: 'cellediting',
                    clicksToEdit: 2,
                    listeners: {
                        //edit: 'onEditPlanCell',
                        //beforeedit: 'onBeforeEditCell'
                    }
                },
                hideHeaders: true,
                columns: [{
                        xtype: 'rownumberer',
                        text: '<b>TT</b>',
                        width: 65,
                        align: 'center',
                    },
                    {
                        text: '<b>PROJECT</b>',
                        dataIndex: 'name',
                        flex: 2,
                        align: 'left',
                        renderer: function(value, meta, rec) {
                            return '<b>' + value + '</b>';
                        },
                        summaryType: 'count',
                        summaryRenderer: function(value, summaryData, dataIndex) {
                            return '<b>Total</b>';
                        }
                    },
                    {
                        text: '<b>ORDER</b>',
                        dataIndex: 'sub_no',
                        width: 100,
                        align: 'center',
                    },
                    {
                        text: '<b>WOOD GRAIN<br>(Tôn vân gỗ)</b>',
                        flex: 2,
                        columns: [{
                            text: 'Frame',
                            dataIndex: 'wood_grain_frame',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }, {
                            text: 'Leaf',
                            dataIndex: 'wood_grain_leaf',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }]
                    },
                    {
                        text: '<b>STANDARD DOOR<br>(Cửa tiêu chuẩn)</b>',
                        flex: 2,
                        columns: [{
                            text: 'Frame',
                            dataIndex: 'standard_door_frame',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }, {
                            text: 'Leaf',
                            dataIndex: 'standard_door_leaf',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }]
                    },
                    {
                        text: '<b>SMOOR DOOR</b>',
                        flex: 2,
                        columns: [{
                            text: 'Frame',
                            dataIndex: 'smoor_door_frame',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }, {
                            text: 'Leaf',
                            dataIndex: 'smoor_door_leaf',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }]
                    },
                    {
                        text: '<b>SP HANGER</b>',
                        flex: 2,
                        columns: [{
                            text: 'Frame',
                            dataIndex: 'sp_hanger_frame',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }, {
                            text: 'Leaf',
                            dataIndex: 'sp_hanger_leaf',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }]
                    },
                    {
                        text: '<b>QUICK SAVE</b>',
                        flex: 4,
                        columns: [{
                            text: 'GR-S',
                            dataIndex: 'quicksave_grs',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }, {
                            text: 'SR-N',
                            dataIndex: 'quicksave_srn',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }, {
                            text: 'S13',
                            dataIndex: 'quicksave_s13',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }, {
                            text: 'S14',
                            dataIndex: 'quicksave_s14',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }]
                    },
                    {
                        text: '<b>SHUTTER</b>',
                        flex: 2,
                        columns: [{
                            text: 'Grill',
                            dataIndex: 'shutter_grill',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }, {
                            text: 'Typhoon Hook',
                            dataIndex: 'shutter_typhoon_hook',
                            flex: 1,
                            align: 'center',
                            summaryType: 'sum',
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                return '<b>' + value + '</b>';
                            },
                            editor: {
                                xtype: 'numberfield',
                                selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true
                            },
                        }]
                    }
                ],
                features: [{
                    ftype: 'summary',
                    dock: 'bottom'
                }]
            },
        ];

        /*me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true
        };*/
        //me.menu = me.buildMenu();


        me.callParent();
        //me.getViewModel().getStore('thongKeLoaiCuaStore').reload();
        //me.getStore().filter('year',(new Date()).getFullYear());
    },
    buildMenu: function() {
        var controller = this.getController();
        return Ext.create('Ext.menu.Menu', {
            items: [{
                    text: 'Sửa thông tin',
                    iconCls: 'fas fa-edit text-dark-blue',
                    handler: function() {
                        controller.onEditButtonClick();
                    }
                },
                {
                    text: 'Xóa role',
                    iconCls: 'far fa-trash-alt text-dark-blue',
                    handler: function() {
                        controller.onDeleteButtonClick();
                    }
                }
            ]
        });
    },
    listeners: {
        'itemcontextmenu': 'onItemContextMenu',
        'itemdblclick': 'onEditButtonClick'
    },
    realoadView: function() {
        var me = this;
        me.el.mask('Đang tải...');
        me.getStore().reload();
        me.el.unmask();
    }
});