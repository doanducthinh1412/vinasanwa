Ext.define('CMS.view.DanhMucNguyenVatLieu.DanhMucNguyenVatLieu', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.Panel'
    ],
    controller: 'DanhMucNguyenVatLieu',
    viewModel: 'DanhMucNguyenVatLieu',
    xtype: 'DanhMucNguyenVatLieu',
    layout: 'fit',
    reference: 'DanhMucNguyenVatLieuGrid',
    autoScroll: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
	viewConfig: {
		stripeRows: true,
		columnLines: true
	},
	bind: {
		store: '{danhMucNguyenVatLieuStore}',
	},
    initComponent: function() {
        var me = this;
        me.tbar = [/*{
            xtype: 'button',
            text: 'Tạo mới',
            iconCls: 'fas fa-plus-circle',
            ui: 'soft-orange',
            handler: 'onCreateButtonClick'
        }, '-', {
            text: 'Thao tác',
            iconCls: 'fas fa-bars',
            ui: 'dark-blue',
            disabled: true,
            bind: {
                disabled: '{!QuanLyRoleGrid.selection}',
            },
            menu: me.buildMenu()
        }, */'->', {
            xtype: 'textfield',
            emptyText: 'Nhập tìm kiếm',
            width: 250,
            triggers: {
                clear: {
                    weight: 0,
                    cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                    hidden: true,
                    handler: 'onClearClick',
                    scope: 'this'
                },
                search: {
                    weight: 1,
                    cls: Ext.baseCSSPrefix + 'form-search-trigger',
                    handler: 'onSearchClick',
                    scope: 'this'
                }
            },
            onClearClick: function() {
                this.setValue('');
                this.getTrigger('clear').hide();
                this.updateLayout();
                me.getStore().clearFilter();
            },
            onSearchClick: function() {
                var value = this.getValue();
                if (value.length > 0) {
					this.getTrigger('clear').show();
					me.getStore().filterBy(function(rec, id) {
						var name = rec.get('name').toLowerCase();
						var code = rec.get('code').toLowerCase();
						value = value.toLowerCase();
						if ((name.indexOf(value) != -1) || (code.indexOf(value) != -1)){
							return true;
						}
						else {
							return false;
						}
					});
                } else {
                    this.getTrigger('clear').hide();
                    me.getStore().clearFilter();

                }
            },
            listeners: {
                'specialkey': function(f, e) {
                    if (e.getKey() == e.ENTER) {
                        this.onSearchClick();
                    }
                }
            }
        }, /*{
            xtype: 'button',
            text: 'Lọc theo',
            iconCls: 'fas fa-filter',
            ui: 'dark-blue'
        }, */{
            text: 'Công cụ',
            iconCls: 'fas fa-briefcase',
            ui: 'dark-blue',
            menu: {
                xtype: 'menu',
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                    handler: 'onPrintButtonClick'
                }, {
                    text: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    handler: 'onExportdButtonClick'
                }, '-', {
                    text: 'Làm mới danh sách',
                    iconCls: 'fas fa-sync-alt text-dark-blue',
                    handler: 'onReloadButtonClick'
                }]
            }
        }];
        me.columns = [{
                xtype: 'rownumberer',
                text: '<b>TT</b>',
                width: 65,
                align: 'center'
            },
            {
                text: '<b>Phân loại</b>',
                dataIndex: 'category',
                xtype:'checkcolumn',
				width: 100,
            },
            {
                text: '<b>Material code</b>',
                dataIndex: 'code',
				width: 150,
                align: 'center',

            },
			{
                text: '<b>Material name</b>',
                dataIndex: 'name',
                width: 200,
                align: 'center',
				renderer: function(value, metadata) {
					metadata.style = 'white-space: normal;';
					return value;
				},
            },
			{
                text: '<b>Unit</b>',
                dataIndex: 'unit',
                width: 100,
                align: 'center',
            },
			{
                text: '<b>Delivery time</b>',
                dataIndex: 'delivery_time',
                width: 100,
                align: 'center',
            },
			{
                text: '<b>GR-S</b>',
                columns: [{
					text: 'Qty/Set',
					dataIndex: 'grs_qty_set',
					width: 100,
					align: 'center',
				},{
					text: 'Unit',
					dataIndex: 'grs_unit',
					width: 100,
					align: 'center',
				}]
            },
			{
                text: '<b>S14</b>',
				flex: 2,
                columns: [{
					text: 'Qty/Set',
					dataIndex: 's14_qty_set',
					width: 100,
					align: 'center',
				},{
					text: 'Unit',
					dataIndex: 's14_unit',
					width: 100,
					align: 'center',
				}]
            },
			{
                text: '<b>S13</b>',
				flex: 2,
                columns: [{
					text: 'Qty/Set',
					dataIndex: 's13_qty_set',
					width: 100,
					align: 'center',
				},{
					text: 'Unit',
					dataIndex: 's13_unit',
					width: 100,
					align: 'center',
				}]
            },
			{
                text: '<b>Tensei</b>',
				flex: 2,
                columns: [{
					text: 'Qty/Set',
					dataIndex: 'tensei_qty_set',
					width: 100,
					align: 'center',
				},{
					text: 'Unit',
					dataIndex: 'tensei_unit',
					width: 100,
					align: 'center',
				}]
            },
			{
                text: '<b>JP Shutter</b>',
				flex: 2,
                columns: [{
					text: 'Qty/Set',
					dataIndex: 'jp_qty_set',
					width: 100,
					align: 'center',
				},{
					text: 'Unit',
					dataIndex: 'jp_unit',
					width: 100,
					align: 'center',
				}]
            },
			{
                text: '<b>Steel door</b>',
				flex: 3,
                columns: [{
					text: 'Double',
					dataIndex: 'steel_door_double',
					width: 100,
					align: 'center',
				},{
					text: 'Single',
					dataIndex: 'steel_door_single',
					width: 100,
					align: 'center',
				},{
					text: 'Unit',
					dataIndex: 'steel_door_unit',
					width: 100,
					align: 'center',
				}]
            },
			{
                text: '<b>Sliding door</b>',
				flex: 3,
                columns: [{
					text: 'Double',
					dataIndex: 'sliding_door_double',
					width: 100,
					align: 'center',
				},{
					text: 'Single',
					dataIndex: 'sliding_door_single',
					width: 100,
					align: 'center',
				},{
					text: 'Unit',
					dataIndex: 'sliding_door_unit',
					width: 100,
					align: 'center',
				}]
            },
			{
                text: '<b>Số lượng cần dùng</b>',
                dataIndex: 'total_need',
                width: 100,
                align: 'center',
            },
			{
                text: '<b>Note</b>',
                dataIndex: 'note',
                width: 100,
                align: 'center',
            },
        ];
		/*me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true
        };*/
        me.menu = me.buildMenu();
        me.callParent();
    },
    buildMenu: function() {
        var controller = this.getController();
        return Ext.create('Ext.menu.Menu', {
            items: [{
                    text: 'Sửa thông tin',
                    iconCls: 'fas fa-edit text-dark-blue',
                    handler: function() {
                        controller.onEditButtonClick();
                    }
                },
                {
                    text: 'Xóa role',
                    iconCls: 'far fa-trash-alt text-dark-blue',
                    handler: function() {
                        controller.onDeleteButtonClick();
                    }
                }
            ]
        });
    },
    listeners: {
        'itemcontextmenu': 'onItemContextMenu',
        'itemdblclick': 'onEditButtonClick'
    },
    realoadView: function() {
        var me = this;
        me.el.mask('Đang tải...');
        me.getStore().reload();
        me.el.unmask();
    }
});