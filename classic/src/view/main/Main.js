/* eslint-disable no-unused-vars */
Ext.define('CMS.view.main.Main', {
    extend: 'Ext.container.Viewport',
    requires: [
        'Ext.tab.Panel',
        'CMS.view.main.ChangePass',
        'CMS.view.QuanLyNguoiDung.QuanLyNguoiDung',
    ],
    controller: 'main',
    viewModel: 'main',
    itemId: 'mainView',
    id: 'mainViewPort',
    layout: 'fit',
    initComponent() {
        const me = this;
        const controller = me.getController();
        const lang = 'vi';

        me.items = {
            xtype: 'panel',
            layout: 'border',
            cls: 'sencha-dash-viewport',
            dockedItems: [{
                xtype: 'toolbar',
                cls: 'sencha-dash-dash-headerbar shadow',
                height: 64,
                itemId: 'headerBar',
                dock: 'top',
                items: [{
                        xtype: 'component',
                        reference: 'senchaLogo',
                        cls: 'sencha-logo',
                        html: '<div class="main-logo"><img width="50px" height="auto" src="resources/images/logo.png"><span style="position:relative;bottom:8px;">VINA-SANWA</span></div>',
                        width: 200,
                    }, {
                        xtype: 'button',
                        text: '<font size="2">DASHBOARD</font>',
                        ui: 'header',
                        iconCls: 'text-White x-fa fa-desktop',
                        iconAlign: 'top',
                        handler: 'onClickFunctionMenu',
                        functionMenu: 'Dashboard',
                    }, {
                        xtype: 'button',
                        bind: {
                            text: '<font size="2">{language.CHUCNANG}</font>',
                        },
                        ui: 'header',
                        iconCls: 'text-White fas fa-briefcase',
                        iconAlign: 'top',
                        reference: 'btnmenuChucNang',
                        menu: {
                            xtype: 'menu',
                            plain: true,
                            mouseLeaveDelay: 10,
                            reference: 'menuChucNang',
                            items: [],
                        },
                    }, {
                        xtype: 'button',
                        bind: {
                            text: '<font size="2">{language.QUANTRI}</font>',
                        },
                        ui: 'header',
                        iconCls: 'text-White x-fa fa-cogs',
                        iconAlign: 'top',
                        reference: 'btnmenuQuanTri',
                        menu: {
                            xtype: 'menu',
                            plain: true,
                            mouseLeaveDelay: 10,
                            reference: 'menuQuanTri',
                            items: [],
                        },
                    }, '-', {
                        xtype: 'button',
                        bind: {
                            text: '<font size="2">{language.THONGBAO}</font>',
                        },
                        ui: 'header',
                        iconCls: 'text-White fas fa-bell',
                        iconAlign: 'top',
                    },
                    '->',
                    /* {
                        xtype: 'textfield',
                        bind: {
                            emptyText: '{language.TXT_TIMKIEM_TOPBAR}',
                        },
                        width: 250,
                        triggers: {
                            clear: {
                                weight: 0,
                                cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                                hidden: true,
                                handler: 'onClearClick',
                                scope: 'this'
                            },
                            search: {
                                weight: 1,
                                cls: Ext.baseCSSPrefix + 'form-search-trigger',
                                handler: 'onSearchClick',
                                scope: 'this'
                            }
                        },
                        onClearClick: function() {
                            this.setValue('');
                            this.getTrigger('clear').hide();
                            this.updateLayout();
                        },
                        onSearchClick: function() {
                            var value = this.getValue();
                            if (value.length > 0) {
                                this.getTrigger('clear').show();
                                controller.searchFunction(value);
                                this.setValue('');
                                this.getTrigger('clear').hide();
                            } else {
                                this.getTrigger('clear').hide();

                            }

                        },
                        listeners: {
                            'specialkey': function(f, e) {
                                if (e.getKey() == e.ENTER) {
                                    this.onSearchClick();
                                }
                            }
                        }
                    }, '-', */
                    {
                        xtype: 'image',
                        cls: 'header-right-profile-image',
                        height: 35,
                        width: 35,
                        alt: 'User',
                        reference: 'avartaUser',
                        margin: '0 0 0 0',
                        src: 'resources/images/user-160x160.jpg',
                    },
                    {
                        xtype: 'button',
                        bind: {
                            text: '<font size="2">{MainTxtUserName}</font>',
                        },
                        ui: 'header',
                        menu: {
                            xtype: 'menu',
                            plain: true,
                            mouseLeaveDelay: 10,
                            items: [{
                                bind: {
                                    text: '{language.USERINFO}',
                                },
                                iconCls: 'text-dark-blue fas fa-user',
                                handler: 'onViewInfoButtonClick',
                            }, {
                                bind: {
                                    text: '{language.CHANGEPASS}',
                                },
                                iconCls: 'text-dark-blue fas fa-key',
                                handler: 'onChangePassButtonClick',
                            }, '-', {
                                bind: {
                                    text: '{language.LOGOUT}',
                                },
                                iconCls: 'text-dark-blue fas fa-sign-out-alt',
                                handler: 'onClickBtnLogout',
                            }],
                        },
                    },
                ],
            }, {
                xtype: 'toolbar',
                cls: 'sencha-dash-dash-headerbar shadow',
                itemId: 'footerBar',
                dock: 'bottom',
                items: [{
                    xtype: 'button',
                    bind: {
                        text: `<font size="2">v${Config.version}</font>`,
                    },
                    ui: 'header',
                }, '->', {
                    xtype: 'button',
                    bind: {
                        text: '<font size="2">{language.TXT_COPPYRIGHT}</font>',
                    },
                    ui: 'header',
                }, '->', '-', {
                    xtype: 'button',
                    bind: {
                        text: '<i class="fas fa-question-circle"></i> <font size="2">{language.TXT_SUPPORT}</font>',
                    },
                    ui: 'header',
                }],
            }],
            items: [{
                xtype: 'tabpanel',
                region: 'center',
                reference: 'mainView',
                itemId: 'mainView',
                layout: 'fit',
                scrollable: false,
                autoScroll: false,
                plain: true,
                cls: 'tabviewport',
                style: {
                    'background-image': 'url(resources/images/square.gif)',
                },
                listeners: {
                    tabchange: 'onMainViewTabChange',
                },
                items: [],
            }],
        };
        this.callParent(arguments);
    },
    initPermission() {
        const me = this;
        const controller = me.getController();
        const refs = controller.getReferences();
        const { menuChucNang } = refs;
        const { btnmenuChucNang } = refs;
        const { menuQuanTri } = refs;
        const { btnmenuQuanTri } = refs;
        const module_chucnang_ = [];
        const module_quantri_ = [];
        // Kiểm tra quyền Menu chức năng
        if (ClassMain.checkDepartment('kehoach') === true) {
            if (ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'list') === true) {
                module_chucnang_.push({
                    text: 'Quản lý đơn đặt hàng',
                    iconCls: 'text-dark-blue fas fa-file-invoice-dollar',
                    functionMenu: 'QuanLyDonDatHang',
                    handler: 'onClickFunctionMenu',
                });
            }
            if (ClassMain.checkPermission('quan_ly_ke_hoach_san_xuat', 'ProductSchedule', 'list') === true) {
                module_chucnang_.push({
                    text: 'Quản lý kế hoạch sản xuất',
                    iconCls: 'text-dark-blue fas fa-calendar',
                    functionMenu: 'QuanLyKeHoachSanXuat',
                    handler: 'onClickFunctionMenu',
                });
            }
            if (ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'list') === true) {
                module_chucnang_.push({
                    text: 'Quản lý dự án',
                    iconCls: 'text-dark-blue fas fa-briefcase',
                    functionMenu: 'QuanLyDuAn',
                    handler: 'onClickFunctionMenu',
                });
            }
            const module_baocao_ = [];
            if (ClassMain.checkPermission('quan_ly_ke_hoach_san_xuat', 'ProductSchedule', 'report') === true) {
                module_baocao_.push({
                    text: 'Kế hoạch sản xuất tháng',
                    functionMenu: 'KeHoachSanXuatThang',
                    iconCls: 'text-dark-blue fas fa-file-contract',
                    handler: 'onClickFunctionMenu',
                });
            }
            if (ClassMain.checkPermission('quan_ly_ke_hoach_san_xuat', 'ProductSchedule', 'report') === true) {
                module_baocao_.push({
                    text: 'Đơn đặt hàng trong tháng',
                    functionMenu: 'KeHoachSanXuatDonDatHangTrongThang',
                    iconCls: 'text-dark-blue fas fa-file-contract',
                    handler: 'onClickFunctionMenu',
                });
            }
            if (ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'statisticDoor') === true) {
                module_baocao_.push({
                    text: 'Thống kê loại cửa',
                    functionMenu: 'ThongKeLoaiCua',
                    iconCls: 'text-dark-blue fas fa-file-contract',
                    handler: 'onClickFunctionMenu',
                });
            }
            if (module_baocao_.length !== 0) {
                module_chucnang_.push('-');
                module_chucnang_.push({
                    text: 'Báo cáo thống kê',
                    iconCls: 'text-dark-blue fas fa-chart-line',
                    menu: {
                        xtype: 'menu',
                        plain: true,
                        mouseLeaveDelay: 10,
                        items: module_baocao_,
                    },
                });
            }
            module_chucnang_.push('-');
            module_chucnang_.push({
                text: 'Quản lý danh mục',
                iconCls: 'text-dark-blue fas fa-clipboard-list',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    mouseLeaveDelay: 10,
                    items: [{
                        text: 'Định mức thời gian sản xuất',
                        functionMenu: 'DinhMucThoiGianSanXuat',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }, {
                        text: 'Danh mục dự án',
                        functionMenu: 'DanhMucDuAn',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }, {
                        text: 'Danh mục nguyên vật liệu',
                        functionMenu: 'DanhMucNguyenVatLieu',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }],
                },
            });
        }

        if (ClassMain.checkDepartment('ve') === true) {
            if (ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'list') === true) {
                module_chucnang_.push({
                    text: 'Danh sách đơn đặt hàng',
                    iconCls: 'text-dark-blue fas fa-file-invoice-dollar',
                    functionMenu: 'DanhSachDonDatHang',
                    handler: 'onClickFunctionMenu',
                });
            }

            if (ClassMain.checkPermission('quan_ly_ban_ve', 'AssignmentOrderSheet', 'list') === true) {
                module_chucnang_.push('-');
                module_chucnang_.push({
                    text: 'Quản lý phân công vẽ',
                    iconCls: 'text-dark-blue fas fa-drafting-compass',
                    functionMenu: 'PhanCongTheoDoi',
                    handler: 'onClickFunctionMenu',
                });
            }

            if (ClassMain.checkPermission('quan_ly_ban_ve', 'AssignmentOrderSheet', 'listAssigned') === true) {
                module_chucnang_.push('-');
                module_chucnang_.push({
                    text: 'Phân công vẽ cần thực hiện',
                    iconCls: 'text-dark-blue fas fa-drafting-compass',
                    functionMenu: 'DanhSachPhanCongVe',
                    handler: 'onClickFunctionMenu',
                });
            }

            if (ClassMain.checkPermission('quan_ly_ban_ve', 'Drawing', 'list') === true) {
                module_chucnang_.push({
                    text: 'Working Drawing',
                    iconCls: 'text-dark-blue fas fa-clipboard-list',
                    functionMenu: 'WorkingDrawing',
                    handler: 'onClickFunctionMenu',
                });
            }
        }

        if (ClassMain.checkDepartment('tinhgia') === true) {
            if (ClassMain.checkPermission('quan_ly_don_dat_hang', 'OrderSheet', 'list') === true) {
                module_chucnang_.push({
                    text: 'Danh sách đơn đặt hàng',
                    iconCls: 'text-dark-blue fas fa-file-invoice-dollar',
                    functionMenu: 'DanhSachDonDatHang_TinhGia',
                    handler: 'onClickFunctionMenu',
                });
            }
            module_chucnang_.push('-');
            module_chucnang_.push({
                text: 'Báo cáo, thống kê',
                iconCls: 'text-dark-blue fas fa-clipboard-list',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    mouseLeaveDelay: 10,
                    items: [{
                        text: 'Báo cáo giá dự án',
                        functionMenu: 'BaoCaoGiaDuAn',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }, {
                        text: 'Báo cáo giá hàng đã giao',
                        functionMenu: 'BaoCaoGiaHangDaGiao',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }, {
                        text: 'Báo cáo giá giao thực tế',
                        functionMenu: 'BaoCaoGiaGiaoThucTe',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }, {
                        text: 'Báo cáo lợi nhuận của nhà máy',
                        functionMenu: 'BaoCaoLoiNhuanNhaMay',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }],
                },
            });
            module_chucnang_.push('-');
            module_chucnang_.push({
                text: 'Quản lý danh mục',
                iconCls: 'text-dark-blue fas fa-clipboard-list',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    mouseLeaveDelay: 10,
                    items: [{
                        text: 'Danh mục NVL/phụ kiện hàng tiêu chuẩn',
                        functionMenu: 'DanhMucNVLHangTieuChuan',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }, {
                        text: 'Khoảng giá sản phẩm',
                        functionMenu: 'KhoangGiaSanPham',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }, {
                        text: 'Danh mục thành phầm',
                        functionMenu: 'DanhMucThanhPham',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }, {
                        text: 'Danh mục giá nhập kho thành phầm',
                        functionMenu: 'DanhMucGiaNhapKhoThanhPham',
                        iconCls: 'text-dark-blue fas fa-clipboard-list',
                        handler: 'onClickFunctionMenu',
                    }],
                },
            });
        }

        if (ClassMain.checkDepartment('quantri') === true) {
            if (ClassMain.checkPermission('quan_ly_nguoi_dung', 'Users', 'list') === true) {
                module_quantri_.push({
                    text: 'Quản lý người dùng',
                    iconCls: 'text-dark-blue fas fa-users',
                    functionMenu: 'QuanLyNguoiDung',
                    handler: 'onClickFunctionMenu',
                });
            }
            if (ClassMain.checkPermission('quan_ly_nguoi_dung', 'Role', 'list') === true) {
                module_quantri_.push({
                    text: 'Quản lý phân quyền',
                    iconCls: 'text-dark-blue fas fa-id-card',
                    functionMenu: 'QuanLyRole',
                    handler: 'onClickFunctionMenu',
                });
            }
        }

        /* [{
            text: 'Cấu hình hệ thống',
            iconCls: 'text-dark-blue fas fa-wrench',
            functionMenu: 'CauHinhHeThong',
            handler: 'onClickFunctionMenu'
        }, {
            text: 'Log hệ thống',
            iconCls: 'text-dark-blue fas fa-clipboard-list',
            functionMenu: 'LogHeThong',
            handler: 'onClickFunctionMenu'
        }
        ] */

        if (module_chucnang_.length === 0) {
            btnmenuChucNang.hide();
        } else {
            menuChucNang.removeAll();
            menuChucNang.add(module_chucnang_);
            menuChucNang.updateLayout();
        }

        if (module_quantri_.length === 0) {
            btnmenuQuanTri.hide();
        } else {
            menuQuanTri.removeAll();
            menuQuanTri.add(module_quantri_);
            menuQuanTri.updateLayout();
        }
    },
});