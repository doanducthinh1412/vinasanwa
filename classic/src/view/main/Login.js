Ext.define('CMS.view.main.Login', {
    extend: 'Ext.window.Window',
    xtype: 'login',
    requires: [
        'Ext.layout.container.VBox'
    ],
    cls: 'auth-locked-window',
    closable: false,
    resizable: false,
    autoShow: true,
    titleAlign: 'center',
    maximized: true,
    modal: true,
    header: false,
    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'center'
    },
    items: [{
        xtype: 'component',
        cls: 'sencha-logo',
        html: '<div class="main-logo"><img width="50px" height="auto" src="resources/images/logo.png"><span style="position:relative;bottom:8px;">PHẦN MỀM QUẢN LÝ SẢN XUẤT</span></div>',
        width: 415
    }, {
        xtype: 'authdialog',
        defaultButton: 'loginButton',
        autoComplete: true,
        bodyPadding: '20 20',
        cls: 'auth-dialog-login',
        header: false,
        width: 415,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        defaults: {
            margin: '5 0'
        },

        items: [{
                xtype: 'label',
                text: 'Đăng nhập hệ thống'
            },
            {
                xtype: 'textfield',
                cls: 'auth-textbox',
                name: 'username',
                bind: '{user_id}',
                height: 55,
                hideLabel: true,
                allowBlank: false,
                emptyText: 'Tài khoản',
                triggers: {
                    glyphed: {
                        cls: 'trigger-glyph-noop auth-email-trigger'
                    }
                }
            },
            {
                xtype: 'textfield',
                cls: 'auth-textbox',
                height: 55,
                hideLabel: true,
                emptyText: 'Mật khẩu',
                inputType: 'password',
                name: 'password',
                bind: '{password}',
                allowBlank: false,
                triggers: {
                    glyphed: {
                        cls: 'trigger-glyph-noop auth-password-trigger'
                    }
                }
            },
            {
                xtype: 'container',
                layout: 'hbox',
                items: [{
                    xtype: 'label',
                    height: 30,
                    bind: '{notification}'
                }]
            },
            {
                xtype: 'button',
                reference: 'loginButton',
                scale: 'large',
                ui: 'dark-blue',
                iconAlign: 'right',
                iconCls: 'x-fa fa-angle-right',
                text: 'Đăng nhập',
                formBind: true,
                listeners: {
                    click: 'onLoginButton'
                }
            }
        ]
    }],
    initComponent: function() {
        this.addCls('user-login-register-container');
        this.callParent(arguments);
    }
});