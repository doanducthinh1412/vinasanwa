Ext.define('CMS.view.main.UserInfo', {
    extend: 'Ext.window.Window',
    xtype: 'UserInfo',
    viewModel: 'userinfo',
    controller: 'userinfo',
    modal: true,
    minWidth: 450,
    minHeight: 550,
    layout: 'border',
    width: 650,
    height: 600,
    glyph: 'xf007@FontAwesome',
    listeners: {
        //render: 'onLoadData'
    },
    bind: {
        title: '{language.TITLE_USER_INFO}'
    },
    initComponent: function() {
        var me = this;
        var rec = Config.userInfo;
        me.items = [{
            xtype: 'form',
            autoScroll: true,
            reference: 'userInfoForm',
            bodyPadding: 10,
            region: 'center',
            listeners: {
                'dirtychange': function(dirty) {
                    var frm = this;
                    var btn = frm.down('toolbar').down('button[reference=btnEdit]');
                    var dirty = frm.isDirty();
                    if (dirty) {
                        btn.setDisabled(false);
                    } else {
                        btn.setDisabled(true);
                    }
                }
            },
            items: [{
                xtype: 'panel',
                height: 220,
                layout: {
                    type: 'hbox',
                    pack: 'start',
                    align: 'stretch'
                },
                items: [{
                    width: 220,
                    style: { border: '1px solid #d0d0d0' },
                    margin: '20 10 10 0',
                    layout: {
                        type: 'vbox',
                        pack: 'start',
                        align: 'stretch'
                    },
                    items: [{
                        xtype: 'component',
                        reference: 'avatarUser',
                        html: '<center><img width="auto" height="140px" src="/api/users/image?_dc=' + new Date().getTime()+'"></center>',
                        height: 145,
                    }, {
                        xtype: 'button',
                        bind: {
                            text: '{language.TXT_CHANGE_AVARTA}'
                        },
                        ui: 'soft-orange',
                        glyph: 'xf044@FontAwesome',
                        flex: 1,
                        handler: 'onEditAvatarButtonClick'
                    }]
                }, {
                    flex: 1,
                    xtype: 'fieldset',
                    bind: {
                        title: '<b>{language.TXT_THONG_TIN_CHUNG}</b>'
                    },
                    defaultType: 'textfield',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelAlign: 'top'
                    },
                    fieldDefaults: {
                        msgTarget: 'side',
                        autoFitErrors: true
                    },
                    items: [{
                            xtype: 'fieldcontainer',
                            fieldLabel: '<b>Họ tên</b>',
                            labelAlign: 'top',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                            ],
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                xtype: 'textfield',
                                emptyCls: 'emptyText',
                                allowBlank: false
                            },
                            items: [{
                                name: 'last_name',
                                emptyText: 'Họ',
                                margin: '0 10 0 0',
                                value: rec.last_name
                            }, {
                                name: 'first_name',
                                emptyText: 'Tên',
                                value: rec.first_name
                            }, ]
                        }, {
                            bind: {
                                fieldLabel: '<b>{language.TXT_F_ACCOUNT}</b>',
                                emptyText: '{language.TXT_F_ACCOUNT_EMPTY}',
                                //blankText: '{language.TXT_BLANKTEXT}'
                            },
                            name: 'user_name',
                            allowBlank: false,
                            readOnly: true,
                            afterLabelTextTpl: [
                                '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                            ],
                            value: rec.username
                        }
                    ]
                }]
            }, {
                xtype: 'fieldset',
                bind: {
                    title: '<b>{language.TXT_THONG_TIN_KHAC}</b>'
                },
                defaultType: 'textfield',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelAlign: 'top'
                },
                fieldDefaults: {
                    msgTarget: 'side',
                    autoFitErrors: true
                },
                items: [{
                    name: 'position',
                    bind: {
                        fieldLabel: '<b>{language.TXT_F_CHUCVU}</b>',
                        emptyText: '{language.TXT_F_CHUCVU_EMPTY}',
                        //blankText: '{language.TXT_BLANKTEXT}'
                    },
                    readOnly: true,
                    allowBlank: true,
                    afterLabelTextTpl: [
                        '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                    ],
                    value: rec.position
                }, {
                    name: 'department',
                    bind: {
                        fieldLabel: '<b>{language.TXT_F_BOPHAN}</b>',
                        emptyText: '{language.TXT_F_BOPHAN_EMPTY}',
                        //blankText: '{language.TXT_BLANKTEXT}'
                    },
                    xtype: 'combobox',
                    value: rec.department,
                    queryMode: 'remote',
                    displayField: 'name',
                    valueField: 'id',
                    editable: false,
                    allowBlank: false,
                    editable: false,
                    afterLabelTextTpl: [
                        '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                    ],
                    store: Ext.create('Ext.data.Store', {
                        fields: ['id', 'name'],
                        autoLoad: true,
                        proxy: {
                            type: 'ajax',
                            url: '/api/department',
                            reader: {
                                type: 'json',
                                rootProperty: 'data'
                            }
                        }
                    })
                }, {
                    bind: {
                        fieldLabel: '<b>{language.TXT_F_NGONNGU}</b>',
                        emptyText: '{language.TXT_F_NGONNGU_EMPTY}',
                        //blankText: '{language.TXT_BLANKTEXT}'
                    },
                    name: 'lang',
                    xtype: 'combobox',
                    queryMode: 'local',
                    displayField: 'name',
                    valueField: 'val',
                    allowBlank: false,
                    editable: false,
                    afterLabelTextTpl: [
                        '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                    ],
                    store: Ext.create('Ext.data.Store', {
                        fields: ['val', 'name'],
                        data: [
                            { "val": "vi", "name": "Tiếng Việt" },
                            { "val": "en", "name": "English" }
                        ]
                    }),
                    value: rec.lang
                }, {
                    bind: {
                        fieldLabel: '<b>{language.TXT_F_EMAIL}</b>',
                        emptyText: '{language.TXT_F_EMAIL_EMPTY}',
                        //blankText: '{language.TXT_BLANKTEXT}'
                    },
                    name: 'email',
                    vtype: 'email',
                    allowBlank: false,
                    afterLabelTextTpl: [
                        '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                    ],
                    value: rec.email
                }, {
                    bind: {
                        fieldLabel: '<b>{language.TXT_F_SODIENTHOAI}</b>',
                        emptyText: '{language.TXT_F_SODIENTHOAI_EMPTY}',
                        //blankText: '{language.TXT_BLANKTEXT}'
                    },
                    name: 'phone_number',
                    allowBlank: true,
                    value: rec.phone_number
                }, {
                    bind: {
                        fieldLabel: '<b>{language.TXT_F_DIACHI}</b>',
                        emptyText: '{language.TXT_F_DIACHI_EMPTY}',
                        //blankText: '{language.TXT_BLANKTEXT}'
                    },
                    name: 'address',
                    allowBlank: true,
                    value: rec.address
                }, {
                    bind: {
                        fieldLabel: '<b>{language.TXT_F_NGAYSINH}</b>',
                        emptyText: '{language.TXT_F_NGAYSINH_EMPTY}',
                        //blankText: '{language.TXT_BLANKTEXT}'
                    },
                    name: 'date_of_birth',
                    xtype: 'datefield',
                    format: 'd/m/Y',
                    submitFormat: 'Y-m-d',
                    //maxValue: new Date(),
                    allowBlank: true,
                    value: rec.date_of_birth
                }, {
                    bind: {
                        fieldLabel: '<b>{language.TXT_F_GIOITINH}</b>'
                    },
                    name: 'gender',
                    xtype: 'radiogroup',
                    columns: [100, 100],
                    items: [
                        { boxLabel: 'Nam', inputValue: '1', checked: (rec.gender == '1') ? true : false },
                        { boxLabel: 'Nữ', inputValue: '0', checked: (rec.gender == '0') ? true : false }
                    ],

                }]
            }],
            buttons: [{
                bind: {
                    text: '{language.BTN_CLOSE}'
                },
                glyph: 'xf00d@FontAwesome',
                handler: function() {
                    this.up('window').close();
                }
            }, {
                bind: {
                    text: '{language.BTN_SAVE}',
                },
                glyph: 'xf0c7@FontAwesome',
                ui: 'soft-orange',
                disabled: true,
                formBind: true,
                reference: 'btnEdit',
                handler: 'onEditButtonClick'
            }]
        }];
        this.callParent(arguments);
    }
});