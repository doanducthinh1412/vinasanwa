Ext.define('CMS.view.main.ChangePass', {
    extend: 'Ext.window.Window',
    xtype: 'ChangePass',
    viewModel: 'ChangePass',
    controller: 'ChangePass',
    modal: true,
    title: 'Đổi mật khẩu người dùng',
    height: 400,
    width: 500,
    minWidth: 350,
    minHeight: 400,
    layout: 'fit',
    maximizable: true,
    glyph: 'xf007@FontAwesome',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            region: 'north',
            bodyPadding: 10,
            layout: 'anchor',
            defaultType: 'textfield',
            defaults: {
                anchor: '100%',
                labelAlign: 'top'
            },
            fieldDefaults: {
                msgTarget: 'under',
                autoFitErrors: false
            },
            buttons: [{
                text: 'Thoát',
                glyph: 'xf00d@FontAwesome',
                handler: function() {
                    me.close();
                }
            }, {
                text: 'Đổi mật khẩu',
                glyph: 'xf0c7@FontAwesome',
                ui: 'soft-orange',
                formBind: true,
                disabled: true,
                itemId: 'btnChangePass',
                handler: 'onClickChangePass'
            }],
            items: [{
                fieldLabel: '<i class="fas fa-key"></i> <b>Mật khẩu cũ</b>',
                xtype: 'textfield',
                name: 'user_old_pass',
                allowBlank: false,
                blankText: 'Trường bắt buộc nhập thông tin',
                afterLabelTextTpl: [
                    '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                ],
                inputType: 'password'
            }, {
                fieldLabel: '<i class="fas fa-key"></i> <b>Mật khẩu mới</b>',
                xtype: 'textfield',
                name: 'user_new_pass',
                minLength: 8,
                allowBlank: false,
                minLengthText: 'Độ dài mật khẩu lớn hơn 8 ký tự',
                blankText: 'Trường bắt buộc nhập thông tin',
                afterLabelTextTpl: [
                    '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                ],
                inputType: 'password'
            }, {
                fieldLabel: '<i class="fas fa-key"></i> <b>Nhập lại mật khẩu</b>',
                xtype: 'textfield',
                name: 'user_confirm_new_pass',
                allowBlank: false,
                minLength: 8,
                minLengthText: 'Độ dài mật khẩu lớn hơn 8 ký tự',
                blankText: 'Trường bắt buộc nhập thông tin',
                afterLabelTextTpl: [
                    '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                ],
                inputType: 'password',
                validator: function(value) {
                    var password1 = this.previousSibling('[name=user_new_pass]');
                    return (value === password1.getValue()) ? true : 'Mật khẩu không đúng.'
                }
            }]
        }];
        this.callParent(arguments);
    }
});