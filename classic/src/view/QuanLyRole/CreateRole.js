Ext.define('CMS.view.CreateRole.CreateRole', {
    extend: 'Ext.form.Panel',
    requires: [
        
    ],
    controller: 'CreateRole',
    viewModel: 'CreateRole',
    xtype: 'CreateRole',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'left',
        autoFitErrors: true,
        width: 420,
		labelWidth:105,
		
    },
	config: {
		gridPanel: null,
		records: null
	},
    autoScroll: false,
    initComponent: function() {
        var me = this;
		//var quanLyRoleStore = Ext.create('CMS.store.QuanLyRoleStore');
		//quanLyRoleStore.load();
        var items = [{
			xtype: 'fieldset',
            title: 'Thông tin chung',
			defaultType: 'textfield',
			bodyPadding: 10,
			defaults: {
				width: '50%',
				margin: 10
			},
			layout: {
				type: 'hbox'
			},
			items: [{
				fieldLabel: 'Tên role',
				name: 'name',
			},{
				fieldLabel: 'Mô tả',
				name: 'description',
			}]
		},{
			xtype: 'hiddenfield',
			name: 'resources',
			reference: 'resourcesField'
		},
		/*{
			xtype: 'fieldset',
            title: 'Phân quyền theo chức năng',
			items: [{
				xtype: 'grid',
				store: quanLyRoleStore,
				columns: [{
					xtype: 'rownumberer',
					text: '<b>TT</b>',
					width: 65,
					align: 'center'
				},
				{
					text: '<b>Tên chức năng</b>',
					dataIndex: 'name',
					flex: 0.5,
					align: 'center',
					renderer: function(value, meta, rec) {
						return '<b>' + value+ '</b>';
					}
				},{
					text: '<b>Phân quyền</b>',
					xtype: 'widgetcolumn',
					flex: 1,
					widget: {
						xtype: 'tagfield',
						bind: {
							store: {
								fields: ['id','name'],
								data: '{record.resources}'
							},
							value: null
						},
						displayField: 'name',
						valueField: 'id',
						filterPickList: true,
						
					},
					align: 'left'
				}
				]
			}]
		}*/
		];
		me.buttons = ['->',{
			text: 'Tạo mới',
			glyph: 'xf00c@FontAwesome',
			ui: 'soft-orange',
			formBind: true,
			handler: 'onCreateButtonClicked'
		}, {
			text: 'Hủy bỏ',
			glyph: 'xf00d@FontAwesome',
			handler: function() {
				this.up('window').close();
			}
		},'->'];
		var records = me.getRecords();
		var arr = [];
		for (i=0;i<records.length;i++) {
			var data = records[i].data;
			arr.push({
				xtype: 'tagfield',
				fieldLabel: data.name,
				labelWidth: 200,
				width: '100%',
				bind: {
					store: {
						fields: ['id','name'],
						data: data.resources
					},
					value: null
				},
				displayField: 'name',
				valueField: 'id',
				filterPickList: true,
				
			});
		}
		items.push({
			xtype: 'fieldset',
			title: 'Phân quyền theo chức năng',
			items: arr
		});
		me.items = items;
        me.callParent();
    },

     
});