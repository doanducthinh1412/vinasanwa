/* eslint-disable radix */
/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPVe.WorkingDrawing.WorkingDrawing', {
    extend: 'Ext.grid.Panel',
    controller: 'WorkingDrawing',
    viewModel: 'WorkingDrawing',
    xtype: 'WorkingDrawing',
    layout: 'fit',
    store: {
        type: 'WorkingDrawingStore',
        autoLoad: false,
    },
    reference: 'WorkingDrawingGrid',
    autoScroll: true,
    columnLines: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    config: {
        filterString: null,
    },
    features: [{
        ftype: 'grouping',
        startCollapsed: false,
        groupHeaderTpl: '<font color="blue"><b>Dự án:</b> {name} ({rows.length} đơn đặt hàng)</font>',
    }],
    initComponent() {
        const me = this;
        const tbar = [{
            text: 'Thao tác',
            iconCls: 'fas fa-bars',
            ui: 'dark-blue',
            disabled: true,
            bind: {
                disabled: '{!WorkingDrawingGrid.selection}',
                menu: '{actionMenu}',
            },
        }, '-', {
            xtype: 'combobox',
            fieldLabel: '<b>Lọc theo</b>',
            labelAlign: 'left',
            store: {
                field: ['name', 'val'],
                data: [{
                    name: 'Xem tất cả',
                    val: 'status:gt_0_lt_3',
                }, {
                    name: 'Chưa gửi sang BP Kế hoạch',
                    val: 'status:eq_1',
                }, {
                    name: 'Đã gửi sang BP Kế hoạch',
                    val: 'status:eq_2',
                }],
            },
            queryMode: 'local',
            editable: false,
            labelWidth: 80,
            width: 350,
            displayField: 'name',
            value: 'status:gt_0_lt_3',
            valueField: 'val',
            reference: 'ComboboxFilterBanVe',
            listeners: {
                select: 'onSelectFilterBanVe',
            },
        }, '->', {
            xtype: 'textfield',
            emptyText: 'Nhập tìm kiếm',
            width: 250,
            reference: 'SearchField',
            triggers: {
                clear: {
                    weight: 0,
                    cls: `${Ext.baseCSSPrefix}form-clear-trigger`,
                    hidden: true,
                    handler: 'onClearClick',
                    scope: 'this',
                },
                search: {
                    weight: 1,
                    cls: `${Ext.baseCSSPrefix}form-search-trigger`,
                    handler: 'onSearchClick',
                    scope: 'this',
                },
            },
            onClearClick() {
                this.setValue('');
                this.getTrigger('clear').hide();
                this.updateLayout();
                me.getController().loadDataGrid();
            },
            onSearchClick() {
                const value = this.getValue();
                if (value.length > 0) {
                    this.getTrigger('clear').show();
                    this.updateLayout();
                } else {
                    this.getTrigger('clear').hide();
                }
                me.getController().loadDataGrid();
            },
            listeners: {
                specialkey(f, e) {
                    if (e.getKey() === e.ENTER) {
                        this.onSearchClick();
                    }
                },
            },
        }, {
            text: 'Công cụ',
            iconCls: 'fas fa-briefcase',
            ui: 'dark-blue',
            menu: {
                xtype: 'menu',
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                    handler: 'onPrintButtonClick',
                }, {
                    text: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    handler: 'onExportGriddButtonClick',
                }, '-', {
                    text: 'Làm mới danh sách',
                    iconCls: 'fas fa-sync-alt text-dark-blue',
                    handler: 'onReloadButtonClick',
                }],
            },
        }];

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            items: tbar,
        }];
        me.columns = [{
            xtype: 'rownumberer',
            text: '<font style="font-size:11px">TT</font>',
            width: 50,
            locked: true,
            sortable: false,
            align: 'center',
        }, {
            text: '',
            xtype: 'actioncolumn',
            width: 100,
            sortable: false,
            menuDisabled: true,
            locked: true,
            align: 'center',
            items: [{
                iconCls: 'far fa-eye text-blue',
                tooltip: 'Xem bản vẽ',
                handler: 'onViewButtonClickGrid',
            }, {
                width: 5,
            }, {
                iconCls: 'fas fa-download text-blue',
                tooltip: 'Tải bản vẽ',
                handler: 'onExportdButtonClickGrid',
            }, {
                width: 5,
            }, {
                // tooltip: 'Gửi kết quả vẽ',
                getTip: 'getSendTip',
                handler: 'onSenddButtonClickGrid',
                getClass: 'getSendActionClass',
            }],
        }, {
            text: 'Note',
            width: 70,
            dataIndex: 'note',
            align: 'center',
            locked: true,
            renderer(value, meta, rec) {
                if (parseInt(rec.get('status')) === 2) {
                    return `<font color='blue'><b>${value}</b></font>`;
                }
                return `<b>${value}</b>`;
            },
        }, {
            text: 'Project Name',
            width: 120,
            locked: true,
            dataIndex: 'project_name',
            align: 'center',
        }, {
            text: 'Order No',
            width: 110,
            locked: true,
            dataIndex: 'order_no',
            align: 'center',
        }, {
            text: 'Sub No',
            width: 90,
            locked: true,
            dataIndex: 'sub_no',
            align: 'center',
        }, {
            text: 'Quatity',
            columns: [{
                text: 'FRAME',
                columns: [{
                    text: 'SD',
                    width: 50,
                    dataIndex: 'frame_sd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'LSD',
                    width: 50,
                    dataIndex: 'frame_lsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'FSD',
                    width: 50,
                    dataIndex: 'frame_fsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'FLSD',
                    width: 60,
                    dataIndex: 'frame_flsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'WD',
                    width: 60,
                    dataIndex: 'frame_wd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'SUSD',
                    width: 60,
                    dataIndex: 'frame_susd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'LEAF',
                columns: [{
                    text: 'SD',
                    width: 50,
                    dataIndex: 'leaf_sd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'LSD',
                    width: 50,
                    dataIndex: 'leaf_lsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'FSD',
                    width: 50,
                    dataIndex: 'leaf_fsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'FLSD',
                    width: 60,
                    dataIndex: 'leaf_flsd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'WD',
                    width: 60,
                    dataIndex: 'leaf_wd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'SUSD',
                    width: 60,
                    dataIndex: 'leaf_susd',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'SMOOD DOOR',
                columns: [{
                    text: 'Frame',
                    width: 65,
                    dataIndex: 'frame_sld',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'Leaf',
                    width: 60,
                    dataIndex: 'leaf_sld',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'SP HANGER',
                columns: [{
                    text: 'Frame',
                    width: 65,
                    dataIndex: 'frame_sp',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'Leaf',
                    width: 60,
                    dataIndex: 'leaf_sp',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'SHUTTER',
                columns: [{
                    text: 'JP',
                    width: 50,
                    dataIndex: 'ssjp',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'Aichi',
                    width: 60,
                    dataIndex: 'ssaichi',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'Grill',
                    width: 50,
                    dataIndex: 'ssgrill',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'QSAVE',
                columns: [{
                    text: 'GR-S',
                    width: 50,
                    dataIndex: 'grs',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'S-13',
                    width: 50,
                    dataIndex: 's13',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }, {
                    text: 'S-14',
                    width: 50,
                    dataIndex: 's14',
                    renderer: 'renderNumberColumn',
                    align: 'center',
                }],
            }, {
                text: 'SW',
                width: 50,
                dataIndex: 'sw',
                renderer: 'renderNumberColumn',
                align: 'center',
            }, {
                text: 'LOUVER',
                width: 75,
                dataIndex: 'lv',
                renderer: 'renderNumberColumn',
                align: 'center',
            }, {
                text: 'Other',
                width: 60,
                dataIndex: 'other_num',
                renderer: 'renderNumberColumn',
                align: 'center',
            }],
        }];
        me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true,
        };
        me.callParent();
    },

    listeners: {
        itemcontextmenu: 'onItemContextMenu',
        select: 'onItemActionMenu',
        itemdblclick: 'onViewButtonClick',
        render(panel) {
            panel.getController().loadDataGrid();
        },
    },
    reloadView() {
        const me = this;
        me.getController().loadDataGrid();
    },
});