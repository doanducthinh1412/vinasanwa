Ext.define('CMS.view.BPVe.DanhSachPhanCongVe.ViewKetQuaVe', {
    extend: 'Ext.panel.Panel',
    controller: 'ViewKetQuaVe',
    viewModel: 'ViewKetQuaVe',
    xtype: 'ViewKetQuaVe',
    layout: 'border',
    config: {
        recordPanel: null
    },
    initComponent: function() {
        var me = this;
        var recordPanel = me.getRecordPanel();
        var data_working = [];
        var items_phancong = [];
        for (wd in recordPanel.data.assignment_frame_type_detail) {
            data_working.push({
                name: wd,
                value: wd,
                num: parseInt(recordPanel.data.assignment_frame_type_detail[wd])
            });
            items_phancong.push({
                fieldLabel: '<b>' + wd.toUpperCase() + '</b>',
                labelAlign: 'left',
                editable: false,
                value: recordPanel.data.assignment_frame_type_detail[wd]
            });
        }
        if (recordPanel.data.drawer) {
            if (recordPanel.data.drawer['last_name'] !== null && recordPanel.data.drawer['last_name'] !== undefined) {
                items_phancong.push({
                    fieldLabel: '<b>Người vẽ</b>',
                    labelAlign: 'left',
                    editable: false,
                    value: recordPanel.data.drawer['last_name']
                });
            }
        }
        me.items = [{
            region: 'north',
            bodyPadding: 5,
            //layout: 'anchor',
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
            xtype: 'form',
            defaultType: 'textfield',
            reference: 'FormCreateBanVe',
            fieldDefaults: {
                labelWidth: 150,
                width: '100%'
            },
            items: [{
                flex: 0.3,
                xtype: 'fieldset',
                title: 'Phân công vẽ',
                margin: '0 5 0 0',
                defaultType: 'textfield',
                items: items_phancong
            }, {
                xtype: 'fieldset',
                title: 'Nhập kết quả vẽ',
                flex: 1,
                defaultType: 'textfield',
                margin: '0 0 0 0',
                items: [{
                        xtype: 'combobox',
                        fieldLabel: '<b>Loại Working Drawing</b>',
                        labelAlign: 'left',
                        allowBlank: false,
                        reference: 'SoLuongWorkingCombobox',
                        store: Ext.create('Ext.data.Store', {
                            fields: ['value', 'name'],
                            data: data_working
                        }),
                        queryMode: 'local',
                        editable: false,
                        labelWidth: 180,
                        displayField: 'name',
                        valueField: 'value',
                        name: 'type',
                        allowBlank: false,
                        emptyText: 'Chọn loại Working Drawing',
                    },
                    {
                        fieldLabel: '<b>File Excel</b>',
                        allowBlank: false,
                        labelWidth: 180,
                        xtype: 'filefield',
                        emptyText: 'Chọn File Excel Working Drawing',
                        name: 'drawing_import',
                        accept: 'application/vnd.ms-excel',
                        buttonConfig: {
                            iconCls: 'fas fa-file-excel'
                        }
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'assignmentOrderSheet',
                        value: recordPanel ? recordPanel.data.id : ''
                    },
                    {
                        xtype: 'button',
                        iconCls: 'fas fa-drafting-compass',
                        text: 'Nhập kết quả',
                        formBind: true,
                        handler: 'onClickCreateBanVeButton'
                    }
                ]
            }]
        }, {
            region: 'center',
            xtype: 'grid',
            columnLines: true,
            height: '100%',
            layout: 'fit',
            reference: 'GridListKetQuaBanVe',
            border: true,
            ui: 'light',
            title: '<i class="far fa-list-alt"></i> <b>Danh sách kết quả vẽ</b>',
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
            store: {
                type: 'DanhSachKetQuaBanVeStore',
                proxy: {
                    url: '/api/ordersheet/get/' + recordPanel.data.order_sheet.id
                },
                autoLoad: true
            },
            listeners: {
                'itemdblclick': 'onClickButtonViewFileDBClick'
            },
            actions: {
                check: {
                    getClass: 'getCheckClass',
                    getTip: 'getCheckTip',
                    handler: 'onCheckClick',
                    scope: me.getController()
                },
                send: {
                    getClass: 'getSendClass',
                    getTip: 'getSendTip',
                    handler: 'onSendClick',
                    scope: me.getController()
                }
            },
            columns: [{
                xtype: 'rownumberer',
                text: '<b>STT</b>',
                width: 65,
                align: 'center'
            }, {
                menuDisabled: true,
                sortable: false,
                xtype: 'actioncolumn',
                width: 70,
                text: 'Phê duyệt',
                align: 'center',
                items: ['@check']
            }, {
                menuDisabled: true,
                sortable: false,
                xtype: 'actioncolumn',
                width: 70,
                text: 'Gửi BPKH',
                align: 'center',
                items: ['@send']
            }, {
                text: 'Note',
                flex: 1,
                align: 'center',
                dataIndex: 'note'
            }, {
                text: 'LOT',
                flex: 1,
                align: 'center',
                dataIndex: 'lot'
            }, {
                text: 'Ngày nhập kết quả',
                flex: 1,
                align: 'center',
                dataIndex: 'createdAt',
                renderer: 'renderDateColumn'
            }, {
                text: 'Quatity',
                flex: 1,
                columns: [{
                    text: 'Type',
                    flex: 1,
                    dataIndex: 'type',
                    align: 'center'
                }, {
                    text: 'Quatity',
                    flex: 1,
                    dataIndex: 'number',
                    align: 'center'
                }]
            }, {
                text: '',
                xtype: 'actioncolumn',
                width: 105,
                sortable: false,
                menuDisabled: true,
                text: 'Thao tác',
                align: 'center',
                items: [{
                    //iconCls: 'fas fa-times text-blue',
                    getClass: 'getRemoveActionClass',
                    tooltip: 'Xóa',
                    handler: 'onClickButtonRemoveFile'
                }, {
                    width: 5,
                }, {
                    iconCls: 'far fa-eye text-blue',
                    tooltip: 'Xem bản vẽ',
                    handler: 'onClickButtonViewFile'
                }, {
                    width: 5
                }, {
                    iconCls: 'fas fa-download text-blue',
                    tooltip: 'Tải bản vẽ',
                    handler: 'onClickButtonExportFile'
                }]
            }]
        }];
        me.callParent();
    }
});