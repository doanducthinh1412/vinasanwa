/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
Ext.define('CMS.view.BPVe.DanhSachPhanCongVe.DanhSachPhanCongVe', {
    extend: 'Ext.panel.Panel',
    controller: 'DanhSachPhanCongVe',
    viewModel: 'DanhSachPhanCongVe',
    xtype: 'DanhSachPhanCongVe',
    layout: 'fit',
    autoScroll: false,
    config: {},
    reference: 'DanhSachPhanCongVe',
    initComponent() {
        const me = this;
        me.items = {
            autoScroll: false,
            layout: 'fit',
            items: me.createTabPhanCongVe(),
        };
        me.callParent();
    },
    createTabPhanCongVe() {
        const me = this;
        return {
            xtype: 'panel',
            layout: 'border',
            bodyBorder: true,
            tbar: [{
                xtype: 'combobox',
                fieldLabel: '<b>Xem theo</b>',
                labelAlign: 'left',
                store: {
                    field: ['name', 'val'],
                    data: [{
                        name: 'Xem tất cả',
                        val: 'status:eq_1;2;3;4',
                    }, {
                        name: 'Chưa hoàn thành vẽ',
                        val: 'status:eq_3;1',
                    }, {
                        name: 'Đã hoàn thành vẽ',
                        val: 'status:eq_2',
                    }],
                },
                queryMode: 'local',
                editable: false,
                labelWidth: 80,
                width: 350,
                displayField: 'name',
                value: 'status:eq_3;1',
                valueField: 'val',
                reference: 'ComboboxFilterBanVe',
                listeners: {
                    select: 'onSelectFilterBanVe',
                },
            }, '->', {
                xtype: 'textfield',
                emptyText: 'Nhập tìm kiếm',
                width: 250,
                reference: 'SearchField',
                triggers: {
                    clear: {
                        weight: 0,
                        cls: `${Ext.baseCSSPrefix}form-clear-trigger`,
                        hidden: true,
                        handler: 'onClearClick',
                        scope: 'this',
                    },
                    search: {
                        weight: 1,
                        cls: `${Ext.baseCSSPrefix}form-search-trigger`,
                        handler: 'onSearchClick',
                        scope: 'this',
                    },
                },
                onClearClick() {
                    this.setValue('');
                    this.getTrigger('clear').hide();
                    this.updateLayout();
                    me.getController().loadDataGrid();
                },
                onSearchClick() {
                    const value = this.getValue();
                    if (value.length > 0) {
                        this.getTrigger('clear').show();
                        me.getController().loadDataGrid();
                    } else {
                        this.getTrigger('clear').hide();
                        me.getController().loadDataGrid();
                    }
                },
                listeners: {
                    specialkey(f, e) {
                        if (e.getKey() === e.ENTER) {
                            this.onSearchClick();
                        }
                    },
                },
            }, {
                xtype: 'button',
                text: 'Lọc theo',
                iconCls: 'fas fa-filter',
                ui: 'dark-blue',
                menuAlign: 'tr-br',
                reference: 'FilterMenu',
                menu: Ext.create('Ext.menu.Menu', {
                    style: {
                        overflow: 'visible',
                    },
                    mouseLeaveDelay: 10,
                    width: 370,
                    margin: '10 10 10 10',
                    referenceHolder: true,
                    buttons: [{
                        text: 'Lọc',
                        ui: 'soft-orange',
                        handler() {
                            me.getController().loadDataGrid();
                        },
                    }, {
                        text: 'Xóa bộ lọc',
                        handler(btn) {
                            const refs = btn.up().up().getReferences();
                            refs.forEach((item, key) => {
                                item.reset();
                                item.getTrigger('clear').hide();
                            });
                            btn.up().up().hide();
                            me.getController().loadDataGrid();
                        },
                    }],
                    items: [{
                        xtype: 'datefield',
                        fieldLabel: '<b>Order Date</b>',
                        labelAlign: 'left',
                        labelWidth: 120,
                        reference: 'orderdatefilter',
                        margin: '10 10 10 10',
                        fieldIndex: 'order_date',
                        format: 'd/m/Y',
                        altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                        enableKeyEvents: true,
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: -1,
                                hidden: true,
                                scope: 'this',
                                handler(field) {
                                    field.reset();
                                    field.getTrigger('clear').hide();
                                    me.getController().loadDataGrid();
                                },
                            },
                        },
                        listeners: {
                            change(field, newval) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                            },
                            select(field, record, eOpts) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                                me.getController().loadDataGrid();
                            },
                            keypress(field, event) {
                                if (event.getKey() === event.ENTER) {
                                    me.getController().loadDataGrid();
                                }
                            },
                        },
                    }, {
                        xtype: 'datefield',
                        fieldLabel: '<b>Ngày giao  hàng</b>',
                        labelAlign: 'left',
                        labelWidth: 120,
                        reference: 'datedeliveryfilter',
                        margin: '10 10 10 10',
                        fieldIndex: 'delivery_date',
                        format: 'd/m/Y',
                        altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                        enableKeyEvents: true,
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: -1,
                                hidden: true,
                                scope: 'this',
                                handler(field) {
                                    field.reset();
                                    field.getTrigger('clear').hide();
                                    me.getController().loadDataGrid();
                                },
                            },
                        },
                        listeners: {
                            change(field, newval) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                            },
                            select(field, record, eOpts) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                                me.getController().loadDataGrid();
                            },
                            keypress(field, event) {
                                if (event.getKey() === event.ENTER) {
                                    me.getController().loadDataGrid();
                                }
                            },
                        },
                    }, {
                        xtype: 'datefield',
                        fieldLabel: '<b>Thời hạn vẽ</b>',
                        labelAlign: 'left',
                        labelWidth: 120,
                        reference: 'deadlinefilter',
                        margin: '10 10 10 10',
                        fieldIndex: 'deadline',
                        format: 'd/m/Y',
                        altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                        enableKeyEvents: true,
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: -1,
                                hidden: true,
                                scope: 'this',
                                handler(field) {
                                    field.reset();
                                    field.getTrigger('clear').hide();
                                    me.getController().loadDataGrid();
                                },
                            },
                        },
                        listeners: {
                            change(field, newval) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                            },
                            select(field, record, eOpts) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                                me.getController().loadDataGrid();
                            },
                            keypress(field, event) {
                                if (event.getKey() === event.ENTER) {
                                    me.getController().loadDataGrid();
                                }
                            },
                        },
                    }],
                }),
            }, {
                text: 'Công cụ',
                iconCls: 'fas fa-briefcase',
                ui: 'dark-blue',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    mouseLeaveDelay: 10,
                    items: [{
                        text: 'In ấn',
                        iconCls: 'fas fa-print text-dark-blue',
                        handler: 'onPrintButtonClick',
                    }, {
                        text: 'Xuất Excel',
                        iconCls: 'fas fa-file-export text-dark-blue',
                        handler: 'onExportdButtonClick',
                    }, '-', {
                        text: 'Làm mới danh sách',
                        iconCls: 'fas fa-sync-alt text-dark-blue',
                        handler: 'onReloadButtonClick',
                    }],
                },
            }],
            items: [{
                xtype: 'grid',
                reference: 'PhanCongTheoDoiGrid',
                autoScroll: true,
                columnLines: true,
                region: 'center',
                emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
                listeners: {
                    render(grid) {
                        grid.getStore().load();
                    },
                    itemcontextmenu: 'onItemContextMenu',
                    itemdblclick: 'onViewKetQuaVeButtonClick',
                },
                bufferedRenderer: false,
                syncRowHeight: false,
                viewConfig: {
                    columnLines: true,
                    trackOver: false,
                    markDirty: false,
                    stripeRows: true,
                },
                features: [{
                    ftype: 'grouping',
                    startCollapsed: false,
                    groupHeaderTpl: '<font color="blue"><b>Dự án:</b> {name} ({rows.length} đơn đặt hàng)</font>',
                }],
                bbar: {
                    xtype: 'pagingtoolbar',
                    displayInfo: true,
                },
                store: {
                    type: 'PhanCongNhanVienVeVeStore',
                    autoLoad: false,
                    grouper: {
                        groupFn(item) {
                            const datenow = new Date();
                            const time = Ext.Date.parse(item.get('assignment_date'), 'Y-m-d');
                            if (time) {
                                if (Ext.Date.diff(time, datenow, Ext.Date.DAY) === 0) {
                                    return `<i class="far fa-calendar-alt"></i> <font color="blue" style="font-size:11px;"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')} (Today)</font> `;
                                }
                                if (Ext.Date.diff(time, datenow, Ext.Date.DAY) <= -1) {
                                    return `<i class="far fa-calendar-alt"></i> <font color="blue" style="font-size:11px;"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')}</font> `;
                                }
                                return `<i class="far fa-calendar-alt"></i> <font color="blue" style="font-size:11px;"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')}</font>`;
                            }
                            return '';
                        },
                    },
                    listeners: {
                        scope: me.getController(),
                    },
                },
                actions: {
                    save: {
                        getClass: 'getSaveClass',
                        getTip: 'getSaveTip',
                        scope: me.getController(),
                    },
                },
                columns: [{
                        xtype: 'rownumberer',
                        text: '<font style="font-size:11px">TT</font>',
                        width: 45,
                        locked: true,
                        sortable: false,
                        align: 'center',
                    }, {
                        menuDisabled: true,
                        sortable: false,
                        xtype: 'actioncolumn',
                        width: 50,
                        locked: true,
                        align: 'center',
                        items: ['@save'],
                    }, {
                        text: '<font style="font-size:11px">Trạng thái</font>',
                        dataIndex: 'status',
                        sortable: false,
                        locked: true,
                        width: 130,
                        renderer: 'renderStatusColumn',
                    }, {
                        text: '<font style="font-size:11px">Tên dự án</font>',
                        dataIndex: 'project_name',
                        sortable: false,
                        locked: true,
                        width: 120,
                    }, {
                        text: '<font style="font-size:11px">Order No</font>',
                        dataIndex: 'sub_no',
                        sortable: false,
                        locked: true,
                        width: 90,
                    }, {
                        text: '<font style="font-size:11px">Order Date</font>',
                        dataIndex: 'order_date',
                        renderer: 'renderDateColumn',
                        sortable: false,
                        width: 65,
                    }, {
                        text: '<font style="font-size:11px">Delivery</font>',
                        dataIndex: 'delivery_date',
                        renderer: 'renderDeliveryColumn',
                        sortable: false,
                        width: 75,
                    }, {
                        text: '<font style="font-size:11px">Người vẽ</font>',
                        dataIndex: 'drawer_id',
                        width: 75,
                        renderer: 'renderDrawerColumn',
                        sortable: false,
                    }, {
                        text: '<font style="font-size:11px">Thời hạn</font>',
                        dataIndex: 'deadline',
                        sortable: false,
                        width: 75,
                        renderer: 'renderDateColumn',
                    },
                    {
                        text: '<font style="font-size:11px">Unfinish</font>',
                        dataIndex: 'unfinish',
                        sortable: false,
                        width: 75,
                    },
                    {
                        text: '<font style="font-size:11px">Finish</font>',
                        dataIndex: 'finish',
                        sortable: false,
                        width: 75,
                    }, {
                        text: 'Quanity',
                        columns: [{
                            text: '<font style="font-size:10px"><br>FRAME<br></font>',
                            columns: [{
                                text: '<font style="font-size:10px">SD</font>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'frame_sd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px;">LSD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'frame_lsd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FSD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'frame_fsd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FLSD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'frame_flsd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">WD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'frame_wd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">SUSD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'frame_susd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }],
                        }, {
                            text: '<font style="font-size:10px"><br>LEAF<br></font>',
                            columns: [{
                                text: '<font style="font-size:10px">SD</font>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'leaf_sd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">LSD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'leaf_lsd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FSD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'leaf_fsd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FLSD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'leaf_flsd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">WD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'leaf_wd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">SUSD</font></p>',
                                width: 40,
                                align: 'center',
                                dataIndex: 'leaf_susd',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }],
                        }, {
                            text: '<font style="font-size:10px">SMOOD<br> DOOR</font>',
                            columns: [{
                                text: '<p style="width:25px;word-break: break-all;"><font style="font-size:10px">Frame</font></p>',
                                dataIndex: 'frame_sld',
                                sortable: false,
                                width: 45,
                                align: 'center',
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:25px;word-break: break-all;"><font style="font-size:10px">Leaf</font></p>',
                                dataIndex: 'leaf_sld',
                                width: 45,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }],
                        }, {
                            text: '<font style="font-size:10px">SP<br> HANGER</font>',
                            columns: [{
                                text: '<p style="width:25px;word-break: break-all;"><font style="font-size:10px">Frame</font></p>',
                                dataIndex: 'frame_sp',
                                width: 45,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:25px;word-break: break-all;"><font style="font-size:10px">Leaf</font></p>',
                                dataIndex: 'leaf_sp',
                                width: 45,
                                sortable: false,
                                align: 'center',
                                renderer: 'renderNumberColumn',
                            }],
                        }, {
                            text: '<font style="font-size:10px"><br>SHUTTER<br></font>',
                            columns: [{
                                text: '<font style="font-size:10px">JP</font>',
                                dataIndex: 'ssjp',
                                width: 40,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Aichi</font></p>',
                                dataIndex: 'ssaichi',
                                width: 40,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Grill</font></p>',
                                dataIndex: 'ssgrill',
                                width: 40,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }],
                        }, {
                            text: '<font style="font-size:10px"><br>QSAVE<br></font>',
                            columns: [{
                                text: '<font style="font-size:10px">GR-S</font>',
                                dataIndex: 'grs',
                                width: 40,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<font style="font-size:10px">S-13</font>',
                                dataIndex: 's13',
                                width: 40,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }, {
                                text: '<font style="font-size:10px">S-14</font>',
                                dataIndex: 's14',
                                width: 40,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn',
                            }],
                        }, {
                            text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">LOUVER</font></p>',
                            dataIndex: 'lv',
                            width: 40,
                            align: 'center',
                            sortable: false,
                            renderer: 'renderNumberColumn',
                        }, {
                            text: '<font style="font-size:10px">SW</font>',
                            dataIndex: 'sw',
                            width: 40,
                            align: 'center',
                            sortable: false,
                            renderer: 'renderNumberColumn',
                        }, {
                            text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">Other</font></p>',
                            dataIndex: 'other',
                            width: 40,
                            align: 'center',
                            sortable: false,
                            renderer: 'renderNumberColumn',
                        }],
                    },
                ],
            }],
        };
    },
});