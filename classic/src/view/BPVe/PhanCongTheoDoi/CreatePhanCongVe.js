Ext.define('CMS.view.BPVe.PhanCongTheoDoi.CreatePhanCongVe', {
    extend: 'Ext.form.Panel',
    controller: 'CreatePhanCongVe',
    viewModel: 'CreatePhanCongVe',
    xtype: 'CreatePhanCongVe',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        gridPanel: null,
        recordPanel: null,
        modeedit: null
    },
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'left',
        autoFitErrors: true,
        width: 420,
        labelWidth: 105,
    },
    autoScroll: false,
    initComponent: function() {
        var me = this;
        var num_frame = null;
        var num_leaf = null;
        var type_num = null;
        var other = null;
        var order_sheet = null;
        var recordPanel = me.getRecordPanel();
        var modeedit = me.getModeedit();
        if (recordPanel) {
            num_frame = recordPanel.data.frame;
            num_leaf = recordPanel.data.leaf;
            type_num = recordPanel.data.frame_type_detail[recordPanel.data.frame_type];
            other = recordPanel.data.other;
            order_sheet = recordPanel.data.id;
        }
        me.items = [{
            xtype: 'fieldset',
            title: 'Thông tin phân công',
            defaultType: 'textfield',
            items: [{
                fieldLabel: 'Người làm',
                allowBlank: false,
                afterLabelTextTpl: [
                    '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                ],
                xtype: 'combobox',
                name: 'drawer',
                bind: {
                    store: '{nguoithuchienStore}'
                },
                valueField: 'id',
                displayField: 'name',
                queryMode: 'remote',
                editable: false,
                value: modeedit === true ? recordPanel.data.assignmentOrderSheet[0].drawer.id : null
            }, {
                fieldLabel: 'Ngày nhận BV',
                xtype: 'datefield',
                format: 'd/m/Y',
                submitFormat: 'Y-m-d',
                name: 'working_drawing_receive_date',
                value: modeedit === true ? recordPanel.data.assignmentOrderSheet[0].working_drawing_receive_date : null
            }, {
                fieldLabel: 'Ngày giao hàng',
                xtype: 'datefield',
                format: 'd/m/Y',
                submitFormat: 'Y-m-d',
                name: 'delivery_date',
                value: modeedit === true ? recordPanel.data.assignmentOrderSheet[0].delivery_date : null
            }, {
                fieldLabel: 'Ngày kết thúc',
                xtype: 'datefield',
                name: 'end_date',
                submitFormat: 'Y-m-d',
                format: 'd/m/Y',
                value: modeedit === true ? recordPanel.data.assignmentOrderSheet[0].end_date : null
            }, {
                fieldLabel: 'Pend/Notes',
                name: 'note',
                value: modeedit === true ? recordPanel.data.assignmentOrderSheet[0].note : null
            }, {
                xtype: 'hiddenfield',
                name: 'frame',
                value: num_frame
            }, {
                xtype: 'hiddenfield',
                name: 'leaf',
                value: num_leaf
            }, {
                xtype: 'hiddenfield',
                name: 'type_num',
                value: type_num
            }, {
                xtype: 'hiddenfield',
                name: 'other',
                value: other
            }, {
                xtype: 'hiddenfield',
                name: 'order_sheet',
                value: order_sheet
            }, {
                xtype: 'hiddenfield',
                name: 'status',
                value: 1
            }]
        }];
        me.buttons = ['->', {
            text: 'Phân công',
            glyph: 'xf00c@FontAwesome',
            ui: 'soft-orange',
            handler: 'onClickPhanCongButton',
            hidden: ClassMain.checkPermission('quan_ly_ban_ve', 'AssignmentOrderSheet', 'create') === true ? false : true,
            formBind: true
        }, {
            text: 'Hủy bỏ',
            glyph: 'xf00d@FontAwesome',
            handler: function() {
                this.up('window').close();
            }
        }, '->'];
        me.callParent();
    },


});