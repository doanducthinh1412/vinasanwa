/* eslint-disable max-len */
/* eslint-disable eol-last */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
Ext.define('CMS.view.BPVe.PhanCongTheoDoi.PhanCongTheoDoi', {
    extend: 'Ext.panel.Panel',
    controller: 'PhanCongTheoDoi',
    viewModel: 'PhanCongTheoDoi',
    xtype: 'PhanCongTheoDoi',
    layout: 'fit',
    autoScroll: false,
    config: {
        modeViewFull: 35,
        maxDateKeHoach: new Date(),
    },
    reference: 'PhanCongBanVe',
    initComponent() {
        const me = this;
        me.DanhSachDonDatHangChuaVeStore = Ext.create('CMS.store.DanhSachDonDatHangChuaVe', {
            storeId: 'DanhSachDonDatHangChuaVeStore',
        });
        if (window.innerWidth >= 1920) {
            const wdith_cell = ((window.innerWidth - 653) / 25) - 1;
            me.setModeViewFull(wdith_cell);
        }
        me.items = {
            xtype: 'tabpanel',
            layout: 'fit',
            tabPosition: 'left',
            tabRotation: 0,
            cls: 'tabxemchitiet',
            plain: true,
            style: {
                'background-image': 'url(resources/images/square.gif)',
            },
            listeners: {},
            items: [{
                title: '<b>PHÂN<br>CÔNG VẼ</b>',
                iconCls: 'fas fa-calendar-alt',
                autoScroll: false,
                layout: 'fit',
                iconAlign: 'top',
                itemId: 'PhanCongVeTab',
                items: me.createTabPhanCongVe(),
            }, {
                title: '<b>CHƯA<br>PHÂN CÔNG</b>',
                iconCls: 'fas fa-clipboard-list',
                iconAlign: 'top',
                autoScroll: false,
                layout: 'fit',
                itemId: 'SoLuongChuaPhanCongTab',
                items: {
                    xtype: 'DanhSachChuaPhanCongVe',
                },
            }, {
                title: '<b>WORKING<br>DRAWING</b>',
                iconCls: 'fas fa-clipboard-list',
                iconAlign: 'top',
                autoScroll: false,
                layout: 'fit',
                itemId: 'WorkingDrawingTab',
                items: {
                    xtype: 'WorkingDrawing',
                },
            }],
        };
        me.callParent();
    },
    createTabPhanCongVe() {
        const me = this;
        const modeViewFull = me.getModeViewFull();
        return {
            xtype: 'panel',
            layout: 'border',
            bodyBorder: true,
            tbar: [{
                text: 'Bảng phân công vẽ',
                iconCls: 'fas fa-calendar-alt',
                ui: 'soft-orange',
                handler: 'onBtnViewDangPhanCongVe',
            }, '-', {
                xtype: 'fieldcontainer',
                fieldLabel: '<b>Xem theo ngày</b>',
                labelWidth: 110,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    name: 'startDate',
                    reference: 'StartDateField',
                    labelWidth: 30,
                    bind: {
                        value: '{startdate}',
                    },
                    format: 'd/m/Y',
                    submitFormat: 'Y-m-d',
                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                    width: 180,
                    margin: '0 5 0 0',
                }, {
                    xtype: 'datefield',
                    name: 'endDate',
                    reference: 'EndDateField',
                    labelWidth: 30,
                    width: 180,
                    minValue: new Date(),
                    bind: {
                        minValue: '{startdate}',
                        value: '{enddate}',
                    },
                    format: 'd/m/Y',
                    submitFormat: 'Y-m-d',
                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                    margin: '0 5 0 0',
                    fieldLabel: 'đến',
                }, {
                    xtype: 'button',
                    text: 'Tra cứu',
                    ui: 'dark-blue',
                    iconCls: 'fas fa-search',
                    handler: 'onBtnSearchRangeDate',
                }],
            }, '->', {
                xtype: 'textfield',
                emptyText: 'Nhập tìm kiếm',
                width: 250,
                reference: 'SearchField',
                triggers: {
                    clear: {
                        weight: 0,
                        cls: `${Ext.baseCSSPrefix}form-clear-trigger`,
                        hidden: true,
                        handler: 'onClearClick',
                        scope: 'this',
                    },
                    search: {
                        weight: 1,
                        cls: `${Ext.baseCSSPrefix}form-search-trigger`,
                        handler: 'onSearchClick',
                        scope: 'this',
                    },
                },
                onClearClick() {
                    this.setValue('');
                    this.getTrigger('clear').hide();
                    this.updateLayout();
                    me.getController().loadDataGrid();
                },
                onSearchClick() {
                    const value = this.getValue();
                    if (value.length > 0) {
                        this.getTrigger('clear').show();
                        me.getController().loadDataGrid();
                    } else {
                        this.getTrigger('clear').hide();
                        me.getController().loadDataGrid();
                    }
                },
                listeners: {
                    specialkey(f, e) {
                        if (e.getKey() === e.ENTER) {
                            this.onSearchClick();
                        }
                    },
                },
            }, {
                xtype: 'button',
                text: 'Lọc theo',
                iconCls: 'fas fa-filter',
                ui: 'dark-blue',
                menuAlign: 'tr-br',
                reference: 'FilterMenu',
                menu: Ext.create('Ext.menu.Menu', {
                    style: {
                        overflow: 'visible',
                    },
                    mouseLeaveDelay: 10,
                    width: 370,
                    margin: '10 10 10 10',
                    referenceHolder: true,
                    buttons: [{
                        text: 'Lọc',
                        ui: 'soft-orange',
                        handler() {
                            me.getController().loadDataGrid();
                        },
                    }, {
                        text: 'Xóa bộ lọc',
                        handler(btn) {
                            const refs = btn.up().up().getReferences();
                            refs.forEach((item, key) => {
                                item.reset();
                                item.getTrigger('clear').hide();
                            });
                            btn.up().up().hide();
                            me.getController().loadDataGrid();
                        },
                    }],
                    items: [{
                        xtype: 'datefield',
                        fieldLabel: '<b>Order Date</b>',
                        labelAlign: 'left',
                        labelWidth: 120,
                        reference: 'orderdatefilter',
                        margin: '10 10 10 10',
                        fieldIndex: 'order_date',
                        format: 'd/m/Y',
                        altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                        enableKeyEvents: true,
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: -1,
                                hidden: true,
                                scope: 'this',
                                handler(field) {
                                    field.reset();
                                    field.getTrigger('clear').hide();
                                    me.getController().loadDataGrid();
                                },
                            },
                        },
                        listeners: {
                            change(field, newval) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                            },
                            select(field, record, eOpts) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                                me.getController().loadDataGrid();
                            },
                            keypress(field, event) {
                                if (event.getKey() === event.ENTER) {
                                    me.getController().loadDataGrid();
                                }
                            },
                        },
                    }, {
                        xtype: 'datefield',
                        fieldLabel: '<b>Ngày giao  hàng</b>',
                        labelAlign: 'left',
                        labelWidth: 120,
                        reference: 'datedeliveryfilter',
                        margin: '10 10 10 10',
                        fieldIndex: 'delivery_date',
                        format: 'd/m/Y',
                        altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                        enableKeyEvents: true,
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: -1,
                                hidden: true,
                                scope: 'this',
                                handler(field) {
                                    field.reset();
                                    field.getTrigger('clear').hide();
                                    me.getController().loadDataGrid();
                                },
                            },
                        },
                        listeners: {
                            change(field, newval) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                            },
                            select(field, record, eOpts) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                                me.getController().loadDataGrid();
                            },
                            keypress(field, event) {
                                if (event.getKey() === event.ENTER) {
                                    me.getController().loadDataGrid();
                                }
                            },
                        },
                    }, {
                        xtype: 'combo',
                        fieldLabel: '<b>Người vẽ</b>',
                        typeAhead: true,
                        labelAlign: 'left',
                        labelWidth: 120,
                        triggerAction: 'all',
                        queryMode: 'local',
                        displayField: 'last_name',
                        reference: 'drawerfilter',
                        fieldIndex: 'drawer_id',
                        valueField: 'id',
                        margin: '10 10 10 10',
                        anyMatch: true,
                        store: {
                            type: 'DanhSachThanhVienPhongBanStore',
                        },
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: -1,
                                hidden: true,
                                scope: 'this',
                                handler(field) {
                                    field.reset();
                                    field.getTrigger('clear').hide();
                                    me.getController().loadDataGrid();
                                },
                            },
                        },
                        listeners: {
                            select(combo, record, eOpts) {
                                combo.getTrigger('clear').show();
                                combo.updateLayout();
                                me.getController().loadDataGrid();
                            },
                        },
                    }, {
                        xtype: 'datefield',
                        fieldLabel: '<b>Thời hạn vẽ</b>',
                        labelAlign: 'left',
                        labelWidth: 120,
                        reference: 'deadlinefilter',
                        margin: '10 10 10 10',
                        fieldIndex: 'deadline',
                        format: 'd/m/Y',
                        altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                        enableKeyEvents: true,
                        triggers: {
                            clear: {
                                cls: 'x-form-clear-trigger',
                                weight: -1,
                                hidden: true,
                                scope: 'this',
                                handler(field) {
                                    field.reset();
                                    field.getTrigger('clear').hide();
                                    me.getController().loadDataGrid();
                                },
                            },
                        },
                        listeners: {
                            change(field, newval) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                            },
                            select(field, record, eOpts) {
                                field.getTrigger('clear').show();
                                field.updateLayout();
                                me.getController().loadDataGrid();
                            },
                            keypress(field, event) {
                                if (event.getKey() === event.ENTER) {
                                    me.getController().loadDataGrid();
                                }
                            },
                        },
                    }],
                }),
            }, {
                text: 'Công cụ',
                iconCls: 'fas fa-briefcase',
                ui: 'dark-blue',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    mouseLeaveDelay: 10,
                    items: [{
                        text: 'In ấn',
                        iconCls: 'fas fa-print text-dark-blue',
                        handler: 'onPrintButtonClick',
                    }, {
                        text: 'Xuất Excel',
                        iconCls: 'fas fa-file-export text-dark-blue',
                        handler: 'onExportdButtonClick',
                    }, '-', {
                        text: 'Làm mới danh sách',
                        iconCls: 'fas fa-sync-alt text-dark-blue',
                        handler: 'onReloadButtonClick',
                    }],
                },
            }],
            bbar: [{
                xtype: 'button',
                text: 'Thêm ngày',
                ui: 'dark-blue',
                iconCls: 'fas fa-plus-circle',
                bind: {
                    hidden: ClassMain.checkPermission('quan_ly_ban_ve', 'AssignmentOrderSheet', 'create') === true ? '{!modePhanCong}' : true,
                },
                handler: 'onClickAddDayButton',
            }, '->', {
                xtype: 'button',
                text: 'Làm mới danh sách',
                iconCls: 'fas fa-sync-alt text-dark-blue',
                handler: 'onReloadButtonClick',
            }],
            items: [{
                xtype: 'grid',
                reference: 'PhanCongTheoDoiGrid',
                autoScroll: true,
                columnLines: true,
                region: 'center',
                cls: 'grid-kehoach',
                emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
                listeners: {
                    render(grid) {
                        grid.getStore().load();
                    },
                    viewready: 'onViewGridReady',
                    destroy() {
                        this.tips = Ext.destroy(this.tip);
                    },
                    cellcontextmenu: 'onCellContextMenuGrid',
                    cellkeydown: 'onItemKeydown',
                    celldblclick: 'onCellDBClick',
                },
                bufferedRenderer: false,
                plugins: {
                    ptype: 'cellediting',
                    clicksToEdit: 2,
                    listeners: {
                        edit: 'onEditCell',
                        beforeedit: 'onBeforeEditCell',
                    },
                },
                selModel: {
                    type: 'spreadsheet',
                    columnSelect: true,
                    checkboxSelect: false,
                    rowSelect: false,
                    checkboxHeaderWidth: 40,
                    pruneRemoved: false,
                    extensible: 'y',
                },
                syncRowHeight: false,
                viewConfig: {
                    columnLines: true,
                    trackOver: false,
                    markDirty: false,
                    stripeRows: true,
                },
                features: [{
                    id: 'group',
                    ftype: 'groupingsummary',
                    groupHeaderTpl: '<b>{name}</b>',
                    hideGroupedHeader: false,
                    enableGroupingMenu: false,
                    showSummaryRow: false,
                    collapsible: false,
                }],
                store: {
                    type: 'PhanCongTheoDoiVeStore',
                    autoLoad: false,
                    grouper: {
                        groupFn(item) {
                            const datenow = new Date();
                            const time = Ext.Date.parse(item.get('assignment_date'), 'Y-m-d');
                            let btn_guikehoach = '';
                            if (time) {
                                if (ClassMain.checkPermission('quan_ly_ban_ve', 'AssignmentOrderSheet', 'create') === true) {
                                    btn_guikehoach = `<a class="button_sanxuat" href="#" onclick="GuiPhanCongVe('${Ext.Date.format(time, 'Y-m-d')}');return false;"><i class="fas fa-play"></i> Phân công vẽ</a>`;
                                }
                                if (Ext.Date.diff(time, datenow, Ext.Date.DAY) === 0) {
                                    return `<i class="far fa-calendar-alt"></i> <font color="blue" style="font-size:11px;"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')} (Today)</font> ${btn_guikehoach}`;
                                }
                                if (Ext.Date.diff(time, datenow, Ext.Date.DAY) <= -1) {
                                    return `<i class="far fa-calendar-alt"></i> <font color="blue" style="font-size:11px;"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')}</font> ${btn_guikehoach}`;
                                }
                                return `<i class="far fa-calendar-alt"></i> <font color="blue" style="font-size:11px;"> ${time.getDOY()}. Ngày ${Ext.Date.format(time, 'd/m/Y')}</font>`;
                            }
                            return '';
                        },
                    },
                    listeners: {
                        load: 'onLoadGridKeHoachSanXuat',
                        update: 'onUpdateGridKeHoach',
                        scope: me.getController(),
                    },
                },
                actions: {
                    save: {
                        getClass: 'getSaveClass',
                        getTip: 'getSaveTip',
                        handler: 'onSaveClick',
                        scope: me.getController(),
                    },
                },
                columns: [{
                        xtype: 'rownumberer',
                        text: '<font style="font-size:11px">TT</font>',
                        width: 45,
                        locked: true,
                        sortable: false,
                        align: 'center',
                    }, {
                        menuDisabled: true,
                        sortable: false,
                        xtype: 'actioncolumn',
                        width: 50,
                        locked: true,
                        align: 'center',
                        items: ['@save'],
                    }, {
                        text: '<font style="font-size:11px">Tên dự án</font>',
                        dataIndex: 'project_name',
                        sortable: false,
                        locked: true,
                        width: 115,
                    }, {
                        text: '<font style="font-size:11px">Order No</font>',
                        dataIndex: 'sub_no',
                        sortable: false,
                        locked: true,
                        width: 80,
                        editor: {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            queryMode: 'local',
                            displayField: 'sub_no',
                            valueField: 'sub_no',
                            // selectOnFocus: true,
                            anyMatch: true,
                            // forceSelection: true,
                            store: me.DanhSachDonDatHangChuaVeStore,
                            listeners: {
                                select(combo, record, eOpts) {
                                    combo.ownerCt.completeEdit();
                                },
                                afterrender(field) {
                                    field.bodyEl.setWidth(120);
                                },
                            },
                        },
                    }, {
                        text: '<font style="font-size:11px">Order Date</font>',
                        dataIndex: 'order_date',
                        renderer: 'renderDateColumn',
                        sortable: false,
                        width: 60,
                    }, {
                        text: '<font style="font-size:10px">Delivery</font>',
                        dataIndex: 'delivery_date',
                        renderer: 'renderDeliveryColumn',
                        sortable: false,
                        width: 65,
                    }, {
                        text: '<font style="font-size:11px">Người vẽ</font>',
                        dataIndex: 'drawer_id',
                        width: 70,
                        renderer: 'renderDrawerColumn',
                        sortable: false,
                        editor: {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            queryMode: 'local',
                            displayField: 'last_name',
                            valueField: 'id',
                            // selectOnFocus: true,
                            anyMatch: true,
                            // forceSelection: true,
                            store: {
                                type: 'DanhSachThanhVienPhongBanStore',
                            },
                            listeners: {
                                select(combo, record, eOpts) {
                                    combo.ownerCt.completeEdit();
                                },
                                afterrender(field) {
                                    field.bodyEl.setWidth(120);
                                },
                            },
                        },
                    }, {
                        text: '<font style="font-size:11px">Thời hạn</font>',
                        dataIndex: 'deadline',
                        sortable: false,
                        width: 60,
                        editor: {
                            xtype: 'datefield',
                            minValue: new Date(),
                            emptyText: 'dd/mm/YY',
                            format: 'd/m/Y',
                            altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                            listeners: {
                                select(field, record, eOpts) {
                                    field.ownerCt.completeEdit();
                                },
                                afterrender(field) {
                                    field.bodyEl.setWidth(120);
                                },
                            },
                        },
                        renderer: 'renderDateColumn',
                    },
                    /* {
                        text: '<p style="width:25px;word-break: break-all;"><font style="font-size:11px">Unfinish</font></p>',
                        dataIndex: 'working_drawing_finish',
                        sortable: false,
                        width: 50,
                        renderer: 'renderUnFinishColumn'
                    },
                    {
                        text: '<p style="width:25px;word-break: break-all;"><font style="font-size:11px">Finish</font></p>',
                        dataIndex: 'working_drawing_finish',
                        sortable: false,
                        width: 50,
                        renderer: 'renderFinishColumn'
                    }, */
                    {
                        text: 'Quanity',
                        columns: [{
                            text: '<font style="font-size:10px"><br>FRAME<br></font>',
                            columns: [{
                                text: '<font style="font-size:10px">SD</font>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'frame_sd',
                                sortable: false,
                                renderer: 'renderNumberColumn_FRAME_SD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px;">LSD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'frame_lsd',
                                sortable: false,
                                renderer: 'renderNumberColumn_FRAME_LSD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FSD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'frame_fsd',
                                sortable: false,
                                renderer: 'renderNumberColumn_FRAME_FSD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FLSD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'frame_flsd',
                                sortable: false,
                                renderer: 'renderNumberColumn_Frame_FLSD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">WD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'frame_wd',
                                sortable: false,
                                renderer: 'renderNumberColumn_Frame_WD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">SUSD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'frame_susd',
                                sortable: false,
                                renderer: 'renderNumberColumn_Frame_SUSD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }],
                        }, {
                            text: '<font style="font-size:10px"><br>LEAF<br></font>',
                            columns: [{
                                text: '<font style="font-size:10px">SD</font>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'leaf_sd',
                                sortable: false,
                                renderer: 'renderNumberColumn_LEAF_SD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">LSD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'leaf_lsd',
                                sortable: false,
                                renderer: 'renderNumberColumn_LEAF_LSD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FSD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'leaf_fsd',
                                sortable: false,
                                renderer: 'renderNumberColumn_LEAF_FSD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FLSD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'leaf_flsd',
                                sortable: false,
                                renderer: 'renderNumberColumn_LEAF_FLSD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">WD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'leaf_wd',
                                sortable: false,
                                renderer: 'renderNumberColumn_LEAF_WD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">SUSD</font></p>',
                                width: modeViewFull,
                                align: 'center',
                                dataIndex: 'leaf_susd',
                                sortable: false,
                                renderer: 'renderNumberColumn_LEAF_SUSD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }],
                        }, {
                            text: '<font style="font-size:10px">SMOOD<br> DOOR</font>',
                            columns: [{
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">Frame</font></p>',
                                dataIndex: 'frame_sld',
                                sortable: false,
                                width: modeViewFull,
                                align: 'center',
                                renderer: 'renderNumberColumn_FRAME_SLD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Leaf</font></p>',
                                dataIndex: 'leaf_sld',
                                width: modeViewFull,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn_LEAF_SLD',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }],
                        }, {
                            text: '<font style="font-size:10px">SP<br> HANGER</font>',
                            columns: [{
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">Frame</font></p>',
                                dataIndex: 'frame_sp',
                                width: modeViewFull,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn_FRAME_SP',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Leaf</font></p>',
                                dataIndex: 'leaf_sp',
                                width: modeViewFull,
                                sortable: false,
                                align: 'center',
                                renderer: 'renderNumberColumn_LEAF_SP',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }],
                        }, {
                            text: '<font style="font-size:10px"><br>SHUTTER<br></font>',
                            columns: [{
                                text: '<font style="font-size:10px">JP</font>',
                                dataIndex: 'ssjp',
                                width: modeViewFull,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn_SSJP',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Aichi</font></p>',
                                dataIndex: 'ssaichi',
                                width: modeViewFull,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn_SSAICHI',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Grill</font></p>',
                                dataIndex: 'ssgrill',
                                width: modeViewFull,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn_SSGRILL',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }],
                        }, {
                            text: '<font style="font-size:10px"><br>QSAVE<br></font>',
                            columns: [{
                                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">GR-S</font></p>',
                                dataIndex: 'grs',
                                width: modeViewFull,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn_GRS',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<font style="font-size:10px">S-13</font>',
                                dataIndex: 's13',
                                width: modeViewFull,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn_S13',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }, {
                                text: '<font style="font-size:10px">S-14</font>',
                                dataIndex: 's14',
                                width: modeViewFull,
                                align: 'center',
                                sortable: false,
                                renderer: 'renderNumberColumn_S14',
                                editor: {
                                    xtype: 'numberfield',
                                    // selectOnFocus: true,
                                    hideTrigger: true,
                                    minValue: 0,
                                    allowBlank: true,
                                },
                            }],
                        }, {
                            text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">LOUVER</font></p>',
                            dataIndex: 'lv',
                            width: modeViewFull,
                            align: 'center',
                            sortable: false,
                            renderer: 'renderNumberColumn_LV',
                            editor: {
                                xtype: 'numberfield',
                                // selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true,
                            },
                        }, {
                            text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">SW</font></p>',
                            dataIndex: 'frame_sw',
                            width: modeViewFull,
                            align: 'center',
                            sortable: false,
                            renderer: 'renderNumberColumn_FRAME_SW',
                            editor: {
                                xtype: 'numberfield',
                                // selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true,
                            },
                        }, {
                            text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">Other</font></p>',
                            dataIndex: 'other',
                            width: modeViewFull,
                            align: 'center',
                            sortable: false,
                            renderer: 'renderNumberColumn_OTHER',
                            editor: {
                                xtype: 'numberfield',
                                // selectOnFocus: true,
                                hideTrigger: true,
                                minValue: 0,
                                allowBlank: true,
                            },
                        }],
                    },
                ],
            }],
        };
    },
});

function GuiPhanCongVe(time) {
    if (Ext.getCmp('mainViewPort').getController().getReferences().PhanCongBanVe) {
        Ext.getCmp('mainViewPort').getController().getReferences().PhanCongBanVe.getController().onClickPhanCongVeButton(time);
    }
}