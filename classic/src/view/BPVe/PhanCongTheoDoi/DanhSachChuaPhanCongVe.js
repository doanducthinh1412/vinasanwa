/* eslint-disable eol-last */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
Ext.define('CMS.view.BPVe.PhanCongTheoDoi.DanhSachChuaPhanCongVe', {
    extend: 'Ext.grid.Panel',
    controller: 'DanhSachChuaPhanCongVe',
    viewModel: 'DanhSachChuaPhanCongVe',
    xtype: 'DanhSachChuaPhanCongVe',
    layout: 'fit',
    autoScroll: true,
    config: {
        modeViewFull: 40,
    },
    reference: 'DanhSachChuaPhanCongVe',
    columnLines: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    bufferedRenderer: false,
    viewConfig: {
        columnLines: true,
        trackOver: false,
        markDirty: false,
        stripeRows: true,
    },
    features: [{
        ftype: 'grouping',
        startCollapsed: false,
        groupHeaderTpl: '<font color="blue"><b>Dự án:</b> {name} ({rows.length} đơn đặt hàng)</font>',
    }],
    listeners: {
        // 'itemdblclick': 'onViewButtonClick',
        itemcontextmenu: 'onItemContextMenu',
    },
    initComponent() {
        const me = this;
        if (window.innerWidth >= 1920) {
            const wdith_cell = ((window.innerWidth - 553) / 25) - 1;
            me.setModeViewFull(wdith_cell);
        }
        const modeViewFull = me.getModeViewFull();
        if (Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore')) {
            me.store = Ext.data.StoreManager.lookup('DanhSachDonDatHangChuaVeStore');
        } else {
            me.store = Ext.create('CMS.store.DanhSachDonDatHangChuaVe');
        }
        me.bbar = {
            xtype: 'pagingtoolbar',
            displayInfo: true,
        };
        me.tbar = ['->', {
            xtype: 'textfield',
            emptyText: 'Nhập tìm kiếm',
            width: 250,
            reference: 'SearchField',
            triggers: {
                clear: {
                    weight: 0,
                    cls: `${Ext.baseCSSPrefix}form-clear-trigger`,
                    hidden: true,
                    handler: 'onClearClick',
                    scope: 'this',
                },
                search: {
                    weight: 1,
                    cls: `${Ext.baseCSSPrefix}form-search-trigger`,
                    handler: 'onSearchClick',
                    scope: 'this',
                },
            },
            onClearClick() {
                this.setValue('');
                this.getTrigger('clear').hide();
                this.updateLayout();
                me.getController().loadDataGrid();
            },
            onSearchClick() {
                const value = this.getValue();
                if (value.length > 0) {
                    this.getTrigger('clear').show();
                    this.updateLayout();
                } else {
                    this.getTrigger('clear').hide();
                }
                me.getController().loadDataGrid();
            },
            listeners: {
                specialkey(f, e) {
                    if (e.getKey() === e.ENTER) {
                        this.onSearchClick();
                    }
                },
            },
        }, '-', {
            xtype: 'button',
            text: 'Lọc theo',
            iconCls: 'fas fa-filter',
            ui: 'dark-blue',
            menuAlign: 'tr-br',
            reference: 'FilterMenu',
            menu: Ext.create('Ext.menu.Menu', {
                style: {
                    overflow: 'visible',
                },
                mouseLeaveDelay: 10,
                width: 370,
                margin: '10 10 10 10',
                referenceHolder: true,
                buttons: [{
                    text: 'Lọc',
                    ui: 'soft-orange',
                    handler() {
                        me.getController().loadDataGrid();
                    },
                }, {
                    text: 'Xóa bộ lọc',
                    handler(btn) {
                        const refs = btn.up().up().getReferences();
                        refs.forEach((item, key) => {
                            item.reset();
                            item.getTrigger('clear').hide();
                        });
                        btn.up().up().hide();
                        me.getController().loadDataGrid();
                    },
                }],
                items: [{
                    xtype: 'datefield',
                    fieldLabel: '<b>Order Date</b>',
                    labelAlign: 'left',
                    labelWidth: 120,
                    reference: 'orderdatefilter',
                    margin: '10 10 10 10',
                    fieldIndex: 'order_date',
                    format: 'd/m/Y',
                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                    enableKeyEvents: true,
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            weight: -1,
                            hidden: true,
                            scope: 'this',
                            handler(field) {
                                field.reset();
                                field.getTrigger('clear').hide();
                                me.getController().loadDataGrid();
                            },
                        },
                    },
                    listeners: {
                        change(field, newval) {
                            field.getTrigger('clear').show();
                            field.updateLayout();
                        },
                        select(field, record, eOpts) {
                            field.getTrigger('clear').show();
                            field.updateLayout();
                            me.getController().loadDataGrid();
                        },
                        keypress(field, event) {
                            if (event.getKey() === event.ENTER) {
                                me.getController().loadDataGrid();
                            }
                        },
                    },
                }, {
                    xtype: 'datefield',
                    fieldLabel: '<b>Ngày giao  hàng</b>',
                    labelAlign: 'left',
                    labelWidth: 120,
                    reference: 'datedeliveryfilter',
                    margin: '10 10 10 10',
                    fieldIndex: 'delivery_date',
                    format: 'd/m/Y',
                    altFormats: 'd-m-Y|d.m.Y|dmY|dm',
                    enableKeyEvents: true,
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            weight: -1,
                            hidden: true,
                            scope: 'this',
                            handler(field) {
                                field.reset();
                                field.getTrigger('clear').hide();
                                me.getController().loadDataGrid();
                            },
                        },
                    },
                    listeners: {
                        change(field, newval) {
                            field.getTrigger('clear').show();
                            field.updateLayout();
                        },
                        select(field, record, eOpts) {
                            field.getTrigger('clear').show();
                            field.updateLayout();
                            me.getController().loadDataGrid();
                        },
                        keypress(field, event) {
                            if (event.getKey() === event.ENTER) {
                                me.getController().loadDataGrid();
                            }
                        },
                    },
                }],
            }),
        }, {
            text: 'Công cụ',
            iconCls: 'fas fa-briefcase',
            ui: 'dark-blue',
            menu: {
                xtype: 'menu',
                plain: true,
                mouseLeaveDelay: 10,
                items: [{
                    text: 'In ấn',
                    iconCls: 'fas fa-print text-dark-blue',
                    handler: 'onPrintButtonClick',
                }, {
                    text: 'Xuất Excel',
                    iconCls: 'fas fa-file-export text-dark-blue',
                    handler: 'onExportGriddButtonClick',
                }, '-', {
                    text: 'Làm mới danh sách',
                    iconCls: 'fas fa-sync-alt text-dark-blue',
                    handler: 'onReloadButtonClick',
                }],
            },
        }];
        me.columns = [{
            xtype: 'rownumberer',
            text: 'TT',
            width: 60,
            sortable: false,
            align: 'center',
        }, {
            text: 'Tên dự án',
            dataIndex: 'project_name',
            sortable: false,
            width: 150,
        }, {
            text: 'Order No',
            dataIndex: 'sub_no',
            sortable: false,
            width: 95,
        }, {
            text: 'Order Date',
            dataIndex: 'order_date',
            renderer: 'renderDateColumn',
            sortable: false,
            width: 65,
        }, {
            text: 'Delivery',
            dataIndex: 'delivery_date',
            renderer: 'renderDeliveryColumn',
            sortable: false,
            width: 75,
        }, {
            text: 'Quanity',
            columns: [{
                text: '<font style="font-size:10px"><br>FRAME<br></font>',
                columns: [{
                    text: '<font style="font-size:10px">SD</font>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'frame_sd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px;">LSD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'frame_lsd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FSD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'frame_fsd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FLSD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'frame_flsd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">WD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'frame_wd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">SUSD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'frame_susd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }],
            }, {
                text: '<font style="font-size:10px"><br>LEAF<br></font>',
                columns: [{
                    text: '<font style="font-size:10px">SD</font>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'leaf_sd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">LSD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'leaf_lsd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FSD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'leaf_fsd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">FLSD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'leaf_flsd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">WD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'leaf_wd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">SUSD</font></p>',
                    width: modeViewFull,
                    align: 'center',
                    dataIndex: 'leaf_susd',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }],
            }, {
                text: '<font style="font-size:10px">SMOOD<br> DOOR</font>',
                columns: [{
                    text: '<p style="width:25px;word-break: break-all;"><font style="font-size:10px">Frame</font></p>',
                    dataIndex: 'frame_sld',
                    sortable: false,
                    width: modeViewFull,
                    align: 'center',
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:25px;word-break: break-all;"><font style="font-size:10px">Leaf</font></p>',
                    dataIndex: 'leaf_sld',
                    width: modeViewFull,
                    align: 'center',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }],
            }, {
                text: '<font style="font-size:10px">SP<br> HANGER</font>',
                columns: [{
                    text: '<p style="width:25px;word-break: break-all;"><font style="font-size:10px">Frame</font></p>',
                    dataIndex: 'frame_sp',
                    width: modeViewFull,
                    align: 'center',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:25px;word-break: break-all;"><font style="font-size:10px">Leaf</font></p>',
                    dataIndex: 'leaf_sp',
                    width: modeViewFull,
                    sortable: false,
                    align: 'center',
                    renderer: 'renderNumberColumn',
                }],
            }, {
                text: '<font style="font-size:10px"><br>SHUTTER<br></font>',
                columns: [{
                    text: '<font style="font-size:10px">JP</font>',
                    dataIndex: 'ssjp',
                    width: modeViewFull,
                    align: 'center',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Aichi</font></p>',
                    dataIndex: 'ssaichi',
                    width: modeViewFull,
                    align: 'center',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<p style="width:8px;word-break: break-all;"><font style="font-size:10px">Grill</font></p>',
                    dataIndex: 'ssgrill',
                    width: modeViewFull,
                    align: 'center',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }],
            }, {
                text: '<font style="font-size:10px"><br>QSAVE<br></font>',
                columns: [{
                    text: '<font style="font-size:10px">GR-S</font>',
                    dataIndex: 'grs',
                    width: modeViewFull,
                    align: 'center',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<font style="font-size:10px">S-13</font>',
                    dataIndex: 's13',
                    width: modeViewFull,
                    align: 'center',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }, {
                    text: '<font style="font-size:10px">S-14</font>',
                    dataIndex: 's14',
                    width: modeViewFull,
                    align: 'center',
                    sortable: false,
                    renderer: 'renderNumberColumn',
                }],
            }, {
                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">LOUVER</font></p>',
                dataIndex: 'lv',
                width: modeViewFull,
                align: 'center',
                sortable: false,
                renderer: 'renderNumberColumn',
            }, {
                text: '<font style="font-size:10px">SW</font>',
                dataIndex: 'sw',
                width: modeViewFull,
                align: 'center',
                sortable: false,
                renderer: 'renderNumberColumn',
            }, {
                text: '<p style="width:10px;word-break: break-all;"><font style="font-size:10px">Other</font></p>',
                dataIndex: 'other',
                width: modeViewFull,
                align: 'center',
                sortable: false,
                renderer: 'renderNumberColumn',
            }],
        }];
        me.callParent();
    },
});