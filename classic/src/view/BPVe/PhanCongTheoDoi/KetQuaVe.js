Ext.define('CMS.view.BPVe.PhanCongTheoDoi.KetQuaVe', {
    extend: 'Ext.panel.Panel',
    controller: 'KetQuaVe',
    viewModel: 'KetQuaVe',
    xtype: 'KetQuaVe',
    layout: 'border',
    config: {
        recordPanel: null,
        viewMode: false
    },
    initComponent: function() {
        var me = this;
        var recordPanel = me.getRecordPanel();
        var viewMode = me.getViewMode();
        me.items = [{
            region: 'north',
            bodyPadding: 5,
            layout: 'anchor',
            hidden: (recordPanel.get('status_phancong') === "3" || viewMode === true || ClassMain.checkPermission('quan_ly_ban_ve', 'Drawing', 'create') === false) ? true : false,
            xtype: 'form',
            defaultType: 'textfield',
            reference: 'FormCreateBanVe',
            fieldDefaults: {
                labelWidth: 150,
                width: '100%'
            },
            items: [{
                xtype: 'fieldset',
                title: 'Nhập kết quả vẽ',
                defaultType: 'textfield',
                items: [{
                        xtype: 'combobox',
                        fieldLabel: '<b>Số lượng Working Drawing</b>',
                        labelAlign: 'left',
                        allowBlank: false,
                        reference: 'SoLuongWorkingCombobox',
                        store: Ext.create('Ext.data.Store', {
                            fields: ['value', 'name'],
                            data: []
                        }),
                        queryMode: 'local',
                        editable: false,
                        labelWidth: 180,
                        displayField: 'name',
                        valueField: 'value',
                        name: 'type',
                        allowBlank: false,
                        emptyText: 'Chọn loại Working Drawing',
                    },
                    {
                        fieldLabel: '<b>NOTE</b>',
                        allowBlank: false,
                        labelWidth: 180,
                        name: 'note',
                        emptyText: 'Nhập số NOTE Working Drawing',
                    },
                    {
                        fieldLabel: '<b>File Excel</b>',
                        allowBlank: false,
                        labelWidth: 180,
                        xtype: 'filefield',
                        emptyText: 'Chọn File Excel Working Drawing',
                        name: 'drawing_import',
                        accept: 'application/vnd.ms-excel',
                        buttonConfig: {
                            iconCls: 'fas fa-file-excel'
                        }
                    },
                    {
                        xtype: 'hiddenfield',
                        name: 'assignmentOrderSheet',
                        value: recordPanel.data.assignmentOrderSheet[0] ? recordPanel.data.assignmentOrderSheet[0].id : ''
                    },
                    {
                        xtype: 'button',
                        iconCls: 'fas fa-drafting-compass',
                        text: 'Nhập kết quả',
                        formBind: true,
                        handler: 'onClickCreateBanVeButton'
                    }
                ]
            }]
        }, {
            region: 'center',
            xtype: 'grid',
            columnLines: true,
            height: '100%',
            layout: 'fit',
            reference: 'GridListKetQuaBanVe',
            border: true,
            ui: 'light',
            title: '<i class="far fa-list-alt"></i> <b>Danh sách kết quả vẽ</b>',
            emptyText: '<div class="no-content-grid"><center>Không có dữ liệu</center></div>',
            store: {
                type: 'DanhSachKetQuaBanVeStore',
                proxy: {
                    url: '/api/ordersheet/get/' + recordPanel.data.id
                },
                autoLoad: true,
                listeners: {
                    'load': function(store, record) {
                        var arr = [];
                        var new_listWD = [];
                        for (i = 0; i < record.length; i++) {}
                        if (recordPanel.data.frame_type_detail) {
                            for (wd in recordPanel.data.frame_type_detail) {
                                if (store.find("type", wd) === -1) {
                                    new_listWD.push({
                                        name: wd + ' : ' + recordPanel.data.frame_type_detail[wd],
                                        value: wd
                                    });
                                }
                            }
                        }
                        me.getController().getReferences().SoLuongWorkingCombobox.getStore().loadData(new_listWD);
                    }
                },
            },
            listeners: {
                'itemdblclick': 'onClickButtonViewFileDBClick'
            },
            columns: [{
                xtype: 'rownumberer',
                text: '<b>STT</b>',
                width: 65,
                align: 'center'
            }, {
                text: 'Note',
                flex: 1,
                align: 'center',
                dataIndex: 'note'
            }, {
                text: 'LOT',
                flex: 1,
                align: 'left',
                dataIndex: 'lot'
            }, {
                text: 'Quatity',
                flex: 1,
                columns: [{
                    text: 'Type',
                    flex: 1,
                    dataIndex: 'type',
                    align: 'center'
                }, {
                    text: 'Quatity',
                    flex: 1,
                    dataIndex: 'number',
                    align: 'center'
                }]
            }, {
                text: '',
                xtype: 'actioncolumn',
                width: 105,
                sortable: false,
                menuDisabled: true,
                align: 'center',
                items: [{
                    iconCls: 'fas fa-times text-blue',
                    tooltip: 'Xóa',
                    hidden: (recordPanel.get('status_phancong') === "3" || viewMode === true || ClassMain.checkPermission('quan_ly_ban_ve', 'Drawing', 'delete') === false) ? true : false,
                    handler: 'onClickButtonRemoveFile'
                }, {
                    width: 5,
                    hidden: (recordPanel.get('status_phancong') === "3" || viewMode === true || ClassMain.checkPermission('quan_ly_ban_ve', 'Drawing', 'delete') === false) ? true : false
                }, {
                    iconCls: 'far fa-eye text-blue',
                    tooltip: 'Xem bản vẽ',
                    handler: 'onClickButtonViewFile'
                }, {
                    width: 5
                }, {
                    iconCls: 'fas fa-download text-blue',
                    tooltip: 'Tải bản vẽ',
                    handler: 'onClickButtonExportFile'
                }]
            }]
        }];
        me.callParent();
    }
});