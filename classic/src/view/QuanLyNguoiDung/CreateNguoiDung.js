Ext.define('CMS.view.CreateNguoiDung.CreateNguoiDung', {
    extend: 'Ext.form.Panel',
    requires: [
        
    ],
    controller: 'CreateNguoiDung',
    viewModel: 'CreateNguoiDung',
    xtype: 'CreateNguoiDung',
    defaultType: 'textfield',
    bodyPadding: 10,
    layout: 'anchor',
    config: {
        gridPanel: null
    },
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'left',
        autoFitErrors: true,
        width: 420,
		labelWidth:105,
    },
    autoScroll: false,
    initComponent: function() {
        var me = this;
        me.items = [{
			xtype: 'fieldset',
            title: 'Thông tin nhân viên',
			defaultType: 'textfield',
			items: [{
				xtype: 'container',
				margin: '0 0 10 0',
				layout: {
					type: 'hbox'
				},
				padding: 0,
				defaults: {
					xtype: 'textfield',
					width: 210,
					labelWidth:105,
					emptyCls:'emptyText',
					allowBlank: false,
					
				},
				items: [{
					fieldLabel: 'Họ tên',
					name: 'last_name',
					emptyText: 'Họ',
					margin: '0 10 0 0',
					afterLabelTextTpl: [
                        '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                    ],
				},{
					name: 'first_name',
					emptyText: 'Tên',
					width: 200,
					afterLabelTextTpl: [
                        '<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
                    ],
				},
				]
			},{
				fieldLabel: 'Email',
				name: 'email',
				afterLabelTextTpl: [
					'<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
				],
			},{
				fieldLabel: 'Địa chỉ',
				name: 'address'
			},{
				fieldLabel: 'Ngày sinh',
				xtype: 'datefield',
				name: 'date_of_birth',
				format: 'Y-m-d',
			}, {
				xtype      : 'fieldcontainer',
				fieldLabel : 'Giới tính',
				defaultType: 'radiofield',
				defaults: {
					width: 150
				},
				layout: 'hbox',
				items: [
				{
					boxLabel  	: 'Nam',
					name      	: 'gender',
					inputValue	: '1',
					checked		: true
				}, {
					boxLabel  : 'Nữ',
					name      : 'gender',
					inputValue: '0',
				}]
			},{
				name: 'position',
				fieldLabel: 'Chức vụ',
				allowBlank: true,
			},{
				xtype: 'combobox',
				name: 'department',
				fieldLabel: 'Phòng',
				allowBlank: true,
				bind: {
					store: '{departmentStore}'
				},
				valueField: 'id',
				displayField: 'name',
				editable: false

			}]
		},{
			xtype: 'fieldset',
            title: 'Thông tin người dùng',
			defaultType: 'textfield',
			defaults: {
				afterLabelTextTpl: [
					'<span style="color:red;font-weight:bold" data-qtip="Required"> (*)</span>'
				],
			},
			items: [{
				fieldLabel: 'Tài khoản',
				name: 'username',
				allowBlank: false,
			},{
				fieldLabel: 'Mật khẩu',
				name: 'password',
				allowBlank: false,
			},{
				xtype: 'tagfield',
				allowBlank: false,
				name: 'role',
				fieldLabel: 'Phân quyền',
				bind: {
					store: '{roleStore}'
				},
				valueField: 'id',
				displayField: 'name',
				filterPickList: true
				//multiSelect: true

			}]
		}];
		me.buttons = ['->',{
			text: 'Tạo mới',
			glyph: 'xf00c@FontAwesome',
			ui: 'soft-orange',
			handler: function() {
				me.getController().createUser();
			},
			formBind: true
		}, {
			text: 'Hủy bỏ',
			glyph: 'xf00d@FontAwesome',
			handler: function() {
				this.up('window').close();
			}
		},'->'];
        me.callParent();
    },

     
});