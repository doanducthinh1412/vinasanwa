Ext.define('CMS.view.QuanLyNguoiDung.QuanLyNguoiDung', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.Panel'
    ],
    controller: 'QuanLyNguoiDung',
    viewModel: {
        type: 'QuanLyNguoiDung'
    },
    xtype: 'QuanLyNguoiDung',
    layout: 'fit',
    bind: {
        store: '{quanLyNguoiDungStore}'
    },
    reference: 'QuanLyNguoiDungGrid',
    autoScroll: true,
	columnLines: true,
    emptyText: '<div class="no-content-grid"><img src="resources/images/empty.jpg"><center>Không có dữ liệu</center></div>',
    initComponent: function() {
        var me = this;
		//var roleStore = Ext.create('CMS.store.RoleStore',{
		//	autoLoad:true
		//});
		//roleStore.load();
		me.tbar = [{
			xtype: 'button',
			text: 'Tạo mới',
			iconCls: 'fas fa-plus-circle',
			ui: 'soft-orange',
			handler: 'onCreateButtonClick'
		}, '-', {
			text: 'Thao tác',
			iconCls: 'fas fa-bars',
			ui: 'dark-blue',
			disabled: true,
			bind: {
				disabled: '{!QuanLyNguoiDungGrid.selection}',
			},
			menu: me.buildMenu()
		}, '->', {
			xtype: 'textfield',
			emptyText: 'Nhập tìm kiếm',
			width: 250,
			triggers: {
				clear: {
					weight: 0,
					cls: Ext.baseCSSPrefix + 'form-clear-trigger',
					hidden: true,
					handler: 'onClearClick',
					scope: 'this'
				},
				search: {
					weight: 1,
					cls: Ext.baseCSSPrefix + 'form-search-trigger',
					handler: 'onSearchClick',
					scope: 'this'
				}
			},
			onClearClick: function() {
				this.setValue('');
				this.getTrigger('clear').hide();
				this.updateLayout();
				me.getStore().clearFilter();
			},
			onSearchClick: function() {
				var value = this.getValue();
				if (value.length > 0) {
					this.getTrigger('clear').show();
					me.getStore().filterBy(function(rec, id) {
						var fullname = rec.get('last_name') + ' ' + rec.get('first_name'); 
						fullname = fullname.toLowerCase();
						if(fullname.indexOf(value) != -1){
							return true;
						}
						else {
							return false;
						}
					});
				} else {
					this.getTrigger('clear').hide();
					me.getStore().clearFilter();

				}
			},
			listeners: {
				'specialkey': function(f, e) {
					if (e.getKey() == e.ENTER) {
						this.onSearchClick();
					}
				}
			}
		}, {
			xtype: 'button',
			text: 'Lọc theo',
			iconCls: 'fas fa-filter',
			ui: 'dark-blue',
			menuAlign: 'tr-br',
			menu: {
				width: 370,
				margin: '10 10 10 10',
				mouseLeaveDelay: 10,
				referenceHolder: true,
				buttons: [{
					text: 'Lọc',
					ui: 'soft-orange',
					handler: function() {
						this.up('menu').hide();
						me.getStore().clearFilter();
						var refs = me.getReferences();
						var statusfilter = refs.statusfilter.getValue();
						if (statusfilter) {
							me.getStore().filter('status',statusfilter);
						}
						var rolefilter = refs.rolefilter.getValue();
						if (rolefilter) {
							me.getStore().filterBy(function(rec, id) {
								if(rec.get('role').id  === rolefilter ){
									return true;
								}
								else {
									return false;
								}
							});
						}
					}
				},{
					text: 'Xóa bộ lọc',
					handler: function(btn) {
						btn.up().up().hide();
						me.getStore().clearFilter();
					}
				}],
				items: [{
					xtype: 'container',
					layout: 'vbox',
					items: [{
						xtype: 'combobox',
						fieldLabel: '<b>Trạng thái</b>',
						labelAlign: 'left',
						reference: 'statusfilter',
						labelWidth: 120,
						margin: '10 10 10 10',
						editable: false,
						store: Ext.create('Ext.data.Store', {
							fields: ['val', 'name'],
							data: [
								{ "val": "1", "name": "Đang hoạt động" },
								{ "val": "0", "name": "Tài khoản đóng" }
							]
						}),
						displayField: 'name',
						valueField: 'val',
						/*triggers: {
							clear: {
								cls: 'x-form-clear-trigger',
								weight: -1,
								hidden: true,
								scope: 'this',
								handler: function(combo) {
									combo.clearValue();
									combo.getTrigger('clear').hide();
									me.filterGrid();
								}
							}
						},
						listeners: {
							'select': function(combo) {
								combo.getTrigger('clear').show();
								combo.updateLayout();
								me.filterGrid();
							}
						}*/
					},{
						xtype: 'combobox',
						fieldLabel: '<b>Loại tài khoản</b>',
						labelAlign: 'left',
						reference: 'rolefilter',
						margin: '10 10 10 10',
						editable: false,
						labelWidth: 120,
						bind: {
							store: '{roleStore}'
						},
						displayField: 'name',
						valueField: 'id',
						/*triggers: {
							clear: {
								cls: 'x-form-clear-trigger',
								weight: -1,
								hidden: true,
								scope: 'this',
								handler: function(combo) {
									combo.clearValue();
									combo.getTrigger('clear').hide();
									me.filterGrid();
								}
							}
						},
						listeners: {
							'select': function(combo) {
								combo.getTrigger('clear').show();
								combo.updateLayout();
								me.filterGrid();
							}
						}*/
					},/*{
						xtype: 'toolbar',
						defaults: {
							width: 100
						},
						items: [{
							text: 'Lọc',
							ui: 'soft-orange',
							handler: function() {
								this.up('menu').hide();
								me.getStore().clearFilter();
								var refs = me.getReferences();
								var statusfilter = refs.statusfilter.getValue();
								if (statusfilter) {
									me.getStore().filter('status',statusfilter);
								}
								var rolefilter = refs.rolefilter.getValue();
								if (rolefilter) {
									me.getStore().filterBy(function(rec, id) {
										if(rec.get('role').id  === rolefilter ){
											return true;
										}
										else {
											return false;
										}
									});
								}
							}
						},{
							text: 'Xóa bộ lọc',
							handler: function() {
								this.up('menu').hide();
								me.getStore().clearFilter();
							}
						}]
					}*/],
				}]
			},
		}, {
			text: 'Công cụ',
			iconCls: 'fas fa-briefcase',
			ui: 'dark-blue',
			menu: {
				xtype: 'menu',
				plain: true,
				mouseLeaveDelay: 10,
				items: [{
					text: 'In ấn',
					iconCls: 'fas fa-print text-dark-blue',
					handler: 'onPrintButtonClick'
				}, {
					text: 'Xuất Excel',
					iconCls: 'fas fa-file-export text-dark-blue',
					handler: 'onExportdButtonClick'
				}, '-', {
					text: 'Làm mới danh sách',
					iconCls: 'fas fa-sync-alt text-dark-blue',
					handler: 'onReloadButtonClick'
				}]
			}
		}];
		me.columns = [{
				xtype: 'rownumberer',
				text: '<b>TT</b>',
				width: 65,
				align: 'center'
			},
			{
				text: '<b>Họ và tên</b>',
				dataIndex: 'last_name',
				flex: 1,
				align: 'center',
				renderer: function(value, meta, rec) {
					return '<b>' + rec.get('last_name') + ' ' + rec.get('first_name') + '</b>';
				}
			},
			{
				text: '<b>Tài khoản</b>',
				dataIndex: 'username',
				flex: 1,
				align: 'center',
				renderer: function(value, meta, rec) {
					return value;
				}
			},
			{
				text: '<b>Trạng thái</b>',
				dataIndex: 'status',
				flex: 1,
				align: 'center',
				renderer: function(value, meta, rec) {
					if (value == 1) {
						return '<span style="color: #2d9a45;">Đang hoạt động</span>';
					} else {
						return '<span style="color: #838684;">Tài khoản đóng</span>';
					}
				}
			},
			{
				text: '<b>Loại tài khoản</b>',
				dataIndex: 'role',
				flex: 1,
				align: 'center',
				renderer: function(value, meta, rec) {
					return value.name
				}
			},{
				text: '<b>Ngày tạo</b>',
				dataIndex: 'createdAt',
				flex: 1,
				align: 'center',
				renderer: function(value, meta, rec) {
					var date = new Date(value.date);
					return date.toLocaleString();;
				}
			}, {
				text: '<b>Thao tác</b>',
				xtype: 'actioncolumn',
				width: 120,
				sortable: false,
				menuDisabled: true,
				align: 'center',
				items: [{
					iconCls: 'fas fa-edit text-blue',
					tooltip: 'Sửa thông tin',
					handler: 'onEditButtonClickGrid'
				}, {
					width: 10
				}, {
					iconCls: 'fas fa-unlock-alt text-blue',
					tooltip: 'Khôi phục mật khẩu',
					handler: 'onResetPasswordButtonClickGrid'
				}, {
					width: 10
				}, {
					iconCls: 'fas fa-trash-alt text-blue',
					tooltip: 'Xóa người dùng',
					handler: 'onDeleteButtonClickGrid'
				}]
			}
		];
		me.menu = me.buildMenu();
		/*me.bbar = {
			xtype: 'pagingtoolbar',
			displayInfo: true
		};*/
		//me.getViewModel().getStore('quanLyNguoiDungStore').filter('isDeleted',false);
		me.callParent();
        
    },
    /*viewConfig: {
    	getRowClass: function(record, rowIndex, rowParams, store){
        	return (record.get('status')  == 0) ? "deactivated" : "activated";
    	}
	},*/
    buildMenu: function() {
        var controller = this.getController();
        return Ext.create('Ext.menu.Menu', {
            items: [{
                text: 'Xem thông tin',
                iconCls: 'fas fa-info-circle text-dark-blue',
                handler: function() {
                    controller.onViewButtonClick();
                }
            }, '-', {
                text: 'Sửa thông tin',
                iconCls: 'fas fa-edit text-dark-blue',
                handler: function() {
                    controller.onEditButtonClick();
                }
            }, {
                text: 'Xóa người dùng',
                iconCls: 'far fa-trash-alt text-dark-blue',
                handler: function() {
                    controller.onDeleteButtonClick();
                }
            }, '-', {
                text: 'Khôi phục mật khẩu',
                iconCls: 'fas fa-unlock-alt text-dark-blue',
                handler: function() {
                    controller.onResetPasswordButtonClick();
                }
            }]
        });
    },
    listeners: {
        'itemcontextmenu': 'onItemContextMenu',
        'itemdblclick': 'onEditButtonClick'
    },
    realoadView: function() {
        var me = this;
        me.el.mask('Đang tải...');
        me.getStore().reload();
        me.el.unmask();
    }
});