Ext.define('CMS.view.EditNguoiDung.EditNguoiDung', {
    extend: 'Ext.form.Panel',
    requires: [
        
    ],
    controller: 'EditNguoiDung',
    viewModel: 'EditNguoiDung',
    xtype: 'EditNguoiDung',
    defaultType: 'textfield',
    bodyPadding: 8,
    layout: 'anchor',
    config: {
        rec: null,
		gridPanel: null
    },
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'left',
        autoFitErrors: true,
        width: 420,
		labelWidth:105,
		//disabled: true
    },
    autoScroll: false,
    initComponent: function() {
        var me = this;
		var record = this.getRec();
		console.log(record.get('date_of_birth'));
        me.items = [{
			xtype: 'fieldset',
            title: 'Thông tin nhân viên',
			defaultType: 'textfield',
			items: [{
				xtype: 'container',
				margin: '0 0 10 0',
				layout: {
					type: 'hbox'
				},
				padding: 0,
				defaults: {
					xtype: 'textfield',
					width: 210,
					labelWidth:105,
					emptyCls:'emptyText',
					allowBlank: false,
					
				},
				items: [{
					fieldLabel: 'Họ tên',
					name: 'last_name',
					margin: '0 10 0 0',
					emptyText: 'Họ',
					value: record.get('last_name')
				},{
					name: 'first_name',
					emptyText: 'Tên',
					width: 200,
					value: record.get('first_name')
				},
				]
			},{
				fieldLabel: 'Email',
				name: 'email',
				value: record.get('email')
			},{
				fieldLabel: 'Địa chỉ',
				name: 'address',
				value: record.get('address')
			},{
				fieldLabel: 'Ngày sinh',
				xtype: 'datefield',
				name: 'date_of_birth',
				format: 'd/m/Y',
                submitFormat: 'Y-m-d',
				//altFormats: 'd,m,Y|d.m.Y|d m Y',
				value: record.get('date_of_birth')
			}, {
				xtype      : 'fieldcontainer',
				fieldLabel : 'Giới tính',
				defaultType: 'radiofield',
				defaults: {
					width: 150
				},
				layout: 'hbox',
				items: [
				{
					boxLabel  : 'Nam',
					name      : 'gender',
					inputValue: '1',
					checked: (record.get('gender') == '1')?true:false
				}, {
					boxLabel  : 'Nữ',
					name      : 'gender',
					inputValue: '0',
					checked: (record.get('gender') == '0')?true:false
				}]
			},{
				xtype      : 'fieldcontainer',
				fieldLabel : 'Trạng thái',
				defaultType: 'radiofield',
				defaults: {
					width: 150
				},
				layout: 'hbox',
				items: [
				{
					boxLabel  : 'Kích hoạt',
					name      : 'status',
					inputValue: '1',
					checked: (record.get('status') == '1')?true:false
				}, {
					boxLabel  : 'Chưa kích hoạt',
					name      : 'status',
					inputValue: '0',
					checked: (record.get('status') == '0')?true:false
				}]
			},{
				xtype: 'textfield',
				name: 'position',
				fieldLabel: 'Chức vụ',
				allowBlank: true,
				value: record.get('position'),
				/*bind: {
					store: '{positionStore}'
				},
				valueField: 'position_id',
				displayField: 'position_description'*/

			},{
				xtype: 'combobox',
				name: 'department',
				fieldLabel: 'Phòng',
				allowBlank: true,
				value: record.get('department'),
				bind: {
					store: '{departmentStore}'
				},
				valueField: 'id',
				displayField: 'name',
				editable: false

			}]
		},{
			xtype: 'fieldset',
            fieldLabel: 'Thông tin người dùng',
			defaultType: 'textfield',
			items: [{
				fieldLabel: 'Tài khoản',
				name: 'username',
				allowBlank: false,
				value: record.get('username')
			},{
				xtype: 'combobox',
				allowBlank: false,
				name: 'role',
				fieldLabel: 'Phân quyền',
				value: record.get('role').id,
				bind: {
					store: '{roleStore}'
				},
				valueField: 'id',
				displayField: 'name',
				multiSelect: false,
				//filterPickList: true

			}]
		}];
		me.buttons = ['->',{
			text: 'Lưu lại',
			glyph: 'xf00c@FontAwesome',
			ui: 'soft-orange',
			handler: 'onSaveButtonClick',
			formBind: true
		},{
			text: 'Hủy bỏ',
			glyph: 'xf00d@FontAwesome',
			handler: function() {
				this.up('window').close();
			}
		},'->'];
        me.callParent();
    },
	
     
});